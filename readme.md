# GedBook 

## Description

Book generator from gedcom data

### Features

* Imports gedcom file into an internal EMF model
* Generates latex files
* Perform various check reports (such as list of events without any sources, lack of consistency in dates, etc...)
* Possibility to extend gedcom data with non-standard information (e.g. to link sources with external documents, define coordinates on external images to easily extract screenshots and insert them in generated document, add source transcriptions, etc...)
* Multi language support: english, french, portuguese and spanish are natively provided (external contributions can provide other languages through an extension point)
