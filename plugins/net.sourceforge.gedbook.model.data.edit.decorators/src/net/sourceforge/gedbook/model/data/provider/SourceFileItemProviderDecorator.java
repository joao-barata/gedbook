/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.data.provider;

import net.sourceforge.gedbook.model.data.SourceFile;
import net.sourceforge.gedbook.model.data.edit.decorators.ItemProviderAdapterDecorator;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * @author Joao Barata
 */
public class SourceFileItemProviderDecorator extends ItemProviderAdapterDecorator implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider,
    IItemPropertySource {

  private static final String PLUGIN_ID = "net.sourceforge.gedbook.model.data.edit.decorators";

  public SourceFileItemProviderDecorator(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * @see org.eclipse.emf.edit.provider.ItemProviderDecorator#getText(java.lang.Object)
   */
  @Override
  public String getText(Object object) {
    String source = ((SourceFile) object).getPath();
    source = source.substring(source.lastIndexOf("/") + 1);
    return source;
  }

  /**
   * @see org.eclipse.emf.edit.provider.ItemProviderDecorator#getImage(java.lang.Object)
   * 
   * FIXME the images created here must be disposed<br>
   *   (use a registry in the activator plugin instead of the systematic createImage call)
   */
  @Override
  public Object getImage(Object object) {
    String path = ((SourceFile) object).getPath();
    String ext = path.substring(path.lastIndexOf('.') + 1);
    if (ext.equalsIgnoreCase("pdf")) {
      ImageDescriptor desc = AbstractUIPlugin.imageDescriptorFromPlugin(PLUGIN_ID, "icons/document-pdf.png");
      return desc.createImage();
    } else if (ext.equalsIgnoreCase("jpg")) {
      ImageDescriptor desc = AbstractUIPlugin.imageDescriptorFromPlugin(PLUGIN_ID, "icons/document-img.gif");
      return desc.createImage();
    }
    return super.getImage(object);
  }
}
