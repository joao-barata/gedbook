/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.data.provider;

import org.eclipse.emf.common.notify.AdapterFactory;

/**
 * @author Joao Barata
 */
public class SingleSourceFileItemProviderDecorator extends SourceFileItemProviderDecorator {

  public SingleSourceFileItemProviderDecorator(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  @Override
  public String getText(Object object) {
    return super.getText(object);
  }

  @Override
  public Object getImage(Object object) {
    return super.getImage(object);
  }
}
