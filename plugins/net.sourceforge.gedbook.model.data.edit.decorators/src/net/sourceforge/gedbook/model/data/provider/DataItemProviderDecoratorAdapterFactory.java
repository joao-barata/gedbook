/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.data.provider;

import net.sourceforge.gedbook.model.data.Exclusion;
import net.sourceforge.gedbook.model.data.ExclusionGroup;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.FileGroup;
import net.sourceforge.gedbook.model.data.MultipleSourceFile;
import net.sourceforge.gedbook.model.data.Photo;
import net.sourceforge.gedbook.model.data.PhotoGroup;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.SingleSourceFile;
import net.sourceforge.gedbook.model.data.edit.decorators.ForwardingItemProviderAdapterDecorator;

import org.eclipse.emf.edit.provider.DecoratorAdapterFactory;
import org.eclipse.emf.edit.provider.IItemProviderDecorator;

/**
 * @author Joao Barata
 */
public class DataItemProviderDecoratorAdapterFactory extends DecoratorAdapterFactory {

	public DataItemProviderDecoratorAdapterFactory() {
		super(new DataItemProviderAdapterFactory());
	}

  /**
   * @see org.eclipse.emf.edit.provider.DecoratorAdapterFactory#createItemProviderDecorator(java.lang.Object, java.lang.Object)
   */
	protected IItemProviderDecorator createItemProviderDecorator(Object target, Object Type) {
    if (target instanceof Exclusion) {
      return new ExclusionItemProviderDecorator(this);
    } else if (target instanceof ExclusionGroup) {
        return new ExclusionGroupItemProviderDecorator(this);
    } else if (target instanceof ExtendedData) {
      return new ExtendedDataItemProviderDecorator(this);
    } else if (target instanceof Extract) {
      return new ExtractItemProviderDecorator(this);
    } else if (target instanceof FileGroup) {
			return new FileGroupItemProviderDecorator(this);
    } else if (target instanceof MultipleSourceFile) {
      return new MultipleSourceFileItemProviderDecorator(this);
    } else if (target instanceof Photo) {
      return new PhotoItemProviderDecorator(this);
    } else if (target instanceof PhotoGroup) {
      return new PhotoGroupItemProviderDecorator(this);
    } else if (target instanceof Screenshot) {
      return new ScreenshotItemProviderDecorator(this);
		} else if (target instanceof SingleSourceFile) {
      return new SingleSourceFileItemProviderDecorator(this);
    }
		return new ForwardingItemProviderAdapterDecorator(this);
	}
}
