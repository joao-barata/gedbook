/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.helpers;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.SourceFile;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.XMLResource.XMLInfo;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLInfoImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;

/**
 * @author Joao Barata
 */
public class ResourceUtils {

  /**
   * Serialize the EObject in XMI and return as String.
   * 
   * @param object
   *          the object to serialize
   * @return the String representation of serialized EObject
   * @throws IOException
   */
  public static String serialize(EObject object) throws IOException {
    // Store resource
    StringWriter out = new StringWriter();
    serialize(object, out);
    return out.toString();
  }

  /**
   * Serialize the EObject in the given Writer.
   * 
   * @param object
   * @param out
   * @throws IOException
   */
  public static void serialize(EObject object, Writer out) throws IOException {
    XMIResource resource = null;
    if (object.eResource() != null) {
      resource = (XMIResource) object.eResource();
    } else {
      resource = new XMIResourceImpl();
      resource.getContents().add(object);
    }
    // Store resource
    saveResource(resource, out);
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(String file) {
    return loadObject(new File(file));
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(ResourceSet resourceSet_p, String file) {
    return loadObject(resourceSet_p, new File(file));
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(File file) {
    URI uri = URI.createFileURI(file.getAbsolutePath());
    return loadObject(uri);
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(ResourceSet resourceSet_p, File file) {
    URI uri = URI.createFileURI(file.getAbsolutePath());
    return loadObject(resourceSet_p, uri);
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(URI uri) {
    ResourceSet resourceSet = new ResourceSetImpl();
    ECrossReferenceAdapter adapter = new ECrossReferenceAdapter();
    resourceSet.eAdapters().add(adapter);
    return loadObject(resourceSet, uri);
  }

  /**
   * Load a serialized EObject
   * 
   * @param file
   * @return
   */
  public static EObject loadObject(ResourceSet resourceSet_p, URI uri) {
    Resource resource = resourceSet_p.getResource(uri, true);
    return resource.getContents().get(0);
  }

  /**
   * Save an EMF resource set
   * 
   * @param resourceSet
   * @throws IOException
   */
  public static void saveResource(ResourceSet resourceSet_p) throws IOException {
    for (Resource resource : resourceSet_p.getResources()) {
      saveResource(resource);
    }
  }

  /**
   * Save an EMF resource
   * 
   * @param resource
   * @throws IOException
   */
  public static void saveResource(Resource resource) throws IOException {
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put(XMLResource.OPTION_SKIP_ESCAPE_URI, Boolean.FALSE);
    map.put(XMLResource.OPTION_ENCODING, EncodingUtils.ENCODING_UTF8);

    EAttribute description = DataPackage.Literals.PHOTO__DESCRIPTION;
    EAttribute transcription = DataPackage.Literals.SCREENSHOT__TRANSCRIPTION;
    EAttribute comment = DataPackage.Literals.SCREENSHOT__COMMENT;
    EAttribute annotation = DataPackage.Literals.SCREENSHOT__ANNOTATION;
    EAttribute signature = DataPackage.Literals.SCREENSHOT__SIGNATURE;
    XMLMapImpl xmlMap = new XMLMapImpl();
    XMLInfoImpl x = new XMLInfoImpl();
    x.setXMLRepresentation(XMLInfo.ELEMENT);
    xmlMap.add(description, x);
    xmlMap.add(transcription, x);
    xmlMap.add(comment, x);
    xmlMap.add(annotation, x);
    xmlMap.add(signature, x);
    map.put(XMLResource.OPTION_XML_MAP, xmlMap);

    resource.save(map);
  }

  /**
   * Save an EMF resource
   * 
   * @param resource
   * @param out
   * @throws IOException
   */
  public static void saveResource(XMIResource resource, Writer out) throws IOException {
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put(XMLResource.OPTION_SKIP_ESCAPE_URI, Boolean.FALSE);
    map.put(XMLResource.OPTION_ENCODING, EncodingUtils.ENCODING_UTF8);
    resource.save(out, map);
  }

  /**
   * Save an EMF resource
   * 
   * @param object
   * @param file
   * @throws IOException
   */
  public static void saveObject(EObject object, String file) throws IOException {
    saveObject(object, new File(file));
  }

  /**
   * Save an EObject in a file
   * 
   * @param object
   * @param file
   * @throws IOException
   */
  public static void saveObject(EObject object, File file) throws IOException {
    ResourceSet rs = new ResourceSetImpl();
    URI uri = URI.createFileURI(file.getAbsolutePath());
    Resource resource = rs.createResource(uri);
    resource.getContents().add(object);
    saveResource(resource);
  }

  /**
   * This method retrieves all Object who have a EReference 'eRef_p' toward the
   * Object 'eObjectRef_p'
   * 
   * @param eObjectRef_p
   *          : EObject
   * @param eRef_p
   *          : EReference relation
   * @return
   */
  public static List<EObject> getReferencers(EObject eObjectRef_p, EReference eRef_p) {
    List<EObject> result = new ArrayList<EObject>();

    ResourceSet resourceSet = eObjectRef_p.eResource().getResourceSet();
    for (Adapter adapter : resourceSet.eAdapters()) {
      if (adapter instanceof ECrossReferenceAdapter) {
        for (Setting setting : ((ECrossReferenceAdapter) adapter).getInverseReferences(eObjectRef_p, true)) {
          if (eRef_p.equals(setting.getEStructuralFeature())) {
            if (!result.contains(setting.getEObject())) {
              result.add(setting.getEObject());
            }
          }
        }
      }
    }

    return result;
  }

  /**
   * @param document_p
   * @return
   */
  public static List<Extract> findReferencingExtracts(SourceCitation citation_p) {
    List<Extract> result = new ArrayList<Extract>();
    for (EObject obj : getReferencers(citation_p, DataPackage.Literals.EXTRACT__CITATIONS)) {
      result.add((Extract) obj);
    }
    return result;
  }

  /**
   * @param document_p
   * @return
   */
  public static List<SourceFile> findReferencingSourceFiles(Document document_p) {
    List<SourceFile> result = new ArrayList<SourceFile>();
    for (EObject obj : getReferencers(document_p, DataPackage.Literals.SOURCE_FILE__DOCUMENT)) {
      result.add((SourceFile) obj);
    }
    return result;
  }

  /**
   * @param document_p
   * @return
   */
  public static List<Event> findReferencingEvents(Document document_p) {
    List<Event> result = new ArrayList<Event>();

    for (EObject obj : getReferencers(document_p, CorePackage.Literals.SOURCE_CITATION__SOURCE)) {
      EObject owner = obj.eContainer();
      if (owner instanceof Event) {
        result.add((Event) owner);
      }
    }
    return result;
  }
}
