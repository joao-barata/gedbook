/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Joao Barata
 */
public class DateConverter {

  private static DateConverter _instance = null;

  public static final String republicanDays[] = new String[] { "decadi", //$NON-NLS-1$
      "primidi", //$NON-NLS-1$
      "duodi", //$NON-NLS-1$
      "tridi", //$NON-NLS-1$
      "quartidi", //$NON-NLS-1$
      "quintidi", //$NON-NLS-1$
      "sextidi", //$NON-NLS-1$
      "septidi", //$NON-NLS-1$
      "octidi", //$NON-NLS-1$
      "nonidi" //$NON-NLS-1$
  };

  public static final String republicanMonths[] = new String[] { "vend�miaire", //$NON-NLS-1$
      "brumaire", //$NON-NLS-1$
      "frimaire", //$NON-NLS-1$
      "niv�se", //$NON-NLS-1$
      "pluvi�se", //$NON-NLS-1$
      "vent�se", //$NON-NLS-1$
      "germinal", //$NON-NLS-1$
      "flor�al", //$NON-NLS-1$
      "prairial", //$NON-NLS-1$
      "messidor", //$NON-NLS-1$
      "thermidor", //$NON-NLS-1$
      "fructidor" //$NON-NLS-1$
  };

  public static final String sansculottideDays[] = new String[] { "jour de la vertu", //$NON-NLS-1$
      "jour du g�nie", //$NON-NLS-1$
      "jour du travail", //$NON-NLS-1$
      "jour de l'opinion", //$NON-NLS-1$
      "jour des r�compenses", //$NON-NLS-1$
      "jour de la r�volution" //$NON-NLS-1$
  };

  public static final String republicanYears[] = new String[] { "I", //$NON-NLS-1$
      "II", //$NON-NLS-1$
      "III", //$NON-NLS-1$
      "IV", //$NON-NLS-1$
      "V", //$NON-NLS-1$
      "VI", //$NON-NLS-1$
      "VII", //$NON-NLS-1$
      "VIII", //$NON-NLS-1$
      "IX", //$NON-NLS-1$
      "X", //$NON-NLS-1$
      "XI", //$NON-NLS-1$
      "XII", //$NON-NLS-1$
      "XIII", //$NON-NLS-1$
      "XIV" //$NON-NLS-1$
  };

  private DateConverter() {
    //
  }

  public static DateConverter getInstance() {
    if (null == _instance) {
      _instance = new DateConverter();
    }
    return _instance;
  }

  /**
   * 4/13/1 designe le 4eme jour complementaire de l'An I 4/1/1 designe le 4eme
   * jour du mois Vendemiaire de l'An I.
   */
  public String conversionDateGregorienRepublicain2(Date date) {
    String result = null;

    try {
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
      Date refDate1 = df.parse("22/09/1792"); //$NON-NLS-1$
      Date refDate2 = df.parse("31/12/1805"); //$NON-NLS-1$

      if (null == date || date.before(refDate1) || date.after(refDate2)) {
        return result;
      }

      long diff = date.getTime() - refDate1.getTime();
      long nj = diff / (1000 * 60 * 60 * 24);
      int na = 1;

      do {
        boolean isBissextile = ((na - 3) % 4) == 0;

        if (isBissextile) {
          if (nj < 366) {
            break;
          }
        } else {
          if (nj < 365) {
            break;
          }
        }
        if (isBissextile) {
          nj -= 366;
        } else {
          nj -= 365;
        }
        na++;
      } while (true);

      long nm = (nj / 30) + 1;
      nj = (nj % 30) + 1;

      result = republicanDays[(int) (nj % 10)] + " " + nj + " " + republicanMonths[(int) nm - 1] + " de l'an " + republicanYears[na - 1]; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    return result;
  }
}
