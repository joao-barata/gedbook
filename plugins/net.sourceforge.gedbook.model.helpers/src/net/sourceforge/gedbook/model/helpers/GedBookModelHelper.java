/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Note;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Residence;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.SourceCitation;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.query.conditions.eobjects.EObjectCondition;
import org.eclipse.emf.query.conditions.eobjects.structuralfeatures.EObjectReferenceValueCondition;
import org.eclipse.emf.query.statements.FROM;
import org.eclipse.emf.query.statements.SELECT;
import org.eclipse.emf.query.statements.WHERE;

/**
 * @author Joao Barata
 */
public class GedBookModelHelper {

  public static final String EMPTY = ""; //$NON-NLS-1$
  public static final String TAB = "\t"; //$NON-NLS-1$
  public static final String NEWLINE = "\n"; //$NON-NLS-1$
  public static final String DOT = "."; //$NON-NLS-1$
  public static final String UNDERSCORE = "_"; //$NON-NLS-1$

  public static final String DOT_FORMAT = "pdf"; //$NON-NLS-1$

  private static Stack<String> footnotestack = new Stack<>();

  /**
   * Static helpers class, no need to instantiate it
   */
  private GedBookModelHelper() {
    // do nothing
  }

  /**
   * @param person
   * @return
   */
  public static String getPersonFullName(Person person) {
    return EncodingUtils.latexEncoding(getFullName(person)) + " \\index[noms]{" + EncodingUtils.latexEncoding(getLastName(person)) + ", " + EncodingUtils.latexEncoding(getFirstName(person)) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  }

  /**
   * @param person
   * @return
   */
  public static String getPersonLastName(Person person) {
    return EncodingUtils.latexEncoding(getLastName(person)) + " \\index[noms]{" + EncodingUtils.latexEncoding(getLastName(person)) + ", " + EncodingUtils.latexEncoding(getFirstName(person)) + "}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  }

  /**
   * @param event
   * @return
   */
  public static String getPlaceName(Event event) {
    return EncodingUtils.latexEncoding(getShortPlace(event)) + "\\index[lieux]{" + EncodingUtils.latexEncoding(getPlaceInvertOrder(event)) + "}"; //$NON-NLS-1$ //$NON-NLS-2$
  }

  /**
   * @param person
   * @return
   */
  public static String getFullName(Person person) {
    String fullName = GedBookModelHelper.EMPTY;
    if (null != person) {
      String firstName = person.getNames().getGivenname();
      if (null != firstName) {
        fullName += firstName;
      }
      fullName += " "; //$NON-NLS-1$
      String lastName = person.getNames().getSurname();
      if (null != lastName) {
        fullName += lastName;
      }
    } else {
      fullName = "<unknown>"; //$NON-NLS-1$
    }
    return fullName;
  }

  /**
   * @param person
   * @return
   */
  public static String getFirstName(Person person) {
    String fullName = GedBookModelHelper.EMPTY;
    if (null != person) {
      String firstName = person.getNames().getGivenname();
      if (null != firstName) {
        fullName += firstName;
      }
    } else {
      fullName = "<unknown>"; //$NON-NLS-1$
    }
    return fullName;
  }

  /**
   * @param person
   * @return
   */
  public static String getLastName(Person person) {
    String fullName = GedBookModelHelper.EMPTY;
    if (null != person) {
      String lastName = person.getNames().getSurname();
      if (null != lastName) {
        fullName += lastName;
      }
    } else {
      fullName = "<unknown>"; //$NON-NLS-1$
    }
    return fullName;
  }

  /**
   * @param event
   * @return
   */
  public static String getShortPlace(Event event) {
    if (null != event) {
      Place place = event.getPlace();
      if (null != place) {
        String commune = place.getCommune();
        String departement = place.getDepartement();

        return ((null != commune) ? commune : "...") //$NON-NLS-1$
            + " (" + ((null != departement) ? departement : "...") + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      }
    }
    return GedBookModelHelper.EMPTY;
  }

  /**
   * @param date
   * @return
   */
  public static String getShortDate(DateWrapper date) {
    if (null == date) {
      return GedBookModelHelper.EMPTY;
    }
    SimpleDateFormat formatter = new SimpleDateFormat(date.getFormat());
    return EncodingUtils.latexEncoding(formatter.format(date.getDate()));
  }

  /**
   * @param event
   * @return
   */
  protected static String getPlaceInvertOrder(Event event) {
    if (null != event) {
      Place place = event.getPlace();
      if (null != place) {
        String commune = place.getCommune();
        String departement = place.getDepartement();
        String region = place.getRegion();

        return ((null != region) ? region : "...") //$NON-NLS-1$
            + "!" + ((null != departement) ? departement : "...") //$NON-NLS-1$ //$NON-NLS-2$
            + "!" + ((null != commune) ? commune : "..."); //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    return GedBookModelHelper.EMPTY;
  }

  /**
   * @param father
   * @param mother
   * @return
   */
  public static Family getFamily(Person father, Person mother) {
    if (null != father && null != mother) {
      for (EObject f1 : ResourceUtils.getReferencers(father, CorePackage.Literals.FAMILY__HUSBAND)) {
        for (EObject f2 : ResourceUtils.getReferencers(mother, CorePackage.Literals.FAMILY__WIFE)) {
          if (f1.equals(f2)) {
            return (Family) f1;
          }
        }
      }
    }
    return null;
  }

  /**
   * Retrieves all the families related to the given person, occurred before the given family.
   * @param person
   * @param family
   * @param familyComparator
   * @return
   */
  public static List<Family> getAllPreviousFamilies(Person person, Family family, Comparator<Family> familyComparator) {
    List<Family> families = getAllFamilies(person);
    families.sort(familyComparator);

    boolean deletionFlag = false;
    List<Family> toBeRemoved = new ArrayList<Family>();
    for (Family f : families) {
      if (f.equals(family)) deletionFlag = true;
      if (deletionFlag) {
        toBeRemoved.add(f);
      }
    }
    families.removeAll(toBeRemoved);

    return families;
  }

  /**
   * Retrieves all the families related to the given person, occurred after the given family.
   * @param person
   * @param family
   * @param familyComparator
   * @return
   */
  public static List<Family> getAllNextFamilies(Person person, Family family, Comparator<Family> familyComparator) {
    List<Family> families = getAllFamilies(person);
    families.sort(familyComparator);

    boolean deletionFlag = true;
    List<Family> toBeRemoved = new ArrayList<Family>();
    for (Family f : families) {
      if (deletionFlag) {
        toBeRemoved.add(f);
      }
      if (f.equals(family)) deletionFlag = false;
    }
    families.removeAll(toBeRemoved);

    return families;
  }

  /**
   * Retrieves all the families related to the given person.
   * @param person
   * @return
   */
  public static List<Family> getAllFamilies(Person person) {
    List<Family> families = new ArrayList<Family>();
    if (null != person) {
      if (SexKind.MALE.equals(person.getSex())) {
        for (EObject family : ResourceUtils.getReferencers(person, CorePackage.Literals.FAMILY__HUSBAND)) {
          families.add((Family) family);
        }
      } else {
        for (EObject family : ResourceUtils.getReferencers(person, CorePackage.Literals.FAMILY__WIFE)) {
          families.add((Family) family);
        }
      }
    }
    return families;
  }

  /**
   * @param project
   * @param father
   * @param mother
   * @return
   */
  public static List<Person> getAllChildren(final Project project, final Person father, final Person mother) {
    EObjectCondition cond1 = new EObjectReferenceValueCondition(CorePackage.eINSTANCE.getPerson_Father(), new EObjectCondition() {
      @Override
      public boolean isSatisfied(EObject eObject) {
        return eObject.equals(father);
      }
    });
    EObjectCondition cond2 = new EObjectReferenceValueCondition(CorePackage.eINSTANCE.getPerson_Mother(), new EObjectCondition() {
      @Override
      public boolean isSatisfied(EObject eObject) {
        return eObject.equals(mother);
      }
    });

    /** Build the select query statement */
    SELECT select = new SELECT(new FROM(project), new WHERE(cond1.AND(cond2)));

    /** Execute query and sort result by birth date */
    List<Person> result = new ArrayList<Person>();
    for (EObject object : select.execute()) {
      result.add((Person) object);
    }
    Collections.sort(result, new Comparator<Person>() {
      @Override
      public int compare(Person p0, Person p1) {
        if (null != p0 && null != p1) {
          Birth b0 = p0.getBirth();
          Birth b1 = p1.getBirth();
          if (null != b0 && null != b0.getDate() && null != b1 && null != b1.getDate()) {
            Date d0 = b0.getDate().getDate();
            Date d1 = b1.getDate().getDate();
            if (null != d0 && null != d1) {
              return d0.compareTo(d1);
            }
          }
        }
        return -1;
      }
    });

    return result;
  }

  /**
   * @param persons
   * @return
   */
  public static int countMales(List<Person> persons) {
    int counter = 0;
    for (Person person : persons) {
      if (SexKind.MALE.equals(person.getSex())) {
        counter++;
      }
    }
    return counter;
  }

  /**
   * @param persons
   * @return
   */
  public static int countFemales(List<Person> persons) {
    int counter = 0;
    for (Person person : persons) {
      if (SexKind.FEMALE.equals(person.getSex())) {
        counter++;
      }
    }
    return counter;
  }

  /**
   * Compute age of someone. Assume birthday is before marriage date.
   * 
   * @param birthday
   *          Date of birth.
   * @param marriage
   *          Date of marriage.
   * @return age
   */
  public static int computeAge(DateWrapper birthday, DateWrapper marriage) {
    if (null != birthday && null != marriage) {
      Calendar cBirthday = new GregorianCalendar();
      Calendar cMarriage = new GregorianCalendar();
      cBirthday.setTime(birthday.getDate());
      cMarriage.setTime(marriage.getDate());

      int yearDiff = cMarriage.get(Calendar.YEAR) - cBirthday.get(Calendar.YEAR);
      cBirthday.set(Calendar.YEAR, cMarriage.get(Calendar.YEAR));
      if (!cBirthday.after(cMarriage)) {
        return yearDiff; // Birthday already celebrated this year
      }
      return Math.max(0, yearDiff - 1); // Need a max to avoid -1 for baby
    }
    return 0;
  }

  /**
   * @param person
   * @param father
   * @param mother
   * @param partner
   * @param deathDateComparator
   * @return
   */
  public static List<Person> getParentsDeathList(Person person, Person father, Person mother, Person partner, Comparator<Person> deathDateComparator) {
    List<Person> parentList = new ArrayList<>(2);
    Death personDeath = person.getDeath();
    if (null != personDeath && null != personDeath.getDate()) {
      Date personDeathDate = personDeath.getDate().getDate();

      if (GedBookModelHelper.addToDeathSentence(father, personDeathDate)) {
        parentList.add(father);
      }
      if (GedBookModelHelper.addToDeathSentence(mother, personDeathDate)) {
        parentList.add(mother);
      }
      if (GedBookModelHelper.addToDeathSentence(partner, personDeathDate)) {
        parentList.add(partner);
      }

      Collections.sort(parentList, deathDateComparator);
    }
    return parentList;
  }

  /**
   * @param person
   * @param refDate
   * @return
   */
  public static boolean addToDeathSentence(Person person, Date refDate) {
    if (null != person && null != refDate) {
      Death personDeath = person.getDeath();
      if (null != personDeath && null != personDeath.getDate()) {
        Date deathDate = personDeath.getDate().getDate();
        if (null != deathDate && deathDate.before(refDate)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param occupations
   * @return
   */
  public static Map<String, Collection<Occupation>> compressOccupations(List<Occupation> occupations) {
    Map<String, Collection<Occupation>> map = new HashMap<>();
    for (Occupation occupation : occupations) {
      String occupationTitle = occupation.getDescription();
      if (map.containsKey(occupationTitle)) {
        Collection<Occupation> lst = map.get(occupationTitle);
        lst.add(occupation);
      } else {
        List<Occupation> lst = new ArrayList<>();
        lst.add(occupation);
        map.put(occupationTitle, lst);
      }
    }
    return map;
  }

  /**
   * @param residences
   * @return
   */
  public static Map<String, Collection<Residence>> compressResidences(List<Residence> residences) {
    Map<String, Collection<Residence>> map = new HashMap<>();
    for (Residence residence : residences) {
      String location = residence.getLocation();
      if (map.containsKey(location)) {
        Collection<Residence> lst = map.get(location);
        lst.add(residence);
      } else {
        List<Residence> lst = new ArrayList<>();
        lst.add(residence);
        map.put(location, lst);
      }
    }
    return map;
  }

  /**
   * @param events
   * @param andCoordinatingConjunction
   * @param eventDateComparator
   * @return
   */
  public static String getMultipleEventFootnote(Collection<? extends Event> events, String andCoordinatingConjunction, Comparator<Event> eventDateComparator) {
    String sentence = GedBookModelHelper.EMPTY;
    if (null != events) {
      List<Event> sortedEvents = new ArrayList<>(events);
      Collections.sort(sortedEvents, eventDateComparator);

      List<SourceCitation> citations = new ArrayList<SourceCitation>();
      for (Event event : sortedEvents) {
        citations.addAll(event.getCitations());
      }
      sentence += " \\footnote{"; //$NON-NLS-1$
      sentence += getCitationsSentence(citations, andCoordinatingConjunction);
      sentence += ".}"; //$NON-NLS-1$
    }
    return sentence;
  }

  /**
   * These notes can be generated in a minipage environment (e.g. when a photo is defined)<br/>
   * To correctly manage this case, this code shall be generated:<br/>
   * 
   * begin{minipage}
   * ...
   * \footnotemark{}
   * \footnotemark{}
   * \footnotemark{} %% any time it's necessary
   * ...
   * end{minipage}
   * \addtocounter{footnote}{0}% %% this number must be equal to (1 - number of marks)
   * \footnotetext{...}
   * \addtocounter{footnote}{+1}%
   * \footnotetext{...}
   * \addtocounter{footnote}{+1}%
   * \footnotetext{...}
   * ...
   * 
   * @param event
   * @param minipage
   * @return
   */
  public static String getEventNoteFootnote(Event event, boolean minipage) {
    String sentence = GedBookModelHelper.EMPTY;
    if (null != event) {
      for (Note note : event.getAnnotations()) {
        if (minipage) {
          sentence += "\\footnotemark{}"; //$NON-NLS-1$
          footnotestack.add(EncodingUtils.latexEncoding(note.getContent()));
        } else {
          sentence += "\\footnote{" + EncodingUtils.latexEncoding(note.getContent()) + "}"; //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
    }
    return sentence;
  }

  /**
   * @return
   */
  public static String flushEventNoteFootnote() {
    String sentence = GedBookModelHelper.EMPTY;
    int stacksize = footnotestack.size();
    if (stacksize > 0) {
      sentence += "\\addtocounter{footnote}{" + (1 - stacksize) + "}%" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$ //$NON-NLS-2$
      sentence += "\\footnotetext{" + footnotestack.pop() + "}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$ //$NON-NLS-2$
      while (!footnotestack.isEmpty()) {
        sentence += "\\addtocounter{footnote}{+1}%" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
        sentence += "\\footnotetext{" + footnotestack.pop() + "}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    
    return sentence;
  }

  /**
   * @param event
   * @param andCoordinatingConjunction
   * @param eventComparator
   * @return
   */
  public static String getEventSourceFootnote(Event event, String andCoordinatingConjunction, Comparator<SourceCitation> eventComparator) {
    String sentence = GedBookModelHelper.EMPTY;
    if (null != event) {
      List<SourceCitation> citations = new ArrayList<SourceCitation>(event.getCitations());
      Collections.sort(citations, eventComparator);

      sentence += " \\footnote{"; //$NON-NLS-1$
      sentence += getCitationsSentence(citations, andCoordinatingConjunction);
      sentence += ".}"; //$NON-NLS-1$
    }
    return sentence;
  }

  /**
   * @param citations
   * @param andCoordinatingConjunction
   * @return
   */
  private static String getCitationsSentence(List<SourceCitation> citations, String andCoordinatingConjunction) {
    String sentence = GedBookModelHelper.EMPTY;
    if (citations != null && !citations.isEmpty()) {
      int nbCitations = citations.size();
      for (int i = 0; i < nbCitations; i++) {
        SourceCitation citation = citations.get(i);
        Document doc = citation.getSource();
        String id = doc.getId();
        String abbr = doc.getAbbreviation();
        String page = citation.getPage();
  
        sentence += "\\cite[p." + page + "]{" + id + "} \\textit{" + ((null != doc.getPlace()) ? EncodingUtils.latexEncoding(doc.getPlace().getCommune()) : "") + " - " + abbr + " (" + EncodingUtils.latexEncoding(doc.getDate()) + ")}"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
        if (i == nbCitations - 2) {
          sentence += " " + andCoordinatingConjunction + " "; //$NON-NLS-1$ //$NON-NLS-2$
        } else if (i < nbCitations - 2) {
          sentence += ", "; //$NON-NLS-1$
        }
      }
    }
    return sentence;
  }
  
  /**
   * @param children
   * @return {@code true} if all children are female ({@code false} if one child at least is male)
   */
  public static boolean femaleWon(List<Person> children) {
    for (Person child : children) {
      if (SexKind.MALE.equals(child.getSex())) {
        return false;
      }
    }
    return true;
  }
}
