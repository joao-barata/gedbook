/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.helpers;

import java.util.Comparator;
import java.util.Date;

import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.SourceCitation;

/**
 * @author Joao Barata
 */
public class GedBookModelComparators {

  public static Comparator<SourceCitation> eventComparator = new Comparator<SourceCitation>() {
    @Override
    public int compare(SourceCitation citation0, SourceCitation citation1) {
      String abbr0 = citation0.getSource().getAbbreviation();
      String abbr1 = citation1.getSource().getAbbreviation();
      if (abbr0.startsWith("TD")) { //$NON-NLS-1$
        return -1;
      } else if (abbr1.startsWith("TD")) { //$NON-NLS-1$
        return 1;
      }
      return 0;
    }
  };

  public static Comparator<Event> eventDateComparator = new Comparator<Event>() {
    @Override
    public int compare(Event evt1, Event evt2) {
      Date d1 = evt1.getDate().getDate();
      Date d2 = evt2.getDate().getDate();
      return (null != d1 && null != d2) ? d1.compareTo(d2) : -1;
    }
  };

  public static Comparator<Person> deathDateComparator = new Comparator<Person>() {
    @Override
    public int compare(Person p1, Person p2) {
      Date d1 = p1.getDeath().getDate().getDate();
      Date d2 = p2.getDeath().getDate().getDate();
      return (null != d1 && null != d2) ? d1.compareTo(d2) : -1;
    }
  };

  public static Comparator<Family> marriageDateComparator = new Comparator<Family>() {
    @Override
    public int compare(Family f1, Family f2) {
      Date d1 = null, d2 = null;
      for (Event evt : f1.getEvents()) {
        if (evt instanceof Marriage) {
          DateWrapper dw = evt.getDate();
          if (dw != null) {
            d1 = dw.getDate();
          }
        }
      }
      for (Event evt : f2.getEvents()) {
        if (evt instanceof Marriage) {
          DateWrapper dw = evt.getDate();
          if (dw != null) {
            d2 = dw.getDate();
          }
        }
      }
      return (null != d1 && null != d2) ? d1.compareTo(d2) : -1;
    }
  };

}
