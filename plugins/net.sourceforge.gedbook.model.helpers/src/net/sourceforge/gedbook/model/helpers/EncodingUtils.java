/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.helpers;

/**
 * @author Joao Barata
 */
public class EncodingUtils {

  /**
   * 
   */
  public static final String ENCODING_UTF8 = "UTF-8"; //$NON-NLS-1$

  /**
   * 
   */
  private static final String COTE = "\\"; //$NON-NLS-1$
  private static final String DBCOTE = COTE + COTE;

  /**
   * Static helpers class, no need to instantiate it
   */
  private EncodingUtils() {
    // do nothing
  }

  /**
   * @param string
   * @return
   */
  public static String latexEncoding(String string) {
    return latexEncoding(string, true);
  }

  /**
   * @param string
   * @param doubleCote
   * @return
   */
  public static String latexEncoding(String string, boolean doubleCote) {
    if (null != string) {
      String encodedString = string;
      encodedString = encodedString.replaceAll("à", (doubleCote ? DBCOTE : COTE) + "`{a}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("è", (doubleCote ? DBCOTE : COTE) + "`{e}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ù", (doubleCote ? DBCOTE : COTE) + "`{u}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("á", (doubleCote ? DBCOTE : COTE) + "'{a}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("é", (doubleCote ? DBCOTE : COTE) + "'{e}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("í", (doubleCote ? DBCOTE : COTE) + "'{i}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ó", (doubleCote ? DBCOTE : COTE) + "'{o}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("â", (doubleCote ? DBCOTE : COTE) + "^{a}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ê", (doubleCote ? DBCOTE : COTE) + "^{e}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("î", (doubleCote ? DBCOTE : COTE) + "^{i}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ô", (doubleCote ? DBCOTE : COTE) + "^{o}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("û", (doubleCote ? DBCOTE : COTE) + "^{u}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ã", (doubleCote ? DBCOTE : COTE) + "~{a}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ñ", (doubleCote ? DBCOTE : COTE) + "~{n}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ä", (doubleCote ? DBCOTE : COTE) + "\"{a}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ë", (doubleCote ? DBCOTE : COTE) + "\"{e}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ï", (doubleCote ? DBCOTE : COTE) + "\"{i}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ö", (doubleCote ? DBCOTE : COTE) + "\"{o}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ü", (doubleCote ? DBCOTE : COTE) + "\"{u}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ç", (doubleCote ? DBCOTE : COTE) + "c {c}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Á", (doubleCote ? DBCOTE : COTE) + "'{A}"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("É", "E"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Í", "I"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ó", "O"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ú", "U"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ã", "A"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ñ", "N"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("°", "\\$^{o}\\$"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("_", (doubleCote ? DBCOTE : COTE) + "_"); //$NON-NLS-1$ //$NON-NLS-2$
      return encodedString;
    }
    return GedBookModelHelper.EMPTY;
  }

  /**
   * @param string
   * @return
   */
  public static String htmlEncoding(String string) {
    if (null != string) {
      String encodedString = string;
      encodedString = encodedString.replaceAll("à", "&agrave;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("è", "&egrave;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ù", "&ugrave;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("á", "&aacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("é", "&eacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("í", "&iacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ó", "&oacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("â", "&acirc;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ê", "&ecirc;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("î", "&icirc;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ô", "&ocirc;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("û", "&ucirc;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ã", "&atilde;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ñ", "&ntilde;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ä", "&auml;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ë", "&euml;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ï", "&iuml;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ö", "&ouml;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ü", "&uuml;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ç", "&ccedil;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Á", "&Aacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("É", "&Eacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Í", "&Iacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ó", "&Oacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ú", "&Uacute;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ã", "&Atilde;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ñ", "&Ntilde;"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("°", "&deg;"); //$NON-NLS-1$ //$NON-NLS-2$
      return encodedString;
    }
    return GedBookModelHelper.EMPTY;
  }

  /**
   * @param string
   * @return
   */
  public static String flatEncoding(String string) {
    if (null != string) {
      String encodedString = string;
      encodedString = encodedString.replaceAll("à", "a"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("è", "e"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ù", "u"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("á", "a"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("é", "e"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("í", "i"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ó", "o"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("â", "a"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ê", "e"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("î", "i"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ô", "o"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("û", "u"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ã", "a"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ñ", "n"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ä", "a"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ë", "e"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ï", "i"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ö", "o"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ü", "u"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("ç", "c"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Á", "A"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("É", "E"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Í", "I"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ó", "O"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ú", "U"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ã", "A"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("Ñ", "N"); //$NON-NLS-1$ //$NON-NLS-2$
      encodedString = encodedString.replaceAll("°", "o"); //$NON-NLS-1$ //$NON-NLS-2$
      return encodedString;
    }
    return GedBookModelHelper.EMPTY;
  }
}
