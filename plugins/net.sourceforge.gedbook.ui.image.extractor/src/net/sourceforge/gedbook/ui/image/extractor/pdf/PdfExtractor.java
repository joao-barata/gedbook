/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.image.extractor.pdf;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.latex.generator.IScreenshotExtractor;
import net.sourceforge.gedbook.model.data.Screenshot;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class PdfExtractor implements IScreenshotExtractor {

  /**
   * @see net.sourceforge.gedbook.latex.generator.IScreenshotExtractor#extractScreenshot(java.lang.String)
   */
  @Override
  public void extractScreenshot(String documentFilepath, String documentType, Screenshot screenshot, String outputScreenshotFilepath, Configuration configurationFile) throws IOException {
    String fileExtension = documentFilepath.substring(documentFilepath.lastIndexOf('.') + 1);
    if ("pdf".equalsIgnoreCase(fileExtension)) {
//      File file = new File(documentFilepath);
//      IPDFFile pdf = new SunPDFFile(file);
//      IPDFPage page = pdf.getPage(screenshot.getPage());
//      Rectangle rect = new Rectangle(
//        screenshot.getXLeftBottom(), ((int) page.getHeight()) - screenshot.getYRightTop(),
//        screenshot.getXRightTop() - screenshot.getXLeftBottom(),
//        screenshot.getYRightTop() - screenshot.getYLeftBottom());
//      if (rect.width > 0 && rect.height > 0) {
//        Image img = page.getImage((int) page.getHeight(), (int) page.getWidth());
//        BufferedImage bImg = toBufferedImage(img);
//        BufferedImage subImg = bImg.getSubimage(rect.x, rect.y, rect.width, rect.height);
//        File yourImageFile = new File(outputScreenshotFilepath + "." + documentType.toLowerCase());
//        ImageIO.write(subImg, documentType.toUpperCase(), yourImageFile);
//      }
//      pdf.close();
      
      File file = new File(outputScreenshotFilepath + "." + documentType);
      if (!file.exists()) {
        export(documentFilepath, screenshot, outputScreenshotFilepath, documentType, configurationFile);
      }
    }
  }

  public void export(String documentFilepath, Screenshot screenshot, String outputScreenshotFilepath, String documentType, Configuration configurationFile) {
    String outputFilename = outputScreenshotFilepath + "." + documentType;
    try {
      String binary = configurationFile.getString(IConfigurationConstants.EXTRACT_CONVERT_BINARY);
      int density = Integer.parseInt(configurationFile.getString(IConfigurationConstants.EXTRACT_DENSITY));
      Runtime runtime = Runtime.getRuntime();
      Process process = runtime.exec(new String[] {
          binary,
          "-density",  String.valueOf(density),
          "-trim",
          documentFilepath + "[" + (screenshot.getPage() - 1) + "]",
          "-quality", "100",
          outputFilename
      });
      process.waitFor();

      try {
        int ratio = Integer.parseInt(configurationFile.getString(IConfigurationConstants.EXTRACT_RATIO));
        java.awt.image.BufferedImage fullImage = javax.imageio.ImageIO.read(new java.io.File(outputFilename));
        
        java.awt.Rectangle rect = new java.awt.Rectangle(
          screenshot.getXLeftBottom() * ratio,
          fullImage.getHeight() - (screenshot.getYRightTop() * ratio),
          (screenshot.getXRightTop() - screenshot.getXLeftBottom()) * ratio,
          (screenshot.getYRightTop() - screenshot.getYLeftBottom()) * ratio);
        java.awt.image.BufferedImage croppedImage = fullImage.getSubimage(rect.x, rect.y, rect.width, rect.height);

        java.io.File croppedFile = new java.io.File(outputFilename);
        javax.imageio.ImageIO.write(croppedImage, documentType.toUpperCase(), croppedFile);
      }
      catch (java.io.IOException ex) {
        System.err.println("Error extracting file '" + documentFilepath + "' to file '" + outputFilename + "'");
      }
    } catch (java.lang.Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Converts a given Image into a BufferedImage
   *
   * @param img the Image to be converted
   * @return the converted BufferedImage
   */
  public BufferedImage toBufferedImage(Image img) {
    if (img instanceof BufferedImage) {
      return (BufferedImage) img;
    }

    // Create a buffered image with transparency
    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

    // Draw the image on to the buffered image
    Graphics2D bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();

    return bimage;
  }
}
