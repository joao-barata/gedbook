/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.core.provider;

import net.sourceforge.gedbook.model.core.Anulment;
import net.sourceforge.gedbook.model.core.Banns;
import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Burial;
import net.sourceforge.gedbook.model.core.Census;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Divorce;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.DocumentPkg;
import net.sourceforge.gedbook.model.core.EventPkg;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyPkg;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Name;
import net.sourceforge.gedbook.model.core.Nationality;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Ordination;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.PersonPkg;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.PlacePkg;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Property;
import net.sourceforge.gedbook.model.core.Repository;
import net.sourceforge.gedbook.model.core.RepositoryPkg;
import net.sourceforge.gedbook.model.core.Residence;
import net.sourceforge.gedbook.model.core.Retirement;
import net.sourceforge.gedbook.model.core.SocialSecurityNumber;
import net.sourceforge.gedbook.model.core.Source;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.core.SourcePkg;
import net.sourceforge.gedbook.model.core.Title;
import net.sourceforge.gedbook.model.core.Will;
import net.sourceforge.gedbook.model.core.edit.decorators.ForwardingItemProviderAdapterDecorator;

import org.eclipse.emf.edit.provider.DecoratorAdapterFactory;
import org.eclipse.emf.edit.provider.IItemProviderDecorator;

/**
 * @author Joao Barata
 */
public class CoreItemProviderDecoratorAdapterFactory extends DecoratorAdapterFactory {

	public CoreItemProviderDecoratorAdapterFactory() {
		super(new CoreItemProviderAdapterFactory());
	}

	/**
	 * @see org.eclipse.emf.edit.provider.DecoratorAdapterFactory#createItemProviderDecorator(java.lang.Object, java.lang.Object)
	 */
	protected IItemProviderDecorator createItemProviderDecorator(Object target, Object Type) {
    if (target instanceof Project) {
      return new ProjectItemProviderDecorator(this);
    } else if (target instanceof Repository) {
      return new RepositoryItemProviderDecorator(this);
    } else if (target instanceof RepositoryPkg) {
      return new RepositoryPkgItemProviderDecorator(this);
    } else if (target instanceof Source) {
      return new SourceItemProviderDecorator(this);
    } else if (target instanceof SourcePkg) {
      return new SourcePkgItemProviderDecorator(this);
    } else if (target instanceof SourceCitation) {
      return new SourceCitationItemProviderDecorator(this);
    } else if (target instanceof Document) {
      return new DocumentItemProviderDecorator(this);
    } else if (target instanceof DocumentPkg) {
      return new DocumentPkgItemProviderDecorator(this);
    } else if (target instanceof Family) {
      return new FamilyItemProviderDecorator(this);
    } else if (target instanceof FamilyPkg) {
      return new FamilyPkgItemProviderDecorator(this);
    } else if (target instanceof DateWrapper) {
      return new DateWrapperItemProviderDecorator(this);
    } else if (target instanceof Birth) {
      return new BirthItemProviderDecorator(this);
    } else if (target instanceof Batism) {
      return new BatismItemProviderDecorator(this);
    } else if (target instanceof Death) {
      return new DeathItemProviderDecorator(this);
    } else if (target instanceof Burial) {
      return new BurialItemProviderDecorator(this);
    } else if (target instanceof Will) {
      return new WillItemProviderDecorator(this);
    } else if (target instanceof Occupation) {
      return new OccupationItemProviderDecorator(this);
    } else if (target instanceof Title) {
      return new TitleItemProviderDecorator(this);
    } else if (target instanceof Nationality) {
      return new NationalityItemProviderDecorator(this);
    } else if (target instanceof SocialSecurityNumber) {
      return new SocialSecurityNumberItemProviderDecorator(this);
    } else if (target instanceof Property) {
      return new PropertyItemProviderDecorator(this);
    } else if (target instanceof Residence) {
      return new ResidenceItemProviderDecorator(this);
    } else if (target instanceof Ordination) {
      return new OrdinationItemProviderDecorator(this);
    } else if (target instanceof Retirement) {
      return new RetirementItemProviderDecorator(this);
    } else if (target instanceof Census) {
      return new CensusItemProviderDecorator(this);
    } else if (target instanceof Marriage) {
      return new MarriageItemProviderDecorator(this);
    } else if (target instanceof Banns) {
      return new BannsItemProviderDecorator(this);
    } else if (target instanceof Divorce) {
      return new DivorceItemProviderDecorator(this);
    } else if (target instanceof Anulment) {
      return new AnulmentItemProviderDecorator(this);
    } else if (target instanceof Person) {
      return new PersonItemProviderDecorator(this);
    } else if (target instanceof PersonPkg) {
      return new PersonPkgItemProviderDecorator(this);
    } else if (target instanceof Name) {
      return new NameItemProviderDecorator(this);
    } else if (target instanceof Place) {
      return new PlaceItemProviderDecorator(this);
    } else if (target instanceof PlacePkg) {
      return new PlacePkgItemProviderDecorator(this);
    } else if (target instanceof EventPkg) {
      return new EventPkgItemProviderDecorator(this);
		}
		return new ForwardingItemProviderAdapterDecorator(this);
	}
}
