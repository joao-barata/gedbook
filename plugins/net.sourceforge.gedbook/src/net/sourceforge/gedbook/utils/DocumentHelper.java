/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.data.MultipleSourceFile;
import net.sourceforge.gedbook.model.data.SourceFile;

/**
 * @author Joao Barata
 */
public class DocumentHelper {

  /**
   * @param sourceFiles_p
   * @return
   */
  public static File findCorrespondingFile(List<SourceFile> sourceFiles_p) {
    for (SourceFile sourceFile : sourceFiles_p) {
      String path = System.getenv(IConfigurationConstants.GEDBOOK_ARCHIVE_REPOSITORY) + sourceFile.getPath();
      File file = new File(path);
      if (file.exists()) {
        return file;
      }
    }
    return null;
  }

  /**
   * @param sourceFiles_p
   * @return
   */
  public static int findCorrespondingPages(List<SourceFile> sourceFiles_p) {
    for (SourceFile sourceFile : sourceFiles_p) {
      if (sourceFile instanceof MultipleSourceFile) {
        return ((MultipleSourceFile) sourceFile).getNumberOfPages();
      }
    }
    return 1;
  }

  /**
   * @param pages_p
   * @param offset_p
   * @return
   */
  public static List<Integer> analysePages(String pages_p, int offset_p) {
    List<Integer> result = new ArrayList<Integer>();
    if (null != pages_p) {
    	for (String str : pages_p.split(";")) {
    		if (!str.contains("-")) {
    			Integer page = getValue(str);
    			result.add(Integer.valueOf(page.intValue() + offset_p));
    		} else {
    			String[] pages = str.split("-");
    			Integer min = getValue(pages[0]);
    			Integer max = getValue(pages[1]);
    			for (int i = min.intValue(); i <= max.intValue(); i++) {
    				result.add(Integer.valueOf(i + offset_p));
    			}
    		}
    	}
    }
    return result;
  }

  /**
   * @param str_p
   * @return
   */
  private static Integer getValue(String str_p) {
    try {
      return Integer.valueOf(str_p);
    } catch (NumberFormatException ex) {
      ex.printStackTrace();
    }
    return Integer.valueOf(0);
  }

}
