/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joao Barata
 */
public class DirScanner {
  private static final String SEP = System.getProperty("file.separator"); //$NON-NLS-1$
  private final List<File> files = new ArrayList<File>();
  private final List<String> patts = new ArrayList<String>();

  /**
   * @param pattern
   * @return
   */
  private static boolean isSubtract(String pattern) {
    return pattern.startsWith("-"); //$NON-NLS-1$
  }

  /**
   * @param pattern
   * @return
   */
  private static String rawPatt(String pattern) {
    if (!isSubtract(pattern)) {
      return pattern;
    }
    return pattern.substring(1);
  }

  /**
   * @param patterns
   * @return
   */
  public static Iterable<File> scan(File dir, String... patterns) {
    DirScanner s = new DirScanner();
    for (String p : patterns) {
      p = p.replace(SEP, "/"); //$NON-NLS-1$
      p = p.replace(".", "\\."); //$NON-NLS-1$ //$NON-NLS-2$
      p = p.replace("*", ".*"); //$NON-NLS-1$ //$NON-NLS-2$
      p = p.replace("?", ".?"); //$NON-NLS-1$ //$NON-NLS-2$
      s.patts.add(p);
    }
    s.scan(dir, new File("/")); //$NON-NLS-1$
    return s.files;
  }

  /**
   * @param dir
   * @param path
   * @return
   */
  private void scan(File dir, File path) {
    for (File f : dir.listFiles()) {
      File rel = new File(path, f.getName());
      if (f.isDirectory()) {
        scan(f, rel);
        continue;
      }
      if (match(patts, rel)) {
        files.add(rel);
      }
    }
  }

  /**
   * @param patterns
   * @param rel
   * @return
   */
  private static boolean match(Iterable<String> patterns, File rel) {
    boolean ok = false;
    for (String p : patterns) {
      boolean subtract = isSubtract(p);
      p = rawPatt(p);
      boolean b = match(p, rel);
      if (b && subtract) {
        return false;
      }
      if (b) {
        ok = true;
      }
    }
    return ok;
  }

  /**
   * @param p
   * @param rel
   * @return
   */
  private static boolean match(String p, File rel) {
    String s = rel.getName();
    if (p.indexOf('/') >= 0) {
      s = rel.toString();
    }
    s = s.replace(SEP, "/"); //$NON-NLS-1$
    return s.matches(p);
  }
}