/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.XMLSave;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * @author Joao Barata
 */
public class GedBookXMIResourceFactoryImpl extends XMIResourceFactoryImpl {
	/**
	 * Constructor.
	 */
	public GedBookXMIResourceFactoryImpl() {
		super();
	}

	private Map<Object, Object> nameToFeatureMap = new HashMap<Object, Object>();

	@SuppressWarnings({ "rawtypes", "unchecked" })
  @Override
	public Resource createResource(URI uri) {
	  XMIResourceImpl result = new XMIResourceImpl(uri) {
			@Override
			protected XMLSave createXMLSave() {
				return new GedBookXMLSaveimpl(createXMLHelper());
			}
		};
		result.getDefaultLoadOptions().put(XMLResource.OPTION_DEFER_ATTACHMENT, Boolean.TRUE);
		result.getDefaultLoadOptions().put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
    result.getDefaultLoadOptions().put(XMLResource.OPTION_USE_XML_NAME_TO_FEATURE_MAP, nameToFeatureMap);
		result.setIntrinsicIDToEObjectMap(new HashMap());
		return result;
	}
}
