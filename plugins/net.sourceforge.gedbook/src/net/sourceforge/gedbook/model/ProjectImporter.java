/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import java.io.File;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.gedcom.GedcomImporter;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;

/**
 * @author Joao Barata
 */
public class ProjectImporter {

  private static ProjectImporter _instance;

  private ResourceSet _resourceSet;
  private Project _currentProject;
  private ExtendedData _currentExtendedData;

  public static ProjectImporter getInstance() {
    if (null == _instance) {
      _instance = new ProjectImporter();
    }
    return _instance;
  }

  private ProjectImporter() {
    //System.out.println("Register factory ..."); //$NON-NLS-1$
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new GedBookXMIResourceFactoryImpl());
    //System.out.println("Register package ..."); //$NON-NLS-1$
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();

    _resourceSet = new ResourceSetImpl();
    ECrossReferenceAdapter adapter = new ECrossReferenceAdapter();
    _resourceSet.eAdapters().add(adapter);
  }

  public boolean loadModel(GedcomImporter importer_p) {
    preLoadModel();

    EObject object = importer_p.createModel(_resourceSet, importer_p.getFile());

    return postLoadModel(object);
  }

  public boolean loadModel(File file_p) {
    preLoadModel();

    EObject object = ResourceUtils.loadObject(_resourceSet, file_p);

    return postLoadModel(object);
  }

  public boolean loadModel(String filename_p) {
    return loadModel(new File(filename_p));
  }

  private void preLoadModel() {
    unloadModel();
  }

  private boolean postLoadModel(EObject object_p) {
    if (object_p instanceof Project) {
      _currentProject = (Project) object_p;
      return true;
    } else if (object_p instanceof ExtendedData) {
      _currentExtendedData = (ExtendedData) object_p;
      _currentProject = _currentExtendedData.getReferencedProject();
      return true;
    }
    return false;
  }

  public void unloadModel() {
    if (null != _currentProject | null != _currentExtendedData) {
      _currentProject = null;
      _currentExtendedData = null;
      _resourceSet.getResources().get(0).unload();
      _resourceSet.getResources().clear();
    }
  }

  public Project getCurrentProject() {
    if (null == _currentProject) {
      if (null != _currentExtendedData) {
        return _currentExtendedData.getReferencedProject();
      }
    }
    return _currentProject;
  }

  public ExtendedData getCurrentExtendedData() {
    return _currentExtendedData;
  }
}
