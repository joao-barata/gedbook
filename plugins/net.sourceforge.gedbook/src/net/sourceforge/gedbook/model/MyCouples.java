/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import java.util.ArrayList;
import java.util.Date;

import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;

/**
 * @author Joao Barata
 */
@SuppressWarnings("serial")
public class MyCouples extends ArrayList<Family> {

  Date minBirthDate;
  Date maxBirthDate;
  Date minDeathDate;
  Date maxDeathDate;

  void checkBirthDate(Person p) {
    Birth birth = p.getBirth();
    if (null != birth && null != birth.getDate()) {
      Date date = birth.getDate().getDate();
      if (null != date) {
        if (null == minBirthDate) {
          minBirthDate = date;
        } else {
          minBirthDate = date.before(minBirthDate) ? date : minBirthDate;
        }
        if (null == maxBirthDate) {
          maxBirthDate = date;
        } else {
          maxBirthDate = date.after(maxBirthDate) ? date : maxBirthDate;
        }
      }
    }
  }

  void checkDeathDate(Person p) {
    Death death = p.getDeath();
    if (null != death && null != death.getDate()) {
      Date date = death.getDate().getDate();
      if (null != date) {
        if (null == minDeathDate) {
          minDeathDate = date;
        } else {
          minDeathDate = date.before(minDeathDate) ? date : minDeathDate;
        }
        if (null == maxDeathDate) {
          maxDeathDate = date;
        } else {
          maxDeathDate = date.after(maxDeathDate) ? date : maxDeathDate;
        }
      }
    }
  }

  @Override
  public boolean add(Family e) {
    if (null != e) {
      if (null != e.getHusband()) {
        checkBirthDate(e.getHusband());
        checkDeathDate(e.getHusband());
      }
      if (null != e.getWife()) {
        checkBirthDate(e.getWife());
        checkDeathDate(e.getWife());
      }
      return super.add(e);
    }
    return false;
  }

  public boolean contains(Person p) {
    if (p != null) {
      for (Family f : this) {
        if (p.equals(f.getHusband()) || p.equals(f.getWife())) {
          return true;
        }
      }
    }
    return false;
  }
}
