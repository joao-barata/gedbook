/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import net.sourceforge.gedbook.model.core.Person;

/**
 * @author Joao Barata
 */
public class Couple {
  public Person _husband;
  public Person _wife;
  public Couple(Person husband_p, Person wife_p) {
	_husband = husband_p;
	_wife = wife_p;
  }

  public boolean equals(Couple couple) {
  	if (null != couple && this._husband == couple._husband && this._wife == couple._wife) {
  		return true;
  	}
  	return false;
  }
}
