/*******************************************************************************
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.gedcom;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DataFormatException;

import net.sourceforge.gedbook.generator.AbstractGenerator;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Anulment;
import net.sourceforge.gedbook.model.core.Banns;
import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Burial;
import net.sourceforge.gedbook.model.core.Census;
import net.sourceforge.gedbook.model.core.CoreFactory;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Divorce;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.DocumentPkg;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.EventPkg;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyPkg;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Nationality;
import net.sourceforge.gedbook.model.core.Note;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Ordination;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.PersonEvent;
import net.sourceforge.gedbook.model.core.PersonPkg;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.PlacePkg;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Property;
import net.sourceforge.gedbook.model.core.Repository;
import net.sourceforge.gedbook.model.core.RepositoryPkg;
import net.sourceforge.gedbook.model.core.Residence;
import net.sourceforge.gedbook.model.core.Retirement;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.SocialSecurityNumber;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.core.Title;
import net.sourceforge.gedbook.model.core.Will;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.familysearch.gedcomparser.GedcomLine;
import org.familysearch.gedcomparser.GedcomParser;
import org.familysearch.gedcomparser.GedcomTag;
import org.familysearch.gedcomparser.record.ChildRecord;
import org.familysearch.gedcomparser.record.Citable;
import org.familysearch.gedcomparser.record.EventRecord;
import org.familysearch.gedcomparser.record.FamilyEventRecord;
import org.familysearch.gedcomparser.record.FamilyRecord;
import org.familysearch.gedcomparser.record.IndividualAttributeStructureRecord;
import org.familysearch.gedcomparser.record.IndividualRecord;
import org.familysearch.gedcomparser.record.NoteRecord;
import org.familysearch.gedcomparser.record.NoteStructureRecord;
import org.familysearch.gedcomparser.record.PersonalNameStructureRecord;
import org.familysearch.gedcomparser.record.PlaceRecord;
import org.familysearch.gedcomparser.record.RepositoryRecord;
import org.familysearch.gedcomparser.record.SourceCitationRecord;
import org.familysearch.gedcomparser.record.SourceRecord;

/**
 * @author Joao Barata
 */
public class GedcomImporter implements Runnable {

  /**
   * Importer listener
   */
  public GedcomImporterProgressListener progressListener;

  /**
   * Importer handler
   */
  public GedcomImporterRecordHandler recordHandler;

  private Map<String, Event> eventCache;
  private Map<String, Person> personCache;
  private Map<String, Document> documentCache;
  private File file;

  /**
   * Constructor
   * 
   * @param filename
   */
  public GedcomImporter(String filename) {
    this(new File(filename));
  }

  /**
   * Constructor
   * 
   * @param file
   */
  public GedcomImporter(File file) {
    this.file = file;
  }

  /**
   * @return
   */
  public File getFile() {
    return file;
  }

  /**
   * @return
   */
  protected Person getPersonById(Project project, String id) {
    Person person;
    if (null != personCache) {
      person = personCache.get(id);
      if (null != person) {
        return person;
      }
    }
    person = ProjectQueryHelper.getInstance().getExistingPerson(project, id);
    if (null != personCache && null != person) {
      personCache.put(id, person);
    }
    return person;
  }

  /**
   * @return
   */
  protected Document getDocumentById(Project project, String id) {
    Document document;
    if (null != documentCache) {
      document = documentCache.get(id);
      if (null != document) {
        return document;
      }
    }
    document = ProjectQueryHelper.getInstance().getExistingDocument(project, id);
    if (null != documentCache && null != document) {
      documentCache.put(id, document);
    }
    return document;
  }

  /**
   * @return
   */
  protected Event getEventById(Project project, String id) {
    Event event;
    if (null != eventCache) {
      event = eventCache.get(id);
      if (null != event) {
        return event;
      }
    }
    event = ProjectQueryHelper.getInstance().getExistingEvent(project, id);
    if (null != eventCache && null != event) {
      eventCache.put(id, event);
    }
    return event;
  }

  /**
   * Initialize internal data
   */
  public void initialize() {
    progressListener = new GedcomImporterProgressListener();
    recordHandler = new GedcomImporterRecordHandler();
    eventCache = new HashMap<>();
    personCache = new HashMap<>();
    documentCache = new HashMap<>();
  }

  /**
   * Clean internal data
   */
  @Override
  public void finalize() {
    if (null != eventCache) {
      eventCache.clear();
      eventCache = null;
    }
    if (null != personCache) {
      personCache.clear();
      personCache = null;
    }
    if (null != documentCache) {
      documentCache.clear();
      documentCache = null;
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      initialize();

      GedcomParser parser = new GedcomParser();
      InputStream inStream = new FileInputStream(file);
      boolean result = parser.importGedcomFile(inStream, file.length(), progressListener, recordHandler);
      if (!result) {
        System.err.println("Import failed."); //$NON-NLS-1$
      }
      inStream.close();
    } catch (IOException exception) {
      exception.printStackTrace();
    } catch (DataFormatException exception) {
      exception.printStackTrace();
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      finalize();
      System.out.println("gedcom import finished."); //$NON-NLS-1$
    }
  }

  /**
   * 
   */
  public EObject createModel(ResourceSet resourceSet, File file) {
    EObject project = createModel(new NullProgressMonitor());
    if (null != project) {
      URI uri = URI.createFileURI(file.getAbsolutePath());
      Resource resource = resourceSet.createResource(uri);
      resource.getContents().add(project);
    }
    return project;
  }

  /**
   * @return
   */
  private int evaluateAmountOfWork() {
    return (recordHandler.getSources().size() * 2)
      + recordHandler.getRepositories().size()
      + recordHandler.getIndividuals().size()
      + recordHandler.getFamilies().size()
      + recordHandler.getNotes().size();
  }

  /**
   * @param progressMonitor
   * @return
   */
  public EObject createModel(IProgressMonitor progressMonitor) {
    Project project = CoreFactory.eINSTANCE.createProject();
    project.setName("MyProject"); //$NON-NLS-1$
    project.setDescription("Super project"); //$NON-NLS-1$

    progressMonitor.beginTask("Model creation", evaluateAmountOfWork());

    int counter = 1;
    int amount = recordHandler.getSources().size();
    for (String sourceKey : recordHandler.getSources().keySet()) {
      progressMonitor.setTaskName("Import sources (" + String.valueOf(counter++) + "/" + String.valueOf(amount) + ")");
      SourceRecord record = recordHandler.getSources().get(sourceKey);
      createDocument(project, record);
      progressMonitor.worked(1);
    }

    counter = 1;
    amount = recordHandler.getRepositories().size();
    for (String sourceKey : recordHandler.getRepositories().keySet()) {
      progressMonitor.setTaskName("Import repositories (" + String.valueOf(counter++) + "/" + String.valueOf(amount) + ")");
      RepositoryRecord record = recordHandler.getRepositories().get(sourceKey);
      if (null != record) {
        Repository repository = CoreFactory.eINSTANCE.createRepository();
        repository.setId(record.getId());
        
        RepositoryPkg pkg = project.getOwnedRepositoryPkg();
        if (null == pkg) {
          pkg = CoreFactory.eINSTANCE.createRepositoryPkg();
          project.setOwnedRepositoryPkg(pkg);
        }
        pkg.getOwnedRepositories().add(repository);
      }
      progressMonitor.worked(1);
    }

    counter = 1;
    amount = recordHandler.getIndividuals().size();
    for (String sourceKey : recordHandler.getIndividuals().keySet()) {
      progressMonitor.setTaskName("Import individuals (" + String.valueOf(counter++) + "/" + String.valueOf(amount) + ")");
      IndividualRecord record = recordHandler.getIndividuals().get(sourceKey);
      if (null != record) {
        Person person = CoreFactory.eINSTANCE.createPerson();
        person.setNames(CoreFactory.eINSTANCE.createName());
        manageNames(person, record);

        String sex = record.getSex();
        person.setSex("M".equals(sex) ? SexKind.MALE : "F".equals(sex) ? SexKind.FEMALE : SexKind.UNKNOWN); //$NON-NLS-1$ //$NON-NLS-2$
        person.setId(record.getId());

        EventRecord[] events = record.getEvents();
        if (null != events) {
          for (EventRecord event : events) {
            createEvent(project, person, event);
          }
        }

        IndividualAttributeStructureRecord[] attributes = record.getAttributes();
        if (null != attributes) {
          for (IndividualAttributeStructureRecord attribute : attributes) {
            EventRecord event = attribute.getEvent();
            if (null != event) {
              createEvent(project, person, event);
            }
          }
        }

        PersonPkg pkg = project.getOwnedPersonPkg();
        if (null == pkg) {
          pkg = CoreFactory.eINSTANCE.createPersonPkg();
          project.setOwnedPersonPkg(pkg);
        }
        pkg.getOwnedPersons().add(person);
      }
      progressMonitor.worked(1);
    }

    counter = 1;
    amount = recordHandler.getFamilies().size();
    for (String sourceKey : recordHandler.getFamilies().keySet()) {
      progressMonitor.setTaskName("Import families (" + String.valueOf(counter++) + "/" + String.valueOf(amount) + ")");
      FamilyRecord record = recordHandler.getFamilies().get(sourceKey);
      if (null != record) {
        Family family = CoreFactory.eINSTANCE.createFamily();
        family.setId(record.getId());
        Person husband = getPersonById(project, record.getHusbandId());
        family.setHusband(husband);
        Person wife = getPersonById(project, record.getWifeId());
        family.setWife(wife);

        ChildRecord[] children = record.getChildrenInfos();
        if (null != children) {
          for (ChildRecord childRecord : children) {
            Person child = getPersonById(project, childRecord.getChildGedcomId());
            if (null != child) {
              child.setFather(husband);
              child.setMother(wife);
              family.getChildren().add(child);
            }
          }
        }

        FamilyEventRecord[] events = record.getEvents();
        if (null != events) {
          for (FamilyEventRecord event : events) {
            GedcomTag tag = event.getEvent().getTag();
            if (GedcomTag.MARB.equals(tag)) {
              Banns banns = createBannsEvent(project, record, event);
              banns.setFamily(family);
            } else if (GedcomTag.MARR.equals(tag)) {
              Marriage marriage = createMarriageEvent(project, record, event);
              marriage.setFamily(family);
            } else if (GedcomTag.DIV.equals(tag)) {
              Divorce divorce = createDivorceEvent(project, record, event);
              divorce.setFamily(family);
            } else if (GedcomTag.ANUL.equals(tag)) {
              Anulment anulment = createAnulmentEvent(project, record, event);
              anulment.setFamily(family);
            } else if (GedcomTag.CENS.equals(tag)) {
              Census census = createCensusEvent(project, record, event);
              census.setFamily(family);
            } else {
              System.err.println("unhandled family tag : " + tag.getName()); //$NON-NLS-1$
            }
          }
        }

        FamilyPkg pkg = project.getOwnedFamilyPkg();
        if (null == pkg) {
          pkg = CoreFactory.eINSTANCE.createFamilyPkg();
          project.setOwnedFamilyPkg(pkg);
        }
        pkg.getOwnedFamilies().add(family);
      }
      progressMonitor.worked(1);
    }

    counter = 1;
    amount = recordHandler.getSources().size();
    for (String sourceKey : recordHandler.getSources().keySet()) {
      progressMonitor.setTaskName("Update sources (" + String.valueOf(counter++) + "/" + String.valueOf(amount) + ")");
      SourceRecord record = recordHandler.getSources().get(sourceKey);
      updateDocument(project, record);
      progressMonitor.worked(1);
    }

    System.out.println("model creation finished."); //$NON-NLS-1$

    return project;
  }

  /**
   * 
   */
  private Document updateDocument(Project project, SourceRecord record) {
    if (null != record) {
      Document document = getDocumentById(project, record.getId());
      if (null != document) {
        GedcomLine line = record.getGedcomLine();
        if (null != line) {
          String plac = line.getChildValue(GedcomTag.PLAC);
          Place place = ProjectQueryHelper.getInstance().getExistingPlace(project, plac);
          if (null != place) {
            document.setPlace(place);
          }
          return document;
        }
      }
    }
    return null;
  }

  /**
   * 
   */
  private Banns createBannsEvent(Project project, FamilyRecord record, FamilyEventRecord event) {
    Person husband = getPersonById(project, record.getHusbandId());
    Person wife = getPersonById(project, record.getWifeId());
    if (null != husband && null != wife) {
      Banns evt = CoreFactory.eINSTANCE.createBanns();

      DateWrapper date = GedcomHelper.getInstance().getParsedDate(event.getEvent().getDate());
      String id = husband.getId() + "-" + wife.getId() + "-" + event.getEvent().getTag().getTag() + "-" + AbstractGenerator.getIdDate(date); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      Event eventWithSameId = getEventById(project, id);
      if (null == eventWithSameId) {
        evt.setId(id);
      } else {
        int i = 0;
        while (null != eventWithSameId) {
          eventWithSameId = getEventById(project, id + String.valueOf(i++));
        }
        evt.setId(id + String.valueOf(i));
      }

      manageSources(project, event, evt);
      manageDate(event.getEvent(), evt);
      managePlace(project, event.getEvent(), evt);

      EventPkg pkg = project.getOwnedEventPkg();
      if (null == pkg) {
        pkg = CoreFactory.eINSTANCE.createEventPkg();
        project.setOwnedEventPkg(pkg);
      }
      pkg.getOwnedEvents().add(evt);
      
      return evt;
    }
    return null;
  }

  /**
   * 
   */
  private Marriage createMarriageEvent(Project project, FamilyRecord record, FamilyEventRecord event) {
    Marriage evt = CoreFactory.eINSTANCE.createMarriage();

    Person husband = getPersonById(project, record.getHusbandId());
    Person wife = getPersonById(project, record.getWifeId());
    evt.setId((null!=husband?husband.getId():"") + "-" + (null!=wife?wife.getId():"") + "-" + event.getEvent().getTag().getTag()); //$NON-NLS-1$ //$NON-NLS-2$

    manageSources(project, event, evt);
    manageDate(event.getEvent(), evt);
    managePlace(project, event.getEvent(), evt);

    EventPkg pkg = project.getOwnedEventPkg();
    if (null == pkg) {
      pkg = CoreFactory.eINSTANCE.createEventPkg();
      project.setOwnedEventPkg(pkg);
    }
    pkg.getOwnedEvents().add(evt);

    return evt;
  }

  /**
   * 
   */
  private Divorce createDivorceEvent(Project project, FamilyRecord record, FamilyEventRecord event) {
    Divorce evt = CoreFactory.eINSTANCE.createDivorce();

    Person husband = getPersonById(project, record.getHusbandId());
    Person wife = getPersonById(project, record.getWifeId());
    evt.setId((null!=husband?husband.getId():"") + "-" + (null!=wife?wife.getId():"") + "-" + event.getEvent().getTag().getTag()); //$NON-NLS-1$ //$NON-NLS-2$

    manageSources(project, event, evt);
    manageDate(event.getEvent(), evt);
    managePlace(project, event.getEvent(), evt);

    EventPkg pkg = project.getOwnedEventPkg();
    if (null == pkg) {
      pkg = CoreFactory.eINSTANCE.createEventPkg();
      project.setOwnedEventPkg(pkg);
    }
    pkg.getOwnedEvents().add(evt);
    
    return evt;
  }

  /**
   * 
   */
  private Anulment createAnulmentEvent(Project project, FamilyRecord record, FamilyEventRecord event) {
    Anulment evt = CoreFactory.eINSTANCE.createAnulment();

    Person husband = getPersonById(project, record.getHusbandId());
    Person wife = getPersonById(project, record.getWifeId());
    evt.setId((null!=husband?husband.getId():"") + "-" + (null!=wife?wife.getId():"") + "-" + event.getEvent().getTag().getTag()); //$NON-NLS-1$ //$NON-NLS-2$

    manageSources(project, event, evt);
    manageDate(event.getEvent(), evt);
    managePlace(project, event.getEvent(), evt);

    EventPkg pkg = project.getOwnedEventPkg();
    if (null == pkg) {
      pkg = CoreFactory.eINSTANCE.createEventPkg();
      project.setOwnedEventPkg(pkg);
    }
    pkg.getOwnedEvents().add(evt);
    
    return evt;
  }

  /**
   * 
   */
  private Census createCensusEvent(Project project, FamilyRecord record, FamilyEventRecord event) {
    Census evt = CoreFactory.eINSTANCE.createCensus();

    Person husband = getPersonById(project, record.getHusbandId());
    Person wife = getPersonById(project, record.getWifeId());
    evt.setId((null!=husband?husband.getId():"") + "-" + (null!=wife?wife.getId():"") + "-" + event.getEvent().getTag().getTag()); //$NON-NLS-1$ //$NON-NLS-2$

    manageSources(project, event, evt);
    manageDate(event.getEvent(), evt);
    managePlace(project, event.getEvent(), evt);

    EventPkg pkg = project.getOwnedEventPkg();
    if (null == pkg) {
      pkg = CoreFactory.eINSTANCE.createEventPkg();
      project.setOwnedEventPkg(pkg);
    }
    pkg.getOwnedEvents().add(evt);
    
    return evt;
  }

  /**
   * 
   */
  private Event createEvent(Project project, Person person, EventRecord record) {
    PersonEvent evt = null;
    GedcomTag tag = record.getTag();
    if (GedcomTag.BIRT.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createBirth();
      person.setBirth((Birth) evt);
    } else if (GedcomTag.BAPM.equals(tag) || GedcomTag.CHR.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createBatism();
      person.setBatism((Batism) evt);
    } else if (GedcomTag.DEAT.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createDeath();
      person.setDeath((Death) evt);
    } else if (GedcomTag.BURI.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createBurial();
      person.setBurial((Burial) evt);
    } else if (GedcomTag.WILL.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createWill();
      person.setWill((Will) evt);
    } else if (GedcomTag.OCCU.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createOccupation();
      DateWrapper date = GedcomHelper.getInstance().getParsedDate(record.getDate());
      if  (date != null) {
        ((Occupation) evt).setDate(date);
        ((Occupation) evt).setId(person.getId() + "-" + tag.getTag() + "-" + AbstractGenerator.getIdDate(date)); //$NON-NLS-1$ //$NON-NLS-2$
      }
      ((Occupation) evt).setDescription(record.getDescription());
      person.getOccupations().add((Occupation) evt);
    } else if (GedcomTag.RESI.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createResidence();
      DateWrapper date = GedcomHelper.getInstance().getParsedDate(record.getDate());
      if  (date != null) {
        ((Residence) evt).setDate(date);
        ((Residence) evt).setId(person.getId() + "-" + tag.getTag() + "-" + AbstractGenerator.getIdDate(date)); //$NON-NLS-1$ //$NON-NLS-2$
      }
      ((Residence) evt).setLocation(record.getDescription());
      person.getResidences().add((Residence) evt);
    } else if (GedcomTag.TITL.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createTitle();
      ((Title) evt).setTitle(record.getDescription());
      person.getTitles().add((Title) evt);
    } else if (GedcomTag.NATI.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createNationality();
      ((Nationality) evt).setDescription(record.getDescription());
    } else if (GedcomTag.SSN.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createSocialSecurityNumber();
      ((SocialSecurityNumber) evt).setSSN(record.getDescription());
    } else if (GedcomTag.PROP.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createProperty();
      ((Property) evt).setDescription(record.getDescription());
    } else if (GedcomTag.ORDN.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createOrdination();
      ((Ordination) evt).setDescription(record.getDescription());
    } else if (GedcomTag.RETI.equals(tag)) {
      evt = CoreFactory.eINSTANCE.createRetirement();
      ((Retirement) evt).setDescription(record.getDescription());
    } else {
      System.err.println("unhandled individual tag : " + tag.getName()); //$NON-NLS-1$
    }
    if (null != evt) {
      evt.setPerson(person);
      if (evt.getId() == null || evt.getId().isEmpty()) {
        evt.setId(person.getId() + "-" + tag.getTag()); //$NON-NLS-1$
      }
      
      managePlace(project, record, evt);
      manageSources(project, record, evt);
      manageDate(record, evt);
      manageNotes(record, evt);

      EventPkg pkg = project.getOwnedEventPkg();
      if (null == pkg) {
        pkg = CoreFactory.eINSTANCE.createEventPkg();
        project.setOwnedEventPkg(pkg);
      }
      pkg.getOwnedEvents().add(evt);
    }
    return evt;
  }

  /**
   * 
   */
  private void manageNames(Person person, IndividualRecord record) {
    PersonalNameStructureRecord[] nameStructs = record.getNames();
    if (null != nameStructs) {
      for (PersonalNameStructureRecord nameStruct : nameStructs) {
        person.getNames().setAdoptionname(nameStruct.getAdoptionName());
        person.getNames().setBirthname(nameStruct.getBirthName());
        person.getNames().setCensusname(nameStruct.getCensusName());
        person.getNames().setCurrentname(nameStruct.getCurrentName());
        person.getNames().setFormalname(nameStruct.getFormalName());
        String given = nameStruct.getGiven();
        person.getNames().setGivenname(given);
        person.getNames().setMarriedname(nameStruct.getMarriedName());
        String name = nameStruct.getName();
        person.getNames().setName(name);
        person.getNames().setNickname(nameStruct.getNickName());
        person.getNames().setOthername(nameStruct.getOtherName());
        String surname = nameStruct.getSurname();
        person.getNames().setSurname(surname);

        if ((null == given || given.isEmpty()) && (null == surname || surname.isEmpty())) {
          String[] names = name.split("/");
          if (names.length > 0) {
            person.getNames().setGivenname(names[0].trim());
            if (names.length > 1) {
              person.getNames().setSurname(names[1].trim());
            }
          }
        }
      }
    }
  }

  /**
   * 
   */
  private void manageNotes(EventRecord record, Event evt) {
    NoteStructureRecord[] notes = record.getNotes();
    if (null != notes) {
      for (NoteStructureRecord note : notes) {
        NoteRecord cnt = recordHandler.getNotes().get(note.getId());
        
        Note annotation = CoreFactory.eINSTANCE.createNote();
        annotation.setId(cnt.getId());
        annotation.setContent(cnt.getText().trim());

        evt.getAnnotations().add(annotation);
      }
    }
  }

  /**
   * 
   */
  private void manageSources(Project project, Citable record, Event event) {
    SourceCitationRecord[] sources = record.getSourceCitations();
    createCitations(project, event, sources);
  }

  /**
   * 
   */
  private void manageDate(EventRecord record, Event evt) {
    String date = record.getDate();
    if (null != date) {
      evt.setDate(GedcomHelper.getInstance().getParsedDate(date));
    }
  }

  /**
   * 
   */
  private void managePlace(Project project, EventRecord record, Event event) {
    Place place = createPlace(project, record.getPlace());
    if (null != place) {
      event.setPlace(place);
    }
  }

  /**
   * 
   */
  private void createCitations(Project project, Event event, SourceCitationRecord[] records) {
    if (null != records) {
      for (SourceCitationRecord source : records) {
        String id = source.getId();
        String page = source.getPage();
        Document document = getDocumentById(project, id);
        if (null != document) {
          SourceCitation citation = CoreFactory.eINSTANCE.createSourceCitation();
          citation.setSource(document);
          citation.setPage(page);
          citation.setId(event.getId() + "-" + id); //$NON-NLS-1$
          event.getCitations().add(citation);
        }
      }
    }
  }

  /**
   * 
   */
  private Document createDocument(Project project, SourceRecord record) {
    if (null != record) {
      GedcomLine line = record.getGedcomLine();
      if (null != line) {
        String titl = line.getChildValue(GedcomTag.TITL);
        String abbr = line.getChildValue(GedcomTag.ABBR);
        String agnc = line.getChildValue(GedcomTag.AGNC);
        String date = line.getChildValue(GedcomTag.DATE);
        String plac = line.getChildValue(GedcomTag.PLAC);
        String refn = line.getChildValue(GedcomTag.REFN);
        String auth = line.getChildValue(GedcomTag.AUTH);

        Document document = CoreFactory.eINSTANCE.createDocument();
        document.setId(record.getId());
        document.setAbbreviation(abbr);
        document.setAuthor(auth);
        document.setAgency(agnc);
        document.setDate(date);
        Place place = ProjectQueryHelper.getInstance().getExistingPlace(project, plac);
        if (null != place) {
          document.setPlace(place);
        } else {
          place = createPlace(project, plac);
          if (null != place) {
            document.setPlace(place);
          }
        }
        document.setReference(refn);
        document.setTitle(titl);

        DocumentPkg pkg = project.getOwnedDocumentPkg();
        if (null == pkg) {
          pkg = CoreFactory.eINSTANCE.createDocumentPkg();
          project.setOwnedDocumentPkg(pkg);
        }
        pkg.getOwnedDocuments().add(document);

        return document;
      }
    }
    return null;
  }

  /**
   * 
   */
  private Place createPlace(Project project, PlaceRecord record) {
    if (null != record) {
      return createPlace(project, record.getName());
    }
    return null;
  }

  /**
   * 
   */
  private Place createPlace(Project project, String record) {
    if (null != record) {
      String[] details = record.split(","); //$NON-NLS-1$
      if (details.length == 4) {
        details[0] = details[0] != null ? details[0].trim() : "";
        details[1] = details[1] != null ? details[1].trim() : "";
        details[2] = details[2] != null ? details[2].trim() : "";
        details[3] = details[3] != null ? details[3].trim() : "";

        Place place = ProjectQueryHelper.getInstance().getExistingPlace(project, details[3], details[2], details[1], details[0]);
        if (null == place) {
          place = CoreFactory.eINSTANCE.createPlace();
          place.setCommune(details[0].trim());
          place.setDepartement(details[1].trim());
          place.setRegion(details[2].trim());
          place.setPays(details[3].trim());

          PlacePkg pkg = project.getOwnedPlacePkg();
          if (null == pkg) {
            pkg = CoreFactory.eINSTANCE.createPlacePkg();
            project.setOwnedPlacePkg(pkg);
          }
          pkg.getOwnedPlaces().add(place);
        }

        return place;
      }
    }
    return null;
  }
}
