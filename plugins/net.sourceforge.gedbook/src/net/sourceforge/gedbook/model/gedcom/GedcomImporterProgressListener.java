/*******************************************************************************
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.gedcom;

import org.familysearch.gedcomparser.ProgressListener;

/**
 * @author Joao Barata
 */
public class GedcomImporterProgressListener implements ProgressListener {

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingFamily() {
    //System.out.println("MyProgressListener.importingFamily()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingHeader() {
    //System.out.println("MyProgressListener.importingHeader()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingMedia() {
    //System.out.println("MyProgressListener.importingMedia()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingNote() {
    //System.out.println("MyProgressListener.importingNote()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingPerson() {
    //System.out.println("MyProgressListener.importingPerson()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingRepository() {
    //System.out.println("MyProgressListener.importingRepository()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingSource() {
    //System.out.println("MyProgressListener.importingSource()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingSubmission() {
    //System.out.println("MyProgressListener.enclosing_method()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingSubmitter() {
    //System.out.println("MyProgressListener.importingSubmitter()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void importingTPinDate() {
    //System.out.println("MyProgressListener.importingTPinDate()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setFileSize(long arg0) {
    //System.out.println("MyProgressListener.setFileSize()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void updateRemaining(long arg0) {
    //System.out.println("MyProgressListener.updateRemaining()"); //$NON-NLS-1$
  }
}
