/*******************************************************************************
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.gedcom;

import java.util.HashMap;
import java.util.Map;

import org.familysearch.gedcomparser.handler.RecordHandler;
import org.familysearch.gedcomparser.record.FamilyRecord;
import org.familysearch.gedcomparser.record.HeaderRecord;
import org.familysearch.gedcomparser.record.IndividualRecord;
import org.familysearch.gedcomparser.record.MultiMediaRecord;
import org.familysearch.gedcomparser.record.NoteRecord;
import org.familysearch.gedcomparser.record.RepositoryRecord;
import org.familysearch.gedcomparser.record.SourceRecord;
import org.familysearch.gedcomparser.record.SubmissionRecord;
import org.familysearch.gedcomparser.record.SubmitterRecord;
import org.familysearch.gedcomparser.record.TPinDateRecord;

/**
 * @author Joao Barata
 */
public class GedcomImporterRecordHandler implements RecordHandler {

  /**
   * 
   */
  private Map<String, NoteRecord> notes;
  private Map<String, SourceRecord> sources;
  private Map<String, FamilyRecord> families;
  private Map<String, IndividualRecord> individuals;
  private Map<String, RepositoryRecord> repositories;

  /**
   * {@inheritDoc}
   */
  @Override
  public void initialize() {
    notes = new HashMap<>();
    sources = new HashMap<>();
    families = new HashMap<>();
    individuals = new HashMap<>();
    repositories = new HashMap<>();
  }

  /**
   * Clean internal maps
   */
  public void dispose() {
    notes.clear();
    notes = null;
    sources.clear();
    sources = null;
    families.clear();
    families = null;
    individuals.clear();
    individuals = null;
    repositories.clear();
    repositories = null;
  }

  /**
   * 
   */
  public Map<String, NoteRecord> getNotes() {
    return notes;
  }

  /**
   * 
   */
  public Map<String, SourceRecord> getSources() {
    return sources;
  }

  /**
   * 
   */
  public Map<String, FamilyRecord> getFamilies() {
    return families;
  }

  /**
   * 
   */
  public Map<String, IndividualRecord> getIndividuals() {
    return individuals;
  }

  /**
   * 
   */
  public Map<String, RepositoryRecord> getRepositories() {
    return repositories;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void close() {
    //System.out.println("MyRecordHandler.close()"); //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleFamilyRecord(FamilyRecord family) {
    families.put(family.getId(), family);
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleHeaderRecord(HeaderRecord arg0) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleIndividualRecord(IndividualRecord individual) {
    individuals.put(individual.getId(), individual);
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleMultiMediaRecord(MultiMediaRecord arg0) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleNoteRecord(NoteRecord note) {
    notes.put(note.getId(), note);
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleRepositoryRecord(RepositoryRecord repository) {
    repositories.put(repository.getId(), repository);
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleSourceRecord(SourceRecord source) {
    sources.put(source.getId(), source);
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleSubmissionRecord(SubmissionRecord arg0) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleSubmitterRecord(SubmitterRecord arg0) {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean handleTPinDateRecord(TPinDateRecord arg0, String arg1, String arg2) {
    return true;
  }
}
