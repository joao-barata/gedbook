/*******************************************************************************
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model.gedcom;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import net.sourceforge.gedbook.model.core.CoreFactory;
import net.sourceforge.gedbook.model.core.DateQualifier;
import net.sourceforge.gedbook.model.core.DateWrapper;

import org.familysearch.gedcomparser.record.Citable;
import org.familysearch.gedcomparser.record.EventRecord;
import org.familysearch.gedcomparser.record.FamilyEventRecord;

import edu.neumont.gedcom.model.GedcomDate;

/**
 * @author Joao Barata
 */
public class GedcomHelper {

  private static GedcomHelper instance;

  private GedcomHelper() {
    //
  }

  public static GedcomHelper getInstance() {
    if (null == instance) {
      instance = new GedcomHelper();
    }
    return instance;
  }

  private static final String DATE_FORMAT1 = "dd/MM/yyyy"; //$NON-NLS-1$
  private static final String DATE_FORMAT2 = "dd MMM yyyy"; //$NON-NLS-1$
  private static final String DATE_FORMAT3 = "MM/yyyy"; //$NON-NLS-1$
  private static final String DATE_FORMAT4 = "yyyy"; //$NON-NLS-1$

  private static final String AFTER_PREFIX = "> "; //$NON-NLS-1$
  private static final String BEFORE_PREFIX = "< "; //$NON-NLS-1$

  /**
   * @param date
   * @return
   */
  public DateWrapper getParsedDate(String date) {
    if (null == date) {
      return null;
    }

    DateWrapper dw = getParsedDate(date, DATE_FORMAT1);
    if (null == dw) {
      dw = getParsedDate(date, DATE_FORMAT2, Locale.ENGLISH);
      if (null == dw) {
        dw = getParsedDate(date, DATE_FORMAT2, Locale.FRENCH);
        if (null == dw) {
          dw = getParsedDate(date, DATE_FORMAT3);
          if (null == dw) {
            if (date.matches("([<>] )?\\d+")) { //$NON-NLS-1$
              dw = getParsedDate(date, DATE_FORMAT4);
              if (null == dw) {
                dw = getParsedDate2(date);
              } else {
                dw.setDay(false);
                dw.setMonth(false);
                dw.setYear(true);
                dw.setFormat(DATE_FORMAT4);
              }
            }
          } else {
            dw.setDay(false);
            dw.setMonth(true);
            dw.setYear(true);
            dw.setFormat(DATE_FORMAT3);
          }
        } else {
          dw.setDay(true);
          dw.setMonth(true);
          dw.setYear(true);
          dw.setFormat(DATE_FORMAT2);
        }
      } else {
        dw.setDay(true);
        dw.setMonth(true);
        dw.setYear(true);
        dw.setFormat(DATE_FORMAT2);
      }
    } else {
      dw.setDay(true);
      dw.setMonth(true);
      dw.setYear(true);
      dw.setFormat(DATE_FORMAT1);
    }
    return dw;
  }

  /**
   * @param date
   * @return
   */
  public DateWrapper getParsedDate2(String date) {
    DateQualifier dq = DateQualifier.EXACT;
    if (date.startsWith(BEFORE_PREFIX)) {
      dq = DateQualifier.BEFORE;
      date = date.replaceFirst(BEFORE_PREFIX, ""); //$NON-NLS-1$
    } else if (date.startsWith(AFTER_PREFIX)) {
      dq = DateQualifier.AFTER;
      date = date.replaceFirst(AFTER_PREFIX, ""); //$NON-NLS-1$
    }
    GedcomDate gd = new GedcomDate(date);
    int day = gd.getDay();
    int month = gd.getMonth() - 1;
    int year = gd.getYear() - 1900;
    Date d = new Date(year, month, (day == 0) ? 1 : day);
    DateWrapper dw = CoreFactory.eINSTANCE.createDateWrapper();
    dw.setDate(d);
    dw.setQualifier(dq);
    dw.setDay(true);
    dw.setMonth(true);
    dw.setYear(true);
    dw.setFormat(DATE_FORMAT1);
    return dw;
  }

  /**
   * @param date
   * @param format
   * @return
   */
  public DateWrapper getParsedDate(String date, String format) {
    return getParsedDate(date, format, Locale.getDefault());
  }

  /**
   * @param date
   * @param format
   * @param locale
   * @return
   */
  public DateWrapper getParsedDate(String date, String format, Locale locale) {
    try {
      DateQualifier dq = DateQualifier.EXACT;
      if (date.startsWith(BEFORE_PREFIX)) {
        dq = DateQualifier.BEFORE;
        date = date.replaceFirst(BEFORE_PREFIX, ""); //$NON-NLS-1$
      } else if (date.startsWith(AFTER_PREFIX)) {
        dq = DateQualifier.AFTER;
        date = date.replaceFirst(AFTER_PREFIX, ""); //$NON-NLS-1$
      }

      SimpleDateFormat df = new SimpleDateFormat(format, locale);
      Date d = df.parse(date);

      DateWrapper dateWrapper = CoreFactory.eINSTANCE.createDateWrapper();
      dateWrapper.setDate(d);
      dateWrapper.setQualifier(dq);
      return dateWrapper;
    } catch (ParseException exception) {
      // exception.printStackTrace();
    }
    return null;
  }

  /**
   * @param citable
   * @return
   */
  public String getDate(Citable citable) {
    String date = ""; //$NON-NLS-1$
    if (citable instanceof FamilyEventRecord) {
      date = getDate(((FamilyEventRecord) citable).getEvent());
    } else if (citable instanceof EventRecord) {
      date = ((EventRecord) citable).getDate();
    }
    return date;
  }
}
