/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import java.util.List;

import net.sourceforge.gedbook.model.core.CorePackage;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.XMLSave;
import org.eclipse.emf.ecore.xmi.impl.XMISaveImpl;

/**
 * {@link XMLSave} implementation that serializes an XSL header at the beginning of the resource.
 * 
 * @author Joao Barata
 */
public class GedBookXMLSaveimpl extends XMISaveImpl {
  /**
   * XSL header.
   */
  public static final String GEDBOOK_XSL_HEADER = "<?xml-stylesheet type=\"text/xsl\" href=\"xmi2bib.xsl\"?>\n"; //$NON-NLS-1$

  /**
   * Keep a mark at the beginning of the document just below the XML declaration.
   */
  private Object _XSLHeaderMark;

  /**
   * Constructor.
   * @param helper_p
   */
  public GedBookXMLSaveimpl(XMLHelper helper_p) {
    super(helper_p);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected boolean shouldSaveFeature(EObject object_p, EStructuralFeature feature_p) {
    // Always save the ID feature.
    if (CorePackage.Literals.IDENTIFIABLE_OBJECT__ID.equals(feature_p)) {
      return true;
    }
    //return super.shouldSaveFeature(object_p, feature_p);
    return true;
  }

  /**
   * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#addDoctypeInformation()
   */
  @Override
  protected void addDoctypeInformation() {
    // Add XSL header as comment.
    doc.resetToMark(_XSLHeaderMark);
    //doc.addComment(GEDBOOK_XSL_HEADER);
    doc.add(GEDBOOK_XSL_HEADER);
    //doc.addLine();
    super.addDoctypeInformation();
  }

  /**
   * @see org.eclipse.emf.ecore.xmi.impl.XMISaveImpl#writeTopObjects(java.util.List)
   */
  @Override
  public Object writeTopObjects(List<? extends EObject> contents_p) {
	  _XSLHeaderMark = doc.mark();
    return super.writeTopObjects(contents_p);
  }

  /**
   * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#writeTopObject(org.eclipse.emf.ecore.EObject)
   */
  @Override
  protected Object writeTopObject(EObject top) {
	  _XSLHeaderMark = doc.mark();
    return super.writeTopObject(top);
  }
  
  /**
   * 
   * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#saveElementID(org.eclipse.emf.ecore.EObject)
   */
  @Override
  protected void saveElementID(EObject o_p) {
    saveFeatures(o_p);
  }
}
