/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.query.conditions.eobjects.EObjectCondition;
import org.eclipse.emf.query.conditions.eobjects.structuralfeatures.EObjectAttributeValueCondition;
import org.eclipse.emf.query.conditions.strings.StringValue;
import org.eclipse.emf.query.statements.FROM;
import org.eclipse.emf.query.statements.IQueryResult;
import org.eclipse.emf.query.statements.SELECT;
import org.eclipse.emf.query.statements.WHERE;

/**
 * @author Joao Barata
 */
public class ProjectQueryHelper {

  private static ProjectQueryHelper _instance;
  private static Map<String, Place> _placeCache;

  private ProjectQueryHelper() {
    //
  }

  public static ProjectQueryHelper getInstance() {
    if (null == _instance) {
      _instance = new ProjectQueryHelper();
      _placeCache = new HashMap<String, Place>();
    }
    return _instance;
  }

  /**
   * 
   */
  public Place getExistingPlace(Project project_p, String pays_p, String region_p, String departement_p, String commune_p) {
    for (EObject obj : searchExistingPlace(project_p, pays_p, region_p, departement_p, commune_p)) {
      if (obj instanceof Place) {
        return (Place) obj;
      }
    }
    return null;
  }

  /**
	   * 
	   */
  private IQueryResult searchExistingPlace(Project project_p, String pays_p, String region_p, String departement_p, String commune_p) {
    EObjectCondition cond1 = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getPlace_Pays(), new StringValue(pays_p));
    EObjectCondition cond2 = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getPlace_Region(), new StringValue(region_p));
    EObjectCondition cond3 = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getPlace_Departement(), new StringValue(departement_p));
    EObjectCondition cond4 = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getPlace_Commune(), new StringValue(commune_p));

    /** Build the select query statement */
    SELECT select = new SELECT(new FROM(project_p), new WHERE(cond4.AND(cond3).AND(cond2).AND(cond1)));

    /** Execute query */
    return select.execute();
  }

  /**
     * 
     */
  public Place getExistingPlace(Project project_p, String commune_p) {
    Place place = _placeCache.get(commune_p);
    if (null != commune_p && !commune_p.isEmpty()) {
      place = _placeCache.get(commune_p);
      if (place == null) {
        for (EObject obj : searchExistingPlace(project_p, commune_p)) {
          if (obj instanceof Place) {
            place = (Place) obj;
            _placeCache.put(commune_p, place);
          }
        }
      }
    }
    return place;
  }

  /**
	   * 
	   */
  private IQueryResult searchExistingPlace(Project project_p, String commune_p) {
    EObjectCondition cond = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getPlace_Commune(), new StringValue(commune_p));

    /** Build the select query statement */
    SELECT select = new SELECT(new FROM(project_p), new WHERE(cond));

    /** Execute query */
    return select.execute();
  }

  /**
   * 
   */
  public Event getExistingEvent(Project project_p, String id_p) {
    if (null != id_p && !id_p.isEmpty()) {
      for (EObject obj : getObjectByID(project_p, id_p)) {
        if (obj instanceof Event) {
          return (Event) obj;
        }
      }
    }
    return null;
  }

  /**
	 * 
   */
  public Document getExistingDocument(Project project_p, String id_p) {
    if (null != id_p && !id_p.isEmpty()) {
      for (EObject obj : getObjectByID(project_p, id_p)) {
        if (obj instanceof Document) {
          return (Document) obj;
        }
      }
    }
    return null;
  }

  /**
	   * 
	   */
  public Person getExistingPerson(Project project_p, String id_p) {
    if (null != id_p && !id_p.isEmpty()) {
      for (EObject obj : getObjectByID(project_p, id_p)) {
        if (obj instanceof Person) {
          return (Person) obj;
        }
      }
    }
    return null;
  }

  /**
	 * 
	 */
  private Set<? extends EObject> getObjectByID(Project project_p, String id_p) {
    EObjectCondition cond = new EObjectAttributeValueCondition(CorePackage.eINSTANCE.getIdentifiableObject_Id(), new StringValue(id_p));

    /** Build the select query statement */
    SELECT select = new SELECT(new FROM(project_p), new WHERE(cond));

    /** Execute query */
    IQueryResult result = select.execute();

    return result.getEObjects();
  }

}
