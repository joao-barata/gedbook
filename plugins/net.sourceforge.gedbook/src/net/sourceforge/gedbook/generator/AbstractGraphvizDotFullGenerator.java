/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.MyCouples;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public abstract class AbstractGraphvizDotFullGenerator extends AbstractGraphvizDotGenerator {

  private Person _husband;
  private Person _wife;

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param husband_p
   * @param wife_p
   */
  public AbstractGraphvizDotFullGenerator(Project project_p, String output_p, Configuration conf_p, Person husband_p, Person wife_p) {
    super(project_p, output_p, conf_p);
    _husband = husband_p;
    _wife = wife_p;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      BufferedWriter header = getFileBuffer(_output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + ".fulltree.tex"); //$NON-NLS-1$ //$NON-NLS-2$
      generateHeader(header);
      header.close();

      String filepath = _output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) //$NON-NLS-1$
          + ".fulltree/" + _husband.getId() + "_" + _wife.getId();
      BufferedWriter writer = getFileBuffer(filepath + ".dot"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      MyCouples couples = new MyCouples();
      couples.add(GedBookModelHelper.getFamily(_husband, _wife));
      List<MyCouples> generations = generation(couples);
      generateBook(writer, generations);
      writer.close();

      Runtime.getRuntime().exec("dot -T" + GedBookModelHelper.DOT_FORMAT + " " + filepath + ".dot -o " + filepath + "." + GedBookModelHelper.DOT_FORMAT).waitFor();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   *
   */
  public void generateHeader(BufferedWriter header) throws IOException {
    header.append("\\documentclass{standalone}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\usepackage{graphics}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\begin{document}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\includegraphics{" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + ".fulltree/" + _husband.getId() + "_" + _wife.getId() + "." + GedBookModelHelper.DOT_FORMAT + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\end{document}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
  }

  /**
	 *
	 */
  public List<MyCouples> generation(MyCouples families_p) {
    List<MyCouples> result = new ArrayList<MyCouples>();
    result.add(families_p);
    /** go next generation */
    MyCouples nextGeneration = getNextGeneration(families_p);
    if (!nextGeneration.isEmpty()) {
      result.addAll(generation(nextGeneration));
    }
    return result;
  }

  /**
	 *
	 */
  protected abstract MyCouples getNextGeneration(List<Family> families_p);

  /**
     * 
     */
  protected abstract void generateBook(BufferedWriter writer, List<MyCouples> families_p) throws IOException;
}
