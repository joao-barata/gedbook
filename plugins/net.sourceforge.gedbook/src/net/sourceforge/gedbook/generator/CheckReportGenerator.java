/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import net.sourceforge.gedbook.checker.CheckEventsWithOnlyTDRelatedSource;
import net.sourceforge.gedbook.checker.CheckEventsWithoutRelatedSource;
import net.sourceforge.gedbook.checker.CheckEventsWithoutTDRelatedSource;
import net.sourceforge.gedbook.checker.CheckPersonsWithoutProfession;
import net.sourceforge.gedbook.checker.CheckSourcesWithoutTranscription;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.ExtendedData;

/**
 * @author Joao Barata
 */
public class CheckReportGenerator extends AbstractGenerator {

  protected ExtendedData _extendedData;
  protected String _husband;
  protected String _wife;

  /**
   * @param project
   * @param output
   */
  public CheckReportGenerator(Project project, String output) {
    super(project, output, null);
  }

  /**
   * @param project
   * @param output
   * @param extendedData
   * @param husband
   * @param wife
   */
  public CheckReportGenerator(Project project, String output, ExtendedData extendedData, String husband, String wife) {
    super(project, output, null);
    _extendedData = extendedData;
    _husband = husband;
    _wife = wife;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      generateReport();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //System.out.println("generation finished."); //$NON-NLS-1$
    }
  }

  /**
   * 
   */
  private void generateReport() {
    CheckEventsWithoutRelatedSource checker1 = new CheckEventsWithoutRelatedSource(_project, _husband, _wife);
    checker1.check(_project);

    CheckEventsWithOnlyTDRelatedSource checker2 = new CheckEventsWithOnlyTDRelatedSource(_project, _husband, _wife);
    checker2.check(_project);

    CheckEventsWithoutTDRelatedSource checker3 = new CheckEventsWithoutTDRelatedSource(_project, _husband, _wife);
    checker3.check(_project);

    CheckSourcesWithoutTranscription checker4 = new CheckSourcesWithoutTranscription(null, null, null);
    checker4.check(_extendedData);

    CheckPersonsWithoutProfession checker5 = new CheckPersonsWithoutProfession(_project, _husband, _wife);
    checker5.check(_project);
  }
}
