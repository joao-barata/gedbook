/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.DocumentPkg;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.SourceFile;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;
import net.sourceforge.gedbook.utils.DocumentHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class LatexBibGenerator extends AbstractGenerator {

  /**
   * @param project_p
   * @param output
   * @param conf_p
   */
  public LatexBibGenerator(Project project_p, String output, Configuration conf_p) {
    super(project_p, output, conf_p);
  }

  /**
   * @param project_p
   * @param bibFilePath_p
   * @return true if bib file doesn't exist or if it's last modification is
   *         after the last modification of the project's resource file, false
   *         otherwise.
   */
  private boolean checkIfGenerationIsNeeded(Project project_p, String bibFilePath_p) {
    File bibFile = new File(bibFilePath_p);
    if (bibFile.exists()) {
      long bibFileLastModificationTime = bibFile.lastModified();
      String modelFilePath = project_p.eResource().getURI().path();
      File modelFile = new File(modelFilePath);
      long modelFileLastModificationTime = modelFile.lastModified();

      return (modelFileLastModificationTime > bibFileLastModificationTime);
    }
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      String bibfile = _output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + ".bib";
      if (checkIfGenerationIsNeeded(_project, bibfile)) {
        BufferedWriter writer = getFileBuffer(bibfile);
        generateBib(writer);
        writer.close();
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      System.out.println("generation of bibliography finished.");
    }
  }

  /**
   * @param writer
   * @throws IOException
   */
  private void generateBib(BufferedWriter writer) throws IOException {
    writer.write("%% This BibTeX bibliography file was generated." + GedBookModelHelper.NEWLINE);
    writer.newLine();

    DocumentPkg pkg = _project.getOwnedDocumentPkg();
    if (null != pkg) {
      for (Document document : pkg.getOwnedDocuments()) {
        Place place = document.getPlace();
        List<SourceFile> sources = ResourceUtils.findReferencingSourceFiles(document);
        File file = DocumentHelper.findCorrespondingFile(sources);
        int pages = DocumentHelper.findCorrespondingPages(sources);
        String ref = EncodingUtils.latexEncoding(document.getReference(), true);
        String author = EncodingUtils.latexEncoding(document.getAuthor(), true);

        writer.write("@book{" + document.getId() + "," + GedBookModelHelper.NEWLINE);
        writer.write("author = {" + author + "}," + GedBookModelHelper.NEWLINE);
        writer.write("title = {" + EncodingUtils.latexEncoding(document.getTitle(), true) + (null != ref ? " (ref: " + ref + ")" : "") + "}," + GedBookModelHelper.NEWLINE);
        writer.write("cote = {" + ref + "}," + GedBookModelHelper.NEWLINE);
        writer.write("year = {" + document.getDate() + "}," + GedBookModelHelper.NEWLINE);
        writer.write("type = {" + document.getAbbreviation() + "}," + GedBookModelHelper.NEWLINE);
        writer.write("publisher = {" + document.getAgency() + "}," + GedBookModelHelper.NEWLINE);
        writer.write("institution = {" + author + "}," + GedBookModelHelper.NEWLINE);
        writer.write("lieu = {" + EncodingUtils.latexEncoding((null == place ? "" : place.getCommune() + ", " + place.getDepartement() + ", " + place.getRegion() + ", " + place.getPays()), true) + "}," + GedBookModelHelper.NEWLINE);
        writer.write("language = {Fran\\c {c}ais}," + GedBookModelHelper.NEWLINE);
        writer.write("pdf = {" + (null == file ? "" : EncodingUtils.flatEncoding(file.getAbsolutePath())) + "}," + GedBookModelHelper.NEWLINE);
        writer.write("url = {" + document.getAgency() + "}," + GedBookModelHelper.NEWLINE);
        writer.write("abstract = {Registre des " + author + "}," + GedBookModelHelper.NEWLINE);
        writer.write("affiliation = {" + author + "}," + GedBookModelHelper.NEWLINE);
        writer.write("pages = {" + pages + "}," + GedBookModelHelper.NEWLINE);
        writer.write("audience = {non sp\\'{e}cifi\\'{e}e}" + GedBookModelHelper.NEWLINE);
        writer.write("}" + GedBookModelHelper.NEWLINE);
        writer.newLine();
      }
    }
  }
}
