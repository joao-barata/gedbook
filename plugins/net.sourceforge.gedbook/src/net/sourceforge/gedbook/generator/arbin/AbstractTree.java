package net.sourceforge.gedbook.generator.arbin;

import java.util.Stack;

/**
 *
 */
public abstract class AbstractTree implements ITree {

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return "<" + getContent() + "," + leftChild() + "," + rightChild() + ">";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void preOrdre(Stack<ITree> chemin) {
    chemin.push(this);
    visit(chemin);
    if (leftChild() != null)
      leftChild().preOrdre(chemin);
    if (rightChild() != null)
      rightChild().preOrdre(chemin);
    chemin.pop();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void inOrdre(Stack<ITree> chemin) {
    chemin.push(this);
    if (leftChild() != null)
      leftChild().inOrdre(chemin);
    visit(chemin);
    if (rightChild() != null)
      rightChild().inOrdre(chemin);
    chemin.pop();
  }

  protected void visit(Stack<ITree> chemin) {
    for (int i = chemin.size(); i > 0; i--)
      System.out.print(" . ");
    System.out.println(((ITree) chemin.peek()).getContent());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int calculatePositions(int xPosition, int yPosition) {
    if (leftChild() != null)
      xPosition = leftChild().calculatePositions(xPosition, yPosition + 1);
    setPosition(xPosition, yPosition);
    xPosition++;
    if (rightChild() != null)
      xPosition = rightChild().calculatePositions(xPosition, yPosition + 1);
    return xPosition;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int height() {
    int a;
    int b;
    a = b = 0;
    if (leftChild() != null)
      a = leftChild().height();
    if (rightChild() != null)
      b = rightChild().height();
    return Math.max(a, b) + 1;
  }
}
