package net.sourceforge.gedbook.generator.arbin;

import net.sourceforge.gedbook.model.core.Person;

/**
 *
 */
public class Tree extends AbstractTree {

  private Object content;
  private Tree leftChild;
  private Tree rightChild;
  private int x;
  private int y;

  /**
   * @param person
   */
  public Tree(Person person) {
    if (person != null) {
      content = person.getNames().getName().replaceAll("/", "");
    } else {
      content = "root";
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object getContent() {
    return content;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ITree leftChild() {
    return leftChild;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ITree rightChild() {
    return rightChild;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createLeftChild(Person person) {
    leftChild = new Tree(person);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void createRightChild(Person person) {
    rightChild = new Tree(person);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setPosition(int x, int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getX() {
    return x;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int getY() {
    return y;
  }
}
