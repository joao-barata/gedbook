package net.sourceforge.gedbook.generator.arbin;

import java.util.Stack;

import net.sourceforge.gedbook.model.core.Person;

/**
 *
 */
public interface ITree {

  /**
   * @return
   */
  ITree leftChild();

  /**
   * @return
   */
  ITree rightChild();
  
  /**
   * @param person
   * @return
   */
  void createLeftChild(Person person);
  
  /**
   * @param person
   * @return
   */
  void createRightChild(Person person);

  /**
   * @return
   */
  Object getContent();

  /**
   * @param path
   * @return
   */
  void preOrdre(Stack<ITree> path);

  /**
   * @param path
   * @return
   */
  void inOrdre(Stack<ITree> path);
  
  /**
   * @return
   */
  int height();

  /**
   * @param x
   * @param y
   * @return
   */
  void setPosition(int x, int y);

  /**
   * @return
   */
  int getX();

  /**
   * @return
   */
  int getY();

  /**
   * @param xPosition
   * @param yPosition
   * @return
   */
  int calculatePositions(int xPosition, int yPosition);
}
