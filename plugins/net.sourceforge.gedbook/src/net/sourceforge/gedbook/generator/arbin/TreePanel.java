package net.sourceforge.gedbook.generator.arbin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 *
 */
public class TreePanel extends JPanel {

  private static final long serialVersionUID = 3064689532349800473L;

  private static final int TEXT_RECT_WIDTH = 70;
  private static final int TEXT_RECT_HEIGHT = 8;
  private static final int TEXT_MARGIN = 3;
  private static final Color couleurInterne = new Color(200, 255, 200);
  private static final Color couleurExterne = new Color(255, 200, 200);
  private static final Font customFont = new Font(Font.SANS_SERIF, Font.PLAIN, 10);

  private ITree arbre;
  private int hauteur;
  private int largeur;
  private double dx;
  private double dy;
  private int mx;
  private int my;

  /**
   * @param a
   */
  public TreePanel(ITree a) {
    arbre = a;
    largeur = arbre.calculatePositions(0, 0);
    hauteur = arbre.height();
    setPreferredSize(new Dimension((largeur + 2) * TEXT_RECT_WIDTH, 450));
    setBackground(Color.white);
  }

  /**
   * @param g
   */
  @Override
  public void paint(Graphics g) {
    if (arbre != null) {
      g.setFont(customFont);
      Dimension d = getSize();
      dx = ((double) d.width) / (largeur + 1);
      dy = ((double) d.height) / hauteur;
      mx = (int) dx;
      my = (int) (dy / 2);
      int a1 = (int) (mx + dx * arbre.getX());
      int b1 = (int) (my + dy * arbre.getY());
      draw(arbre, g, a1, b1);
    }
  }

  /**
   * @param a
   * @param g
   * @param a1
   * @param b1
   */
  private void draw(ITree a, Graphics g, int a1, int b1) {
    int a2;
    int b2;
    if (a.leftChild() != null) {
      a2 = (int) (mx + dx * a.leftChild().getX());
      b2 = (int) (my + dy * a.leftChild().getY());
      g.drawLine(a1, b1, a2, b2);
      draw(a.leftChild(), g, a2, b2);
    }
    if (a.rightChild() != null) {
      a2 = (int) (mx + dx * a.rightChild().getX());
      b2 = (int) (my + dy * a.rightChild().getY());
      g.drawLine(a1, b1, a2, b2);
      draw(a.rightChild(), g, a2, b2);
    }

    String text = a.getContent().toString();
    int width = g.getFontMetrics().stringWidth(text) + ((TEXT_MARGIN * 2) - 1);

    Color c = g.getColor();
    g.setColor((a.leftChild() == null && a.rightChild() == null) ? couleurExterne : couleurInterne);
    g.fillRect(a1 - TEXT_RECT_WIDTH, b1 - TEXT_RECT_HEIGHT, width, 2 * TEXT_RECT_HEIGHT);
    g.setColor(c);
    g.drawRect(a1 - TEXT_RECT_WIDTH, b1 - TEXT_RECT_HEIGHT, width, 2 * TEXT_RECT_HEIGHT);
    g.drawString(text, a1 - TEXT_RECT_WIDTH + TEXT_MARGIN, b1 + TEXT_RECT_HEIGHT - 2);
  }
}
