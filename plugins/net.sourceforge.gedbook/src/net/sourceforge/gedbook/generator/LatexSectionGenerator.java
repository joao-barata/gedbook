/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.util.Set;

import net.sourceforge.gedbook.latex.generator.GedBookSectionGenerator;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.Extract;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class LatexSectionGenerator extends AbstractGenerator {

  private Family _family;
  private BufferedWriter _appendix;
  private BufferedWriter _section;
  private Set<Extract> _appendixBuffer;
  private Set<Family> _familyBuffer;
  private String _familyIdentifier;

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param section_p
   * @param appendix_p
   */
  public LatexSectionGenerator(Project project_p, String output_p, Configuration conf_p, Family family_p, BufferedWriter section_p, BufferedWriter appendix_p, Set<Extract> appendixBuffer, Set<Family> familyBuffer, String familyIdentifier) {
    super(project_p, output_p, conf_p);
    _family = family_p;
    _section = section_p;
    _appendix = appendix_p;
    _appendixBuffer = appendixBuffer;
    _familyBuffer = familyBuffer;
    _familyIdentifier = familyIdentifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      GedBookSectionGenerator.getInstance().generateSection(_section, _project, _family, _configurationFile, _appendix, _appendixBuffer, _familyBuffer, _familyIdentifier, _output);
    } catch (Exception e) {
      e.printStackTrace();
    }

    GraphvizDotFamilyGenerator generator = new GraphvizDotFamilyGenerator(_project, _output, _configurationFile, _family, _familyIdentifier);
    generator.run();
  }

  /**
   * 
   */
  String getPrefix(Document document_p) {
    if (null != document_p) {
      String abbr = document_p.getAbbreviation();
      if (null != abbr && !abbr.isEmpty()) {
        return abbr.replaceAll("+", "_").replaceAll("-", "_").replaceAll(" ", "_") + "_";
      }
    }
    return "";
  }
}
