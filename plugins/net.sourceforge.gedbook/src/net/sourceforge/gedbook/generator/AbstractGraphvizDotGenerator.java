/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;
import org.eclipse.emf.ecore.EObject;

/**
 * @author Joao Barata
 */
public abstract class AbstractGraphvizDotGenerator extends AbstractGenerator {

  private static final String PERSON_NODE_TEMPLATE_HEADER = GedBookModelHelper.TAB
      + "%s [shape=box,style=\"rounded%s\",label=<" + GedBookModelHelper.NEWLINE
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + "<table border=\"0\" cellborder=\"0\">" + GedBookModelHelper.NEWLINE
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + "<tr><td colspan=\"2\">%s %s</td></tr>" + GedBookModelHelper.NEWLINE;

  private static final String PERSON_NODE_TEMPLATE_EVENT = GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + "<tr><td rowspan=\"2\">%s</td><td align=\"left\">%s</td></tr>" + GedBookModelHelper.NEWLINE
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + GedBookModelHelper.TAB
      + "<tr><td align=\"left\">%s</td></tr>" + GedBookModelHelper.NEWLINE;

  private static final String PERSON_NODE_TEMPLATE_HLINE = GedBookModelHelper.TAB + GedBookModelHelper.TAB + GedBookModelHelper.TAB + "<hr/>" + GedBookModelHelper.NEWLINE;
  
  private static final String PERSON_NODE_TEMPLATE_FOOTER = GedBookModelHelper.TAB + GedBookModelHelper.TAB + "</table>>];" + GedBookModelHelper.NEWLINE;

  private static final String FAMILY_NODE_TEMPLATE_HEADER = GedBookModelHelper.TAB + "%s [shape=ellipse,label=";

  private static final String FAMILY_NODE_TEMPLATE_POSTHEADER1 = "<" + GedBookModelHelper.NEWLINE
      + GedBookModelHelper.TAB + GedBookModelHelper.TAB + "<table border=\"0\" cellborder=\"0\">" + GedBookModelHelper.NEWLINE;
  private static final String FAMILY_NODE_TEMPLATE_POSTHEADER2 = "\" \"";

  private static final String FAMILY_NODE_TEMPLATE_PREFOOTER1 = GedBookModelHelper.TAB + GedBookModelHelper.TAB + "</table>>" + GedBookModelHelper.NEWLINE;
  private static final String FAMILY_NODE_TEMPLATE_PREFOOTER2 = GedBookModelHelper.EMPTY;

  private static final String FAMILY_NODE_TEMPLATE_FOOTER = "];" + GedBookModelHelper.NEWLINE;

  private static final String MARRIAGE_NODE_TEMPLATE =
      GedBookModelHelper.TAB + GedBookModelHelper.TAB + GedBookModelHelper.TAB + "<tr><td>%s %s</td></tr>" + GedBookModelHelper.NEWLINE
      + GedBookModelHelper.TAB + GedBookModelHelper.TAB + GedBookModelHelper.TAB + "<tr><td>%s</td></tr>" + GedBookModelHelper.NEWLINE;

  /**
   * @param project_p
   * @param conf_p
   * @param husband_p
   * @param wife_p
   */
  public AbstractGraphvizDotGenerator(Project project_p, String output_p, Configuration conf_p) {
    super(project_p, output_p, conf_p);
  }

  /**
   * 
   */
  protected void generateLinks(BufferedWriter writer, Person father_p, Person mother_p, Family family_p, List<Person> children_p) throws IOException {
    if (null != family_p) {
      if (null != father_p) {
        writer.write(GedBookModelHelper.TAB
          + father_p.getId()
          + " -> "
          + family_p.getId().replaceAll("-", GedBookModelHelper.EMPTY)
          + ";" + GedBookModelHelper.NEWLINE);
      }
      if (null != mother_p) {
        writer.write(GedBookModelHelper.TAB
          + mother_p.getId()
          + " -> "
          + family_p.getId().replaceAll("-", GedBookModelHelper.EMPTY)
          + ";" + GedBookModelHelper.NEWLINE);
      }
      if (null != children_p) {
        for (EObject child : children_p) {
          writer.write(GedBookModelHelper.TAB
            + family_p.getId().replaceAll("-", GedBookModelHelper.EMPTY)
            + " -> "
            + ((Person) child).getId()
            + ";" + GedBookModelHelper.NEWLINE);
        }
      }
    }
  }

  /**
	 * 
	 */
  protected String generatePersonNode(Person person_p, boolean colorized_p) {
    if (null != person_p) {
      Birth birth = person_p.getBirth();
      Death death = person_p.getDeath();

      return String.format(PERSON_NODE_TEMPLATE_HEADER,
          person_p.getId(),
          colorized_p ? ",filled" : GedBookModelHelper.EMPTY,
          EncodingUtils.htmlEncoding(GedBookModelHelper.getFullName(person_p)), getPersonDecorator(person_p))
          + ((birth == null && death == null) ? GedBookModelHelper.NEWLINE : PERSON_NODE_TEMPLATE_HLINE)
          + ((birth == null) ? GedBookModelHelper.EMPTY : String.format(
              PERSON_NODE_TEMPLATE_EVENT, getBirthDecorator(), GedBookModelHelper.getShortDate(birth.getDate()), EncodingUtils.htmlEncoding(getVeryShortPlace(birth))))
          + ((birth != null && death != null) ? PERSON_NODE_TEMPLATE_HLINE : GedBookModelHelper.EMPTY)
          + ((death == null) ? GedBookModelHelper.EMPTY : String.format(
              PERSON_NODE_TEMPLATE_EVENT, getDeathDecorator(), GedBookModelHelper.getShortDate(death.getDate()), EncodingUtils.htmlEncoding(getVeryShortPlace(death))))
          + String.format(PERSON_NODE_TEMPLATE_FOOTER);
    }
    return GedBookModelHelper.EMPTY;
  }

  /**
   * 
   */
  protected String generatePersonNode(Person person_p) {
    return generatePersonNode(person_p, false);
  }

  /**
   * 
   */
  protected String generatePersonNodes(Person person_p, List<Person> children_p) {
    String result = GedBookModelHelper.EMPTY;
    if (!children_p.isEmpty()) {
      for (Person child : children_p) {
        boolean colorized = child.equals(person_p);
        result += generatePersonNode(child, colorized);
      }
    } else {
      result = generatePersonNode(person_p, true);
    }
    return result;
  }

  /**
     * 
     */
  protected String generatePersonNodes(List<Person> children_p) {
    String result = GedBookModelHelper.EMPTY;
    for (Person child : children_p) {
      result += generatePersonNode(child, false);
    }
    return result;
  }

  /**
	 * 
	 */
  protected String generateFamilyNode(Family family_p) {
    String node = GedBookModelHelper.EMPTY;
    if (null != family_p) {
      node += String.format(FAMILY_NODE_TEMPLATE_HEADER,
        family_p.getId().replaceAll("-", GedBookModelHelper.EMPTY)); //$NON-NLS-1$
      Collection<FamilyEvent> evts = family_p.getEvents();
      if (!evts.isEmpty()) {
        node += FAMILY_NODE_TEMPLATE_POSTHEADER1;
      } else {
        node += FAMILY_NODE_TEMPLATE_POSTHEADER2;
      }
      for (FamilyEvent evt : evts) {
        if (evt instanceof Marriage) {
          node += String.format(MARRIAGE_NODE_TEMPLATE, getMarriageDecorator(),
            GedBookModelHelper.getShortDate(evt.getDate()), EncodingUtils.htmlEncoding(getVeryShortPlace(evt)));
        }
      }
      if (!evts.isEmpty()) {
        node += FAMILY_NODE_TEMPLATE_PREFOOTER1;
      } else {
        node += FAMILY_NODE_TEMPLATE_PREFOOTER2;
      }
      node += FAMILY_NODE_TEMPLATE_FOOTER;
    }
    return node;
  }

  /**
	 * 
	 */
  protected String getPersonDecorator(Person person) {
    switch (person.getSex()) {
    case MALE:
      return "&#9794;";
    case FEMALE:
      return "&#9792;";
    default:
      return GedBookModelHelper.EMPTY;
    }
  }

  /**
	 * 
	 */
  protected String getBirthDecorator() {
    return "&loz;";
  }

  /**
	 * 
	 */
  protected String getDeathDecorator() {
    return "&dagger;";
  }

  /**
	 * 
	 */
  protected String getMarriageDecorator() {
    return "&infin;";
  }

  /**
   * 
   */
  protected String getDivorceDecorator() {
    return "&infin;";
  }
}
