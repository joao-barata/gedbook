/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class BookGraphsGenerator extends AbstractGenerator {

  private Person _husband;
  private Person _wife;

  /**
   * @param project_p
   * @param output
   * @param conf_p
   * @param husbandId_p
   * @param wifeId_p
   */
  public BookGraphsGenerator(Project project_p, String output, Configuration conf_p, String husbandId_p, String wifeId_p) {
    super(project_p, output, conf_p);
    _husband = ProjectQueryHelper.getInstance().getExistingPerson(_project, husbandId_p);
    _wife = ProjectQueryHelper.getInstance().getExistingPerson(_project, wifeId_p);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      for (int i = 1; i <= 10; i++) {
        generateMe(i);
        generateLatexFile(i);
      }
    } finally {
      System.out.println("generation of \"" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + "\" graphs finished."); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
	 *
	 */
  public void generateLatexFile(int generation) {
    try {
      BufferedWriter writer = getFileBuffer(_output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/circles/generation_" + generation + ".tex"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      if (generation == 1) {
        writer.append("\\begin{center}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      }
      writer.append("\\includegraphics[max size={\\textwidth}{\\textheight}]{" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/circles/generation_" + generation + ".jpg}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$
      if (generation == 1) {
        writer.append("\\end{center}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      }
      writer.append("\\clearpage" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      writer.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
	 *
	 */
  public void generateMe(int maxGenerations) {
    int size = 120;
    int rotationThreshold = 3;
    int frameMargin = 10;
    int frameWidth = (size * maxGenerations * 2) + frameMargin;
    int frameHeight = (size * maxGenerations * 2) + frameMargin;
    int maxAncestors = (int) Math.pow(2, maxGenerations + 1);

    Person test[][] = new Person[maxGenerations][maxAncestors];
    test[0][0] = _husband;
    test[0][1] = _wife;
    for (int generation = 0; generation < maxGenerations - 1; generation++) {
      for (int index = 0; index < maxAncestors / 2; index++) {
        Person person = test[generation][index];
        if (null != person) {
          Person father = person.getFather();
          Person mother = person.getMother();

          test[generation + 1][2 * index] = father;
          test[generation + 1][(2 * index) + 1] = mother;
        }
      }
    }

    BufferedImage bImg = new BufferedImage(frameWidth, frameHeight, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2d = (Graphics2D) bImg.getGraphics();
    g2d.setColor(Color.white);
    g2d.fillRect(0, 0, frameWidth, frameHeight);
    g2d.setColor(Color.black);
    SunriseGraph graph = new SunriseGraph(test, frameWidth, frameHeight, size, maxGenerations, rotationThreshold);
    graph.paintMe(g2d);
    g2d.dispose();
    try {
      ImageIO.write(bImg, "JPG", getFile(_output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/circles/generation_" + maxGenerations + ".jpg")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
