/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import net.sourceforge.gedbook.model.core.Person;

/**
 * @author Joao Barata
 */
public class SunriseGraph {

  private int _width;
  private int _height;
  private int _size;
  private int _maxGenerations;
  private int _rotationThreshold;
  private Person[][] _data;

  /**
   * 
   */
  @SuppressWarnings("boxing")
  private static final Double horizontalMatrix[][] = { { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null },
      { 315.00, 45.00, null, null, null, null, null, null, null, null, null, null, null, null, null, null },
      { 292.50, 337.50, 22.50, 67.50, null, null, null, null, null, null, null, null, null, null, null, null }, /*
                                                                                                                 * +
                                                                                                                 * /
                                                                                                                 * -
                                                                                                                 * 22.500
                                                                                                                 */
      { 281.25, 303.75, 326.25, 348.75, 11.25, 33.75, 56.25, 78.75, null, null, null, null, null, null, null, null }, /*
                                                                                                                       * +
                                                                                                                       * /
                                                                                                                       * -
                                                                                                                       * 11.250
                                                                                                                       */
      { 275.625, 286.875, 298.125, 309.375, 320.625, 331.875, 343.125, 354.375, 5.625, 16.875, 28.125, 39.375, 50.625, 61.875, 73.125, 84.375 } /*
                                                                                                                                                 * +
                                                                                                                                                 * /
                                                                                                                                                 * -
                                                                                                                                                 * 5.625
                                                                                                                                                 */
  };

  /**
   * 
   */
  private class Configuration {
    public boolean showFirstName;
    public boolean showLastName;

    public Configuration(boolean showFirstName_p, boolean showLastName_p) {
      showFirstName = showFirstName_p;
      showLastName = showLastName_p;
    }
  }

  /**
   * 
   */
  Configuration configuration[] = { new Configuration(true, true), new Configuration(true, true), new Configuration(true, true), new Configuration(true, true), new Configuration(false, true),
      new Configuration(false, true), new Configuration(false, true), new Configuration(false, true), new Configuration(false, true), new Configuration(false, true) };

  /**
   * @param width
   * @param height
   * @param size
   * @param maxGenerations
   * @param rotationThreshold
   */
  public SunriseGraph(Person[][] data, int width, int height, int size, int maxGenerations, int rotationThreshold) {
    _data = data;
    _width = width;
    _height = height;
    _size = size;
    _maxGenerations = maxGenerations;
    _rotationThreshold = rotationThreshold;
  }

  /**
   * 
   */
  public void paintMe(Graphics2D ga) {
    ga.setBackground(Color.WHITE);
    ga.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    ga.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
    ga.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    ga.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    ga.setFont(new Font(Font.SERIF, Font.BOLD, 14));

    int xCenter = _width / 2;
    int yCenter = _height / 2;

    for (int i = _maxGenerations; i > 0; i--) {
      drawArcs(ga, Color.black, i, xCenter, yCenter);
    }
    for (int i = 0; i < _maxGenerations; i++) {
      drawSeparators(ga, Color.black, i + 1, xCenter, yCenter);
    }
    for (int i = 0; i < _maxGenerations; i++) {
      drawTexts(ga, Color.black, i, xCenter, yCenter);
    }
  }

  /**
   * 
   */
  protected void drawArcs(Graphics2D g, Color color, int generation, int xCenter, int yCenter) {
    g.setColor(color);
    g.drawArc(xCenter - (_size * generation), yCenter - (_size * generation), 2 * _size * generation, 2 * _size * generation, 0, 360);
  }

  /**
   * 
   */
  protected void drawSeparators(Graphics2D g, Color color, int generation, int xCenter, int yCenter) {
    g.setColor(color);
    if (generation == 0) {
      g.drawLine(xCenter - _size, yCenter, xCenter + _size, yCenter);
    } else {
      double parts = Math.pow(2, generation - 1) * 2;
      double step = 360 / parts;
      for (int i = 0; i <= parts; i++) {
        double rad = Math.toRadians(i * step);
        double x1 = xCenter - (_size * generation * Math.cos(rad));
        double y1 = yCenter - (_size * generation * Math.sin(rad));
        double x2 = xCenter - (_size * (generation - 1) * Math.cos(rad));
        double y2 = yCenter - (_size * (generation - 1) * Math.sin(rad));
        g.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
      }
    }
  }

  /**
   * 
   */
  protected void drawTexts(Graphics2D g, Color color, int generation, int xCenter, int yCenter) {
    g.setColor(color);

    double parts = Math.pow(2, generation + 1);
    double step = 360 / parts;
    for (int i = 0; i < parts; i++) {
      double theta1 = i * step;
      double theta2 = (i + 1) * step;
      double rad1 = Math.toRadians(theta1);
      double rad2 = Math.toRadians(theta2);
      double x1 = xCenter - (_size * (generation + 1) * Math.cos(rad1));
      double y1 = yCenter - (_size * (generation + 1) * Math.sin(rad1));
      double x3 = xCenter - (_size * (generation + 1) * Math.cos(rad2));
      double y3 = yCenter - (_size * (generation + 1) * Math.sin(rad2));

      double xt = (x3 + x1) / 2;
      double yt = (y3 + y1) / 2;

      int linePosition = 1;
      int nbLines = (configuration[generation].showLastName ? 1 : 0) + (configuration[generation].showFirstName ? 1 : 0) + 1;

      String lastName = (null != _data[generation][i]) ? _data[generation][i].getNames().getSurname() : null;
      if (null != lastName && configuration[generation].showLastName) {
        Point pt = getTextPosition(g, lastName, generation, xt, yt, theta1, theta2, linePosition, nbLines);
        g.setTransform(AffineTransform.getRotateInstance(getTheta(i, parts, generation) + getQuadrantRotation(theta1, theta2, generation), xt, yt));
        g.drawString(lastName, pt.x, pt.y);
        linePosition++;
      }
      String firstName = (null != _data[generation][i]) ? _data[generation][i].getNames().getGivenname() : null;
      if (null != firstName && configuration[generation].showFirstName) {
        Point pt = getTextPosition(g, firstName, generation, xt, yt, theta1, theta2, linePosition, nbLines);
        g.setTransform(AffineTransform.getRotateInstance(getTheta(i, parts, generation) + getQuadrantRotation(theta1, theta2, generation), xt, yt));
        g.drawString(firstName, pt.x, pt.y);
      }
    }
  }

  /**
   * 
   */
  protected Point getTextPosition(Graphics2D g, String text, int generation, double xt, double yt, double theta1, double theta2, int linePosition, int nbLines) {
    Point pt = new Point(0, 0);
    FontMetrics fm = g.getFontMetrics();
    Rectangle2D r = fm.getStringBounds(text, g);

    if (generation <= _rotationThreshold) {
      int quadrant = getQuadrant(theta1, theta2);
      pt.x = (int) (xt - (r.getWidth() / 2));
      if (generation == 0) {
        if (quadrant == 0 || quadrant == 1) {
          pt.y = (int) (yt - ((nbLines - linePosition) * _size / nbLines) + r.getHeight());
        } else {
          pt.y = (int) (yt + (linePosition * _size / nbLines));
        }
      } else if (generation == 1) {
        int coeff = (linePosition == 1) ? -1 : 1;
        if (quadrant == 0 || quadrant == 1) {
          pt.y = (int) (yt + (coeff * (_size * Math.sin(Math.toRadians(90 / (generation + 1))) / nbLines)) - (coeff * r.getHeight() / 2));
        } else {
          // pt.y = (int) (yt + (r.getHeight() / 2));
          pt.y = (int) (yt + (coeff * (_size * Math.sin(Math.toRadians(90 / (generation + 1))) / nbLines)) - (coeff * r.getHeight() / 2));
        }
      } else {
        int coeff = (linePosition == 1) ? 1 : -1;
        if (quadrant == 0 || quadrant == 1) {
          pt.y = (int) (yt + (r.getHeight() * 2) + fm.getAscent() - (coeff * r.getHeight()));
        } else {
          pt.y = (int) (yt - (r.getHeight() * 2) - fm.getAscent() - (coeff * r.getHeight()));
        }
      }
    } else {
      int quadrant = getQuadrant(theta1, theta2);
      double width = r.getWidth();
      // double margins = (_size - width) / 2;
      pt.x = (int) (xt + (((quadrant == 0 || quadrant == 3) ? 1 : -2) * (width / 2)));
      pt.y = (int) yt;
    }
    return pt;
  }

  /**
   * 
   */
  protected double getTheta(int i, double parts, int generation) {
    Double deg = null;
    if (generation <= _rotationThreshold) {
      deg = horizontalMatrix[generation][i % ((int) (parts / 2))];
    } else {
      double theta1 = i * (360 / parts);
      double theta2 = (i + 1) * (360 / parts);
      deg = Double.valueOf(theta1 + ((theta2 - theta1) / 2));
    }
    return Math.toRadians((null != deg) ? deg.doubleValue() : 0);
  }

  /**
   * 
   */
  protected double getQuadrantRotation(double theta1, double theta2, int generation) {
    if (generation <= _rotationThreshold) {
      return 0;
    }
    int quadrant = getQuadrant(theta1, theta2);
    return quadrant * ((quadrant % 2 == 0) ? 1 : quadrant + 1) * Math.PI / 2.0;
  }

  /**
   * 
   */
  protected int getQuadrant(double theta1, double theta2) {
    int quadrant = 0;

    if (theta1 >= 0 && theta2 <= 90) {
      quadrant = 0;
    } else if (theta1 >= 90 && theta2 <= 180) {
      quadrant = 1;
    } else if (theta1 >= 0 && theta2 <= 180) {
      quadrant = 0;
    } else if (theta1 >= 180 && theta2 <= 270) {
      quadrant = 2;
    } else if (theta1 >= 270 && theta2 <= 360) {
      quadrant = 3;
    } else if (theta1 >= 180 && theta2 <= 360) {
      quadrant = 2;
    }

    return quadrant;
  }
}
