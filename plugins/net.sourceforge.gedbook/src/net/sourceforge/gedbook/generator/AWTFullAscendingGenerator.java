/*******************************************************************************
 * Copyright (c) 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.configuration.Configuration;

import net.sourceforge.gedbook.generator.arbin.ITree;
import net.sourceforge.gedbook.generator.arbin.Tree;
import net.sourceforge.gedbook.generator.arbin.TreePanel;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;

/**
 * @author Joao Barata
 */
public class AWTFullAscendingGenerator extends AbstractGenerator {

  private Person husband;
  private Person wife;

//  private static final int N = 40;

  /**
   * @param project
   * @param output
   * @param configuration
   * @param husband
   * @param wife
   */
  public AWTFullAscendingGenerator(Project project, String output, Configuration configuration, Person husband, Person wife) {
    super(project, output, configuration);
    this.husband = husband;
    this.wife = wife;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    ITree a = new Tree(null);
    //TreeFactory.randomTree(a, N, 0, 99);
    buildTree(a, husband, wife);

    // print it (debug)
    //a.inOrdre(new Stack<ITree>());

    Frame cadre = new Frame("Family Tree");
    cadre.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent evt) {
        System.exit(0);
      }
    });
    cadre.setSize(800, 450);
    //cadre.add(new TreePanel(a));
    JPanel container = new JPanel();
    container.add(new TreePanel(a));
    JScrollPane jsp = new JScrollPane(container);
    cadre.add(jsp);
    cadre.setVisible(true);  
  }

  protected void buildTree(ITree a, Person husband, Person wife) {
    if (husband != null) {
      a.createLeftChild(husband);
      buildTree(a.leftChild(), husband.getFather(), husband.getMother());
    }
    if (wife != null) {
      a.createRightChild(wife);
      buildTree(a.rightChild(), wife.getFather(), wife.getMother());
    }
  }
}
