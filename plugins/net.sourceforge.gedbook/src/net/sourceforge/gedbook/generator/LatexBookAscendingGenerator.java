/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.util.List;

import net.sourceforge.gedbook.model.MyCouples;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class LatexBookAscendingGenerator extends LatexBookGenerator {

  /**
   * @param project_p
   * @param output
   * @param conf_p
   * @param husbandId_p
   * @param wifeId_p
   */
  public LatexBookAscendingGenerator(Project project_p, String output, Configuration conf_p, String husbandId_p, String wifeId_p) {
    super(project_p, output, conf_p, husbandId_p, wifeId_p);
  }

  /**
	 *
	 */
  protected MyCouples getNextGeneration(List<Family> families_p) {
    MyCouples nextGeneration = new MyCouples();
    for (Family family : families_p) {
      if (null != family.getHusband()) {
        if (null != family.getHusband().getFather() || null != family.getHusband().getMother()) {
          nextGeneration.add(GedBookModelHelper.getFamily(family.getHusband().getFather(), family.getHusband().getMother()));
        }
      }
      if (null != family.getWife()) {
        if (null != family.getWife().getFather() || null != family.getWife().getMother()) {
          nextGeneration.add(GedBookModelHelper.getFamily(family.getWife().getFather(), family.getWife().getMother()));
        }
      }
    }
    return nextGeneration;
  }
}
