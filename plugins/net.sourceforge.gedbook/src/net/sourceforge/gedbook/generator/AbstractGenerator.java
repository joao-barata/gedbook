/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public abstract class AbstractGenerator {

  protected Project _project;
  protected Configuration _configurationFile;
  protected String _output;

  /**
   * @param project_p
   * @param output_p
   * @param familyLabel_p
   */
  public AbstractGenerator(Project project_p, String output_p, Configuration conf_p) {
    _project = project_p;
    _configurationFile = conf_p;
    _output = output_p;
  }

  public abstract void run();

  int roundInf(int value, int multiple) {
    while (!(value % multiple == 0)) {
      value--;
    }
    return value;
  }

  int roundSup(int value, int multiple) {
    while (!(value % multiple == 0)) {
      value++;
    }
    return value;
  }

  /**
	 * 
	 */
  protected String getYear(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy"); //$NON-NLS-1$
    return (null != date) ? formatter.format(date) : ""; //$NON-NLS-1$
  }

  /**
   * 
   */
  public static String getIdDate(DateWrapper date) {
    return (null != date) ? getIdDate(date.getDate()) : ""; //$NON-NLS-1$
  }

  /**
	 * 
	 */
  public static String getIdDate(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd"); //$NON-NLS-1$
    return (null != date) ? formatter.format(date) : ""; //$NON-NLS-1$
  }

  /**
 * 
 */
  protected String getVeryShortPlace(Event event_p) {
    if (null != event_p) {
      Place place = event_p.getPlace();
      if (null != place) {
        String commune = place.getCommune();

        return ((null != commune) ? commune : ""); //$NON-NLS-1$
      }
    }
    return ""; //$NON-NLS-1$
  }

  /**
   * 
   */
  protected String getPlace(Event event_p) {
    if (null != event_p) {
      Place place = event_p.getPlace();
      if (null != place) {
        String commune = place.getCommune();
        String departement = place.getDepartement();
        String region = place.getRegion();
        String pays = place.getPays();

        return ((null != commune) ? commune : "") //$NON-NLS-1$
            + ", " + ((null != departement) ? departement : "") //$NON-NLS-1$ //$NON-NLS-2$
            + ", " + ((null != region) ? region : "") //$NON-NLS-1$ //$NON-NLS-2$
            + ", " + ((null != pays) ? pays : ""); //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    return ""; //$NON-NLS-1$
  }

  /**
   * 
   */
  public static String getShortFirstName(Person person_p) {
    String fullName = ""; //$NON-NLS-1$
    if (null != person_p) {
      String firstName = person_p.getNames().getGivenname();
      if (null != firstName) {
        String[] names = firstName.split(" "); //$NON-NLS-1$
        for (int i = 0; i < names.length; i++) {
          if (i == 0) {
            fullName = names[i];
          } else {
            fullName += " " + names[i].charAt(0) + "."; //$NON-NLS-1$ //$NON-NLS-2$
          }
        }
      }
    } else {
      fullName = "<unknown>"; //$NON-NLS-1$
    }
    return fullName;
  }

  /**
   * 
   */
  protected File getFile(String filename) {
    File file = new File(filename);
    File parentDir = file.getParentFile();
    if (!parentDir.exists()) {
      parentDir.mkdirs();
    }
    return file;
  }

  /**
   * 
   */
  protected BufferedWriter getFileBuffer(String filename) {
    OutputStreamWriter writer = null;
    try {
      File file = getFile(filename);
      FileOutputStream stream = new FileOutputStream(file);
      writer = new OutputStreamWriter(stream, EncodingUtils.ENCODING_UTF8);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    if (null != writer) {
      return new BufferedWriter(writer);
    }
    return null;
  }
}
