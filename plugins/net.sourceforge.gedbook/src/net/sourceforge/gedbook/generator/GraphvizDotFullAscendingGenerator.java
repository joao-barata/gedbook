/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import net.sourceforge.gedbook.model.MyCouples;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class GraphvizDotFullAscendingGenerator extends AbstractGraphvizDotFullGenerator {

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param husband_p
   * @param wife_p
   */
  public GraphvizDotFullAscendingGenerator(Project project_p, String output_p, Configuration conf_p, Person husband_p, Person wife_p) {
    super(project_p, output_p, conf_p, husband_p, wife_p);
  }

  /**
	 *
	 */
  protected MyCouples getNextGeneration(List<Family> families_p) {
    MyCouples nextGeneration = new MyCouples();
    for (Family family : families_p) {
      if (null != family.getHusband()) {
        if (null != family.getHusband().getFather() || null != family.getHusband().getMother()) {
          nextGeneration.add(GedBookModelHelper.getFamily(family.getHusband().getFather(), family.getHusband().getMother()));
        }
      }
      if (null != family.getWife()) {
        if (null != family.getWife().getFather() || null != family.getWife().getMother()) {
          nextGeneration.add(GedBookModelHelper.getFamily(family.getWife().getFather(), family.getWife().getMother()));
        }
      }
    }
    return nextGeneration;
  }

  /**
     * 
     */
  protected void generateBook(BufferedWriter writer, List<MyCouples> families_p) throws IOException {
    writer.write("digraph G {" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    writer.write(GedBookModelHelper.TAB + "edge [dir=none];" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    writer.newLine();
    for (MyCouples couples : families_p) {
      for (Family couple : couples) {
        Person husband = couple.getHusband();
        Person wife = couple.getWife();
        writer.write(generatePersonNode(husband));
        writer.write(generatePersonNode(wife));
      }
    }
    writer.newLine();
    for (MyCouples couples : families_p) {
      for (Family couple : couples) {
        writer.write(generateFamilyNode(couple));
      }
    }
    writer.newLine();
    for (MyCouples couples : families_p) {
      if (!couples.isEmpty()) {
        writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
        for (Family couple : couples) {
          Person husband = couple.getHusband();
          Person wife = couple.getWife();
          writer.write((null != husband) ? husband.getId() + ";" : GedBookModelHelper.EMPTY); //$NON-NLS-1$
          writer.write((null != wife) ? wife.getId() + ";" : GedBookModelHelper.EMPTY); //$NON-NLS-1$
        }
        writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
        writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
        for (Family couple : couples) {
          writer.write(couple.getId().replaceAll("-", GedBookModelHelper.EMPTY) + ";"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      }
    }
    writer.newLine();
    for (MyCouples couples : families_p) {
      for (Family couple : couples) {
        Person husband = couple.getHusband();
        if (null != husband) {
          Person father1 = husband.getFather();
          Person mother1 = husband.getMother();
          Family family1 = GedBookModelHelper.getFamily(father1, mother1);
          generateLinks(writer, father1, mother1, family1, Collections.singletonList(husband));
        }
        Person wife = couple.getWife();
        if (null != wife) {
          Person father2 = wife.getFather();
          Person mother2 = wife.getMother();
          Family family2 = GedBookModelHelper.getFamily(father2, mother2);
          generateLinks(writer, father2, mother2, family2, Collections.singletonList(wife));
        }
        if (couples.size() == 1) {
          Family family3 = GedBookModelHelper.getFamily(husband, wife);
          generateLinks(writer, husband, wife, family3, Collections.EMPTY_LIST);
        }
      }
    }
    writer.write("}"); //$NON-NLS-1$
  }
}
