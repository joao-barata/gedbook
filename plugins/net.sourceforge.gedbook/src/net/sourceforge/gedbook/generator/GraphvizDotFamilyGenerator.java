/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;
import org.eclipse.emf.ecore.EObject;

/**
 * @author Joao Barata
 */
public class GraphvizDotFamilyGenerator extends AbstractGraphvizDotGenerator {

  private Person _husband;
  private Person _wife;
  private String _familyIdentifier;

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param family_p
   * @param familyIdentifier
   */
  public GraphvizDotFamilyGenerator(Project project_p, String output_p, Configuration conf_p, Family family_p, String familyIdentifier) {
    super(project_p, output_p, conf_p);
    _husband = family_p.getHusband();
    _wife = family_p.getWife();
    _familyIdentifier = familyIdentifier;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      String filepath = _output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE)
          + IConfigurationConstants.GENEALOGY_SUFFIX + "/graphs/" + _familyIdentifier;
      BufferedWriter writer = getFileBuffer(filepath + ".dot");
      generateBook(writer);
      writer.close();

      Runtime.getRuntime().exec("dot -T" + GedBookModelHelper.DOT_FORMAT + " " + filepath + ".dot -o " + filepath + "." + GedBookModelHelper.DOT_FORMAT).waitFor();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
     * 
     */
  private void generateBook(BufferedWriter writer) throws IOException {
    if (null != _husband && null != _wife) {
      Person father1 = _husband.getFather();
      Person father2 = _wife.getFather();
      Person mother1 = _husband.getMother();
      Person mother2 = _wife.getMother();
      Family family1 = GedBookModelHelper.getFamily(father1, mother1);
      Family family2 = GedBookModelHelper.getFamily(father2, mother2);
      Family family3 = GedBookModelHelper.getFamily(_husband, _wife);
      List<Person> children1 = GedBookModelHelper.getAllChildren(_project, father1, mother1);
      List<Person> children2 = GedBookModelHelper.getAllChildren(_project, father2, mother2);
      List<Person> children3 = GedBookModelHelper.getAllChildren(_project, _husband, _wife);
  
      writer.write("digraph G {" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      writer.write(GedBookModelHelper.TAB + "edge [dir=none];" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      writer.newLine();
      writer.write(generatePersonNode(father1));
      writer.write(generatePersonNode(mother1));
      writer.write(generatePersonNode(father2));
      writer.write(generatePersonNode(mother2));
      writer.write(generatePersonNodes(_husband, children1));
      writer.write(generatePersonNodes(_wife, children2));
      writer.write(generatePersonNodes(children3));
      writer.newLine();
      writer.write(generateFamilyNode(family1));
      writer.write(generateFamilyNode(family2));
      writer.write(generateFamilyNode(family3));
      writer.newLine();
      if (null != father1 || null != mother1 || null != father2 || null != mother2) {
        writer.write(GedBookModelHelper.TAB + "{rank=same;" //$NON-NLS-1$
            + ((null != father1) ? father1.getId() + ";" : "") //$NON-NLS-1$ //$NON-NLS-2$
            + ((null != mother1) ? mother1.getId() + ";" : "") //$NON-NLS-1$ //$NON-NLS-2$
            + ((null != father2) ? father2.getId() + ";" : "") //$NON-NLS-1$ //$NON-NLS-2$
            + ((null != mother2) ? mother2.getId() + ";" : "") + "};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      }
      if (null != family1 || null != family2) {
        writer.write(GedBookModelHelper.TAB + "{rank=same;" //$NON-NLS-1$
            + ((null != family1) ? family1.getId().replaceAll("-", "") + ";" : "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            + ((null != family2) ? family2.getId().replaceAll("-", "") + ";" : "") + "};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
      }
      writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
      if (!children1.isEmpty()) {
        for (EObject child : children1) {
          writer.write(((Person) child).getId() + ";"); //$NON-NLS-1$
        }
      } else {
        writer.write(_husband.getId() + ";"); //$NON-NLS-1$
      }
      if (!children2.isEmpty()) {
        for (EObject child : children2) {
          writer.write(((Person) child).getId() + ";"); //$NON-NLS-1$
        }
      } else {
        writer.write(_wife.getId());
      }
      writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      if (null != family3) {
        writer.write(GedBookModelHelper.TAB + "{rank=same;" + family3.getId().replaceAll("-", "") + "};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      }
      if (!children3.isEmpty()) {
        writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
        for (EObject child : children3) {
          writer.write(((Person) child).getId() + ";"); //$NON-NLS-1$
        }
        writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      }
      writer.newLine();
      generateLinks(writer, father1, mother1, family1, children1);
      generateLinks(writer, father2, mother2, family2, children2);
      generateLinks(writer, _husband, _wife, family3, children3);
      writer.write("}"); //$NON-NLS-1$
    }
  }
}
