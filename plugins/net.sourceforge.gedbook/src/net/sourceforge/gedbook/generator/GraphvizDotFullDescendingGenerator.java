/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.gedbook.model.MyCouples;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.apache.commons.configuration.Configuration;
import org.eclipse.emf.ecore.EObject;

/**
 * @author Joao Barata
 */
public class GraphvizDotFullDescendingGenerator extends AbstractGraphvizDotFullGenerator {

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param husband_p
   * @param wife_p
   */
  public GraphvizDotFullDescendingGenerator(Project project_p, String output_p, Configuration conf_p, Person husband_p, Person wife_p) {
    super(project_p, output_p, conf_p, husband_p, wife_p);
  }

  /**
	 *
	 */
  protected MyCouples getNextGeneration(List<Family> families_p) {
    MyCouples nextGeneration = new MyCouples();
    for (Family family : families_p) {
      if (null != family.getHusband() && null != family.getWife()) {
        for (Person child : family.getChildren()) {
          for (EObject obj : ResourceUtils.getReferencers(child, CorePackage.Literals.FAMILY__HUSBAND)) {
            if (!nextGeneration.contains(obj)) {
              nextGeneration.add((Family) obj);
            }
          }
          for (EObject obj : ResourceUtils.getReferencers(child, CorePackage.Literals.FAMILY__WIFE)) {
            if (!nextGeneration.contains(obj)) {
              nextGeneration.add((Family) obj);
            }
          }
        }
      }
    }
    return nextGeneration;
  }

  /**
     * 
     */
  protected void generateBook(BufferedWriter writer, List<MyCouples> generations) throws IOException {
    writer.write("digraph G {" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    writer.write(GedBookModelHelper.TAB + "edge [dir=none];" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    writer.newLine();
    List<Person> lastGenerationChildren = null;
    for (MyCouples families : generations) {
      List<Person> currentGenerationChildren = new ArrayList<Person>();
      for (Family family : families) {
        Person husband = family.getHusband();
        Person wife = family.getWife();
        writer.write(generatePersonNode(husband));
        writer.write(generatePersonNode(wife));
        currentGenerationChildren.addAll(family.getChildren());
      }
      if (lastGenerationChildren != null) {
        for (Person child : lastGenerationChildren) {
          if (!families.contains(child)) {
            writer.write(generatePersonNode(child));
          }
        }
      }
      lastGenerationChildren = currentGenerationChildren;
    }
    writer.newLine();
    for (MyCouples couples : generations) {
      for (Family couple : couples) {
        writer.write(generateFamilyNode(couple));
      }
    }
    writer.newLine();

    lastGenerationChildren = null;
    for (MyCouples families : generations) {
      if (!families.isEmpty()) {
        List<Person> currentGenerationChildren = new ArrayList<Person>();
        writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
        for (Family family : families) {
          Person husband = family.getHusband();
          Person wife = family.getWife();
          writer.write((null != husband) ? husband.getId() + ";" : ""); //$NON-NLS-1$ //$NON-NLS-2$
          writer.write((null != wife) ? wife.getId() + ";" : ""); //$NON-NLS-1$ //$NON-NLS-2$
          currentGenerationChildren.addAll(family.getChildren());
        }
        if (lastGenerationChildren != null) {
          for (Person child : lastGenerationChildren) {
            if (!families.contains(child)) {
              writer.write((null != child) ? child.getId() + ";" : ""); //$NON-NLS-1$ //$NON-NLS-2$
            }
          }
        }
        lastGenerationChildren = currentGenerationChildren;

        writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
        writer.write(GedBookModelHelper.TAB + "{rank=same;"); //$NON-NLS-1$
        for (Family couple : families) {
          writer.write(couple.getId().replaceAll("-", "") + ";"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        writer.write("};" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
      }
    }
    writer.newLine();
    for (MyCouples families : generations) {
      for (Family family : families) {
        generateLinks(writer, family.getHusband(), family.getWife(), family, family.getChildren());
      }
    }
    writer.write("}"); //$NON-NLS-1$
  }
}
