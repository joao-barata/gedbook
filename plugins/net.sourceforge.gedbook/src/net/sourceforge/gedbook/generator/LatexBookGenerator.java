/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sourceforge.gedbook.latex.generator.GedBookSectionGenerator;
import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.latex.generator.ISectionProvider;
import net.sourceforge.gedbook.model.MyCouples;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public abstract class LatexBookGenerator extends AbstractGenerator {

  private Family _couple;

  /**
   * @param project_p
   * @param output
   * @param conf_p
   * @param husbandId_p
   * @param wifeId_p
   */
  public LatexBookGenerator(Project project_p, String output, Configuration conf_p, String husbandId_p, String wifeId_p) {
    super(project_p, output, conf_p);
    _couple = GedBookModelHelper.getFamily(ProjectQueryHelper.getInstance().getExistingPerson(_project, husbandId_p), ProjectQueryHelper.getInstance().getExistingPerson(_project, wifeId_p));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    String familyLabel = _configurationFile.getString(IConfigurationConstants.FAMILY_COUPLE);
    try {
      BufferedWriter header = getFileBuffer(_output + "/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + ".tex"); //$NON-NLS-1$ //$NON-NLS-2$
      generateHeader(header, familyLabel);
      header.close();

      BufferedWriter externalAppendix = getFileBuffer(_output + "/" + familyLabel + IConfigurationConstants.APPENDIX_SUFFIX + ".tex"); //$NON-NLS-1$ //$NON-NLS-2$
      generateAppendix(externalAppendix, familyLabel);
      externalAppendix.close();

      BufferedWriter writer = getFileBuffer(_output + "/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/" + familyLabel + ".tex"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      BufferedWriter appendix = getFileBuffer(_output + "/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/annexe.tex"); //$NON-NLS-1$ //$NON-NLS-2$

      Set<Extract> appendixBuffer = new HashSet<Extract>();
      Set<Family> familyBuffer = new HashSet<Family>();

      generation(writer, appendix, appendixBuffer, familyBuffer, _configurationFile, Collections.singletonList(_couple), familyLabel, 1);

      writer.close();
      appendix.close();
      /* deletes appendix file if empty */
      File adx = getFile(_output + "/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/annexe.tex"); //$NON-NLS-1$ //$NON-NLS-2$
      if (adx.length() == 0) {
        adx.delete();
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      System.out.println("generation of \"" + familyLabel + "\" finished."); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
   *
   */
  public void generateHeader(BufferedWriter header, String familyLabel) throws IOException {
    header.append("\\newcommand{\\familycouple}{" + familyLabel + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethefirsttitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_FIRSTTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethebigtitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_BIGTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethesubtitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SUBTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheauthor}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_AUTHOR) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethelogo}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_LOGO) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheeditor}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_EDITOR) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethecopyright}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_COPYRIGHT) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\newcommand{\\givemetheappendixname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_APPENDIXNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethechaptername}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_CHAPTERNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethecontentsname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_CONTENTSNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethelistfigurename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_LISTFIGURENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethefigurename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_FIGURENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheglossaryname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_GLOSSARYNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheacronymname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_ACRONYMNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethebibname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_BIBNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethepublicationsname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PUBLICATIONSNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethepreambulename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PREAMBULENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheprefacename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PREFACENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\newcommand{\\givemethenameindextitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_NAMEINDEXTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheplaceindextitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PLACEINDEXTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemetheadressindextitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_ADRESSINDEXTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethejobindextitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_JOBINDEXTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\newcommand{\\givemethescreenshotannotationlabel}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SCREENSHOTANNOTATIONLABEL) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethescreenshotnotelabel}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SCREENSHOTNOTELABEL) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethescreenshotsignaturelabel}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SCREENSHOTSIGNATURELABEL) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\newcommand{\\givemethescreenshottranscriptionlabel}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SCREENSHOTTRANSCRIPTIONLABEL) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\newcommand{\\givemethetranscriptionoriginreferencelabel}[1]{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_TRANSCRIPTIONORIGINREFERENCELABEL) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\newcommand{\\givemethetimelinescale}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_TIMELINE_SCALE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.newLine();
    header.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedPreambule.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedBeginDocument.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\input{" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/" + familyLabel + ".tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    header.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedEndDocument.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
  }

  /**
   *
   */
  public void generateAppendix(BufferedWriter appendix, String familyLabel) throws IOException {
    appendix.append("\\newcommand{\\familycouple}{" + familyLabel + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethefirsttitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_FIRSTTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethebigtitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_BIGTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethesubtitle}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_APPENDIX_SUBTITLE) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemetheauthor}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_AUTHOR) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethelogo}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_LOGO) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemetheeditor}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_EDITOR) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethecopyright}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_COPYRIGHT) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\newcommand{\\givemetheappendixname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_APPENDIXNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethechaptername}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_CHAPTERNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethecontentsname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_CONTENTSNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethelistfigurename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_LISTFIGURENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethefigurename}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_FIGURENAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemetheglossaryname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_GLOSSARYNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemetheacronymname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_ACRONYMNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethebibname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_BIBNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemethepublicationsname}{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PUBLICATIONSNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\newcommand{\\givemethearchivepath}{" + System.getenv(IConfigurationConstants.GEDBOOK_ARCHIVE_REPOSITORY) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\newcommand{\\givemetheimagepath}{" + System.getenv(IConfigurationConstants.GEDBOOK_IMAGE_REPOSITORY) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedPreambule.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedBeginAppendixDocument.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\appendix" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\IfFileExists{" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/annexe.tex}{\\chapter{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_REGISTEREXTRACTNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\input{../manualfiles/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/annexe-intro.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\input{" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/annexe.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\cleardoublepage" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("}{}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\IfFileExists{../manualfiles/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/communes.tex}{\\chapter{" + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_PLACESCHAPTERNAME) + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\input{../manualfiles/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/communes.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("\\cleardoublepage" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.append("}{}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
    appendix.newLine();
    appendix.append("\\input{" + System.getenv(IConfigurationConstants.GEDBOOK_HOME) + "/src/latex/sharedfiles/sharedEndAppendixDocument.tex}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
  }

  /**
	 *
	 */
  public void generation(BufferedWriter writer, BufferedWriter appendix, Set<Extract> appendixBuffer, Set<Family> familyBuffer, Configuration conf, List<Family> families_p, String familyLabel, int generationCounter) throws IOException {

    if (!families_p.isEmpty()) {
      String language = conf.getString(IConfigurationConstants.DOCUMENT_LANGUAGE);
      ISectionProvider provider = GedBookSectionGenerator.getInstance().getSectionProviders().get(language);
      writer.append("\\chapter{"
          + generationCounter + "\\textsuperscript{"
          + (provider != null ? provider.getOrdinalIndicator(generationCounter, SexKind.FEMALE) : GedBookModelHelper.EMPTY)
          + "} " + _configurationFile.getProperty(IConfigurationConstants.DOCUMENT_CHAPTERSUFFIX) + "}"
          + GedBookModelHelper.NEWLINE);
      writer.append("\\InputIfFileExists{" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/circles/generation_" + generationCounter + ".tex}{}{}" + GedBookModelHelper.NEWLINE);
      writer.newLine();
    }

    int index = 0;
    for (Family family : families_p) {
      index++;
      String familyIdentifier = getFamilyIdentifier(family, generationCounter, index);
      writer.append("\\input{" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/sections/" + familyIdentifier + ".tex}" + GedBookModelHelper.NEWLINE);
      writer.append("\\InputIfFileExists{../manualfiles/" + familyLabel + IConfigurationConstants.GENEALOGY_SUFFIX + "/sections/" + family.getId() + ".tex}{}{}" + GedBookModelHelper.NEWLINE);

      BufferedWriter section = getFileBuffer(_output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/sections/" + familyIdentifier + ".tex");
      LatexSectionGenerator sectionGenerator = new LatexSectionGenerator(_project, _output, _configurationFile, family, section, appendix, appendixBuffer, familyBuffer, familyIdentifier);
      sectionGenerator.run();
      section.close();

      TimelineGenerator timelineGenerator = new TimelineGenerator(_project, _output, _configurationFile, family);
      timelineGenerator.run();
    }
    writer.newLine();

    /** go next generation */
    MyCouples nextGeneration = getNextGeneration(families_p);
    if (!nextGeneration.isEmpty()) {
      generation(writer, appendix, appendixBuffer, familyBuffer, conf, nextGeneration, familyLabel, generationCounter + 1);
    }
  }

  private String getFamilyIdentifier(Family family, int generation, int index) {
    return generation + "_" + index + "_" + family.getId();
  }

  /**
	 *
	 */
  protected abstract MyCouples getNextGeneration(List<Family> families_p);
}
