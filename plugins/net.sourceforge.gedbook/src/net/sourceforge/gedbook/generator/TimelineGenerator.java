/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.Configuration;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelComparators;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

/**
 * @author Joao Barata
 */
public class TimelineGenerator extends AbstractGenerator {

  private Person _husband;
  private Person _wife;
  private Family _family;

  /**
   * @param project_p
   * @param output_p
   * @param conf_p
   * @param husband_p
   * @param wife_p
   */
  public TimelineGenerator(Project project_p, String output_p, Configuration conf_p, Family family_p) {
    super(project_p, output_p, conf_p);
    _husband = family_p.getHusband();
    _wife = family_p.getWife();
    _family = family_p;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    try {
      BufferedWriter writer = getFileBuffer(_output + "/" + _configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) //$NON-NLS-1$
          + IConfigurationConstants.GENEALOGY_SUFFIX + "/timelines/" + _family.getId() + ".tex"); //$NON-NLS-1$ //$NON-NLS-2$
      generateTimeline(writer);
      writer.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  private void generateTimeline(BufferedWriter writer) throws IOException {
    if (null != _husband && null != _wife) {
      Birth husbandBirth = _husband.getBirth();
      Death husbandDeath = _husband.getDeath();
      Birth wifeBirth = _wife.getBirth();
      Death wifeDeath = _wife.getDeath();
      if (null != husbandBirth && null != husbandDeath && null != wifeBirth && null != wifeDeath) {
        DateWrapper husbandBirthD = husbandBirth.getDate();
        DateWrapper husbandDeathD = husbandDeath.getDate();
        DateWrapper wifeBirthD = wifeBirth.getDate();
        DateWrapper wifeDeathD = wifeDeath.getDate();
        if (null != husbandBirthD && null != husbandDeathD && null != wifeBirthD && null != wifeDeathD) {
          Date husbandBirthDate = husbandBirthD.getDate();
          Date husbandDeathDate = husbandDeathD.getDate();
          Date wifeBirthDate = wifeBirthD.getDate();
          Date wifeDeathDate = wifeDeathD.getDate();
          if (null != husbandBirthDate && null != husbandDeathDate && null != wifeBirthDate && null != wifeDeathDate) {
            List<Family> husbandPreFamilies = GedBookModelHelper.getAllPreviousFamilies(_husband, _family, GedBookModelComparators.marriageDateComparator);
            List<Family> husbandPostFamilies = GedBookModelHelper.getAllNextFamilies(_husband, _family, GedBookModelComparators.marriageDateComparator);
            List<Family> wifePreFamilies = GedBookModelHelper.getAllPreviousFamilies(_wife, _family, GedBookModelComparators.marriageDateComparator);
            List<Family> wifePostFamilies = GedBookModelHelper.getAllNextFamilies(_wife, _family, GedBookModelComparators.marriageDateComparator);
            List<Person> children = GedBookModelHelper.getAllChildren(_project, _husband, _wife);
            int minDate = roundInf(Math.min(Integer.valueOf(getYear(husbandBirthDate)), Integer.valueOf(getYear(wifeBirthDate))), 10);
            int maxDate = roundSup(Math.max(Integer.valueOf(getYear(husbandDeathDate)), Integer.valueOf(getYear(wifeDeathDate))), 10);

            writer.write("\\vspace*{3mm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write("\\hspace*{\\fill}\\scalebox{\\givemethetimelinescale}{" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\noindent\\begin{tikzpicture}[x=1mm,y=1mm]" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\xshift}{0mm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\myshift}{1cm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\constLifeSpanHeight}{4mm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetmacro{\\constStartYear}{" + minDate + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetmacro{\\constStopYear}{" + maxDate + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "%% Scale: points in one year" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\constMminYear}{.9\\textwidth/(\\constStopYear-\\constStartYear)}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "%% Grid" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\foreach \\i in {" + minDate + "," + (minDate + 10) + ",...," + maxDate + "} {" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            if (children.size() <= 4) {
              writer.write(GedBookModelHelper.TAB + GedBookModelHelper.TAB + "\\drawVertTics{\\i}{.6cm}{5.5cm}{0}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            } else {
              writer.write(GedBookModelHelper.TAB + GedBookModelHelper.TAB + "\\drawVertTics{\\i}{-.5cm}{6.5cm}{-10}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            }
            writer.write(GedBookModelHelper.TAB + "}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "%% husband life span" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\begin{scope}[yshift=\\myshift" + ((children.size() > 4) ? "-40" : "-10") + "]" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            writer.write(GedBookModelHelper.TAB + "\\putLifeSpan{" + getYear(husbandBirthDate) + "}{" + getYear(husbandDeathDate) + "}{" + EncodingUtils.latexEncoding(_husband.getNames().getGivenname()) + "}{lifespancolor}{}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            writer.write(GedBookModelHelper.TAB + "\\end{scope}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\updateshift" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            for (FamilyEvent evt : _family.getEvents()) {
              if (evt instanceof Marriage && null != evt.getDate()) {
                Date marriageDate = evt.getDate().getDate();
                if (null != marriageDate) {
                  int endMarriageDate = Math.min(Integer.valueOf(getYear(husbandDeathDate)), Integer.valueOf(getYear(wifeDeathDate)));
                  writer.write(GedBookModelHelper.TAB + "%% marriage span and children events" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                  writer.write(GedBookModelHelper.TAB + "\\begin{scope}[yshift=\\myshift - .2cm]" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                  writer.write(GedBookModelHelper.TAB + "\\putMarriageSpan{" + getYear(marriageDate) + "}{" + endMarriageDate + "}{$\\infty$}{marriagespancolor}{}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                  writer.write(GedBookModelHelper.TAB + "\\end{scope}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                }
              }
            }
            writer.write(GedBookModelHelper.TAB + "\\begin{scope}[yshift=\\myshift]" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            int counter = 0;
            for (Person child : children) {
              Birth childBirth = child.getBirth();
              if (null != childBirth && null != childBirth.getDate()) {
                Date childBirthDate = childBirth.getDate().getDate();
                if (null != childBirthDate) {
                  if (children.size() <= 4) {
                    if (children.size() <= 2) {
                      writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\xshift}{" //$NON-NLS-1$
                          + ((counter == 0) ? "-.5" : (counter == 1) ? ".5" : "0") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                          + "cm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                    } else {
                      writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\xshift}{" //$NON-NLS-1$
                          + ((counter == 0 || counter == 1) ? "-1" : (counter == 2 || counter == 3) ? "1" : "0") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                          + "cm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                    }
                    writer.write(GedBookModelHelper.TAB + "\\putHEvent[" //$NON-NLS-1$
                        + ((counter == 0) ? ".75" : (counter == 1) ? "-.75" : (counter == 2) ? "1.25" : (counter == 3) ? "-1.25" : "0") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
                        + "]{" + getYear(childBirthDate) + "}{" + getYear(childBirthDate) + "}{" + EncodingUtils.latexEncoding(getShortFirstName(child)) + "}{eventpointcolor}{}{-1}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
                  } else {
                    writer.write(GedBookModelHelper.TAB + "\\pgfmathsetlengthmacro{\\xshift}{.9cm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
                    writer.write(GedBookModelHelper.TAB + "\\putVEvent" //$NON-NLS-1$
                        + (((counter % 2) == 0) ? "[1.5]" : "[-1.5]") //$NON-NLS-1$ //$NON-NLS-2$
                        + "{" + getYear(childBirthDate) //$NON-NLS-1$
                        + "}{" + getYear(childBirthDate) //$NON-NLS-1$
                        + "}{" + EncodingUtils.latexEncoding(getShortFirstName(child)) //$NON-NLS-1$
                        + "}{eventpointcolor}{}{100}" //$NON-NLS-1$
                        + (((counter % 2) == 0) ? "{1}" : "{-1}") //$NON-NLS-1$ //$NON-NLS-2$
                        + GedBookModelHelper.NEWLINE);
                  }
                  counter++;
                }
              }
            }
            writer.write(GedBookModelHelper.TAB + "\\end{scope}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\updateshift" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "%% wife life span" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write(GedBookModelHelper.TAB + "\\begin{scope}[yshift=\\myshift" + ((children.size() > 4) ? "+30" : "") + "]" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            writer.write(GedBookModelHelper.TAB + "\\putLifeSpan{" + getYear(wifeBirthDate) + "}{" + getYear(wifeDeathDate) + "}{" + EncodingUtils.latexEncoding(_wife.getNames().getGivenname()) + "}{lifespancolor}{}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            writer.write(GedBookModelHelper.TAB + "\\end{scope}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.newLine();
            writer.write(GedBookModelHelper.TAB + "\\end{tikzpicture}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write("}\\hspace*{\\fill}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
            writer.write("\\vspace*{5mm}" + GedBookModelHelper.NEWLINE); //$NON-NLS-1$
          }
        }
      }
    }
  }
}
