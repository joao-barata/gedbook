/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.checker;

import java.util.List;

import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Source;
import net.sourceforge.gedbook.model.core.SourceCitation;

/**
 * @author Joao Barata
 */
public class CheckEventsWithOnlyTDRelatedSource extends AbstractChecker {

  /**
   *
   */
  private static final String MESSAGE_PATTERN_EVENT1 = "Events with only TD related source [%d]"; //$NON-NLS-1$

  /**
   *
   */
  public CheckEventsWithOnlyTDRelatedSource(Project project, String husband, String wife) {
    super(MESSAGE_PATTERN_EVENT1, project, husband, wife);
  }

  /**
   *
   */
  @Override
  public Event checkEvent(Event event_p) {
    if (shallHaveTDSource(event_p) && hasTDSource(event_p)) {
      List<SourceCitation> sources = event_p.getCitations();
      if ((null != sources) && (sources.size() == 1)) {
        return event_p;
      }
    }
    return null;
  }

  /**
   *
   */
  @Override
  public Source checkSource(Source source) {
    return null;
  }
}
