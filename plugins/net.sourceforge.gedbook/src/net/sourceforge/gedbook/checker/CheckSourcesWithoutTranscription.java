/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.checker;

import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Source;
import net.sourceforge.gedbook.model.data.ExtendedData;

/**
 * @author Joao Barata
 */
public class CheckSourcesWithoutTranscription extends AbstractChecker {

  /**
   * 
   */
  private static final String MESSAGE_PATTERN_EVENT = "Sources without transcription [%d]"; //$NON-NLS-1$

  /**
   * 
   */
  public CheckSourcesWithoutTranscription(ExtendedData extendedData, String husband, String wife) {
    super(MESSAGE_PATTERN_EVENT, extendedData);
  }

  /**
   * 
   */
  @Override
  public Event checkEvent(Event source) {
    return null;
  }

  /**
   * 
   */
  @Override
  public Source checkSource(Source source) {
    return null;
  }
}
