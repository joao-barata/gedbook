/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.checker;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.PersonPkg;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Source;

import org.eclipse.emf.ecore.EObject;

/**
 * @author Joao Barata
 */
public class CheckPersonsWithoutProfession extends AbstractChecker {

  /**
   * 
   */
  private static final String MESSAGE_PATTERN_EVENT = "Persons without profession [%d]"; //$NON-NLS-1$

  /**
   * 
   */
  public CheckPersonsWithoutProfession(Project project, String husband, String wife) {
    super(MESSAGE_PATTERN_EVENT, project, husband, wife);
  }

  /**
   * 
   */
  @Override
  public void check(EObject object) {
    if (object instanceof Project) {
      Project project = (Project) object;
      PersonPkg pkg = project.getOwnedPersonPkg();
      if (null != pkg) {
        List<Person> persons = new ArrayList<Person>();
        for (Person person : pkg.getOwnedPersons()) {
          if (isFamilyMember(person) && !checkProfession(person)) {
            persons.add(person);
          }
        }
        printPersons(persons, MESSAGE_PATTERN_EVENT);
      }
    }
  }

  /**
   * 
   */
  public boolean checkProfession(Person person) {
    List<Occupation> occupations = person.getOccupations();
    return !occupations.isEmpty();
  }

  /**
   * 
   */
  @Override
  public Event checkEvent(Event source) {
    return null;
  }

  /**
   * 
   */
  @Override
  public Source checkSource(Source source) {
    return null;
  }
}
