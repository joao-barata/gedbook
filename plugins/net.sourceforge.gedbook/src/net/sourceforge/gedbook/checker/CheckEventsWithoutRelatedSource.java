/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.checker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Source;
import net.sourceforge.gedbook.model.core.SourceCitation;

/**
 * @author Joao Barata
 */
public class CheckEventsWithoutRelatedSource extends AbstractChecker {

  /**
   *
   */
  private static final String MESSAGE_PATTERN_EVENT0 = "Events without related source [%d]"; //$NON-NLS-1$
  private Date _refDate;

  /**
   *
   */
  public CheckEventsWithoutRelatedSource(Project project, String husband, String wife) {
    super(MESSAGE_PATTERN_EVENT0, project, husband, wife);
    try {
      SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT1);
      _refDate = df.parse(TD_FIRST_DATE_REF);
    } catch (ParseException exception_p) {
      //
    }
  }

  /**
   *
   */
  @Override
  public Event checkEvent(Event event_p) {
    List<SourceCitation> sources = event_p.getCitations();
    if ((null == sources) || sources.isEmpty()) {
      DateWrapper dw = event_p.getDate();
      if (null != dw) {
        Date d = dw.getDate();
        if ((null == d) || d.after(_refDate)) {
          return null;
        }
        return event_p;
      }
    }
    return null;
  }

  /**
   *
   */
  @Override
  public Source checkSource(Source source) {
    return null;
  }
}
