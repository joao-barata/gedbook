/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.checker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import net.sourceforge.gedbook.model.Couple;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Banns;
import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Burial;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.EventPkg;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.PersonEvent;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.Source;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.core.Will;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.data.FileGroup;
import net.sourceforge.gedbook.model.data.SourceFile;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.eclipse.emf.ecore.EObject;
import org.familysearch.gedcomparser.GedcomTag;

/**
 * @author Joao Barata
 */
public abstract class AbstractChecker {

  /**
   *
   */
  protected String _titlePattern;
  private List<Person> _familyMembers;

  /**
   *
   */
  protected static final String MESSAGE_PATTERN_EVENT2 = "[%s][%s][%s][%s][%s]"; //$NON-NLS-1$
  protected static final String DATE_REF = "01/01/1905"; //$NON-NLS-1$
  protected static final String DATE_FORMAT0 = "yyyy/MM/dd"; //$NON-NLS-1$
  protected static final String DATE_FORMAT1 = "dd/MM/yyyy"; //$NON-NLS-1$
  protected static final String TD_FIRST_DATE_REF = "01/01/1793"; //$NON-NLS-1$
  protected static final String TD_LAST_DATE_REF = "31/12/1911"; //$NON-NLS-1$

  /**
   *
   */
  protected AbstractChecker(String titlePattern, ExtendedData extendedData) {
    _titlePattern = titlePattern;
    _familyMembers = new ArrayList<Person>();
  }

  /**
   *
   */
  protected AbstractChecker(String titlePattern, Project project, String husband, String wife) {
    _titlePattern = titlePattern;
    _familyMembers = new ArrayList<Person>();

    if (null != husband && null != wife) {
      for (Couple couple : getAllGenerations(Collections.singletonList(new Couple(ProjectQueryHelper.getInstance().getExistingPerson(project, husband), ProjectQueryHelper.getInstance()
          .getExistingPerson(project, wife))))) {
        if (null != couple._husband && !_familyMembers.contains(couple._husband)) {
          _familyMembers.add(couple._husband);
        }
        if (null != couple._wife && !_familyMembers.contains(couple._wife)) {
          _familyMembers.add(couple._wife);
        }
        if (null != couple._husband && null != couple._wife) {
          for (Person child : GedBookModelHelper.getAllChildren(project, couple._husband, couple._wife)) {
            if (!_familyMembers.contains(child)) {
              _familyMembers.add(child);
            }
          }
        }
      }
    }
  }

  /**
   *
   */
  protected List<Couple> getAllGenerations(List<Couple> families) {
    List<Couple> familyCouples = new ArrayList<Couple>();

    familyCouples.addAll(families);

    /** go next generations */
    List<Couple> nextGenerations = getNextGeneration(families);
    while (!nextGenerations.isEmpty()) {
      familyCouples.addAll(nextGenerations);
      nextGenerations = getNextGeneration(nextGenerations);
    }

    return familyCouples;
  }

  /**
   *
   */
  protected List<Couple> getNextGeneration(List<Couple> families) {
    List<Couple> nextGeneration = new ArrayList<Couple>();
    for (Couple family : families) {
      if (null != family._husband) {
        if (null != family._husband.getFather() || null != family._husband.getMother()) {
          nextGeneration.add(new Couple(family._husband.getFather(), family._husband.getMother()));
        }
      }
      if (null != family._wife) {
        if (null != family._wife.getFather() || null != family._wife.getMother()) {
          nextGeneration.add(new Couple(family._wife.getFather(), family._wife.getMother()));
        }
      }
    }
    return nextGeneration;
  }

  /**
   *
   */
  public void check(EObject object) {
    if (object instanceof Project) {
      Project project = (Project) object;
      EventPkg pkg = project.getOwnedEventPkg();
      if (null != pkg) {
        List<Event> evts = new ArrayList<Event>();
        for (Event event : pkg.getOwnedEvents()) {
          if (!isException(event) && isFamilyMember(event) && checkEvent(event) != null) {
            evts.add(event);
          }
        }
        Collections.sort(evts, new Comparator<Event>() {
          @Override
          public int compare(Event evt1, Event evt2) {
            Date d1 = evt1.getDate().getDate();
            Date d2 = evt2.getDate().getDate();
            return (null != d1 && null != d2) ? d1.compareTo(d2) : -1;
          }
        });
        printEvents(evts, _titlePattern);
      }
    } else if (object instanceof ExtendedData) {
      ExtendedData extendedData = (ExtendedData) object;
      List<Source> srcs = new ArrayList<Source>();
      for (FileGroup grp : extendedData.getGroups()) {
        for (SourceFile file : grp.getFiles()) {
          //
        }
      }
    }
  }

  /**
   *
   */
  abstract public Event checkEvent(Event event);

  /**
   *
   */
  abstract public Source checkSource(Source source);

  /**
   *
   */
  private boolean isException(Event event) {
    return !ResourceUtils.getReferencers(event, DataPackage.Literals.EXCLUSION__EXCLUDED_EVENTS).isEmpty();
  }

  /**
   *
   */
  protected boolean isFamilyMember(Person person) {
    if (null != _familyMembers && !_familyMembers.isEmpty()) {
      return _familyMembers.contains(person);
    }
    return true;
  }

  /**
   *
   */
  protected boolean isFamilyMember(Event event) {
    if (event instanceof PersonEvent) {
      Person person = ((PersonEvent) event).getPerson();
      return isFamilyMember(person);
    } else if (event instanceof FamilyEvent) {
      Family family = ((FamilyEvent) event).getFamily();
      if (null != family) {
        Person husband = ((FamilyEvent) event).getFamily().getHusband();
        Person wife = ((FamilyEvent) event).getFamily().getWife();
        return isFamilyMember(husband) || isFamilyMember(wife);
      }
      return false;
    }
    return true;
  }

  /**
   *
   */
  protected void printPersons(List<Person> persons, String titlePattern) {
    System.out.println(String.format(titlePattern, Integer.valueOf(persons.size())));
    System.out.println();
    for (Person person : persons) {
      String fullName = GedBookModelHelper.getFullName(person);
      System.out.println(String.format("[%s]", fullName));
    }
    System.out.println();
  }

  /**
   *
   */
  protected void printEvents(List<Event> events, String titlePattern) {
    System.out.println(String.format(titlePattern, Integer.valueOf(events.size())));
    System.out.println();
    for (Event event : events) {
      Date evtDate = event.getDate().getDate();
      SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT0);
      String date = (null != evtDate) ? formatter.format(evtDate) : ""; //$NON-NLS-1$
      String tag = getTag(event);
      String suffix = getSuffix(event);
      String commune = getCommune(event);
      String departement = getDepartement(event);
      System.out.println(String.format(MESSAGE_PATTERN_EVENT2, date, tag, commune, departement, suffix));
    }
    System.out.println();
  }

  /**
   *
   */
  protected boolean hasTDSource(Event event) {
    List<SourceCitation> sources = event.getCitations();
    if (null != sources) {
      for (SourceCitation source : sources) {
        String abbreviation = source.getSource().getAbbreviation();
        if (abbreviation != null && Arrays.asList(abbreviation.split("/")).stream().anyMatch(a -> a.startsWith("TD"))) { //$NON-NLS-1$ //$NON-NLS-2$
          return true;
        }
      }
    }
    return false;
  }

  /**
   *
   */
  protected boolean hasBMSSource(Event event) {
    List<SourceCitation> sources = event.getCitations();
    if (null != sources) {
      for (SourceCitation source : sources) {
        String abbreviation = source.getSource().getAbbreviation();
        if (abbreviation != null && Arrays.asList(abbreviation.split("/")).stream().anyMatch(a -> a.startsWith("BMS"))) { //$NON-NLS-1$ //$NON-NLS-2$
          return true;
        }
      }
    }
    return false;
  }

  /**
   *
   */
  protected boolean shallHaveTDSource(Event event) {
    if (event instanceof Birth || event instanceof Death || event instanceof Marriage) {
      Date tdFirstRefDate = null;
      Date tdLastRefDate = null;
      try {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT1);
        tdFirstRefDate = df.parse(TD_FIRST_DATE_REF);
        tdLastRefDate = df.parse(TD_LAST_DATE_REF);
      } catch (ParseException exception) {
        //
      }
      DateWrapper dw = event.getDate();
      if (null != dw) {
        Date d = dw.getDate();
        if ((null != d) && d.after(tdFirstRefDate) && d.before(tdLastRefDate)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   *
   */
  protected String getCommune(Event event) {
    if (null != event) {
      Place place = event.getPlace();
      if (null != place) {
        String commune = place.getCommune();
        return (null != commune) ? commune : ""; //$NON-NLS-1$
      }
    }
    return ""; //$NON-NLS-1$
  }

  /**
   *
   */
  protected String getDepartement(Event event) {
    if (null != event) {
      Place place = event.getPlace();
      if (null != place) {
        String departement = place.getDepartement();
        return (null != departement) ? departement : ""; //$NON-NLS-1$
      }
    }
    return ""; //$NON-NLS-1$
  }

  /**
   *
   */
  protected String getPays(Event event) {
    if (null != event) {
      Place place = event.getPlace();
      if (null != place) {
        String pays = place.getPays();
        return (null != pays) ? pays : ""; //$NON-NLS-1$
      }
    }
    return ""; //$NON-NLS-1$
  }

  /**
   *
   */
  protected boolean isBatismEvent(Event event) {
    return event instanceof Batism;
  }

  /**
   *
   */
  protected boolean isBurialEvent(Event event) {
    return event instanceof Burial;
  }

  /**
   *
   */
  private String getSuffix(Event event) {
    if (event instanceof PersonEvent) {
      return GedBookModelHelper.getFullName(((PersonEvent) event).getPerson());
    }
    if (event instanceof FamilyEvent) {
      String suffix = GedBookModelHelper.getFullName(((FamilyEvent) event).getFamily().getHusband());
      suffix += " & "; //$NON-NLS-1$
      suffix += GedBookModelHelper.getFullName(((FamilyEvent) event).getFamily().getWife());
      return suffix;
    }
    return "";
  }

  /**
   *
   */
  private String getTag(Event event) {
    String tag = ""; //$NON-NLS-1$
    if (event instanceof Birth) {
      tag = GedcomTag.BIRT.getName();
    } else if (event instanceof Batism) {
      tag = GedcomTag.BAPM.getName();
    } else if (event instanceof Marriage) {
      tag = GedcomTag.MARR.getName();
    } else if (event instanceof Banns) {
      tag = GedcomTag.MARB.getName();
    } else if (event instanceof Death) {
      tag = GedcomTag.DEAT.getName();
    } else if (event instanceof Burial) {
      tag = GedcomTag.BURI.getName();
    } else if (event instanceof Will) {
      tag = GedcomTag.WILL.getName();
    }
    return tag;
  }
}
