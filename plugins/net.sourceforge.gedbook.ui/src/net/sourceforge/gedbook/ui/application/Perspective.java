/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.application;

import net.sourceforge.gedbook.ui.views.navigator.GedBookCommonNavigator;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * @author Joao Barata
 */
public class Perspective implements IPerspectiveFactory {

	/**
	 * The ID of the perspective as specified in the extension.
	 */
	public static final String ID = "net.sourceforge.gedbook.ui.perspective"; //$NON-NLS-1$

	public void createInitialLayout(IPageLayout layout) {
    // Get the editor area.
    String editorArea = layout.getEditorArea();

    // Top left: Navigation view
    IFolderLayout topLeft = layout.createFolder("topLeft", IPageLayout.LEFT, 0.5f, editorArea); //$NON-NLS-1$
    topLeft.addView(GedBookCommonNavigator.ID);

    // Bottom area: Properties view
    IFolderLayout bottom = layout.createFolder("bottom", IPageLayout.BOTTOM, (IPageLayout.DEFAULT_VIEW_RATIO / 0.7f), IPageLayout.ID_EDITOR_AREA);
    bottom.addView(IPageLayout.ID_PROP_SHEET);
	}
}
