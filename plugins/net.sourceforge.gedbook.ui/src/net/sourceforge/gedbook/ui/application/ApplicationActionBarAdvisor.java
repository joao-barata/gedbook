/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.application;

import net.sourceforge.gedbook.ui.Messages;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of
 * the actions added to a workbench window. Each window will be populated with
 * new actions.
 * 
 * @author Joao Barata
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
  
  private IWorkbenchAction fileNewAction;
  private IWorkbenchAction fileSaveAction;
  private IWorkbenchAction fileSaveAllAction;
  private IWorkbenchAction fileImportAction;
  private IWorkbenchAction fileExitAction;

  private IWorkbenchAction editCutAction;
  private IWorkbenchAction editCopyAction;
  private IWorkbenchAction editPasteAction;
  private IWorkbenchAction editDeleteAction;
  private IWorkbenchAction editUndoAction;
  private IWorkbenchAction editRedoAction;

  private IWorkbenchAction helpAboutAction;

  public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
    super(configurer);
  }

  protected void makeActions(final IWorkbenchWindow window) {
    // Creates the actions and registers them.
    // Registering is needed to ensure that key bindings work.
    // The corresponding commands keybindings are defined in the plugin.xml
    // file.
    // Registering also provides automatic disposal of the actions when the
    // window is closed.

    fileNewAction = ActionFactory.NEW.create(window);
    register(fileNewAction);
    fileSaveAction = ActionFactory.SAVE.create(window);
    register(fileSaveAction);
    fileSaveAllAction = ActionFactory.SAVE_ALL.create(window);
    register(fileSaveAllAction);
    fileImportAction = ActionFactory.IMPORT.create(window);
    register(fileImportAction);
    fileExitAction = ActionFactory.QUIT.create(window);
    register(fileExitAction);

    editUndoAction = ActionFactory.UNDO.create(window);
    register(editUndoAction);
    editRedoAction = ActionFactory.REDO.create(window);
    register(editRedoAction);
    editCutAction = ActionFactory.CUT.create(window);
    register(editCutAction);
    editCopyAction = ActionFactory.COPY.create(window);
    register(editCopyAction);
    editPasteAction = ActionFactory.PASTE.create(window);
    register(editPasteAction);
    editDeleteAction = ActionFactory.DELETE.create(window);
    register(editDeleteAction);
    
    helpAboutAction = ActionFactory.ABOUT.create(window);
    register(helpAboutAction);
  }

  protected void fillMenuBar(IMenuManager menuBar) {
    MenuManager fileMenu = new MenuManager(Messages.getString("GedBookMenuBar.Submenu.File"), IWorkbenchActionConstants.M_FILE); //$NON-NLS-1$
    IMenuManager editMenu= new MenuManager(Messages.getString("GedBookMenuBar.Submenu.Edit"), IWorkbenchActionConstants.M_EDIT); //$NON-NLS-1$
    MenuManager helpMenu = new MenuManager(Messages.getString("GedBookMenuBar.Submenu.Help"), IWorkbenchActionConstants.M_HELP); //$NON-NLS-1$

    menuBar.add(fileMenu);
    menuBar.add(editMenu);
    menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
    menuBar.add(helpMenu);

    // File
    fileMenu.add(fileNewAction);
    fileMenu.add(new Separator());
    fileMenu.add(fileSaveAction);
    fileMenu.add(fileSaveAllAction);
    fileMenu.add(new Separator());
    fileMenu.add(fileImportAction);
    fileMenu.add(new Separator());
    fileMenu.add(fileExitAction);

    // Edit
    editMenu.add(editUndoAction);
    editMenu.add(editRedoAction);
    editMenu.add(new Separator());
    editMenu.add(editCutAction);
    editMenu.add(editCopyAction);
    editMenu.add(editPasteAction);
    editMenu.add(new Separator());
    editMenu.add(editDeleteAction);

    // Help
    helpMenu.add(helpAboutAction);
  }

  protected void fillCoolBar(ICoolBarManager coolBar) {
    // do nothing
  }
}
