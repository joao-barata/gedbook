/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views.navigator;

import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.IDecorationContext;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.LabelDecorator;
import org.eclipse.swt.graphics.Image;

/**
 * @author Joao Barata
 */
public class GedBookResourceLabelProvider extends LabelDecorator 
{

	public Image decorateImage(Image image, Object element) {
	  return image;
	}

	public String decorateText(String text, Object element) {
	  if (element instanceof IFile) {
      if (((IFile) element).getFileExtension().equals("gedbook")) {
        String path = ((IFile) element).getFullPath().toString();
        URI uri = URI.createPlatformResourceURI(path, true);
        Resource resource = GedBookEditingDomainProvider.getEditingDomain().getResourceSet().getResource(uri, true);
        resource.isModified();

        TransactionalEditingDomain ted = (TransactionalEditingDomain) GedBookEditingDomainProvider.getEditingDomain();
        BasicCommandStack stack = (BasicCommandStack) ted.getCommandStack();
        
        if (stack.isSaveNeeded()) {
          return text + " *"; //$NON-NLS-1$
        }
      }
    }
		return text;
	}

  @Override
  public void addListener(ILabelProviderListener listener) {
  }

  @Override
  public void dispose() {
  }

  @Override
  public boolean isLabelProperty(Object element, String property) {
    return false;
  }

  @Override
  public void removeListener(ILabelProviderListener listener) {
  }

  @Override
  public Image decorateImage(Image image, Object element, IDecorationContext context) {
    return null;
  }

  @Override
  public String decorateText(String text, Object element, IDecorationContext context) {
    return null;
  }

  @Override
  public boolean prepareDecoration(Object element, String originalText, IDecorationContext context) {
    return false;
  }
}
