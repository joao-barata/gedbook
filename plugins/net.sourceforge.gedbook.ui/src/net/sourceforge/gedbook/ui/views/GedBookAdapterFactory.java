/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views;

import net.sourceforge.gedbook.model.core.provider.CoreItemProviderAdapterFactory;
import net.sourceforge.gedbook.model.data.provider.DataItemProviderAdapterFactory;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;

/**
 * @author Joao Barata
 */
public class GedBookAdapterFactory 
{
  private static ComposedAdapterFactory _basicAdapterFactory;
  private static ComposedAdapterFactory _decoratedAdapterFactory;

  public final static ComposedAdapterFactory getAdapterFactory() {
    return getAdapterFactory(true);
  }

	public final static ComposedAdapterFactory getAdapterFactory(boolean useDecorators) {
		if (useDecorators) {
		  if (_decoratedAdapterFactory == null) {
		    _decoratedAdapterFactory = createDecoratedFactoryList();
		  }
		  return _decoratedAdapterFactory;
		} else {
      if (_basicAdapterFactory == null) {
        _basicAdapterFactory = createBasicFactoryList();
      }
      return _basicAdapterFactory;
		}
	}

  public final static ComposedAdapterFactory createBasicFactoryList() {
    ComposedAdapterFactory factories = new ComposedAdapterFactory();
    factories.addAdapterFactory(new CoreItemProviderAdapterFactory());
    factories.addAdapterFactory(new DataItemProviderAdapterFactory());
    factories.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
    return factories;
  }
  
  public final static ComposedAdapterFactory createDecoratedFactoryList() {
    ComposedAdapterFactory factories = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
    factories.addAdapterFactory(new ResourceItemProviderAdapterFactory());
    return factories;
  }
}
