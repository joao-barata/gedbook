/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views;

import net.sourceforge.gedbook.model.GedBookXMIResourceFactoryImpl;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.data.DataPackage;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

/**
 * @author Joao Barata
 */
public class GedBookEditingDomainProvider {

  private static TransactionalEditingDomain _editingDomain;

  public final static TransactionalEditingDomain getEditingDomain() {
    if (_editingDomain == null) {
      _editingDomain = createEditingDomain();
    }
    return _editingDomain;
  }

  private final static TransactionalEditingDomain createEditingDomain() {
    //System.out.println("Register factory ..."); //$NON-NLS-1$
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("gedbook", new GedBookXMIResourceFactoryImpl());
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("gedmodel", new GedBookXMIResourceFactoryImpl());
    //System.out.println("Register package ..."); //$NON-NLS-1$
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();
    
    TransactionalEditingDomain ted = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain();
    ECrossReferenceAdapter adapter = new ECrossReferenceAdapter();
    ted.getResourceSet().eAdapters().add(adapter);
    
    return ted;
  }
}
