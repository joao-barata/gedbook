/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;

/**
 * @author Joao Barata
 */
public class GedBookContentProvider extends AdapterFactoryContentProvider
  implements IResourceChangeListener, IResourceDeltaVisitor {

  /**
   * Default constructor.
   */
  public GedBookContentProvider() {
		super(GedBookAdapterFactory.getAdapterFactory());
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
	}

  /**
   * {@inheritDoc}
   */
  public void dispose() {
    super.dispose();
    ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
  }

  /**
   * {@inheritDoc}
   */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			String path = ((IFile)parentElement).getFullPath().toString();
			URI uri = URI.createPlatformResourceURI(path, true);
			parentElement = GedBookEditingDomainProvider.getEditingDomain().getResourceSet().getResource(uri, true);
		}
		return super.getChildren(parentElement); 
	}

  /**
   * {@inheritDoc}
   */
	public Object getParent(Object element) {
		if (element instanceof IFile) {
			return ((IResource)element).getParent();
		}
		return super.getParent(element);
	}

  /**
   * {@inheritDoc}
   */
	public boolean hasChildren(Object element) {
		if (element instanceof IFile) {
			return true;
		}
		return super.hasChildren(element);
	}

  /**
   * {@inheritDoc}
   */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

  /**
   * {@inheritDoc}
   */
	public void resourceChanged(IResourceChangeEvent event) {
		try {
			IResourceDelta delta = event.getDelta();
			delta.accept(this);
		} catch (CoreException e) { 
			System.out.println("Resource Changed Fail - " + e.toString());
		}
	}

  /**
   * {@inheritDoc}
   */
	public boolean visit(IResourceDelta delta) throws CoreException {
		IResource changedResource = delta.getResource();
		if (changedResource.getType() == IResource.FILE && changedResource.getFileExtension().equals("gedbook")) {
			try {
				String path = ((IFile)changedResource).getFullPath().toString();
				URI uri = URI.createPlatformResourceURI(path, true);
				Resource res = GedBookEditingDomainProvider.getEditingDomain().getResourceSet().getResource(uri, true);
				res.unload();
				res.load(GedBookEditingDomainProvider.getEditingDomain().getResourceSet().getLoadOptions());
			} catch(IOException ie) {
				System.out.println("Error reloading resource - " + ie.toString());
			}	
			return false;
		}
		return true;
	}
}
