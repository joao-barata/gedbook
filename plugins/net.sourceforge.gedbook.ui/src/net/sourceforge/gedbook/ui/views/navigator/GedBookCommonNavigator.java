/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views.navigator;

import java.util.Iterator;

import net.sourceforge.gedbook.model.data.Screenshot;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * @author Joao Barata
 */
public class GedBookCommonNavigator extends CommonNavigator implements ITabbedPropertySheetPageContributor, IPropertyChangeListener {

  /**
   * The ID of this view.
   */
  public static final String ID = "net.sourceforge.gedbook.ui.navigationView"; //$NON-NLS-1$

  public static final String GEDBOOK_NAVIGATOR_CONTRIBUTOR_ID = "net.sourceforge.gedbook.ui.properties.navigator"; //$NON-NLS-1$

	/**
	 * {@inheritDoc}
	 */
	public void propertyChange(PropertyChangeEvent event_p) {
		// do nothing
	}

  /**
   * {@inheritDoc}
   */
	@Override
  protected void handleDoubleClick(DoubleClickEvent event) {
	  ISelection selection = event.getSelection();
	  if (selection instanceof IStructuredSelection) {
      Iterator<?> iterator = ((IStructuredSelection) selection).iterator();
      while (iterator.hasNext()) {
        Object object = iterator.next();
        if (object instanceof Screenshot) {
          IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
          try {
            handlerService.executeCommand("net.sourceforge.gedbook.ui.command.screenshotEditorOpen", null);
          } catch (Exception ex) {
            ex.printStackTrace();;
          }
        }
      }
	  }
    super.handleDoubleClick(event);
  }

  /**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
  @Override
	public Object getAdapter(Class adapter) {
		if (IPropertySheetPage.class.equals(adapter)) {
			return new TabbedPropertySheetPage(this);
    }
    return super.getAdapter(adapter);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContributorId() {
		return GEDBOOK_NAVIGATOR_CONTRIBUTOR_ID;
	}
}