/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.views;

import java.util.List;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.PersonEvent;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.graphics.Image;

/**
 * @author Joao Barata
 */
public class GedBookLabelProvider extends AdapterFactoryLabelProvider {

  /**
   * Default constructor.
   */
	public GedBookLabelProvider() {
		super(GedBookAdapterFactory.getAdapterFactory());
	}

	/**
	 * {@inheritDoc}
	 */
	public Image getImage(Object element) {
	  if (element instanceof IStructuredSelection) {
	    return super.getImage(((IStructuredSelection) element).getFirstElement());
	  }
		return super.getImage(element);
	}

  /**
   * {@inheritDoc}
   */
	public String getText(Object element) {
	  if (element instanceof IStructuredSelection) {
	    Object firstElement = ((IStructuredSelection) element).getFirstElement();
      String text = super.getText(firstElement);
      if (firstElement instanceof Extract) {
        List<SourceCitation> citations = ((Extract) firstElement).getCitations();
        if (!citations.isEmpty()) {
          List<EObject> events = ResourceUtils.getReferencers(citations.get(0), CorePackage.Literals.EVENT__CITATIONS);
          for (EObject event : events) {
            if (event instanceof PersonEvent) {
              Person person = ((PersonEvent) event).getPerson();
              text += " [" + GedBookModelHelper.getFullName(person) + "]";
            } else if (event instanceof FamilyEvent) {
              Family family = ((FamilyEvent) event).getFamily();
              text += " [" + GedBookModelHelper.getFullName(family.getHusband());
              text += " / " + GedBookModelHelper.getFullName(family.getWife()) + "]";
            }
          }
        }
      }
      return text;
    }
		return super.getText(element);
	}
}
