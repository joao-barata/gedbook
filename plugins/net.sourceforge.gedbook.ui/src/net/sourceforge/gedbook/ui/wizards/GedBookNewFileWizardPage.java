/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import java.io.InputStream;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

/**
 * @author Joao Barata
 */
public class GedBookNewFileWizardPage extends WizardNewFileCreationPage {

  public GedBookNewFileWizardPage(IStructuredSelection selection) {
    super("GedBookNewFileWizardPage", selection);
    setTitle("Config File");
    setDescription("Creates a new GedBook File");
    setFileExtension("gedbook");
  }

  @Override
  protected InputStream getInitialContents() {
    return null; // ignore and create empty comments
  }

  @Override
  public void createControl(Composite parent) {
    // inherit default container and name specification widgets
    super.createControl(parent);
    Composite composite = (Composite) getControl();

    // sample section generation group
    Group group = new Group(composite, SWT.NONE);
    group.setLayout(new GridLayout());
    group.setText("Automatic_sample_section_generation");
    group.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));

    // sample section generation checkboxes
    Button sectionCheckbox = new Button(group, SWT.CHECK);
    sectionCheckbox.setText("Generate_sample_section_titles");
    sectionCheckbox.setSelection(true);
    sectionCheckbox.addListener(SWT.Selection, this);

    Button subsectionCheckbox = new Button(group, SWT.CHECK);
    subsectionCheckbox.setText("Generate_sample_subsection_titles");
    subsectionCheckbox.setSelection(true);
    subsectionCheckbox.addListener(SWT.Selection, this);

    // open file for editing checkbox
    Button openFileCheckbox = new Button(composite, SWT.CHECK);
    openFileCheckbox.setText("Open_file_for_editing_when_done");
    openFileCheckbox.setSelection(true);
  }

}
