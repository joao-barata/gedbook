/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import net.sourceforge.gedbook.ui.Messages;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * @author Joao Barata
 */
public class ImportGedcomGroup {

  /**
   * location text field.
   */
  private Text _locationText;

  /**
   * location label field.
   */
  private Label _locationLabel;

  /**
   * select button.
   */
  private Button _selectButton;

  /**
   * enable button.
   */
  private Button _enableButton;

  /**
   * allowed file extensions.
   */
  private static final String[] FILTER_EXTS = { "*.ged" }; //$NON-NLS-1$

  /**
   * comment for allowed file extensions.
   */
  private static final String[] FILTER_NAMES = { "Gedcom Files (*.ged)" }; //$NON-NLS-1$

  /**
   * Create a new instance of this class.
   * 
   * @param composite parent composite
   */
  public ImportGedcomGroup(Composite composite) {
    Group importGedcomGroup = new Group(composite, SWT.NONE);
    importGedcomGroup.setFont(composite.getFont());
    importGedcomGroup.setText("Gedcom");
    importGedcomGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
    importGedcomGroup.setLayout(new GridLayout(1, false));

    createGroupContent(importGedcomGroup);
  }

  /**
   * 
   */
  private static final int NB_COLUMNS = 3;

  /**
   * @param parent the parent composite.
   */
  private void createGroupContent(final Composite parent) {
    Composite composite = new Composite(parent, SWT.NONE);
    composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
    composite.setLayout(new GridLayout(NB_COLUMNS, false));

    _enableButton = new Button(composite, SWT.CHECK);
    _enableButton.setText("Import data from an existing gedcom file");
    GridData enableData = new GridData(SWT.FILL, SWT.CENTER, true, false);
    enableData.horizontalSpan = NB_COLUMNS;
    _enableButton.setLayoutData(enableData);
    _enableButton.setSelection(false);

    _locationLabel = new Label(composite, SWT.NONE);
    _locationLabel.setText("Location:");

    _locationText = new Text(composite, SWT.READ_ONLY | SWT.BORDER);
    GridData textData = new GridData(SWT.FILL, SWT.CENTER, true, false);
    textData.horizontalSpan = NB_COLUMNS - 2;
    textData.horizontalIndent = 0;
    _locationText.setLayoutData(textData);

    _selectButton = new Button(composite, SWT.PUSH);
    _selectButton.setText("Browse...");
    setButtonLayoutData(_selectButton);
    _selectButton.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        FileDialog dlg = new FileDialog(parent.getShell(), SWT.SINGLE);
        dlg.setFilterNames(FILTER_NAMES);
        dlg.setFilterExtensions(FILTER_EXTS);
        dlg.setText(Messages.getString("GedBookFileSelection.GEDCOM")); //$NON-NLS-1$
        String fileName = dlg.open();
        if (fileName != null) {
          _locationText.setText(fileName);
        }
      }
    });

    _enableButton.addSelectionListener(new SelectionAdapter() {
      @Override
      public void widgetSelected(SelectionEvent e) {
        updateEnableState(_enableButton.getSelection());
      }
    });
    updateEnableState(_enableButton.getSelection());
  }

  /**
   * @param enabled the enabled state.
   */
  private void updateEnableState(boolean enabled) {
    _locationLabel.setEnabled(enabled);
    _locationText.setEnabled(enabled);
    _selectButton.setEnabled(enabled);
  }

  /**
   * Copy from DialogPage with changes to accomodate the lack of a Dialog context.
   * @param button the button
   * @return the grid data.
   */
  private GridData setButtonLayoutData(Button button) {
    button.setFont(JFaceResources.getDialogFont());

    GC gc = new GC(button);
    gc.setFont(button.getFont());
    FontMetrics fontMetrics = gc.getFontMetrics();
    gc.dispose();
    
    GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
    int widthHint = Dialog.convertHorizontalDLUsToPixels(fontMetrics, IDialogConstants.BUTTON_WIDTH);
    Point minSize = button.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
    data.widthHint = Math.max(widthHint, minSize.x);
    button.setLayoutData(data);
    return data;
  }

  /**
   * @return the path of the gedcom file, or null if disabled.
   */
  public String getGedcomFilepath() {
    return !_enableButton.getSelection() ? null : _locationText.getText();
  }
}
