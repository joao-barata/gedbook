/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

/**
 * The GedBook project nature.
 * 
 * @author Joao Barata
 */
public class GedBookProjectNature implements IProjectNature {
  /**
   * The GedBook project nature identifier.
   */
  public static final String ID = "net.sourceforge.gedbook.project.nature"; //$NON-NLS-1$

  /**
   * the project on which the nature is applied.
   */
  private IProject project;

  /**
   * {@inheritDoc}
   */
  public void configure() throws CoreException {
    // do nothing
  }

  /**
   * {@inheritDoc}
   */
  public void deconfigure() throws CoreException {
    // do nothing
  }

  /**
   * {@inheritDoc}
   */
  public IProject getProject() {
    return project;
  }

  /**
   * {@inheritDoc}
   */
  public void setProject(IProject project) {
    this.project = project;
  }

  /**
   * Check if the given project is accessible and it has a gedbook project nature.
   * 
   * @param project the project to check
   * @return <code>true</code> if it has, <code>false</code> otherwise
   */
  public static boolean hasGedBookProjectNature(IProject project) {
    try {
      return project.exists() && project.hasNature(GedBookProjectNature.ID);
    } catch (CoreException e) {
      // project does not exist or is not open
    }
    return false;
  }
}
