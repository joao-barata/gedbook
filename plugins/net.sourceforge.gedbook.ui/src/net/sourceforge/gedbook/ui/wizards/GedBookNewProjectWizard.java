/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import java.io.File;
import java.io.IOException;

import net.sourceforge.gedbook.model.GedBookXMIResourceFactoryImpl;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.DataFactory;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.gedcom.GedcomImporter;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;
import net.sourceforge.gedbook.ui.GedBookUIPlugin;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

/**
 * @author Joao Barata
 */
public class GedBookNewProjectWizard extends Wizard implements INewWizard {

  private GedBookNewProjectWizardPage _mainPage;
  private IStructuredSelection _selection;

  /**
   * Constructor
   */
  public GedBookNewProjectWizard() {
    setWindowTitle("GedBook Project Wizard");
  }

  /**
   * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
   *      org.eclipse.jface.viewers.IStructuredSelection)
   */
  public void init(IWorkbench workbench, IStructuredSelection selection) {
    _selection = selection;
  }

  /**
   * @see org.eclipse.jface.wizard.Wizard#addPages()
   */
  @Override
  public void addPages() {
    super.addPages();

    _mainPage = new GedBookNewProjectWizardPage("GedBook Project Wizard", _selection);
    _mainPage.setTitle("GedBook Project Wizard");
    _mainPage.setDescription("Create a GedBook project.");

    addPage(_mainPage);
  }

  /**
   * @see org.eclipse.jface.wizard.Wizard#performFinish()
   * @return false if a problem occurs, true otherwise.
   */
  public boolean performFinish() {
    setNeedsProgressMonitor(true);

    try {
      final String projectName = _mainPage.getProjectName();
      final String gedcomFilepath = _mainPage.getGedcomFilepath();

      getContainer().run(false, false, new WorkspaceModifyOperation() {
        @Override
        protected void execute(IProgressMonitor progressMonitor) {
          try {
            createProject(projectName, progressMonitor);
          } catch (Exception exception) {
            GedBookUIPlugin.getDefault().getLog().log(
              new Status(Status.ERROR, GedBookUIPlugin.PLUGIN_ID, 0, exception.getMessage(), exception));
          } finally {
            progressMonitor.done();
          }
        }
      });
      getContainer().run(true, true, new WorkspaceModifyOperation() {
        @Override
        protected void execute(IProgressMonitor progressMonitor) {
          try {
            importGedcom(projectName, gedcomFilepath, progressMonitor);
          } catch (Exception exception) {
            GedBookUIPlugin.getDefault().getLog().log(
                new Status(Status.ERROR, GedBookUIPlugin.PLUGIN_ID, 0, exception.getMessage(), exception));
          } finally {
            progressMonitor.done();
          }
        }
      });
      getContainer().run(false, false, new WorkspaceModifyOperation() {
        @Override
        protected void execute(IProgressMonitor progressMonitor) {
          try {
            ResourcesPlugin.getWorkspace().getRoot().getProject(projectName).refreshLocal(IProject.DEPTH_INFINITE, progressMonitor);
          } catch (Exception exception) {
            GedBookUIPlugin.getDefault().getLog().log(
              new Status(Status.ERROR, GedBookUIPlugin.PLUGIN_ID, 0, exception.getMessage(), exception));
          } finally {
            progressMonitor.done();
          }
        }
      });
    } catch (Exception exception) {
      return false;
    }
    return true;
  }

  /**
   * @param progressMonitor
   * @throws CoreException
   */
  private void createProject(String projectName, IProgressMonitor progressMonitor) throws CoreException {
    IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
    IProject project = root.getProject(projectName);
    project.create(progressMonitor);
    project.open(progressMonitor);

    IProjectDescription description = project.getDescription();
    description.setNatureIds(new String[] { "net.sourceforge.gedbook.project.nature" });
    project.setDescription(description, progressMonitor);
  }

  /**
   * @param projectName
   * @param gedcomFilepath
   * @param progressMonitor
   * @throws IOException
   */
  private void importGedcom(String projectName, String gedcomFilepath, IProgressMonitor progressMonitor) throws IOException {
    if (null != gedcomFilepath) {
      File file = new File(gedcomFilepath);
      if (file.exists()) {
        importGedcom(projectName, file, progressMonitor);
      }
    }
  }

  /**
   * Import the File and start to process it.
   * 
   * @param projectName
   * @param file
   *          a Gedcom file
   * @param progressMonitor
   * @throws IOException
   */
  public void importGedcom(String projectName, File file, IProgressMonitor progressMonitor) throws IOException {
    init();

    String filename = file.getName().replace("." + FilenameUtils.getExtension(file.getName()), "");
    String filepath = ResourcesPlugin.getWorkspace().getRoot().getLocation() + "/" + projectName + "/" + filename + ".refmodel";
    String extfilepath = ResourcesPlugin.getWorkspace().getRoot().getLocation() + "/" + projectName + "/" + filename + ".gedbook";

    GedcomImporter importer = new GedcomImporter(file);
    importer.run();
    EObject project = importer.createModel(progressMonitor);
    progressMonitor.setTaskName("Saving model to '" + filepath + "'");
    ResourceUtils.saveObject(project, new File(filepath)); //$NON-NLS-1$

    ExtendedData extendedData = DataFactory.eINSTANCE.createExtendedData();
    extendedData.setReferencedProject((Project) project);
    ResourceUtils.saveObject(extendedData, new File(extfilepath)); //$NON-NLS-1$
  }

  private static void init() {
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new GedBookXMIResourceFactoryImpl());
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();
  }
}
