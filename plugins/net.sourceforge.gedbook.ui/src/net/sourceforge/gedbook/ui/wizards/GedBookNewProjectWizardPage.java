/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

/**
 * @author Joao Barata
 */
public class GedBookNewProjectWizardPage extends WizardNewProjectCreationPage {

  private ImportGedcomGroup _importGedcomGroup;
  private IStructuredSelection _selection;

  public GedBookNewProjectWizardPage(String pageName, IStructuredSelection selection) {
    super(pageName);
    _selection = selection;
  }

  /**
   * @see org.eclipse.ui.dialogs.WizardNewProjectCreationPage#createControl(org.eclipse.swt.widgets.Composite)
   */
  @Override
  public void createControl(Composite parent) {
    super.createControl(parent);

    createWorkingSetGroup((Composite) getControl(), _selection, new String[] { "org.eclipse.jdt.ui.JavaWorkingSetPage", "org.eclipse.pde.ui.pluginWorkingSet", "org.eclipse.ui.resourceWorkingSetPage" });
    _importGedcomGroup = createImportGedcomGroup((Composite) getControl());
  }

  /**
   * Create an import gedcom group for this page.
   * 
   * @param composite the composite in which to create the group
   * @return the created group.
   */
  public ImportGedcomGroup createImportGedcomGroup(Composite composite) {
    return new ImportGedcomGroup(composite);
  }

  /**
   * @return
   */
  public String getGedcomFilepath() {
    return _importGedcomGroup.getGedcomFilepath();
  }
}
