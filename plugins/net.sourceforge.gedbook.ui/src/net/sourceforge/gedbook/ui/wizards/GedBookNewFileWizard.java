/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

/**
 * @author Joao Barata
 */
public class GedBookNewFileWizard extends Wizard implements INewWizard {

  /**
   * 
   */
  private IWorkbench _workbench;

  /**
   * 
   */
  private IStructuredSelection _selection;

  /**
   * 
   */
  private GedBookNewFileWizardPage _mainPage;

  /**
   * 
   */
  public GedBookNewFileWizard() {
    // nothing to do
  }

  /**
   * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
   *      org.eclipse.jface.viewers.IStructuredSelection)
   */
  @Override
  public void init(IWorkbench workbench, IStructuredSelection selection) {
    _workbench = workbench;
    _selection = selection;
    setWindowTitle("New_Readme_File");
    // setDefaultPageImageDescriptor(ReadmeImages.README_WIZARD_BANNER);
  }

  public void addPages() {
    _mainPage = new GedBookNewFileWizardPage(_selection);
    addPage(_mainPage);
  }

  /**
   * @see org.eclipse.jface.wizard.Wizard#performFinish()
   */
  @Override
  public boolean performFinish() {
    IFile file = _mainPage.createNewFile();
    if (file != null) {
      return true;
    }
    return false;
  }
}
