/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Family Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyEventImpl#getFamily <em>Family</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FamilyEventImpl extends EventImpl implements FamilyEvent {
  /**
   * The cached value of the '{@link #getFamily() <em>Family</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFamily()
   * @generated
   * @ordered
   */
  protected Family family;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FamilyEventImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.FAMILY_EVENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Family getFamily() {
    if (family != null && family.eIsProxy()) {
      InternalEObject oldFamily = (InternalEObject)family;
      family = (Family)eResolveProxy(oldFamily);
      if (family != oldFamily) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.FAMILY_EVENT__FAMILY, oldFamily, family));
      }
    }
    return family;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Family basicGetFamily() {
    return family;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFamily(Family newFamily, NotificationChain msgs) {
    Family oldFamily = family;
    family = newFamily;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.FAMILY_EVENT__FAMILY, oldFamily, newFamily);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFamily(Family newFamily) {
    if (newFamily != family) {
      NotificationChain msgs = null;
      if (family != null)
        msgs = ((InternalEObject)family).eInverseRemove(this, CorePackage.FAMILY__EVENTS, Family.class, msgs);
      if (newFamily != null)
        msgs = ((InternalEObject)newFamily).eInverseAdd(this, CorePackage.FAMILY__EVENTS, Family.class, msgs);
      msgs = basicSetFamily(newFamily, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.FAMILY_EVENT__FAMILY, newFamily, newFamily));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        if (family != null)
          msgs = ((InternalEObject)family).eInverseRemove(this, CorePackage.FAMILY__EVENTS, Family.class, msgs);
        return basicSetFamily((Family)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        return basicSetFamily(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        if (resolve) return getFamily();
        return basicGetFamily();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        setFamily((Family)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        setFamily((Family)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY_EVENT__FAMILY:
        return family != null;
    }
    return super.eIsSet(featureID);
  }

} //FamilyEventImpl
