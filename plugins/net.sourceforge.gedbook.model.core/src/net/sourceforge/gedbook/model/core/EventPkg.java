/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Pkg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.EventPkg#getOwnedEvents <em>Owned Events</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getEventPkg()
 * @model
 * @generated
 */
public interface EventPkg extends EObject {
  /**
   * Returns the value of the '<em><b>Owned Events</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Event}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Events</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Events</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getEventPkg_OwnedEvents()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Event> getOwnedEvents();

} // EventPkg
