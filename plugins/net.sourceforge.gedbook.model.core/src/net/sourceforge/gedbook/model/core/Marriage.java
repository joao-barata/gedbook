/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Marriage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Marriage#getWitnesses <em>Witnesses</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getMarriage()
 * @model
 * @generated
 */
public interface Marriage extends FamilyEvent {
  /**
   * Returns the value of the '<em><b>Witnesses</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Person}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Witnesses</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Witnesses</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getMarriage_Witnesses()
   * @model
   * @generated
   */
  EList<Person> getWitnesses();

} // Marriage
