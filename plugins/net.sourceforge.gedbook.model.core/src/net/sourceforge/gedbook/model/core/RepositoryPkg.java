/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repository Pkg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.RepositoryPkg#getOwnedRepositories <em>Owned Repositories</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getRepositoryPkg()
 * @model
 * @generated
 */
public interface RepositoryPkg extends EObject {
  /**
   * Returns the value of the '<em><b>Owned Repositories</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Repository}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Repositories</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Repositories</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getRepositoryPkg_OwnedRepositories()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Repository> getOwnedRepositories();

} // RepositoryPkg
