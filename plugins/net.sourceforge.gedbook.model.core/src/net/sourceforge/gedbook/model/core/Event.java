/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Event#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Event#getPlace <em>Place</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Event#getCitations <em>Citations</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends IdentifiableObject, AnnotatableObject {
  /**
   * Returns the value of the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Date</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Date</em>' containment reference.
   * @see #setDate(DateWrapper)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getEvent_Date()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  DateWrapper getDate();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Event#getDate <em>Date</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Date</em>' containment reference.
   * @see #getDate()
   * @generated
   */
  void setDate(DateWrapper value);

  /**
   * Returns the value of the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Place</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Place</em>' reference.
   * @see #setPlace(Place)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getEvent_Place()
   * @model
   * @generated
   */
  Place getPlace();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Event#getPlace <em>Place</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Place</em>' reference.
   * @see #getPlace()
   * @generated
   */
  void setPlace(Place value);

  /**
   * Returns the value of the '<em><b>Citations</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.SourceCitation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Citations</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Citations</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getEvent_Citations()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<SourceCitation> getCitations();

} // Event
