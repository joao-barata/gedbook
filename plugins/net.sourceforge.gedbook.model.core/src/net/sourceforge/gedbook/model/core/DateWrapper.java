/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Date Wrapper</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#isDay <em>Day</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#isMonth <em>Month</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#isYear <em>Year</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.DateWrapper#getFormat <em>Format</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper()
 * @model
 * @generated
 */
public interface DateWrapper extends EObject {
  /**
   * Returns the value of the '<em><b>Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Date</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Date</em>' attribute.
   * @see #setDate(Date)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Date()
   * @model
   * @generated
   */
  Date getDate();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#getDate <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Date</em>' attribute.
   * @see #getDate()
   * @generated
   */
  void setDate(Date value);

  /**
   * Returns the value of the '<em><b>Qualifier</b></em>' attribute.
   * The literals are from the enumeration {@link net.sourceforge.gedbook.model.core.DateQualifier}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Qualifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Qualifier</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.DateQualifier
   * @see #setQualifier(DateQualifier)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Qualifier()
   * @model
   * @generated
   */
  DateQualifier getQualifier();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#getQualifier <em>Qualifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Qualifier</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.DateQualifier
   * @see #getQualifier()
   * @generated
   */
  void setQualifier(DateQualifier value);

  /**
   * Returns the value of the '<em><b>Day</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Day</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Day</em>' attribute.
   * @see #setDay(boolean)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Day()
   * @model
   * @generated
   */
  boolean isDay();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#isDay <em>Day</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Day</em>' attribute.
   * @see #isDay()
   * @generated
   */
  void setDay(boolean value);

  /**
   * Returns the value of the '<em><b>Month</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Month</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Month</em>' attribute.
   * @see #setMonth(boolean)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Month()
   * @model
   * @generated
   */
  boolean isMonth();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#isMonth <em>Month</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Month</em>' attribute.
   * @see #isMonth()
   * @generated
   */
  void setMonth(boolean value);

  /**
   * Returns the value of the '<em><b>Year</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Year</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Year</em>' attribute.
   * @see #setYear(boolean)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Year()
   * @model
   * @generated
   */
  boolean isYear();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#isYear <em>Year</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Year</em>' attribute.
   * @see #isYear()
   * @generated
   */
  void setYear(boolean value);

  /**
   * Returns the value of the '<em><b>Format</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Format</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Format</em>' attribute.
   * @see #setFormat(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDateWrapper_Format()
   * @model
   * @generated
   */
  String getFormat();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.DateWrapper#getFormat <em>Format</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Format</em>' attribute.
   * @see #getFormat()
   * @generated
   */
  void setFormat(String value);

} // DateWrapper
