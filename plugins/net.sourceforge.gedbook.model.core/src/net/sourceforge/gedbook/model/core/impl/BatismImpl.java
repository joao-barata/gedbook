/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Person;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Batism</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.BatismImpl#getGodfather <em>Godfather</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.BatismImpl#getGodmother <em>Godmother</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BatismImpl extends PersonEventImpl implements Batism {
  /**
   * The cached value of the '{@link #getGodfather() <em>Godfather</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGodfather()
   * @generated
   * @ordered
   */
  protected Person godfather;

  /**
   * The cached value of the '{@link #getGodmother() <em>Godmother</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGodmother()
   * @generated
   * @ordered
   */
  protected Person godmother;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected BatismImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.BATISM;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getGodfather() {
    if (godfather != null && godfather.eIsProxy()) {
      InternalEObject oldGodfather = (InternalEObject)godfather;
      godfather = (Person)eResolveProxy(oldGodfather);
      if (godfather != oldGodfather) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.BATISM__GODFATHER, oldGodfather, godfather));
      }
    }
    return godfather;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetGodfather() {
    return godfather;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGodfather(Person newGodfather) {
    Person oldGodfather = godfather;
    godfather = newGodfather;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.BATISM__GODFATHER, oldGodfather, godfather));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getGodmother() {
    if (godmother != null && godmother.eIsProxy()) {
      InternalEObject oldGodmother = (InternalEObject)godmother;
      godmother = (Person)eResolveProxy(oldGodmother);
      if (godmother != oldGodmother) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.BATISM__GODMOTHER, oldGodmother, godmother));
      }
    }
    return godmother;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetGodmother() {
    return godmother;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGodmother(Person newGodmother) {
    Person oldGodmother = godmother;
    godmother = newGodmother;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.BATISM__GODMOTHER, oldGodmother, godmother));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.BATISM__GODFATHER:
        if (resolve) return getGodfather();
        return basicGetGodfather();
      case CorePackage.BATISM__GODMOTHER:
        if (resolve) return getGodmother();
        return basicGetGodmother();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.BATISM__GODFATHER:
        setGodfather((Person)newValue);
        return;
      case CorePackage.BATISM__GODMOTHER:
        setGodmother((Person)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.BATISM__GODFATHER:
        setGodfather((Person)null);
        return;
      case CorePackage.BATISM__GODMOTHER:
        setGodmother((Person)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.BATISM__GODFATHER:
        return godfather != null;
      case CorePackage.BATISM__GODMOTHER:
        return godmother != null;
    }
    return super.eIsSet(featureID);
  }

} //BatismImpl
