/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Collection;
import net.sourceforge.gedbook.model.core.AnnotatableObject;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Note;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.SourceCitation;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.EventImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.EventImpl#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.EventImpl#getPlace <em>Place</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.EventImpl#getCitations <em>Citations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventImpl extends IdentifiableObjectImpl implements Event {
  /**
   * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnnotations()
   * @generated
   * @ordered
   */
  protected EList<Note> annotations;

  /**
   * The cached value of the '{@link #getDate() <em>Date</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDate()
   * @generated
   * @ordered
   */
  protected DateWrapper date;

  /**
   * The cached value of the '{@link #getPlace() <em>Place</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlace()
   * @generated
   * @ordered
   */
  protected Place place;

  /**
   * The cached value of the '{@link #getCitations() <em>Citations</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCitations()
   * @generated
   * @ordered
   */
  protected EList<SourceCitation> citations;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EventImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.EVENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Note> getAnnotations() {
    if (annotations == null) {
      annotations = new EObjectContainmentEList.Resolving<Note>(Note.class, this, CorePackage.EVENT__ANNOTATIONS);
    }
    return annotations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DateWrapper getDate() {
    if (date != null && date.eIsProxy()) {
      InternalEObject oldDate = (InternalEObject)date;
      date = (DateWrapper)eResolveProxy(oldDate);
      if (date != oldDate) {
        InternalEObject newDate = (InternalEObject)date;
        NotificationChain msgs = oldDate.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.EVENT__DATE, null, null);
        if (newDate.eInternalContainer() == null) {
          msgs = newDate.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.EVENT__DATE, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.EVENT__DATE, oldDate, date));
      }
    }
    return date;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DateWrapper basicGetDate() {
    return date;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDate(DateWrapper newDate, NotificationChain msgs) {
    DateWrapper oldDate = date;
    date = newDate;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.EVENT__DATE, oldDate, newDate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDate(DateWrapper newDate) {
    if (newDate != date) {
      NotificationChain msgs = null;
      if (date != null)
        msgs = ((InternalEObject)date).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.EVENT__DATE, null, msgs);
      if (newDate != null)
        msgs = ((InternalEObject)newDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.EVENT__DATE, null, msgs);
      msgs = basicSetDate(newDate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.EVENT__DATE, newDate, newDate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Place getPlace() {
    if (place != null && place.eIsProxy()) {
      InternalEObject oldPlace = (InternalEObject)place;
      place = (Place)eResolveProxy(oldPlace);
      if (place != oldPlace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.EVENT__PLACE, oldPlace, place));
      }
    }
    return place;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Place basicGetPlace() {
    return place;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlace(Place newPlace) {
    Place oldPlace = place;
    place = newPlace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.EVENT__PLACE, oldPlace, place));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceCitation> getCitations() {
    if (citations == null) {
      citations = new EObjectContainmentEList.Resolving<SourceCitation>(SourceCitation.class, this, CorePackage.EVENT__CITATIONS);
    }
    return citations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.EVENT__ANNOTATIONS:
        return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
      case CorePackage.EVENT__DATE:
        return basicSetDate(null, msgs);
      case CorePackage.EVENT__CITATIONS:
        return ((InternalEList<?>)getCitations()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.EVENT__ANNOTATIONS:
        return getAnnotations();
      case CorePackage.EVENT__DATE:
        if (resolve) return getDate();
        return basicGetDate();
      case CorePackage.EVENT__PLACE:
        if (resolve) return getPlace();
        return basicGetPlace();
      case CorePackage.EVENT__CITATIONS:
        return getCitations();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.EVENT__ANNOTATIONS:
        getAnnotations().clear();
        getAnnotations().addAll((Collection<? extends Note>)newValue);
        return;
      case CorePackage.EVENT__DATE:
        setDate((DateWrapper)newValue);
        return;
      case CorePackage.EVENT__PLACE:
        setPlace((Place)newValue);
        return;
      case CorePackage.EVENT__CITATIONS:
        getCitations().clear();
        getCitations().addAll((Collection<? extends SourceCitation>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.EVENT__ANNOTATIONS:
        getAnnotations().clear();
        return;
      case CorePackage.EVENT__DATE:
        setDate((DateWrapper)null);
        return;
      case CorePackage.EVENT__PLACE:
        setPlace((Place)null);
        return;
      case CorePackage.EVENT__CITATIONS:
        getCitations().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.EVENT__ANNOTATIONS:
        return annotations != null && !annotations.isEmpty();
      case CorePackage.EVENT__DATE:
        return date != null;
      case CorePackage.EVENT__PLACE:
        return place != null;
      case CorePackage.EVENT__CITATIONS:
        return citations != null && !citations.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
    if (baseClass == AnnotatableObject.class) {
      switch (derivedFeatureID) {
        case CorePackage.EVENT__ANNOTATIONS: return CorePackage.ANNOTATABLE_OBJECT__ANNOTATIONS;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
    if (baseClass == AnnotatableObject.class) {
      switch (baseFeatureID) {
        case CorePackage.ANNOTATABLE_OBJECT__ANNOTATIONS: return CorePackage.EVENT__ANNOTATIONS;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

} //EventImpl
