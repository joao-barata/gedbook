/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.DocumentKind;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Source;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getAbbreviation <em>Abbreviation</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getAuthor <em>Author</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getAgency <em>Agency</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getPages <em>Pages</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getSource <em>Source</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl#getPlace <em>Place</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentImpl extends IdentifiableObjectImpl implements Document {
  /**
   * The default value of the '{@link #getAbbreviation() <em>Abbreviation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbbreviation()
   * @generated
   * @ordered
   */
  protected static final String ABBREVIATION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAbbreviation() <em>Abbreviation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAbbreviation()
   * @generated
   * @ordered
   */
  protected String abbreviation = ABBREVIATION_EDEFAULT;

  /**
   * The default value of the '{@link #getAuthor() <em>Author</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAuthor()
   * @generated
   * @ordered
   */
  protected static final String AUTHOR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAuthor() <em>Author</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAuthor()
   * @generated
   * @ordered
   */
  protected String author = AUTHOR_EDEFAULT;

  /**
   * The default value of the '{@link #getAgency() <em>Agency</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAgency()
   * @generated
   * @ordered
   */
  protected static final String AGENCY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAgency() <em>Agency</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAgency()
   * @generated
   * @ordered
   */
  protected String agency = AGENCY_EDEFAULT;

  /**
   * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDate()
   * @generated
   * @ordered
   */
  protected static final String DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDate()
   * @generated
   * @ordered
   */
  protected String date = DATE_EDEFAULT;

  /**
   * The default value of the '{@link #getReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReference()
   * @generated
   * @ordered
   */
  protected static final String REFERENCE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getReference() <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReference()
   * @generated
   * @ordered
   */
  protected String reference = REFERENCE_EDEFAULT;

  /**
   * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected static final String TITLE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected String title = TITLE_EDEFAULT;

  /**
   * The default value of the '{@link #getPages() <em>Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPages()
   * @generated
   * @ordered
   */
  protected static final int PAGES_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getPages() <em>Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPages()
   * @generated
   * @ordered
   */
  protected int pages = PAGES_EDEFAULT;

  /**
   * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKind()
   * @generated
   * @ordered
   */
  protected static final DocumentKind KIND_EDEFAULT = DocumentKind.UNSET;

  /**
   * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKind()
   * @generated
   * @ordered
   */
  protected DocumentKind kind = KIND_EDEFAULT;

  /**
   * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSource()
   * @generated
   * @ordered
   */
  protected Source source;

  /**
   * The cached value of the '{@link #getPlace() <em>Place</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPlace()
   * @generated
   * @ordered
   */
  protected Place place;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocumentImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.DOCUMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAbbreviation() {
    return abbreviation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAbbreviation(String newAbbreviation) {
    String oldAbbreviation = abbreviation;
    abbreviation = newAbbreviation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__ABBREVIATION, oldAbbreviation, abbreviation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAuthor() {
    return author;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAuthor(String newAuthor) {
    String oldAuthor = author;
    author = newAuthor;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__AUTHOR, oldAuthor, author));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAgency() {
    return agency;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAgency(String newAgency) {
    String oldAgency = agency;
    agency = newAgency;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__AGENCY, oldAgency, agency));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDate() {
    return date;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDate(String newDate) {
    String oldDate = date;
    date = newDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__DATE, oldDate, date));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getReference() {
    return reference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReference(String newReference) {
    String oldReference = reference;
    reference = newReference;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__REFERENCE, oldReference, reference));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTitle() {
    return title;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTitle(String newTitle) {
    String oldTitle = title;
    title = newTitle;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__TITLE, oldTitle, title));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getPages() {
    return pages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPages(int newPages) {
    int oldPages = pages;
    pages = newPages;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__PAGES, oldPages, pages));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentKind getKind() {
    return kind;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setKind(DocumentKind newKind) {
    DocumentKind oldKind = kind;
    kind = newKind == null ? KIND_EDEFAULT : newKind;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__KIND, oldKind, kind));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source getSource() {
    if (source != null && source.eIsProxy()) {
      InternalEObject oldSource = (InternalEObject)source;
      source = (Source)eResolveProxy(oldSource);
      if (source != oldSource) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.DOCUMENT__SOURCE, oldSource, source));
      }
    }
    return source;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source basicGetSource() {
    return source;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSource(Source newSource) {
    Source oldSource = source;
    source = newSource;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__SOURCE, oldSource, source));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Place getPlace() {
    if (place != null && place.eIsProxy()) {
      InternalEObject oldPlace = (InternalEObject)place;
      place = (Place)eResolveProxy(oldPlace);
      if (place != oldPlace) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.DOCUMENT__PLACE, oldPlace, place));
      }
    }
    return place;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Place basicGetPlace() {
    return place;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPlace(Place newPlace) {
    Place oldPlace = place;
    place = newPlace;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DOCUMENT__PLACE, oldPlace, place));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.DOCUMENT__ABBREVIATION:
        return getAbbreviation();
      case CorePackage.DOCUMENT__AUTHOR:
        return getAuthor();
      case CorePackage.DOCUMENT__AGENCY:
        return getAgency();
      case CorePackage.DOCUMENT__DATE:
        return getDate();
      case CorePackage.DOCUMENT__REFERENCE:
        return getReference();
      case CorePackage.DOCUMENT__TITLE:
        return getTitle();
      case CorePackage.DOCUMENT__PAGES:
        return getPages();
      case CorePackage.DOCUMENT__KIND:
        return getKind();
      case CorePackage.DOCUMENT__SOURCE:
        if (resolve) return getSource();
        return basicGetSource();
      case CorePackage.DOCUMENT__PLACE:
        if (resolve) return getPlace();
        return basicGetPlace();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.DOCUMENT__ABBREVIATION:
        setAbbreviation((String)newValue);
        return;
      case CorePackage.DOCUMENT__AUTHOR:
        setAuthor((String)newValue);
        return;
      case CorePackage.DOCUMENT__AGENCY:
        setAgency((String)newValue);
        return;
      case CorePackage.DOCUMENT__DATE:
        setDate((String)newValue);
        return;
      case CorePackage.DOCUMENT__REFERENCE:
        setReference((String)newValue);
        return;
      case CorePackage.DOCUMENT__TITLE:
        setTitle((String)newValue);
        return;
      case CorePackage.DOCUMENT__PAGES:
        setPages((Integer)newValue);
        return;
      case CorePackage.DOCUMENT__KIND:
        setKind((DocumentKind)newValue);
        return;
      case CorePackage.DOCUMENT__SOURCE:
        setSource((Source)newValue);
        return;
      case CorePackage.DOCUMENT__PLACE:
        setPlace((Place)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.DOCUMENT__ABBREVIATION:
        setAbbreviation(ABBREVIATION_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__AUTHOR:
        setAuthor(AUTHOR_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__AGENCY:
        setAgency(AGENCY_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__DATE:
        setDate(DATE_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__REFERENCE:
        setReference(REFERENCE_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__TITLE:
        setTitle(TITLE_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__PAGES:
        setPages(PAGES_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__KIND:
        setKind(KIND_EDEFAULT);
        return;
      case CorePackage.DOCUMENT__SOURCE:
        setSource((Source)null);
        return;
      case CorePackage.DOCUMENT__PLACE:
        setPlace((Place)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.DOCUMENT__ABBREVIATION:
        return ABBREVIATION_EDEFAULT == null ? abbreviation != null : !ABBREVIATION_EDEFAULT.equals(abbreviation);
      case CorePackage.DOCUMENT__AUTHOR:
        return AUTHOR_EDEFAULT == null ? author != null : !AUTHOR_EDEFAULT.equals(author);
      case CorePackage.DOCUMENT__AGENCY:
        return AGENCY_EDEFAULT == null ? agency != null : !AGENCY_EDEFAULT.equals(agency);
      case CorePackage.DOCUMENT__DATE:
        return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
      case CorePackage.DOCUMENT__REFERENCE:
        return REFERENCE_EDEFAULT == null ? reference != null : !REFERENCE_EDEFAULT.equals(reference);
      case CorePackage.DOCUMENT__TITLE:
        return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
      case CorePackage.DOCUMENT__PAGES:
        return pages != PAGES_EDEFAULT;
      case CorePackage.DOCUMENT__KIND:
        return kind != KIND_EDEFAULT;
      case CorePackage.DOCUMENT__SOURCE:
        return source != null;
      case CorePackage.DOCUMENT__PLACE:
        return place != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (abbreviation: "); //$NON-NLS-1$
    result.append(abbreviation);
    result.append(", author: "); //$NON-NLS-1$
    result.append(author);
    result.append(", agency: "); //$NON-NLS-1$
    result.append(agency);
    result.append(", date: "); //$NON-NLS-1$
    result.append(date);
    result.append(", reference: "); //$NON-NLS-1$
    result.append(reference);
    result.append(", title: "); //$NON-NLS-1$
    result.append(title);
    result.append(", pages: "); //$NON-NLS-1$
    result.append(pages);
    result.append(", kind: "); //$NON-NLS-1$
    result.append(kind);
    result.append(')');
    return result.toString();
  }

} //DocumentImpl
