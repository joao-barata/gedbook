/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.util;

import net.sourceforge.gedbook.model.core.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.core.CorePackage
 * @generated
 */
public class CoreAdapterFactory extends AdapterFactoryImpl {
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CorePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CoreAdapterFactory() {
    if (modelPackage == null) {
      modelPackage = CorePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object) {
    if (object == modelPackage) {
      return true;
    }
    if (object instanceof EObject) {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CoreSwitch<Adapter> modelSwitch =
    new CoreSwitch<Adapter>() {
      @Override
      public Adapter caseIdentifiableObject(IdentifiableObject object) {
        return createIdentifiableObjectAdapter();
      }
      @Override
      public Adapter caseAnnotatableObject(AnnotatableObject object) {
        return createAnnotatableObjectAdapter();
      }
      @Override
      public Adapter caseNote(Note object) {
        return createNoteAdapter();
      }
      @Override
      public Adapter caseProject(Project object) {
        return createProjectAdapter();
      }
      @Override
      public Adapter caseRepositoryPkg(RepositoryPkg object) {
        return createRepositoryPkgAdapter();
      }
      @Override
      public Adapter caseSourcePkg(SourcePkg object) {
        return createSourcePkgAdapter();
      }
      @Override
      public Adapter caseDocumentPkg(DocumentPkg object) {
        return createDocumentPkgAdapter();
      }
      @Override
      public Adapter casePersonPkg(PersonPkg object) {
        return createPersonPkgAdapter();
      }
      @Override
      public Adapter caseFamilyPkg(FamilyPkg object) {
        return createFamilyPkgAdapter();
      }
      @Override
      public Adapter caseEventPkg(EventPkg object) {
        return createEventPkgAdapter();
      }
      @Override
      public Adapter casePlacePkg(PlacePkg object) {
        return createPlacePkgAdapter();
      }
      @Override
      public Adapter caseRepository(Repository object) {
        return createRepositoryAdapter();
      }
      @Override
      public Adapter caseSource(Source object) {
        return createSourceAdapter();
      }
      @Override
      public Adapter caseSourceCitation(SourceCitation object) {
        return createSourceCitationAdapter();
      }
      @Override
      public Adapter caseDocument(Document object) {
        return createDocumentAdapter();
      }
      @Override
      public Adapter caseFamily(Family object) {
        return createFamilyAdapter();
      }
      @Override
      public Adapter caseEvent(Event object) {
        return createEventAdapter();
      }
      @Override
      public Adapter casePersonEvent(PersonEvent object) {
        return createPersonEventAdapter();
      }
      @Override
      public Adapter caseFamilyEvent(FamilyEvent object) {
        return createFamilyEventAdapter();
      }
      @Override
      public Adapter caseDateWrapper(DateWrapper object) {
        return createDateWrapperAdapter();
      }
      @Override
      public Adapter caseBirth(Birth object) {
        return createBirthAdapter();
      }
      @Override
      public Adapter caseBatism(Batism object) {
        return createBatismAdapter();
      }
      @Override
      public Adapter caseDeath(Death object) {
        return createDeathAdapter();
      }
      @Override
      public Adapter caseBurial(Burial object) {
        return createBurialAdapter();
      }
      @Override
      public Adapter caseWill(Will object) {
        return createWillAdapter();
      }
      @Override
      public Adapter caseOccupation(Occupation object) {
        return createOccupationAdapter();
      }
      @Override
      public Adapter caseTitle(Title object) {
        return createTitleAdapter();
      }
      @Override
      public Adapter caseNationality(Nationality object) {
        return createNationalityAdapter();
      }
      @Override
      public Adapter caseSocialSecurityNumber(SocialSecurityNumber object) {
        return createSocialSecurityNumberAdapter();
      }
      @Override
      public Adapter caseProperty(Property object) {
        return createPropertyAdapter();
      }
      @Override
      public Adapter caseResidence(Residence object) {
        return createResidenceAdapter();
      }
      @Override
      public Adapter caseOrdination(Ordination object) {
        return createOrdinationAdapter();
      }
      @Override
      public Adapter caseRetirement(Retirement object) {
        return createRetirementAdapter();
      }
      @Override
      public Adapter caseCensus(Census object) {
        return createCensusAdapter();
      }
      @Override
      public Adapter caseMarriage(Marriage object) {
        return createMarriageAdapter();
      }
      @Override
      public Adapter caseBanns(Banns object) {
        return createBannsAdapter();
      }
      @Override
      public Adapter caseDivorce(Divorce object) {
        return createDivorceAdapter();
      }
      @Override
      public Adapter caseAnulment(Anulment object) {
        return createAnulmentAdapter();
      }
      @Override
      public Adapter casePerson(Person object) {
        return createPersonAdapter();
      }
      @Override
      public Adapter caseName(Name object) {
        return createNameAdapter();
      }
      @Override
      public Adapter casePlace(Place object) {
        return createPlaceAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object) {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.IdentifiableObject <em>Identifiable Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.IdentifiableObject
   * @generated
   */
  public Adapter createIdentifiableObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.AnnotatableObject <em>Annotatable Object</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.AnnotatableObject
   * @generated
   */
  public Adapter createAnnotatableObjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Note <em>Note</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Note
   * @generated
   */
  public Adapter createNoteAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Project <em>Project</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Project
   * @generated
   */
  public Adapter createProjectAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.RepositoryPkg <em>Repository Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.RepositoryPkg
   * @generated
   */
  public Adapter createRepositoryPkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.SourcePkg <em>Source Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.SourcePkg
   * @generated
   */
  public Adapter createSourcePkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.DocumentPkg <em>Document Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.DocumentPkg
   * @generated
   */
  public Adapter createDocumentPkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.PersonPkg <em>Person Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.PersonPkg
   * @generated
   */
  public Adapter createPersonPkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.FamilyPkg <em>Family Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.FamilyPkg
   * @generated
   */
  public Adapter createFamilyPkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.EventPkg <em>Event Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.EventPkg
   * @generated
   */
  public Adapter createEventPkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.PlacePkg <em>Place Pkg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.PlacePkg
   * @generated
   */
  public Adapter createPlacePkgAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Repository <em>Repository</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Repository
   * @generated
   */
  public Adapter createRepositoryAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Source <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Source
   * @generated
   */
  public Adapter createSourceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.SourceCitation <em>Source Citation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.SourceCitation
   * @generated
   */
  public Adapter createSourceCitationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Document <em>Document</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Document
   * @generated
   */
  public Adapter createDocumentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Event
   * @generated
   */
  public Adapter createEventAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.PersonEvent <em>Person Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.PersonEvent
   * @generated
   */
  public Adapter createPersonEventAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.FamilyEvent <em>Family Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.FamilyEvent
   * @generated
   */
  public Adapter createFamilyEventAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.DateWrapper <em>Date Wrapper</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.DateWrapper
   * @generated
   */
  public Adapter createDateWrapperAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Birth <em>Birth</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Birth
   * @generated
   */
  public Adapter createBirthAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Batism <em>Batism</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Batism
   * @generated
   */
  public Adapter createBatismAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Marriage <em>Marriage</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Marriage
   * @generated
   */
  public Adapter createMarriageAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Banns <em>Banns</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Banns
   * @generated
   */
  public Adapter createBannsAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Death <em>Death</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Death
   * @generated
   */
  public Adapter createDeathAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Burial <em>Burial</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Burial
   * @generated
   */
  public Adapter createBurialAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Will <em>Will</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Will
   * @generated
   */
  public Adapter createWillAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Occupation <em>Occupation</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Occupation
   * @generated
   */
  public Adapter createOccupationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Divorce <em>Divorce</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Divorce
   * @generated
   */
  public Adapter createDivorceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Anulment <em>Anulment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Anulment
   * @generated
   */
  public Adapter createAnulmentAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Title <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Title
   * @generated
   */
  public Adapter createTitleAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Nationality <em>Nationality</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Nationality
   * @generated
   */
  public Adapter createNationalityAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.SocialSecurityNumber <em>Social Security Number</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.SocialSecurityNumber
   * @generated
   */
  public Adapter createSocialSecurityNumberAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Property
   * @generated
   */
  public Adapter createPropertyAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Residence <em>Residence</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Residence
   * @generated
   */
  public Adapter createResidenceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Ordination <em>Ordination</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Ordination
   * @generated
   */
  public Adapter createOrdinationAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Retirement <em>Retirement</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Retirement
   * @generated
   */
  public Adapter createRetirementAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Census <em>Census</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Census
   * @generated
   */
  public Adapter createCensusAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Person <em>Person</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Person
   * @generated
   */
  public Adapter createPersonAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Name <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Name
   * @generated
   */
  public Adapter createNameAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Place <em>Place</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Place
   * @generated
   */
  public Adapter createPlaceAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.core.Family <em>Family</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.core.Family
   * @generated
   */
  public Adapter createFamilyAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter() {
    return null;
  }

} //CoreAdapterFactory
