/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getAdoptionname <em>Adoptionname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getBirthname <em>Birthname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getCensusname <em>Censusname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getCurrentname <em>Currentname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getFormalname <em>Formalname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getGivenname <em>Givenname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getMarriedname <em>Marriedname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getNickname <em>Nickname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getOthername <em>Othername</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Name#getSurname <em>Surname</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getName_()
 * @model
 * @generated
 */
public interface Name extends EObject {
  /**
   * Returns the value of the '<em><b>Adoptionname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Adoptionname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Adoptionname</em>' attribute.
   * @see #setAdoptionname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Adoptionname()
   * @model
   * @generated
   */
  String getAdoptionname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getAdoptionname <em>Adoptionname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Adoptionname</em>' attribute.
   * @see #getAdoptionname()
   * @generated
   */
  void setAdoptionname(String value);

  /**
   * Returns the value of the '<em><b>Birthname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Birthname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Birthname</em>' attribute.
   * @see #setBirthname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Birthname()
   * @model
   * @generated
   */
  String getBirthname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getBirthname <em>Birthname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Birthname</em>' attribute.
   * @see #getBirthname()
   * @generated
   */
  void setBirthname(String value);

  /**
   * Returns the value of the '<em><b>Censusname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Censusname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Censusname</em>' attribute.
   * @see #setCensusname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Censusname()
   * @model
   * @generated
   */
  String getCensusname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getCensusname <em>Censusname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Censusname</em>' attribute.
   * @see #getCensusname()
   * @generated
   */
  void setCensusname(String value);

  /**
   * Returns the value of the '<em><b>Currentname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Currentname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Currentname</em>' attribute.
   * @see #setCurrentname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Currentname()
   * @model
   * @generated
   */
  String getCurrentname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getCurrentname <em>Currentname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Currentname</em>' attribute.
   * @see #getCurrentname()
   * @generated
   */
  void setCurrentname(String value);

  /**
   * Returns the value of the '<em><b>Formalname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formalname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formalname</em>' attribute.
   * @see #setFormalname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Formalname()
   * @model
   * @generated
   */
  String getFormalname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getFormalname <em>Formalname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formalname</em>' attribute.
   * @see #getFormalname()
   * @generated
   */
  void setFormalname(String value);

  /**
   * Returns the value of the '<em><b>Givenname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Givenname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Givenname</em>' attribute.
   * @see #setGivenname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Givenname()
   * @model
   * @generated
   */
  String getGivenname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getGivenname <em>Givenname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Givenname</em>' attribute.
   * @see #getGivenname()
   * @generated
   */
  void setGivenname(String value);

  /**
   * Returns the value of the '<em><b>Marriedname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Marriedname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Marriedname</em>' attribute.
   * @see #setMarriedname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Marriedname()
   * @model
   * @generated
   */
  String getMarriedname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getMarriedname <em>Marriedname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Marriedname</em>' attribute.
   * @see #getMarriedname()
   * @generated
   */
  void setMarriedname(String value);

  /**
   * Returns the value of the '<em><b>Surname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Surname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Surname</em>' attribute.
   * @see #setSurname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Surname()
   * @model
   * @generated
   */
  String getSurname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getSurname <em>Surname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Surname</em>' attribute.
   * @see #getSurname()
   * @generated
   */
  void setSurname(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Nickname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nickname</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nickname</em>' attribute.
   * @see #setNickname(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Nickname()
   * @model
   * @generated
   */
  String getNickname();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getNickname <em>Nickname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nickname</em>' attribute.
   * @see #getNickname()
   * @generated
   */
  void setNickname(String value);

  /**
   * Returns the value of the '<em><b>Othername</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Othername</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Othername</em>' attribute.
   * @see #setOthername(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getName_Othername()
   * @model
   * @generated
   */
  String getOthername();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Name#getOthername <em>Othername</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Othername</em>' attribute.
   * @see #getOthername()
   * @generated
   */
  void setOthername(String value);

} // Name
