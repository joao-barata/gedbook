/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getDescription <em>Description</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedRepositoryPkg <em>Owned Repository Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedSourcePkg <em>Owned Source Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedDocumentPkg <em>Owned Document Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedPersonPkg <em>Owned Person Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedFamilyPkg <em>Owned Family Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedEventPkg <em>Owned Event Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Project#getOwnedPlacePkg <em>Owned Place Pkg</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getProject()
 * @model
 * @generated
 */
public interface Project extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Owned Repository Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Repository Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Repository Pkg</em>' containment reference.
   * @see #setOwnedRepositoryPkg(RepositoryPkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedRepositoryPkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  RepositoryPkg getOwnedRepositoryPkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedRepositoryPkg <em>Owned Repository Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Repository Pkg</em>' containment reference.
   * @see #getOwnedRepositoryPkg()
   * @generated
   */
  void setOwnedRepositoryPkg(RepositoryPkg value);

  /**
   * Returns the value of the '<em><b>Owned Source Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Source Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Source Pkg</em>' containment reference.
   * @see #setOwnedSourcePkg(SourcePkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedSourcePkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  SourcePkg getOwnedSourcePkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedSourcePkg <em>Owned Source Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Source Pkg</em>' containment reference.
   * @see #getOwnedSourcePkg()
   * @generated
   */
  void setOwnedSourcePkg(SourcePkg value);

  /**
   * Returns the value of the '<em><b>Owned Document Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Document Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Document Pkg</em>' containment reference.
   * @see #setOwnedDocumentPkg(DocumentPkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedDocumentPkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  DocumentPkg getOwnedDocumentPkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedDocumentPkg <em>Owned Document Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Document Pkg</em>' containment reference.
   * @see #getOwnedDocumentPkg()
   * @generated
   */
  void setOwnedDocumentPkg(DocumentPkg value);

  /**
   * Returns the value of the '<em><b>Owned Person Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Person Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Person Pkg</em>' containment reference.
   * @see #setOwnedPersonPkg(PersonPkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedPersonPkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  PersonPkg getOwnedPersonPkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedPersonPkg <em>Owned Person Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Person Pkg</em>' containment reference.
   * @see #getOwnedPersonPkg()
   * @generated
   */
  void setOwnedPersonPkg(PersonPkg value);

  /**
   * Returns the value of the '<em><b>Owned Family Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Family Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Family Pkg</em>' containment reference.
   * @see #setOwnedFamilyPkg(FamilyPkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedFamilyPkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  FamilyPkg getOwnedFamilyPkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedFamilyPkg <em>Owned Family Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Family Pkg</em>' containment reference.
   * @see #getOwnedFamilyPkg()
   * @generated
   */
  void setOwnedFamilyPkg(FamilyPkg value);

  /**
   * Returns the value of the '<em><b>Owned Event Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Event Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Event Pkg</em>' containment reference.
   * @see #setOwnedEventPkg(EventPkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedEventPkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EventPkg getOwnedEventPkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedEventPkg <em>Owned Event Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Event Pkg</em>' containment reference.
   * @see #getOwnedEventPkg()
   * @generated
   */
  void setOwnedEventPkg(EventPkg value);

  /**
   * Returns the value of the '<em><b>Owned Place Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Owned Place Pkg</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Owned Place Pkg</em>' containment reference.
   * @see #setOwnedPlacePkg(PlacePkg)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getProject_OwnedPlacePkg()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  PlacePkg getOwnedPlacePkg();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Project#getOwnedPlacePkg <em>Owned Place Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Owned Place Pkg</em>' containment reference.
   * @see #getOwnedPlacePkg()
   * @generated
   */
  void setOwnedPlacePkg(PlacePkg value);

} // Project
