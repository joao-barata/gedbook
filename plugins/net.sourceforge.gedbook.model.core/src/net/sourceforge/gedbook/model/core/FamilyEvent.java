/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Family Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.FamilyEvent#getFamily <em>Family</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getFamilyEvent()
 * @model
 * @generated
 */
public interface FamilyEvent extends Event {
  /**
   * Returns the value of the '<em><b>Family</b></em>' reference.
   * It is bidirectional and its opposite is '{@link net.sourceforge.gedbook.model.core.Family#getEvents <em>Events</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Family</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Family</em>' reference.
   * @see #setFamily(Family)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getFamilyEvent_Family()
   * @see net.sourceforge.gedbook.model.core.Family#getEvents
   * @model opposite="events"
   * @generated
   */
  Family getFamily();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.FamilyEvent#getFamily <em>Family</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Family</em>' reference.
   * @see #getFamily()
   * @generated
   */
  void setFamily(Family value);

} // FamilyEvent
