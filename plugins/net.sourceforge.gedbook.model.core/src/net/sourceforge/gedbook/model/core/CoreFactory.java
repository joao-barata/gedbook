/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.core.CorePackage
 * @generated
 */
public interface CoreFactory extends EFactory {
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CoreFactory eINSTANCE = net.sourceforge.gedbook.model.core.impl.CoreFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Identifiable Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Identifiable Object</em>'.
   * @generated
   */
  IdentifiableObject createIdentifiableObject();

  /**
   * Returns a new object of class '<em>Annotatable Object</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Annotatable Object</em>'.
   * @generated
   */
  AnnotatableObject createAnnotatableObject();

  /**
   * Returns a new object of class '<em>Note</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Note</em>'.
   * @generated
   */
  Note createNote();

  /**
   * Returns a new object of class '<em>Project</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Project</em>'.
   * @generated
   */
  Project createProject();

  /**
   * Returns a new object of class '<em>Repository Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repository Pkg</em>'.
   * @generated
   */
  RepositoryPkg createRepositoryPkg();

  /**
   * Returns a new object of class '<em>Source Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Source Pkg</em>'.
   * @generated
   */
  SourcePkg createSourcePkg();

  /**
   * Returns a new object of class '<em>Document Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Document Pkg</em>'.
   * @generated
   */
  DocumentPkg createDocumentPkg();

  /**
   * Returns a new object of class '<em>Person Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Person Pkg</em>'.
   * @generated
   */
  PersonPkg createPersonPkg();

  /**
   * Returns a new object of class '<em>Family Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Family Pkg</em>'.
   * @generated
   */
  FamilyPkg createFamilyPkg();

  /**
   * Returns a new object of class '<em>Event Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Event Pkg</em>'.
   * @generated
   */
  EventPkg createEventPkg();

  /**
   * Returns a new object of class '<em>Place Pkg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Place Pkg</em>'.
   * @generated
   */
  PlacePkg createPlacePkg();

  /**
   * Returns a new object of class '<em>Repository</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repository</em>'.
   * @generated
   */
  Repository createRepository();

  /**
   * Returns a new object of class '<em>Source</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Source</em>'.
   * @generated
   */
  Source createSource();

  /**
   * Returns a new object of class '<em>Source Citation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Source Citation</em>'.
   * @generated
   */
  SourceCitation createSourceCitation();

  /**
   * Returns a new object of class '<em>Document</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Document</em>'.
   * @generated
   */
  Document createDocument();

  /**
   * Returns a new object of class '<em>Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Event</em>'.
   * @generated
   */
  Event createEvent();

  /**
   * Returns a new object of class '<em>Person Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Person Event</em>'.
   * @generated
   */
  PersonEvent createPersonEvent();

  /**
   * Returns a new object of class '<em>Family Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Family Event</em>'.
   * @generated
   */
  FamilyEvent createFamilyEvent();

  /**
   * Returns a new object of class '<em>Date Wrapper</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Date Wrapper</em>'.
   * @generated
   */
  DateWrapper createDateWrapper();

  /**
   * Returns a new object of class '<em>Birth</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Birth</em>'.
   * @generated
   */
  Birth createBirth();

  /**
   * Returns a new object of class '<em>Batism</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Batism</em>'.
   * @generated
   */
  Batism createBatism();

  /**
   * Returns a new object of class '<em>Marriage</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Marriage</em>'.
   * @generated
   */
  Marriage createMarriage();

  /**
   * Returns a new object of class '<em>Banns</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Banns</em>'.
   * @generated
   */
  Banns createBanns();

  /**
   * Returns a new object of class '<em>Death</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Death</em>'.
   * @generated
   */
  Death createDeath();

  /**
   * Returns a new object of class '<em>Burial</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Burial</em>'.
   * @generated
   */
  Burial createBurial();

  /**
   * Returns a new object of class '<em>Will</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Will</em>'.
   * @generated
   */
  Will createWill();

  /**
   * Returns a new object of class '<em>Occupation</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Occupation</em>'.
   * @generated
   */
  Occupation createOccupation();

  /**
   * Returns a new object of class '<em>Divorce</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Divorce</em>'.
   * @generated
   */
  Divorce createDivorce();

  /**
   * Returns a new object of class '<em>Anulment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Anulment</em>'.
   * @generated
   */
  Anulment createAnulment();

  /**
   * Returns a new object of class '<em>Title</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Title</em>'.
   * @generated
   */
  Title createTitle();

  /**
   * Returns a new object of class '<em>Nationality</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nationality</em>'.
   * @generated
   */
  Nationality createNationality();

  /**
   * Returns a new object of class '<em>Social Security Number</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Social Security Number</em>'.
   * @generated
   */
  SocialSecurityNumber createSocialSecurityNumber();

  /**
   * Returns a new object of class '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Property</em>'.
   * @generated
   */
  Property createProperty();

  /**
   * Returns a new object of class '<em>Residence</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Residence</em>'.
   * @generated
   */
  Residence createResidence();

  /**
   * Returns a new object of class '<em>Ordination</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Ordination</em>'.
   * @generated
   */
  Ordination createOrdination();

  /**
   * Returns a new object of class '<em>Retirement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Retirement</em>'.
   * @generated
   */
  Retirement createRetirement();

  /**
   * Returns a new object of class '<em>Census</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Census</em>'.
   * @generated
   */
  Census createCensus();

  /**
   * Returns a new object of class '<em>Person</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Person</em>'.
   * @generated
   */
  Person createPerson();

  /**
   * Returns a new object of class '<em>Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Name</em>'.
   * @generated
   */
  Name createName();

  /**
   * Returns a new object of class '<em>Place</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Place</em>'.
   * @generated
   */
  Place createPlace();

  /**
   * Returns a new object of class '<em>Family</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Family</em>'.
   * @generated
   */
  Family createFamily();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  CorePackage getCorePackage();

} //CoreFactory
