/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getNames <em>Names</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getSex <em>Sex</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getBirth <em>Birth</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getBatism <em>Batism</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getDeath <em>Death</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getWill <em>Will</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getBurial <em>Burial</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getOccupations <em>Occupations</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getResidences <em>Residences</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getTitles <em>Titles</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getFather <em>Father</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Person#getMother <em>Mother</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Names</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Names</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Names</em>' containment reference.
   * @see #setNames(Name)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Names()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  Name getNames();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getNames <em>Names</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Names</em>' containment reference.
   * @see #getNames()
   * @generated
   */
  void setNames(Name value);

  /**
   * Returns the value of the '<em><b>Sex</b></em>' attribute.
   * The literals are from the enumeration {@link net.sourceforge.gedbook.model.core.SexKind}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sex</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sex</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.SexKind
   * @see #setSex(SexKind)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Sex()
   * @model
   * @generated
   */
  SexKind getSex();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getSex <em>Sex</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sex</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.SexKind
   * @see #getSex()
   * @generated
   */
  void setSex(SexKind value);

  /**
   * Returns the value of the '<em><b>Birth</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Birth</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Birth</em>' reference.
   * @see #setBirth(Birth)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Birth()
   * @model
   * @generated
   */
  Birth getBirth();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getBirth <em>Birth</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Birth</em>' reference.
   * @see #getBirth()
   * @generated
   */
  void setBirth(Birth value);

  /**
   * Returns the value of the '<em><b>Batism</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Batism</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Batism</em>' reference.
   * @see #setBatism(Batism)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Batism()
   * @model
   * @generated
   */
  Batism getBatism();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getBatism <em>Batism</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Batism</em>' reference.
   * @see #getBatism()
   * @generated
   */
  void setBatism(Batism value);

  /**
   * Returns the value of the '<em><b>Death</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Death</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Death</em>' reference.
   * @see #setDeath(Death)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Death()
   * @model
   * @generated
   */
  Death getDeath();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getDeath <em>Death</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Death</em>' reference.
   * @see #getDeath()
   * @generated
   */
  void setDeath(Death value);

  /**
   * Returns the value of the '<em><b>Will</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Will</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Will</em>' reference.
   * @see #setWill(Will)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Will()
   * @model
   * @generated
   */
  Will getWill();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getWill <em>Will</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Will</em>' reference.
   * @see #getWill()
   * @generated
   */
  void setWill(Will value);

  /**
   * Returns the value of the '<em><b>Burial</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Burial</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Burial</em>' reference.
   * @see #setBurial(Burial)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Burial()
   * @model
   * @generated
   */
  Burial getBurial();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getBurial <em>Burial</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Burial</em>' reference.
   * @see #getBurial()
   * @generated
   */
  void setBurial(Burial value);

  /**
   * Returns the value of the '<em><b>Occupations</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Occupation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Occupations</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Occupations</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Occupations()
   * @model
   * @generated
   */
  EList<Occupation> getOccupations();

  /**
   * Returns the value of the '<em><b>Residences</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Residence}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Residences</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Residences</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Residences()
   * @model
   * @generated
   */
  EList<Residence> getResidences();

  /**
   * Returns the value of the '<em><b>Titles</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Title}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Titles</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Titles</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Titles()
   * @model
   * @generated
   */
  EList<Title> getTitles();

  /**
   * Returns the value of the '<em><b>Father</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Father</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Father</em>' reference.
   * @see #setFather(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Father()
   * @model
   * @generated
   */
  Person getFather();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getFather <em>Father</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Father</em>' reference.
   * @see #getFather()
   * @generated
   */
  void setFather(Person value);

  /**
   * Returns the value of the '<em><b>Mother</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mother</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mother</em>' reference.
   * @see #setMother(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPerson_Mother()
   * @model
   * @generated
   */
  Person getMother();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Person#getMother <em>Mother</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mother</em>' reference.
   * @see #getMother()
   * @generated
   */
  void setMother(Person value);

} // Person
