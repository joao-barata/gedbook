/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Death</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getDeath()
 * @model
 * @generated
 */
public interface Death extends PersonEvent {

} // Death
