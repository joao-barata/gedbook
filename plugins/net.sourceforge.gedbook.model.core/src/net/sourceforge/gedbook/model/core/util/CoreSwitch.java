/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.util;

import net.sourceforge.gedbook.model.core.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.core.CorePackage
 * @generated
 */
public class CoreSwitch<T> extends Switch<T> {
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static CorePackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CoreSwitch() {
    if (modelPackage == null) {
      modelPackage = CorePackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage) {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject) {
    switch (classifierID) {
      case CorePackage.IDENTIFIABLE_OBJECT: {
        IdentifiableObject identifiableObject = (IdentifiableObject)theEObject;
        T result = caseIdentifiableObject(identifiableObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.ANNOTATABLE_OBJECT: {
        AnnotatableObject annotatableObject = (AnnotatableObject)theEObject;
        T result = caseAnnotatableObject(annotatableObject);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.NOTE: {
        Note note = (Note)theEObject;
        T result = caseNote(note);
        if (result == null) result = caseIdentifiableObject(note);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PROJECT: {
        Project project = (Project)theEObject;
        T result = caseProject(project);
        if (result == null) result = caseIdentifiableObject(project);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.REPOSITORY_PKG: {
        RepositoryPkg repositoryPkg = (RepositoryPkg)theEObject;
        T result = caseRepositoryPkg(repositoryPkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.SOURCE_PKG: {
        SourcePkg sourcePkg = (SourcePkg)theEObject;
        T result = caseSourcePkg(sourcePkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.DOCUMENT_PKG: {
        DocumentPkg documentPkg = (DocumentPkg)theEObject;
        T result = caseDocumentPkg(documentPkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PERSON_PKG: {
        PersonPkg personPkg = (PersonPkg)theEObject;
        T result = casePersonPkg(personPkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.FAMILY_PKG: {
        FamilyPkg familyPkg = (FamilyPkg)theEObject;
        T result = caseFamilyPkg(familyPkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.EVENT_PKG: {
        EventPkg eventPkg = (EventPkg)theEObject;
        T result = caseEventPkg(eventPkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PLACE_PKG: {
        PlacePkg placePkg = (PlacePkg)theEObject;
        T result = casePlacePkg(placePkg);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.REPOSITORY: {
        Repository repository = (Repository)theEObject;
        T result = caseRepository(repository);
        if (result == null) result = caseIdentifiableObject(repository);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.SOURCE: {
        Source source = (Source)theEObject;
        T result = caseSource(source);
        if (result == null) result = caseIdentifiableObject(source);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.SOURCE_CITATION: {
        SourceCitation sourceCitation = (SourceCitation)theEObject;
        T result = caseSourceCitation(sourceCitation);
        if (result == null) result = caseIdentifiableObject(sourceCitation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.DOCUMENT: {
        Document document = (Document)theEObject;
        T result = caseDocument(document);
        if (result == null) result = caseIdentifiableObject(document);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.FAMILY: {
        Family family = (Family)theEObject;
        T result = caseFamily(family);
        if (result == null) result = caseIdentifiableObject(family);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.EVENT: {
        Event event = (Event)theEObject;
        T result = caseEvent(event);
        if (result == null) result = caseIdentifiableObject(event);
        if (result == null) result = caseAnnotatableObject(event);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PERSON_EVENT: {
        PersonEvent personEvent = (PersonEvent)theEObject;
        T result = casePersonEvent(personEvent);
        if (result == null) result = caseEvent(personEvent);
        if (result == null) result = caseIdentifiableObject(personEvent);
        if (result == null) result = caseAnnotatableObject(personEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.FAMILY_EVENT: {
        FamilyEvent familyEvent = (FamilyEvent)theEObject;
        T result = caseFamilyEvent(familyEvent);
        if (result == null) result = caseEvent(familyEvent);
        if (result == null) result = caseIdentifiableObject(familyEvent);
        if (result == null) result = caseAnnotatableObject(familyEvent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.DATE_WRAPPER: {
        DateWrapper dateWrapper = (DateWrapper)theEObject;
        T result = caseDateWrapper(dateWrapper);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.BIRTH: {
        Birth birth = (Birth)theEObject;
        T result = caseBirth(birth);
        if (result == null) result = casePersonEvent(birth);
        if (result == null) result = caseEvent(birth);
        if (result == null) result = caseIdentifiableObject(birth);
        if (result == null) result = caseAnnotatableObject(birth);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.BATISM: {
        Batism batism = (Batism)theEObject;
        T result = caseBatism(batism);
        if (result == null) result = casePersonEvent(batism);
        if (result == null) result = caseEvent(batism);
        if (result == null) result = caseIdentifiableObject(batism);
        if (result == null) result = caseAnnotatableObject(batism);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.DEATH: {
        Death death = (Death)theEObject;
        T result = caseDeath(death);
        if (result == null) result = casePersonEvent(death);
        if (result == null) result = caseEvent(death);
        if (result == null) result = caseIdentifiableObject(death);
        if (result == null) result = caseAnnotatableObject(death);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.BURIAL: {
        Burial burial = (Burial)theEObject;
        T result = caseBurial(burial);
        if (result == null) result = casePersonEvent(burial);
        if (result == null) result = caseEvent(burial);
        if (result == null) result = caseIdentifiableObject(burial);
        if (result == null) result = caseAnnotatableObject(burial);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.WILL: {
        Will will = (Will)theEObject;
        T result = caseWill(will);
        if (result == null) result = casePersonEvent(will);
        if (result == null) result = caseEvent(will);
        if (result == null) result = caseIdentifiableObject(will);
        if (result == null) result = caseAnnotatableObject(will);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.OCCUPATION: {
        Occupation occupation = (Occupation)theEObject;
        T result = caseOccupation(occupation);
        if (result == null) result = casePersonEvent(occupation);
        if (result == null) result = caseEvent(occupation);
        if (result == null) result = caseIdentifiableObject(occupation);
        if (result == null) result = caseAnnotatableObject(occupation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.TITLE: {
        Title title = (Title)theEObject;
        T result = caseTitle(title);
        if (result == null) result = casePersonEvent(title);
        if (result == null) result = caseEvent(title);
        if (result == null) result = caseIdentifiableObject(title);
        if (result == null) result = caseAnnotatableObject(title);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.NATIONALITY: {
        Nationality nationality = (Nationality)theEObject;
        T result = caseNationality(nationality);
        if (result == null) result = casePersonEvent(nationality);
        if (result == null) result = caseEvent(nationality);
        if (result == null) result = caseIdentifiableObject(nationality);
        if (result == null) result = caseAnnotatableObject(nationality);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.SOCIAL_SECURITY_NUMBER: {
        SocialSecurityNumber socialSecurityNumber = (SocialSecurityNumber)theEObject;
        T result = caseSocialSecurityNumber(socialSecurityNumber);
        if (result == null) result = casePersonEvent(socialSecurityNumber);
        if (result == null) result = caseEvent(socialSecurityNumber);
        if (result == null) result = caseIdentifiableObject(socialSecurityNumber);
        if (result == null) result = caseAnnotatableObject(socialSecurityNumber);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PROPERTY: {
        Property property = (Property)theEObject;
        T result = caseProperty(property);
        if (result == null) result = casePersonEvent(property);
        if (result == null) result = caseEvent(property);
        if (result == null) result = caseIdentifiableObject(property);
        if (result == null) result = caseAnnotatableObject(property);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.RESIDENCE: {
        Residence residence = (Residence)theEObject;
        T result = caseResidence(residence);
        if (result == null) result = casePersonEvent(residence);
        if (result == null) result = caseEvent(residence);
        if (result == null) result = caseIdentifiableObject(residence);
        if (result == null) result = caseAnnotatableObject(residence);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.ORDINATION: {
        Ordination ordination = (Ordination)theEObject;
        T result = caseOrdination(ordination);
        if (result == null) result = casePersonEvent(ordination);
        if (result == null) result = caseEvent(ordination);
        if (result == null) result = caseIdentifiableObject(ordination);
        if (result == null) result = caseAnnotatableObject(ordination);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.RETIREMENT: {
        Retirement retirement = (Retirement)theEObject;
        T result = caseRetirement(retirement);
        if (result == null) result = casePersonEvent(retirement);
        if (result == null) result = caseEvent(retirement);
        if (result == null) result = caseIdentifiableObject(retirement);
        if (result == null) result = caseAnnotatableObject(retirement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.CENSUS: {
        Census census = (Census)theEObject;
        T result = caseCensus(census);
        if (result == null) result = caseFamilyEvent(census);
        if (result == null) result = caseEvent(census);
        if (result == null) result = caseIdentifiableObject(census);
        if (result == null) result = caseAnnotatableObject(census);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.MARRIAGE: {
        Marriage marriage = (Marriage)theEObject;
        T result = caseMarriage(marriage);
        if (result == null) result = caseFamilyEvent(marriage);
        if (result == null) result = caseEvent(marriage);
        if (result == null) result = caseIdentifiableObject(marriage);
        if (result == null) result = caseAnnotatableObject(marriage);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.BANNS: {
        Banns banns = (Banns)theEObject;
        T result = caseBanns(banns);
        if (result == null) result = caseFamilyEvent(banns);
        if (result == null) result = caseEvent(banns);
        if (result == null) result = caseIdentifiableObject(banns);
        if (result == null) result = caseAnnotatableObject(banns);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.DIVORCE: {
        Divorce divorce = (Divorce)theEObject;
        T result = caseDivorce(divorce);
        if (result == null) result = caseFamilyEvent(divorce);
        if (result == null) result = caseEvent(divorce);
        if (result == null) result = caseIdentifiableObject(divorce);
        if (result == null) result = caseAnnotatableObject(divorce);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.ANULMENT: {
        Anulment anulment = (Anulment)theEObject;
        T result = caseAnulment(anulment);
        if (result == null) result = caseFamilyEvent(anulment);
        if (result == null) result = caseEvent(anulment);
        if (result == null) result = caseIdentifiableObject(anulment);
        if (result == null) result = caseAnnotatableObject(anulment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PERSON: {
        Person person = (Person)theEObject;
        T result = casePerson(person);
        if (result == null) result = caseIdentifiableObject(person);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.NAME: {
        Name name = (Name)theEObject;
        T result = caseName(name);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case CorePackage.PLACE: {
        Place place = (Place)theEObject;
        T result = casePlace(place);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Identifiable Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Identifiable Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIdentifiableObject(IdentifiableObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Annotatable Object</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Annotatable Object</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnnotatableObject(AnnotatableObject object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Note</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Note</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNote(Note object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Project</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Project</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProject(Project object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Repository Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Repository Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRepositoryPkg(RepositoryPkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSourcePkg(SourcePkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Document Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Document Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDocumentPkg(DocumentPkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Person Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Person Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePersonPkg(PersonPkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Family Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Family Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFamilyPkg(FamilyPkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Event Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Event Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEventPkg(EventPkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Place Pkg</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Place Pkg</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePlacePkg(PlacePkg object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Repository</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Repository</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRepository(Repository object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSource(Source object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source Citation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source Citation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSourceCitation(SourceCitation object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Document</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Document</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDocument(Document object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseEvent(Event object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Person Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Person Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePersonEvent(PersonEvent object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Family Event</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Family Event</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFamilyEvent(FamilyEvent object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Date Wrapper</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Date Wrapper</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDateWrapper(DateWrapper object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Birth</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Birth</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBirth(Birth object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Batism</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Batism</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBatism(Batism object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Marriage</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Marriage</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMarriage(Marriage object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Banns</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Banns</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBanns(Banns object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Death</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Death</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeath(Death object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Burial</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Burial</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBurial(Burial object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Will</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Will</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWill(Will object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Occupation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Occupation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOccupation(Occupation object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Divorce</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Divorce</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDivorce(Divorce object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Anulment</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Anulment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAnulment(Anulment object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Title</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Title</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTitle(Title object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nationality</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nationality</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNationality(Nationality object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Social Security Number</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Social Security Number</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSocialSecurityNumber(SocialSecurityNumber object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Property</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseProperty(Property object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Residence</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Residence</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseResidence(Residence object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Ordination</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Ordination</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOrdination(Ordination object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Retirement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Retirement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRetirement(Retirement object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Census</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Census</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCensus(Census object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Person</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Person</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePerson(Person object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Name</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Name</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseName(Name object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Place</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Place</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePlace(Place object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Family</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Family</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFamily(Family object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object) {
    return null;
  }

} //CoreSwitch
