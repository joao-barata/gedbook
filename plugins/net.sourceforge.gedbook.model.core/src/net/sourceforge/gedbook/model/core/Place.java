/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Place#getPays <em>Pays</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Place#getRegion <em>Region</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Place#getDepartement <em>Departement</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Place#getCommune <em>Commune</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends EObject {
  /**
   * Returns the value of the '<em><b>Pays</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pays</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pays</em>' attribute.
   * @see #setPays(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPlace_Pays()
   * @model
   * @generated
   */
  String getPays();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Place#getPays <em>Pays</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pays</em>' attribute.
   * @see #getPays()
   * @generated
   */
  void setPays(String value);

  /**
   * Returns the value of the '<em><b>Region</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Region</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Region</em>' attribute.
   * @see #setRegion(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPlace_Region()
   * @model
   * @generated
   */
  String getRegion();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Place#getRegion <em>Region</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Region</em>' attribute.
   * @see #getRegion()
   * @generated
   */
  void setRegion(String value);

  /**
   * Returns the value of the '<em><b>Departement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Departement</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Departement</em>' attribute.
   * @see #setDepartement(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPlace_Departement()
   * @model
   * @generated
   */
  String getDepartement();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Place#getDepartement <em>Departement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Departement</em>' attribute.
   * @see #getDepartement()
   * @generated
   */
  void setDepartement(String value);

  /**
   * Returns the value of the '<em><b>Commune</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Commune</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Commune</em>' attribute.
   * @see #setCommune(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getPlace_Commune()
   * @model
   * @generated
   */
  String getCommune();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Place#getCommune <em>Commune</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Commune</em>' attribute.
   * @see #getCommune()
   * @generated
   */
  void setCommune(String value);

} // Place
