/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Source#getTitle <em>Title</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Source#getArticle <em>Article</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Source#getAuthor <em>Author</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Source#getOrigin <em>Origin</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Source#getStorage <em>Storage</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getSource()
 * @model
 * @generated
 */
public interface Source extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Title</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Title</em>' attribute.
   * @see #setTitle(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSource_Title()
   * @model
   * @generated
   */
  String getTitle();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Source#getTitle <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Title</em>' attribute.
   * @see #getTitle()
   * @generated
   */
  void setTitle(String value);

  /**
   * Returns the value of the '<em><b>Article</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Article</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Article</em>' attribute.
   * @see #setArticle(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSource_Article()
   * @model
   * @generated
   */
  String getArticle();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Source#getArticle <em>Article</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Article</em>' attribute.
   * @see #getArticle()
   * @generated
   */
  void setArticle(String value);

  /**
   * Returns the value of the '<em><b>Author</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Author</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Author</em>' attribute.
   * @see #setAuthor(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSource_Author()
   * @model
   * @generated
   */
  String getAuthor();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Source#getAuthor <em>Author</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Author</em>' attribute.
   * @see #getAuthor()
   * @generated
   */
  void setAuthor(String value);

  /**
   * Returns the value of the '<em><b>Origin</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Origin</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Origin</em>' reference.
   * @see #setOrigin(Repository)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSource_Origin()
   * @model
   * @generated
   */
  Repository getOrigin();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Source#getOrigin <em>Origin</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Origin</em>' reference.
   * @see #getOrigin()
   * @generated
   */
  void setOrigin(Repository value);

  /**
   * Returns the value of the '<em><b>Storage</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Storage</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Storage</em>' reference.
   * @see #setStorage(Repository)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSource_Storage()
   * @model
   * @generated
   */
  Repository getStorage();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Source#getStorage <em>Storage</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Storage</em>' reference.
   * @see #getStorage()
   * @generated
   */
  void setStorage(Repository value);

} // Source
