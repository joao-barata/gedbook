/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Citation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.SourceCitation#getPage <em>Page</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.SourceCitation#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getSourceCitation()
 * @model
 * @generated
 */
public interface SourceCitation extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Page</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Page</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Page</em>' attribute.
   * @see #setPage(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSourceCitation_Page()
   * @model
   * @generated
   */
  String getPage();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.SourceCitation#getPage <em>Page</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Page</em>' attribute.
   * @see #getPage()
   * @generated
   */
  void setPage(String value);

  /**
   * Returns the value of the '<em><b>Source</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source</em>' reference.
   * @see #setSource(Document)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSourceCitation_Source()
   * @model
   * @generated
   */
  Document getSource();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.SourceCitation#getSource <em>Source</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source</em>' reference.
   * @see #getSource()
   * @generated
   */
  void setSource(Document value);

} // SourceCitation
