/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.DocumentPkg;
import net.sourceforge.gedbook.model.core.EventPkg;
import net.sourceforge.gedbook.model.core.FamilyPkg;
import net.sourceforge.gedbook.model.core.PersonPkg;
import net.sourceforge.gedbook.model.core.PlacePkg;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.RepositoryPkg;
import net.sourceforge.gedbook.model.core.SourcePkg;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedRepositoryPkg <em>Owned Repository Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedSourcePkg <em>Owned Source Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedDocumentPkg <em>Owned Document Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedPersonPkg <em>Owned Person Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedFamilyPkg <em>Owned Family Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedEventPkg <em>Owned Event Pkg</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl#getOwnedPlacePkg <em>Owned Place Pkg</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProjectImpl extends IdentifiableObjectImpl implements Project {
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getOwnedRepositoryPkg() <em>Owned Repository Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedRepositoryPkg()
   * @generated
   * @ordered
   */
  protected RepositoryPkg ownedRepositoryPkg;

  /**
   * The cached value of the '{@link #getOwnedSourcePkg() <em>Owned Source Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedSourcePkg()
   * @generated
   * @ordered
   */
  protected SourcePkg ownedSourcePkg;

  /**
   * The cached value of the '{@link #getOwnedDocumentPkg() <em>Owned Document Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedDocumentPkg()
   * @generated
   * @ordered
   */
  protected DocumentPkg ownedDocumentPkg;

  /**
   * The cached value of the '{@link #getOwnedPersonPkg() <em>Owned Person Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedPersonPkg()
   * @generated
   * @ordered
   */
  protected PersonPkg ownedPersonPkg;

  /**
   * The cached value of the '{@link #getOwnedFamilyPkg() <em>Owned Family Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedFamilyPkg()
   * @generated
   * @ordered
   */
  protected FamilyPkg ownedFamilyPkg;

  /**
   * The cached value of the '{@link #getOwnedEventPkg() <em>Owned Event Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedEventPkg()
   * @generated
   * @ordered
   */
  protected EventPkg ownedEventPkg;

  /**
   * The cached value of the '{@link #getOwnedPlacePkg() <em>Owned Place Pkg</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedPlacePkg()
   * @generated
   * @ordered
   */
  protected PlacePkg ownedPlacePkg;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ProjectImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.PROJECT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription() {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription) {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepositoryPkg getOwnedRepositoryPkg() {
    if (ownedRepositoryPkg != null && ownedRepositoryPkg.eIsProxy()) {
      InternalEObject oldOwnedRepositoryPkg = (InternalEObject)ownedRepositoryPkg;
      ownedRepositoryPkg = (RepositoryPkg)eResolveProxy(oldOwnedRepositoryPkg);
      if (ownedRepositoryPkg != oldOwnedRepositoryPkg) {
        InternalEObject newOwnedRepositoryPkg = (InternalEObject)ownedRepositoryPkg;
        NotificationChain msgs = oldOwnedRepositoryPkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_REPOSITORY_PKG, null, null);
        if (newOwnedRepositoryPkg.eInternalContainer() == null) {
          msgs = newOwnedRepositoryPkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_REPOSITORY_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_REPOSITORY_PKG, oldOwnedRepositoryPkg, ownedRepositoryPkg));
      }
    }
    return ownedRepositoryPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepositoryPkg basicGetOwnedRepositoryPkg() {
    return ownedRepositoryPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedRepositoryPkg(RepositoryPkg newOwnedRepositoryPkg, NotificationChain msgs) {
    RepositoryPkg oldOwnedRepositoryPkg = ownedRepositoryPkg;
    ownedRepositoryPkg = newOwnedRepositoryPkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_REPOSITORY_PKG, oldOwnedRepositoryPkg, newOwnedRepositoryPkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedRepositoryPkg(RepositoryPkg newOwnedRepositoryPkg) {
    if (newOwnedRepositoryPkg != ownedRepositoryPkg) {
      NotificationChain msgs = null;
      if (ownedRepositoryPkg != null)
        msgs = ((InternalEObject)ownedRepositoryPkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_REPOSITORY_PKG, null, msgs);
      if (newOwnedRepositoryPkg != null)
        msgs = ((InternalEObject)newOwnedRepositoryPkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_REPOSITORY_PKG, null, msgs);
      msgs = basicSetOwnedRepositoryPkg(newOwnedRepositoryPkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_REPOSITORY_PKG, newOwnedRepositoryPkg, newOwnedRepositoryPkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourcePkg getOwnedSourcePkg() {
    if (ownedSourcePkg != null && ownedSourcePkg.eIsProxy()) {
      InternalEObject oldOwnedSourcePkg = (InternalEObject)ownedSourcePkg;
      ownedSourcePkg = (SourcePkg)eResolveProxy(oldOwnedSourcePkg);
      if (ownedSourcePkg != oldOwnedSourcePkg) {
        InternalEObject newOwnedSourcePkg = (InternalEObject)ownedSourcePkg;
        NotificationChain msgs = oldOwnedSourcePkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_SOURCE_PKG, null, null);
        if (newOwnedSourcePkg.eInternalContainer() == null) {
          msgs = newOwnedSourcePkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_SOURCE_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_SOURCE_PKG, oldOwnedSourcePkg, ownedSourcePkg));
      }
    }
    return ownedSourcePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourcePkg basicGetOwnedSourcePkg() {
    return ownedSourcePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedSourcePkg(SourcePkg newOwnedSourcePkg, NotificationChain msgs) {
    SourcePkg oldOwnedSourcePkg = ownedSourcePkg;
    ownedSourcePkg = newOwnedSourcePkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_SOURCE_PKG, oldOwnedSourcePkg, newOwnedSourcePkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedSourcePkg(SourcePkg newOwnedSourcePkg) {
    if (newOwnedSourcePkg != ownedSourcePkg) {
      NotificationChain msgs = null;
      if (ownedSourcePkg != null)
        msgs = ((InternalEObject)ownedSourcePkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_SOURCE_PKG, null, msgs);
      if (newOwnedSourcePkg != null)
        msgs = ((InternalEObject)newOwnedSourcePkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_SOURCE_PKG, null, msgs);
      msgs = basicSetOwnedSourcePkg(newOwnedSourcePkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_SOURCE_PKG, newOwnedSourcePkg, newOwnedSourcePkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentPkg getOwnedDocumentPkg() {
    if (ownedDocumentPkg != null && ownedDocumentPkg.eIsProxy()) {
      InternalEObject oldOwnedDocumentPkg = (InternalEObject)ownedDocumentPkg;
      ownedDocumentPkg = (DocumentPkg)eResolveProxy(oldOwnedDocumentPkg);
      if (ownedDocumentPkg != oldOwnedDocumentPkg) {
        InternalEObject newOwnedDocumentPkg = (InternalEObject)ownedDocumentPkg;
        NotificationChain msgs = oldOwnedDocumentPkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_DOCUMENT_PKG, null, null);
        if (newOwnedDocumentPkg.eInternalContainer() == null) {
          msgs = newOwnedDocumentPkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_DOCUMENT_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_DOCUMENT_PKG, oldOwnedDocumentPkg, ownedDocumentPkg));
      }
    }
    return ownedDocumentPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentPkg basicGetOwnedDocumentPkg() {
    return ownedDocumentPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedDocumentPkg(DocumentPkg newOwnedDocumentPkg, NotificationChain msgs) {
    DocumentPkg oldOwnedDocumentPkg = ownedDocumentPkg;
    ownedDocumentPkg = newOwnedDocumentPkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_DOCUMENT_PKG, oldOwnedDocumentPkg, newOwnedDocumentPkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedDocumentPkg(DocumentPkg newOwnedDocumentPkg) {
    if (newOwnedDocumentPkg != ownedDocumentPkg) {
      NotificationChain msgs = null;
      if (ownedDocumentPkg != null)
        msgs = ((InternalEObject)ownedDocumentPkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_DOCUMENT_PKG, null, msgs);
      if (newOwnedDocumentPkg != null)
        msgs = ((InternalEObject)newOwnedDocumentPkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_DOCUMENT_PKG, null, msgs);
      msgs = basicSetOwnedDocumentPkg(newOwnedDocumentPkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_DOCUMENT_PKG, newOwnedDocumentPkg, newOwnedDocumentPkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PersonPkg getOwnedPersonPkg() {
    if (ownedPersonPkg != null && ownedPersonPkg.eIsProxy()) {
      InternalEObject oldOwnedPersonPkg = (InternalEObject)ownedPersonPkg;
      ownedPersonPkg = (PersonPkg)eResolveProxy(oldOwnedPersonPkg);
      if (ownedPersonPkg != oldOwnedPersonPkg) {
        InternalEObject newOwnedPersonPkg = (InternalEObject)ownedPersonPkg;
        NotificationChain msgs = oldOwnedPersonPkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PERSON_PKG, null, null);
        if (newOwnedPersonPkg.eInternalContainer() == null) {
          msgs = newOwnedPersonPkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PERSON_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_PERSON_PKG, oldOwnedPersonPkg, ownedPersonPkg));
      }
    }
    return ownedPersonPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PersonPkg basicGetOwnedPersonPkg() {
    return ownedPersonPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedPersonPkg(PersonPkg newOwnedPersonPkg, NotificationChain msgs) {
    PersonPkg oldOwnedPersonPkg = ownedPersonPkg;
    ownedPersonPkg = newOwnedPersonPkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_PERSON_PKG, oldOwnedPersonPkg, newOwnedPersonPkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedPersonPkg(PersonPkg newOwnedPersonPkg) {
    if (newOwnedPersonPkg != ownedPersonPkg) {
      NotificationChain msgs = null;
      if (ownedPersonPkg != null)
        msgs = ((InternalEObject)ownedPersonPkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PERSON_PKG, null, msgs);
      if (newOwnedPersonPkg != null)
        msgs = ((InternalEObject)newOwnedPersonPkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PERSON_PKG, null, msgs);
      msgs = basicSetOwnedPersonPkg(newOwnedPersonPkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_PERSON_PKG, newOwnedPersonPkg, newOwnedPersonPkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FamilyPkg getOwnedFamilyPkg() {
    if (ownedFamilyPkg != null && ownedFamilyPkg.eIsProxy()) {
      InternalEObject oldOwnedFamilyPkg = (InternalEObject)ownedFamilyPkg;
      ownedFamilyPkg = (FamilyPkg)eResolveProxy(oldOwnedFamilyPkg);
      if (ownedFamilyPkg != oldOwnedFamilyPkg) {
        InternalEObject newOwnedFamilyPkg = (InternalEObject)ownedFamilyPkg;
        NotificationChain msgs = oldOwnedFamilyPkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_FAMILY_PKG, null, null);
        if (newOwnedFamilyPkg.eInternalContainer() == null) {
          msgs = newOwnedFamilyPkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_FAMILY_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_FAMILY_PKG, oldOwnedFamilyPkg, ownedFamilyPkg));
      }
    }
    return ownedFamilyPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FamilyPkg basicGetOwnedFamilyPkg() {
    return ownedFamilyPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedFamilyPkg(FamilyPkg newOwnedFamilyPkg, NotificationChain msgs) {
    FamilyPkg oldOwnedFamilyPkg = ownedFamilyPkg;
    ownedFamilyPkg = newOwnedFamilyPkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_FAMILY_PKG, oldOwnedFamilyPkg, newOwnedFamilyPkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedFamilyPkg(FamilyPkg newOwnedFamilyPkg) {
    if (newOwnedFamilyPkg != ownedFamilyPkg) {
      NotificationChain msgs = null;
      if (ownedFamilyPkg != null)
        msgs = ((InternalEObject)ownedFamilyPkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_FAMILY_PKG, null, msgs);
      if (newOwnedFamilyPkg != null)
        msgs = ((InternalEObject)newOwnedFamilyPkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_FAMILY_PKG, null, msgs);
      msgs = basicSetOwnedFamilyPkg(newOwnedFamilyPkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_FAMILY_PKG, newOwnedFamilyPkg, newOwnedFamilyPkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventPkg getOwnedEventPkg() {
    if (ownedEventPkg != null && ownedEventPkg.eIsProxy()) {
      InternalEObject oldOwnedEventPkg = (InternalEObject)ownedEventPkg;
      ownedEventPkg = (EventPkg)eResolveProxy(oldOwnedEventPkg);
      if (ownedEventPkg != oldOwnedEventPkg) {
        InternalEObject newOwnedEventPkg = (InternalEObject)ownedEventPkg;
        NotificationChain msgs = oldOwnedEventPkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_EVENT_PKG, null, null);
        if (newOwnedEventPkg.eInternalContainer() == null) {
          msgs = newOwnedEventPkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_EVENT_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_EVENT_PKG, oldOwnedEventPkg, ownedEventPkg));
      }
    }
    return ownedEventPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventPkg basicGetOwnedEventPkg() {
    return ownedEventPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedEventPkg(EventPkg newOwnedEventPkg, NotificationChain msgs) {
    EventPkg oldOwnedEventPkg = ownedEventPkg;
    ownedEventPkg = newOwnedEventPkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_EVENT_PKG, oldOwnedEventPkg, newOwnedEventPkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedEventPkg(EventPkg newOwnedEventPkg) {
    if (newOwnedEventPkg != ownedEventPkg) {
      NotificationChain msgs = null;
      if (ownedEventPkg != null)
        msgs = ((InternalEObject)ownedEventPkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_EVENT_PKG, null, msgs);
      if (newOwnedEventPkg != null)
        msgs = ((InternalEObject)newOwnedEventPkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_EVENT_PKG, null, msgs);
      msgs = basicSetOwnedEventPkg(newOwnedEventPkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_EVENT_PKG, newOwnedEventPkg, newOwnedEventPkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlacePkg getOwnedPlacePkg() {
    if (ownedPlacePkg != null && ownedPlacePkg.eIsProxy()) {
      InternalEObject oldOwnedPlacePkg = (InternalEObject)ownedPlacePkg;
      ownedPlacePkg = (PlacePkg)eResolveProxy(oldOwnedPlacePkg);
      if (ownedPlacePkg != oldOwnedPlacePkg) {
        InternalEObject newOwnedPlacePkg = (InternalEObject)ownedPlacePkg;
        NotificationChain msgs = oldOwnedPlacePkg.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PLACE_PKG, null, null);
        if (newOwnedPlacePkg.eInternalContainer() == null) {
          msgs = newOwnedPlacePkg.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PLACE_PKG, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PROJECT__OWNED_PLACE_PKG, oldOwnedPlacePkg, ownedPlacePkg));
      }
    }
    return ownedPlacePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlacePkg basicGetOwnedPlacePkg() {
    return ownedPlacePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOwnedPlacePkg(PlacePkg newOwnedPlacePkg, NotificationChain msgs) {
    PlacePkg oldOwnedPlacePkg = ownedPlacePkg;
    ownedPlacePkg = newOwnedPlacePkg;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_PLACE_PKG, oldOwnedPlacePkg, newOwnedPlacePkg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOwnedPlacePkg(PlacePkg newOwnedPlacePkg) {
    if (newOwnedPlacePkg != ownedPlacePkg) {
      NotificationChain msgs = null;
      if (ownedPlacePkg != null)
        msgs = ((InternalEObject)ownedPlacePkg).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PLACE_PKG, null, msgs);
      if (newOwnedPlacePkg != null)
        msgs = ((InternalEObject)newOwnedPlacePkg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PROJECT__OWNED_PLACE_PKG, null, msgs);
      msgs = basicSetOwnedPlacePkg(newOwnedPlacePkg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PROJECT__OWNED_PLACE_PKG, newOwnedPlacePkg, newOwnedPlacePkg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.PROJECT__OWNED_REPOSITORY_PKG:
        return basicSetOwnedRepositoryPkg(null, msgs);
      case CorePackage.PROJECT__OWNED_SOURCE_PKG:
        return basicSetOwnedSourcePkg(null, msgs);
      case CorePackage.PROJECT__OWNED_DOCUMENT_PKG:
        return basicSetOwnedDocumentPkg(null, msgs);
      case CorePackage.PROJECT__OWNED_PERSON_PKG:
        return basicSetOwnedPersonPkg(null, msgs);
      case CorePackage.PROJECT__OWNED_FAMILY_PKG:
        return basicSetOwnedFamilyPkg(null, msgs);
      case CorePackage.PROJECT__OWNED_EVENT_PKG:
        return basicSetOwnedEventPkg(null, msgs);
      case CorePackage.PROJECT__OWNED_PLACE_PKG:
        return basicSetOwnedPlacePkg(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.PROJECT__NAME:
        return getName();
      case CorePackage.PROJECT__DESCRIPTION:
        return getDescription();
      case CorePackage.PROJECT__OWNED_REPOSITORY_PKG:
        if (resolve) return getOwnedRepositoryPkg();
        return basicGetOwnedRepositoryPkg();
      case CorePackage.PROJECT__OWNED_SOURCE_PKG:
        if (resolve) return getOwnedSourcePkg();
        return basicGetOwnedSourcePkg();
      case CorePackage.PROJECT__OWNED_DOCUMENT_PKG:
        if (resolve) return getOwnedDocumentPkg();
        return basicGetOwnedDocumentPkg();
      case CorePackage.PROJECT__OWNED_PERSON_PKG:
        if (resolve) return getOwnedPersonPkg();
        return basicGetOwnedPersonPkg();
      case CorePackage.PROJECT__OWNED_FAMILY_PKG:
        if (resolve) return getOwnedFamilyPkg();
        return basicGetOwnedFamilyPkg();
      case CorePackage.PROJECT__OWNED_EVENT_PKG:
        if (resolve) return getOwnedEventPkg();
        return basicGetOwnedEventPkg();
      case CorePackage.PROJECT__OWNED_PLACE_PKG:
        if (resolve) return getOwnedPlacePkg();
        return basicGetOwnedPlacePkg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.PROJECT__NAME:
        setName((String)newValue);
        return;
      case CorePackage.PROJECT__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case CorePackage.PROJECT__OWNED_REPOSITORY_PKG:
        setOwnedRepositoryPkg((RepositoryPkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_SOURCE_PKG:
        setOwnedSourcePkg((SourcePkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_DOCUMENT_PKG:
        setOwnedDocumentPkg((DocumentPkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_PERSON_PKG:
        setOwnedPersonPkg((PersonPkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_FAMILY_PKG:
        setOwnedFamilyPkg((FamilyPkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_EVENT_PKG:
        setOwnedEventPkg((EventPkg)newValue);
        return;
      case CorePackage.PROJECT__OWNED_PLACE_PKG:
        setOwnedPlacePkg((PlacePkg)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.PROJECT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case CorePackage.PROJECT__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case CorePackage.PROJECT__OWNED_REPOSITORY_PKG:
        setOwnedRepositoryPkg((RepositoryPkg)null);
        return;
      case CorePackage.PROJECT__OWNED_SOURCE_PKG:
        setOwnedSourcePkg((SourcePkg)null);
        return;
      case CorePackage.PROJECT__OWNED_DOCUMENT_PKG:
        setOwnedDocumentPkg((DocumentPkg)null);
        return;
      case CorePackage.PROJECT__OWNED_PERSON_PKG:
        setOwnedPersonPkg((PersonPkg)null);
        return;
      case CorePackage.PROJECT__OWNED_FAMILY_PKG:
        setOwnedFamilyPkg((FamilyPkg)null);
        return;
      case CorePackage.PROJECT__OWNED_EVENT_PKG:
        setOwnedEventPkg((EventPkg)null);
        return;
      case CorePackage.PROJECT__OWNED_PLACE_PKG:
        setOwnedPlacePkg((PlacePkg)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.PROJECT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case CorePackage.PROJECT__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case CorePackage.PROJECT__OWNED_REPOSITORY_PKG:
        return ownedRepositoryPkg != null;
      case CorePackage.PROJECT__OWNED_SOURCE_PKG:
        return ownedSourcePkg != null;
      case CorePackage.PROJECT__OWNED_DOCUMENT_PKG:
        return ownedDocumentPkg != null;
      case CorePackage.PROJECT__OWNED_PERSON_PKG:
        return ownedPersonPkg != null;
      case CorePackage.PROJECT__OWNED_FAMILY_PKG:
        return ownedFamilyPkg != null;
      case CorePackage.PROJECT__OWNED_EVENT_PKG:
        return ownedEventPkg != null;
      case CorePackage.PROJECT__OWNED_PLACE_PKG:
        return ownedPlacePkg != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: "); //$NON-NLS-1$
    result.append(name);
    result.append(", description: "); //$NON-NLS-1$
    result.append(description);
    result.append(')');
    return result.toString();
  }

} //ProjectImpl
