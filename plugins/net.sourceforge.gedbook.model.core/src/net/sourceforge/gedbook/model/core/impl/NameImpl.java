/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Name;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getAdoptionname <em>Adoptionname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getBirthname <em>Birthname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getCensusname <em>Censusname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getCurrentname <em>Currentname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getFormalname <em>Formalname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getGivenname <em>Givenname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getMarriedname <em>Marriedname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getNickname <em>Nickname</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getOthername <em>Othername</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.NameImpl#getSurname <em>Surname</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NameImpl extends MinimalEObjectImpl.Container implements Name {
  /**
   * The default value of the '{@link #getAdoptionname() <em>Adoptionname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdoptionname()
   * @generated
   * @ordered
   */
  protected static final String ADOPTIONNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAdoptionname() <em>Adoptionname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdoptionname()
   * @generated
   * @ordered
   */
  protected String adoptionname = ADOPTIONNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getBirthname() <em>Birthname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBirthname()
   * @generated
   * @ordered
   */
  protected static final String BIRTHNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getBirthname() <em>Birthname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBirthname()
   * @generated
   * @ordered
   */
  protected String birthname = BIRTHNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getCensusname() <em>Censusname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCensusname()
   * @generated
   * @ordered
   */
  protected static final String CENSUSNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCensusname() <em>Censusname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCensusname()
   * @generated
   * @ordered
   */
  protected String censusname = CENSUSNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getCurrentname() <em>Currentname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCurrentname()
   * @generated
   * @ordered
   */
  protected static final String CURRENTNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCurrentname() <em>Currentname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCurrentname()
   * @generated
   * @ordered
   */
  protected String currentname = CURRENTNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getFormalname() <em>Formalname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormalname()
   * @generated
   * @ordered
   */
  protected static final String FORMALNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFormalname() <em>Formalname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormalname()
   * @generated
   * @ordered
   */
  protected String formalname = FORMALNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getGivenname() <em>Givenname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGivenname()
   * @generated
   * @ordered
   */
  protected static final String GIVENNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getGivenname() <em>Givenname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGivenname()
   * @generated
   * @ordered
   */
  protected String givenname = GIVENNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getMarriedname() <em>Marriedname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMarriedname()
   * @generated
   * @ordered
   */
  protected static final String MARRIEDNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMarriedname() <em>Marriedname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMarriedname()
   * @generated
   * @ordered
   */
  protected String marriedname = MARRIEDNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNickname()
   * @generated
   * @ordered
   */
  protected static final String NICKNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNickname()
   * @generated
   * @ordered
   */
  protected String nickname = NICKNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getOthername() <em>Othername</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthername()
   * @generated
   * @ordered
   */
  protected static final String OTHERNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOthername() <em>Othername</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOthername()
   * @generated
   * @ordered
   */
  protected String othername = OTHERNAME_EDEFAULT;

  /**
   * The default value of the '{@link #getSurname() <em>Surname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSurname()
   * @generated
   * @ordered
   */
  protected static final String SURNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSurname() <em>Surname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSurname()
   * @generated
   * @ordered
   */
  protected String surname = SURNAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NameImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.NAME;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAdoptionname() {
    return adoptionname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAdoptionname(String newAdoptionname) {
    String oldAdoptionname = adoptionname;
    adoptionname = newAdoptionname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__ADOPTIONNAME, oldAdoptionname, adoptionname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBirthname() {
    return birthname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBirthname(String newBirthname) {
    String oldBirthname = birthname;
    birthname = newBirthname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__BIRTHNAME, oldBirthname, birthname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCensusname() {
    return censusname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCensusname(String newCensusname) {
    String oldCensusname = censusname;
    censusname = newCensusname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__CENSUSNAME, oldCensusname, censusname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCurrentname() {
    return currentname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCurrentname(String newCurrentname) {
    String oldCurrentname = currentname;
    currentname = newCurrentname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__CURRENTNAME, oldCurrentname, currentname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFormalname() {
    return formalname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFormalname(String newFormalname) {
    String oldFormalname = formalname;
    formalname = newFormalname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__FORMALNAME, oldFormalname, formalname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getGivenname() {
    return givenname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGivenname(String newGivenname) {
    String oldGivenname = givenname;
    givenname = newGivenname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__GIVENNAME, oldGivenname, givenname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMarriedname() {
    return marriedname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMarriedname(String newMarriedname) {
    String oldMarriedname = marriedname;
    marriedname = newMarriedname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__MARRIEDNAME, oldMarriedname, marriedname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSurname() {
    return surname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSurname(String newSurname) {
    String oldSurname = surname;
    surname = newSurname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__SURNAME, oldSurname, surname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNickname() {
    return nickname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNickname(String newNickname) {
    String oldNickname = nickname;
    nickname = newNickname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__NICKNAME, oldNickname, nickname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getOthername() {
    return othername;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOthername(String newOthername) {
    String oldOthername = othername;
    othername = newOthername;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.NAME__OTHERNAME, oldOthername, othername));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.NAME__ADOPTIONNAME:
        return getAdoptionname();
      case CorePackage.NAME__BIRTHNAME:
        return getBirthname();
      case CorePackage.NAME__CENSUSNAME:
        return getCensusname();
      case CorePackage.NAME__CURRENTNAME:
        return getCurrentname();
      case CorePackage.NAME__FORMALNAME:
        return getFormalname();
      case CorePackage.NAME__GIVENNAME:
        return getGivenname();
      case CorePackage.NAME__MARRIEDNAME:
        return getMarriedname();
      case CorePackage.NAME__NAME:
        return getName();
      case CorePackage.NAME__NICKNAME:
        return getNickname();
      case CorePackage.NAME__OTHERNAME:
        return getOthername();
      case CorePackage.NAME__SURNAME:
        return getSurname();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.NAME__ADOPTIONNAME:
        setAdoptionname((String)newValue);
        return;
      case CorePackage.NAME__BIRTHNAME:
        setBirthname((String)newValue);
        return;
      case CorePackage.NAME__CENSUSNAME:
        setCensusname((String)newValue);
        return;
      case CorePackage.NAME__CURRENTNAME:
        setCurrentname((String)newValue);
        return;
      case CorePackage.NAME__FORMALNAME:
        setFormalname((String)newValue);
        return;
      case CorePackage.NAME__GIVENNAME:
        setGivenname((String)newValue);
        return;
      case CorePackage.NAME__MARRIEDNAME:
        setMarriedname((String)newValue);
        return;
      case CorePackage.NAME__NAME:
        setName((String)newValue);
        return;
      case CorePackage.NAME__NICKNAME:
        setNickname((String)newValue);
        return;
      case CorePackage.NAME__OTHERNAME:
        setOthername((String)newValue);
        return;
      case CorePackage.NAME__SURNAME:
        setSurname((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.NAME__ADOPTIONNAME:
        setAdoptionname(ADOPTIONNAME_EDEFAULT);
        return;
      case CorePackage.NAME__BIRTHNAME:
        setBirthname(BIRTHNAME_EDEFAULT);
        return;
      case CorePackage.NAME__CENSUSNAME:
        setCensusname(CENSUSNAME_EDEFAULT);
        return;
      case CorePackage.NAME__CURRENTNAME:
        setCurrentname(CURRENTNAME_EDEFAULT);
        return;
      case CorePackage.NAME__FORMALNAME:
        setFormalname(FORMALNAME_EDEFAULT);
        return;
      case CorePackage.NAME__GIVENNAME:
        setGivenname(GIVENNAME_EDEFAULT);
        return;
      case CorePackage.NAME__MARRIEDNAME:
        setMarriedname(MARRIEDNAME_EDEFAULT);
        return;
      case CorePackage.NAME__NAME:
        setName(NAME_EDEFAULT);
        return;
      case CorePackage.NAME__NICKNAME:
        setNickname(NICKNAME_EDEFAULT);
        return;
      case CorePackage.NAME__OTHERNAME:
        setOthername(OTHERNAME_EDEFAULT);
        return;
      case CorePackage.NAME__SURNAME:
        setSurname(SURNAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.NAME__ADOPTIONNAME:
        return ADOPTIONNAME_EDEFAULT == null ? adoptionname != null : !ADOPTIONNAME_EDEFAULT.equals(adoptionname);
      case CorePackage.NAME__BIRTHNAME:
        return BIRTHNAME_EDEFAULT == null ? birthname != null : !BIRTHNAME_EDEFAULT.equals(birthname);
      case CorePackage.NAME__CENSUSNAME:
        return CENSUSNAME_EDEFAULT == null ? censusname != null : !CENSUSNAME_EDEFAULT.equals(censusname);
      case CorePackage.NAME__CURRENTNAME:
        return CURRENTNAME_EDEFAULT == null ? currentname != null : !CURRENTNAME_EDEFAULT.equals(currentname);
      case CorePackage.NAME__FORMALNAME:
        return FORMALNAME_EDEFAULT == null ? formalname != null : !FORMALNAME_EDEFAULT.equals(formalname);
      case CorePackage.NAME__GIVENNAME:
        return GIVENNAME_EDEFAULT == null ? givenname != null : !GIVENNAME_EDEFAULT.equals(givenname);
      case CorePackage.NAME__MARRIEDNAME:
        return MARRIEDNAME_EDEFAULT == null ? marriedname != null : !MARRIEDNAME_EDEFAULT.equals(marriedname);
      case CorePackage.NAME__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case CorePackage.NAME__NICKNAME:
        return NICKNAME_EDEFAULT == null ? nickname != null : !NICKNAME_EDEFAULT.equals(nickname);
      case CorePackage.NAME__OTHERNAME:
        return OTHERNAME_EDEFAULT == null ? othername != null : !OTHERNAME_EDEFAULT.equals(othername);
      case CorePackage.NAME__SURNAME:
        return SURNAME_EDEFAULT == null ? surname != null : !SURNAME_EDEFAULT.equals(surname);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (adoptionname: "); //$NON-NLS-1$
    result.append(adoptionname);
    result.append(", birthname: "); //$NON-NLS-1$
    result.append(birthname);
    result.append(", censusname: "); //$NON-NLS-1$
    result.append(censusname);
    result.append(", currentname: "); //$NON-NLS-1$
    result.append(currentname);
    result.append(", formalname: "); //$NON-NLS-1$
    result.append(formalname);
    result.append(", givenname: "); //$NON-NLS-1$
    result.append(givenname);
    result.append(", marriedname: "); //$NON-NLS-1$
    result.append(marriedname);
    result.append(", name: "); //$NON-NLS-1$
    result.append(name);
    result.append(", nickname: "); //$NON-NLS-1$
    result.append(nickname);
    result.append(", othername: "); //$NON-NLS-1$
    result.append(othername);
    result.append(", surname: "); //$NON-NLS-1$
    result.append(surname);
    result.append(')');
    return result.toString();
  }

} //NameImpl
