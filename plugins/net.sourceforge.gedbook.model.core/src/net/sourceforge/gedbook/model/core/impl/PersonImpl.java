/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Collection;
import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Burial;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Name;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Residence;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.Title;
import net.sourceforge.gedbook.model.core.Will;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getNames <em>Names</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getSex <em>Sex</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getBirth <em>Birth</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getBatism <em>Batism</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getDeath <em>Death</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getWill <em>Will</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getBurial <em>Burial</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getOccupations <em>Occupations</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getResidences <em>Residences</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getTitles <em>Titles</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getFather <em>Father</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PersonImpl#getMother <em>Mother</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonImpl extends IdentifiableObjectImpl implements Person {
  /**
   * The cached value of the '{@link #getNames() <em>Names</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNames()
   * @generated
   * @ordered
   */
  protected Name names;

  /**
   * The default value of the '{@link #getSex() <em>Sex</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSex()
   * @generated
   * @ordered
   */
  protected static final SexKind SEX_EDEFAULT = SexKind.UNKNOWN;

  /**
   * The cached value of the '{@link #getSex() <em>Sex</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSex()
   * @generated
   * @ordered
   */
  protected SexKind sex = SEX_EDEFAULT;

  /**
   * The cached value of the '{@link #getBirth() <em>Birth</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBirth()
   * @generated
   * @ordered
   */
  protected Birth birth;

  /**
   * The cached value of the '{@link #getBatism() <em>Batism</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBatism()
   * @generated
   * @ordered
   */
  protected Batism batism;

  /**
   * The cached value of the '{@link #getDeath() <em>Death</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeath()
   * @generated
   * @ordered
   */
  protected Death death;

  /**
   * The cached value of the '{@link #getWill() <em>Will</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWill()
   * @generated
   * @ordered
   */
  protected Will will;

  /**
   * The cached value of the '{@link #getBurial() <em>Burial</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBurial()
   * @generated
   * @ordered
   */
  protected Burial burial;

  /**
   * The cached value of the '{@link #getOccupations() <em>Occupations</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOccupations()
   * @generated
   * @ordered
   */
  protected EList<Occupation> occupations;

  /**
   * The cached value of the '{@link #getResidences() <em>Residences</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResidences()
   * @generated
   * @ordered
   */
  protected EList<Residence> residences;

  /**
   * The cached value of the '{@link #getTitles() <em>Titles</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitles()
   * @generated
   * @ordered
   */
  protected EList<Title> titles;

  /**
   * The cached value of the '{@link #getFather() <em>Father</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFather()
   * @generated
   * @ordered
   */
  protected Person father;

  /**
   * The cached value of the '{@link #getMother() <em>Mother</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMother()
   * @generated
   * @ordered
   */
  protected Person mother;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PersonImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.PERSON;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Name getNames() {
    if (names != null && names.eIsProxy()) {
      InternalEObject oldNames = (InternalEObject)names;
      names = (Name)eResolveProxy(oldNames);
      if (names != oldNames) {
        InternalEObject newNames = (InternalEObject)names;
        NotificationChain msgs = oldNames.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PERSON__NAMES, null, null);
        if (newNames.eInternalContainer() == null) {
          msgs = newNames.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PERSON__NAMES, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__NAMES, oldNames, names));
      }
    }
    return names;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Name basicGetNames() {
    return names;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNames(Name newNames, NotificationChain msgs) {
    Name oldNames = names;
    names = newNames;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__NAMES, oldNames, newNames);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNames(Name newNames) {
    if (newNames != names) {
      NotificationChain msgs = null;
      if (names != null)
        msgs = ((InternalEObject)names).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.PERSON__NAMES, null, msgs);
      if (newNames != null)
        msgs = ((InternalEObject)newNames).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.PERSON__NAMES, null, msgs);
      msgs = basicSetNames(newNames, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__NAMES, newNames, newNames));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SexKind getSex() {
    return sex;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSex(SexKind newSex) {
    SexKind oldSex = sex;
    sex = newSex == null ? SEX_EDEFAULT : newSex;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__SEX, oldSex, sex));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Birth getBirth() {
    if (birth != null && birth.eIsProxy()) {
      InternalEObject oldBirth = (InternalEObject)birth;
      birth = (Birth)eResolveProxy(oldBirth);
      if (birth != oldBirth) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__BIRTH, oldBirth, birth));
      }
    }
    return birth;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Birth basicGetBirth() {
    return birth;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBirth(Birth newBirth) {
    Birth oldBirth = birth;
    birth = newBirth;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__BIRTH, oldBirth, birth));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Batism getBatism() {
    if (batism != null && batism.eIsProxy()) {
      InternalEObject oldBatism = (InternalEObject)batism;
      batism = (Batism)eResolveProxy(oldBatism);
      if (batism != oldBatism) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__BATISM, oldBatism, batism));
      }
    }
    return batism;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Batism basicGetBatism() {
    return batism;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBatism(Batism newBatism) {
    Batism oldBatism = batism;
    batism = newBatism;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__BATISM, oldBatism, batism));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Death getDeath() {
    if (death != null && death.eIsProxy()) {
      InternalEObject oldDeath = (InternalEObject)death;
      death = (Death)eResolveProxy(oldDeath);
      if (death != oldDeath) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__DEATH, oldDeath, death));
      }
    }
    return death;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Death basicGetDeath() {
    return death;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDeath(Death newDeath) {
    Death oldDeath = death;
    death = newDeath;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__DEATH, oldDeath, death));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Will getWill() {
    if (will != null && will.eIsProxy()) {
      InternalEObject oldWill = (InternalEObject)will;
      will = (Will)eResolveProxy(oldWill);
      if (will != oldWill) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__WILL, oldWill, will));
      }
    }
    return will;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Will basicGetWill() {
    return will;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWill(Will newWill) {
    Will oldWill = will;
    will = newWill;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__WILL, oldWill, will));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Burial getBurial() {
    if (burial != null && burial.eIsProxy()) {
      InternalEObject oldBurial = (InternalEObject)burial;
      burial = (Burial)eResolveProxy(oldBurial);
      if (burial != oldBurial) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__BURIAL, oldBurial, burial));
      }
    }
    return burial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Burial basicGetBurial() {
    return burial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBurial(Burial newBurial) {
    Burial oldBurial = burial;
    burial = newBurial;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__BURIAL, oldBurial, burial));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Occupation> getOccupations() {
    if (occupations == null) {
      occupations = new EObjectResolvingEList<Occupation>(Occupation.class, this, CorePackage.PERSON__OCCUPATIONS);
    }
    return occupations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Residence> getResidences() {
    if (residences == null) {
      residences = new EObjectResolvingEList<Residence>(Residence.class, this, CorePackage.PERSON__RESIDENCES);
    }
    return residences;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Title> getTitles() {
    if (titles == null) {
      titles = new EObjectResolvingEList<Title>(Title.class, this, CorePackage.PERSON__TITLES);
    }
    return titles;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getFather() {
    if (father != null && father.eIsProxy()) {
      InternalEObject oldFather = (InternalEObject)father;
      father = (Person)eResolveProxy(oldFather);
      if (father != oldFather) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__FATHER, oldFather, father));
      }
    }
    return father;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetFather() {
    return father;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFather(Person newFather) {
    Person oldFather = father;
    father = newFather;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__FATHER, oldFather, father));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getMother() {
    if (mother != null && mother.eIsProxy()) {
      InternalEObject oldMother = (InternalEObject)mother;
      mother = (Person)eResolveProxy(oldMother);
      if (mother != oldMother) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.PERSON__MOTHER, oldMother, mother));
      }
    }
    return mother;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetMother() {
    return mother;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMother(Person newMother) {
    Person oldMother = mother;
    mother = newMother;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PERSON__MOTHER, oldMother, mother));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.PERSON__NAMES:
        return basicSetNames(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.PERSON__NAMES:
        if (resolve) return getNames();
        return basicGetNames();
      case CorePackage.PERSON__SEX:
        return getSex();
      case CorePackage.PERSON__BIRTH:
        if (resolve) return getBirth();
        return basicGetBirth();
      case CorePackage.PERSON__BATISM:
        if (resolve) return getBatism();
        return basicGetBatism();
      case CorePackage.PERSON__DEATH:
        if (resolve) return getDeath();
        return basicGetDeath();
      case CorePackage.PERSON__WILL:
        if (resolve) return getWill();
        return basicGetWill();
      case CorePackage.PERSON__BURIAL:
        if (resolve) return getBurial();
        return basicGetBurial();
      case CorePackage.PERSON__OCCUPATIONS:
        return getOccupations();
      case CorePackage.PERSON__RESIDENCES:
        return getResidences();
      case CorePackage.PERSON__TITLES:
        return getTitles();
      case CorePackage.PERSON__FATHER:
        if (resolve) return getFather();
        return basicGetFather();
      case CorePackage.PERSON__MOTHER:
        if (resolve) return getMother();
        return basicGetMother();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.PERSON__NAMES:
        setNames((Name)newValue);
        return;
      case CorePackage.PERSON__SEX:
        setSex((SexKind)newValue);
        return;
      case CorePackage.PERSON__BIRTH:
        setBirth((Birth)newValue);
        return;
      case CorePackage.PERSON__BATISM:
        setBatism((Batism)newValue);
        return;
      case CorePackage.PERSON__DEATH:
        setDeath((Death)newValue);
        return;
      case CorePackage.PERSON__WILL:
        setWill((Will)newValue);
        return;
      case CorePackage.PERSON__BURIAL:
        setBurial((Burial)newValue);
        return;
      case CorePackage.PERSON__OCCUPATIONS:
        getOccupations().clear();
        getOccupations().addAll((Collection<? extends Occupation>)newValue);
        return;
      case CorePackage.PERSON__RESIDENCES:
        getResidences().clear();
        getResidences().addAll((Collection<? extends Residence>)newValue);
        return;
      case CorePackage.PERSON__TITLES:
        getTitles().clear();
        getTitles().addAll((Collection<? extends Title>)newValue);
        return;
      case CorePackage.PERSON__FATHER:
        setFather((Person)newValue);
        return;
      case CorePackage.PERSON__MOTHER:
        setMother((Person)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.PERSON__NAMES:
        setNames((Name)null);
        return;
      case CorePackage.PERSON__SEX:
        setSex(SEX_EDEFAULT);
        return;
      case CorePackage.PERSON__BIRTH:
        setBirth((Birth)null);
        return;
      case CorePackage.PERSON__BATISM:
        setBatism((Batism)null);
        return;
      case CorePackage.PERSON__DEATH:
        setDeath((Death)null);
        return;
      case CorePackage.PERSON__WILL:
        setWill((Will)null);
        return;
      case CorePackage.PERSON__BURIAL:
        setBurial((Burial)null);
        return;
      case CorePackage.PERSON__OCCUPATIONS:
        getOccupations().clear();
        return;
      case CorePackage.PERSON__RESIDENCES:
        getResidences().clear();
        return;
      case CorePackage.PERSON__TITLES:
        getTitles().clear();
        return;
      case CorePackage.PERSON__FATHER:
        setFather((Person)null);
        return;
      case CorePackage.PERSON__MOTHER:
        setMother((Person)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.PERSON__NAMES:
        return names != null;
      case CorePackage.PERSON__SEX:
        return sex != SEX_EDEFAULT;
      case CorePackage.PERSON__BIRTH:
        return birth != null;
      case CorePackage.PERSON__BATISM:
        return batism != null;
      case CorePackage.PERSON__DEATH:
        return death != null;
      case CorePackage.PERSON__WILL:
        return will != null;
      case CorePackage.PERSON__BURIAL:
        return burial != null;
      case CorePackage.PERSON__OCCUPATIONS:
        return occupations != null && !occupations.isEmpty();
      case CorePackage.PERSON__RESIDENCES:
        return residences != null && !residences.isEmpty();
      case CorePackage.PERSON__TITLES:
        return titles != null && !titles.isEmpty();
      case CorePackage.PERSON__FATHER:
        return father != null;
      case CorePackage.PERSON__MOTHER:
        return mother != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sex: "); //$NON-NLS-1$
    result.append(sex);
    result.append(')');
    return result.toString();
  }

} //PersonImpl
