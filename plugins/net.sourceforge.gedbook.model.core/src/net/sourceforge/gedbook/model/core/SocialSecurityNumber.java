/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Social Security Number</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.SocialSecurityNumber#getSSN <em>SSN</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getSocialSecurityNumber()
 * @model
 * @generated
 */
public interface SocialSecurityNumber extends PersonEvent {
  /**
   * Returns the value of the '<em><b>SSN</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>SSN</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>SSN</em>' attribute.
   * @see #setSSN(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getSocialSecurityNumber_SSN()
   * @model
   * @generated
   */
  String getSSN();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.SocialSecurityNumber#getSSN <em>SSN</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>SSN</em>' attribute.
   * @see #getSSN()
   * @generated
   */
  void setSSN(String value);

} // SocialSecurityNumber
