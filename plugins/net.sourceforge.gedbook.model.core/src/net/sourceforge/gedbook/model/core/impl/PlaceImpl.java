/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Place;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl#getPays <em>Pays</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl#getRegion <em>Region</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl#getDepartement <em>Departement</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl#getCommune <em>Commune</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlaceImpl extends MinimalEObjectImpl.Container implements Place {
  /**
   * The default value of the '{@link #getPays() <em>Pays</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPays()
   * @generated
   * @ordered
   */
  protected static final String PAYS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPays() <em>Pays</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPays()
   * @generated
   * @ordered
   */
  protected String pays = PAYS_EDEFAULT;

  /**
   * The default value of the '{@link #getRegion() <em>Region</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRegion()
   * @generated
   * @ordered
   */
  protected static final String REGION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRegion() <em>Region</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRegion()
   * @generated
   * @ordered
   */
  protected String region = REGION_EDEFAULT;

  /**
   * The default value of the '{@link #getDepartement() <em>Departement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDepartement()
   * @generated
   * @ordered
   */
  protected static final String DEPARTEMENT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDepartement() <em>Departement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDepartement()
   * @generated
   * @ordered
   */
  protected String departement = DEPARTEMENT_EDEFAULT;

  /**
   * The default value of the '{@link #getCommune() <em>Commune</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommune()
   * @generated
   * @ordered
   */
  protected static final String COMMUNE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCommune() <em>Commune</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommune()
   * @generated
   * @ordered
   */
  protected String commune = COMMUNE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PlaceImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.PLACE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPays() {
    return pays;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPays(String newPays) {
    String oldPays = pays;
    pays = newPays;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PLACE__PAYS, oldPays, pays));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRegion() {
    return region;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRegion(String newRegion) {
    String oldRegion = region;
    region = newRegion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PLACE__REGION, oldRegion, region));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDepartement() {
    return departement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDepartement(String newDepartement) {
    String oldDepartement = departement;
    departement = newDepartement;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PLACE__DEPARTEMENT, oldDepartement, departement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCommune() {
    return commune;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommune(String newCommune) {
    String oldCommune = commune;
    commune = newCommune;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.PLACE__COMMUNE, oldCommune, commune));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.PLACE__PAYS:
        return getPays();
      case CorePackage.PLACE__REGION:
        return getRegion();
      case CorePackage.PLACE__DEPARTEMENT:
        return getDepartement();
      case CorePackage.PLACE__COMMUNE:
        return getCommune();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.PLACE__PAYS:
        setPays((String)newValue);
        return;
      case CorePackage.PLACE__REGION:
        setRegion((String)newValue);
        return;
      case CorePackage.PLACE__DEPARTEMENT:
        setDepartement((String)newValue);
        return;
      case CorePackage.PLACE__COMMUNE:
        setCommune((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.PLACE__PAYS:
        setPays(PAYS_EDEFAULT);
        return;
      case CorePackage.PLACE__REGION:
        setRegion(REGION_EDEFAULT);
        return;
      case CorePackage.PLACE__DEPARTEMENT:
        setDepartement(DEPARTEMENT_EDEFAULT);
        return;
      case CorePackage.PLACE__COMMUNE:
        setCommune(COMMUNE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.PLACE__PAYS:
        return PAYS_EDEFAULT == null ? pays != null : !PAYS_EDEFAULT.equals(pays);
      case CorePackage.PLACE__REGION:
        return REGION_EDEFAULT == null ? region != null : !REGION_EDEFAULT.equals(region);
      case CorePackage.PLACE__DEPARTEMENT:
        return DEPARTEMENT_EDEFAULT == null ? departement != null : !DEPARTEMENT_EDEFAULT.equals(departement);
      case CorePackage.PLACE__COMMUNE:
        return COMMUNE_EDEFAULT == null ? commune != null : !COMMUNE_EDEFAULT.equals(commune);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (pays: "); //$NON-NLS-1$
    result.append(pays);
    result.append(", region: "); //$NON-NLS-1$
    result.append(region);
    result.append(", departement: "); //$NON-NLS-1$
    result.append(departement);
    result.append(", commune: "); //$NON-NLS-1$
    result.append(commune);
    result.append(')');
    return result.toString();
  }

} //PlaceImpl
