/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Collection;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Divorce;
import net.sourceforge.gedbook.model.core.Person;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Divorce</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DivorceImpl#getWitnesses <em>Witnesses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DivorceImpl extends FamilyEventImpl implements Divorce {
  /**
   * The cached value of the '{@link #getWitnesses() <em>Witnesses</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWitnesses()
   * @generated
   * @ordered
   */
  protected EList<Person> witnesses;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DivorceImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.DIVORCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Person> getWitnesses() {
    if (witnesses == null) {
      witnesses = new EObjectResolvingEList<Person>(Person.class, this, CorePackage.DIVORCE__WITNESSES);
    }
    return witnesses;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.DIVORCE__WITNESSES:
        return getWitnesses();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.DIVORCE__WITNESSES:
        getWitnesses().clear();
        getWitnesses().addAll((Collection<? extends Person>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.DIVORCE__WITNESSES:
        getWitnesses().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.DIVORCE__WITNESSES:
        return witnesses != null && !witnesses.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //DivorceImpl
