/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Will;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Will</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WillImpl extends PersonEventImpl implements Will {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected WillImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.WILL;
  }

} //WillImpl
