/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import net.sourceforge.gedbook.model.core.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoreFactoryImpl extends EFactoryImpl implements CoreFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CoreFactory init() {
    try {
      CoreFactory theCoreFactory = (CoreFactory)EPackage.Registry.INSTANCE.getEFactory(CorePackage.eNS_URI);
      if (theCoreFactory != null) {
        return theCoreFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CoreFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CoreFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case CorePackage.IDENTIFIABLE_OBJECT: return createIdentifiableObject();
      case CorePackage.ANNOTATABLE_OBJECT: return createAnnotatableObject();
      case CorePackage.NOTE: return createNote();
      case CorePackage.PROJECT: return createProject();
      case CorePackage.REPOSITORY_PKG: return createRepositoryPkg();
      case CorePackage.SOURCE_PKG: return createSourcePkg();
      case CorePackage.DOCUMENT_PKG: return createDocumentPkg();
      case CorePackage.PERSON_PKG: return createPersonPkg();
      case CorePackage.FAMILY_PKG: return createFamilyPkg();
      case CorePackage.EVENT_PKG: return createEventPkg();
      case CorePackage.PLACE_PKG: return createPlacePkg();
      case CorePackage.REPOSITORY: return createRepository();
      case CorePackage.SOURCE: return createSource();
      case CorePackage.SOURCE_CITATION: return createSourceCitation();
      case CorePackage.DOCUMENT: return createDocument();
      case CorePackage.FAMILY: return createFamily();
      case CorePackage.EVENT: return createEvent();
      case CorePackage.PERSON_EVENT: return createPersonEvent();
      case CorePackage.FAMILY_EVENT: return createFamilyEvent();
      case CorePackage.DATE_WRAPPER: return createDateWrapper();
      case CorePackage.BIRTH: return createBirth();
      case CorePackage.BATISM: return createBatism();
      case CorePackage.DEATH: return createDeath();
      case CorePackage.BURIAL: return createBurial();
      case CorePackage.WILL: return createWill();
      case CorePackage.OCCUPATION: return createOccupation();
      case CorePackage.TITLE: return createTitle();
      case CorePackage.NATIONALITY: return createNationality();
      case CorePackage.SOCIAL_SECURITY_NUMBER: return createSocialSecurityNumber();
      case CorePackage.PROPERTY: return createProperty();
      case CorePackage.RESIDENCE: return createResidence();
      case CorePackage.ORDINATION: return createOrdination();
      case CorePackage.RETIREMENT: return createRetirement();
      case CorePackage.CENSUS: return createCensus();
      case CorePackage.MARRIAGE: return createMarriage();
      case CorePackage.BANNS: return createBanns();
      case CorePackage.DIVORCE: return createDivorce();
      case CorePackage.ANULMENT: return createAnulment();
      case CorePackage.PERSON: return createPerson();
      case CorePackage.NAME: return createName();
      case CorePackage.PLACE: return createPlace();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue) {
    switch (eDataType.getClassifierID()) {
      case CorePackage.DOCUMENT_KIND:
        return createDocumentKindFromString(eDataType, initialValue);
      case CorePackage.DATE_QUALIFIER:
        return createDateQualifierFromString(eDataType, initialValue);
      case CorePackage.SEX_KIND:
        return createSexKindFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue) {
    switch (eDataType.getClassifierID()) {
      case CorePackage.DOCUMENT_KIND:
        return convertDocumentKindToString(eDataType, instanceValue);
      case CorePackage.DATE_QUALIFIER:
        return convertDateQualifierToString(eDataType, instanceValue);
      case CorePackage.SEX_KIND:
        return convertSexKindToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IdentifiableObject createIdentifiableObject() {
    IdentifiableObjectImpl identifiableObject = new IdentifiableObjectImpl();
    return identifiableObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AnnotatableObject createAnnotatableObject() {
    AnnotatableObjectImpl annotatableObject = new AnnotatableObjectImpl();
    return annotatableObject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Note createNote() {
    NoteImpl note = new NoteImpl();
    return note;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Project createProject() {
    ProjectImpl project = new ProjectImpl();
    return project;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepositoryPkg createRepositoryPkg() {
    RepositoryPkgImpl repositoryPkg = new RepositoryPkgImpl();
    return repositoryPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourcePkg createSourcePkg() {
    SourcePkgImpl sourcePkg = new SourcePkgImpl();
    return sourcePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentPkg createDocumentPkg() {
    DocumentPkgImpl documentPkg = new DocumentPkgImpl();
    return documentPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PersonPkg createPersonPkg() {
    PersonPkgImpl personPkg = new PersonPkgImpl();
    return personPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FamilyPkg createFamilyPkg() {
    FamilyPkgImpl familyPkg = new FamilyPkgImpl();
    return familyPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventPkg createEventPkg() {
    EventPkgImpl eventPkg = new EventPkgImpl();
    return eventPkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlacePkg createPlacePkg() {
    PlacePkgImpl placePkg = new PlacePkgImpl();
    return placePkg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Repository createRepository() {
    RepositoryImpl repository = new RepositoryImpl();
    return repository;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Source createSource() {
    SourceImpl source = new SourceImpl();
    return source;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SourceCitation createSourceCitation() {
    SourceCitationImpl sourceCitation = new SourceCitationImpl();
    return sourceCitation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Document createDocument() {
    DocumentImpl document = new DocumentImpl();
    return document;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Event createEvent() {
    EventImpl event = new EventImpl();
    return event;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PersonEvent createPersonEvent() {
    PersonEventImpl personEvent = new PersonEventImpl();
    return personEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FamilyEvent createFamilyEvent() {
    FamilyEventImpl familyEvent = new FamilyEventImpl();
    return familyEvent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DateWrapper createDateWrapper() {
    DateWrapperImpl dateWrapper = new DateWrapperImpl();
    return dateWrapper;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Birth createBirth() {
    BirthImpl birth = new BirthImpl();
    return birth;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Batism createBatism() {
    BatismImpl batism = new BatismImpl();
    return batism;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Marriage createMarriage() {
    MarriageImpl marriage = new MarriageImpl();
    return marriage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Banns createBanns() {
    BannsImpl banns = new BannsImpl();
    return banns;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Death createDeath() {
    DeathImpl death = new DeathImpl();
    return death;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Burial createBurial() {
    BurialImpl burial = new BurialImpl();
    return burial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Will createWill() {
    WillImpl will = new WillImpl();
    return will;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Occupation createOccupation() {
    OccupationImpl occupation = new OccupationImpl();
    return occupation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Divorce createDivorce() {
    DivorceImpl divorce = new DivorceImpl();
    return divorce;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Anulment createAnulment() {
    AnulmentImpl anulment = new AnulmentImpl();
    return anulment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Title createTitle() {
    TitleImpl title = new TitleImpl();
    return title;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Nationality createNationality() {
    NationalityImpl nationality = new NationalityImpl();
    return nationality;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SocialSecurityNumber createSocialSecurityNumber() {
    SocialSecurityNumberImpl socialSecurityNumber = new SocialSecurityNumberImpl();
    return socialSecurityNumber;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property createProperty() {
    PropertyImpl property = new PropertyImpl();
    return property;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Residence createResidence() {
    ResidenceImpl residence = new ResidenceImpl();
    return residence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Ordination createOrdination() {
    OrdinationImpl ordination = new OrdinationImpl();
    return ordination;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Retirement createRetirement() {
    RetirementImpl retirement = new RetirementImpl();
    return retirement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Census createCensus() {
    CensusImpl census = new CensusImpl();
    return census;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person createPerson() {
    PersonImpl person = new PersonImpl();
    return person;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Name createName() {
    NameImpl name = new NameImpl();
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Place createPlace() {
    PlaceImpl place = new PlaceImpl();
    return place;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Family createFamily() {
    FamilyImpl family = new FamilyImpl();
    return family;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentKind createDocumentKindFromString(EDataType eDataType, String initialValue) {
    DocumentKind result = DocumentKind.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertDocumentKindToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DateQualifier createDateQualifierFromString(EDataType eDataType, String initialValue) {
    DateQualifier result = DateQualifier.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertDateQualifierToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SexKind createSexKindFromString(EDataType eDataType, String initialValue) {
    SexKind result = SexKind.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertSexKindToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CorePackage getCorePackage() {
    return (CorePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CorePackage getPackage() {
    return CorePackage.eINSTANCE;
  }

} //CoreFactoryImpl
