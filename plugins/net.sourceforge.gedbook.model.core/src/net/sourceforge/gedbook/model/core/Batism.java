/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Batism</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Batism#getGodfather <em>Godfather</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Batism#getGodmother <em>Godmother</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getBatism()
 * @model
 * @generated
 */
public interface Batism extends PersonEvent {
  /**
   * Returns the value of the '<em><b>Godfather</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Godfather</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Godfather</em>' reference.
   * @see #setGodfather(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getBatism_Godfather()
   * @model
   * @generated
   */
  Person getGodfather();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Batism#getGodfather <em>Godfather</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Godfather</em>' reference.
   * @see #getGodfather()
   * @generated
   */
  void setGodfather(Person value);

  /**
   * Returns the value of the '<em><b>Godmother</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Godmother</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Godmother</em>' reference.
   * @see #setGodmother(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getBatism_Godmother()
   * @model
   * @generated
   */
  Person getGodmother();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Batism#getGodmother <em>Godmother</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Godmother</em>' reference.
   * @see #getGodmother()
   * @generated
   */
  void setGodmother(Person value);

} // Batism
