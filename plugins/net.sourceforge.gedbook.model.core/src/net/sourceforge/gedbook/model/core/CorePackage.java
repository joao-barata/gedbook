/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.core.CoreFactory
 * @model kind="package"
 * @generated
 */
public interface CorePackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "core"; //$NON-NLS-1$

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://net.sourceforge/gedbook/core/0.5.0"; //$NON-NLS-1$

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "net.sourceforge.gedbook.model.core"; //$NON-NLS-1$

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CorePackage eINSTANCE = net.sourceforge.gedbook.model.core.impl.CorePackageImpl.init();

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.IdentifiableObjectImpl <em>Identifiable Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.IdentifiableObjectImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getIdentifiableObject()
   * @generated
   */
  int IDENTIFIABLE_OBJECT = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT__ID = 0;

  /**
   * The number of structural features of the '<em>Identifiable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Identifiable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IDENTIFIABLE_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.AnnotatableObjectImpl <em>Annotatable Object</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.AnnotatableObjectImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getAnnotatableObject()
   * @generated
   */
  int ANNOTATABLE_OBJECT = 1;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATABLE_OBJECT__ANNOTATIONS = 0;

  /**
   * The number of structural features of the '<em>Annotatable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATABLE_OBJECT_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Annotatable Object</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANNOTATABLE_OBJECT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.NoteImpl <em>Note</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.NoteImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getNote()
   * @generated
   */
  int NOTE = 2;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOTE__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Content</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOTE__CONTENT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Note</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOTE_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Note</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOTE_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl <em>Project</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.ProjectImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getProject()
   * @generated
   */
  int PROJECT = 3;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__NAME = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__DESCRIPTION = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Owned Repository Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_REPOSITORY_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Owned Source Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_SOURCE_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Owned Document Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_DOCUMENT_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Owned Person Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_PERSON_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Owned Family Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_FAMILY_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Owned Event Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_EVENT_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Owned Place Pkg</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT__OWNED_PLACE_PKG = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 8;

  /**
   * The number of structural features of the '<em>Project</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 9;

  /**
   * The number of operations of the '<em>Project</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROJECT_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.RepositoryPkgImpl <em>Repository Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.RepositoryPkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRepositoryPkg()
   * @generated
   */
  int REPOSITORY_PKG = 4;

  /**
   * The feature id for the '<em><b>Owned Repositories</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY_PKG__OWNED_REPOSITORIES = 0;

  /**
   * The number of structural features of the '<em>Repository Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Repository Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.SourcePkgImpl <em>Source Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.SourcePkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSourcePkg()
   * @generated
   */
  int SOURCE_PKG = 5;

  /**
   * The feature id for the '<em><b>Owned Sources</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_PKG__OWNED_SOURCES = 0;

  /**
   * The number of structural features of the '<em>Source Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Source Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.DocumentPkgImpl <em>Document Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.DocumentPkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocumentPkg()
   * @generated
   */
  int DOCUMENT_PKG = 6;

  /**
   * The feature id for the '<em><b>Owned Documents</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_PKG__OWNED_DOCUMENTS = 0;

  /**
   * The number of structural features of the '<em>Document Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Document Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PersonPkgImpl <em>Person Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PersonPkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPersonPkg()
   * @generated
   */
  int PERSON_PKG = 7;

  /**
   * The feature id for the '<em><b>Owned Persons</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_PKG__OWNED_PERSONS = 0;

  /**
   * The number of structural features of the '<em>Person Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Person Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyPkgImpl <em>Family Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.FamilyPkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamilyPkg()
   * @generated
   */
  int FAMILY_PKG = 8;

  /**
   * The feature id for the '<em><b>Owned Families</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_PKG__OWNED_FAMILIES = 0;

  /**
   * The number of structural features of the '<em>Family Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Family Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.EventPkgImpl <em>Event Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.EventPkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getEventPkg()
   * @generated
   */
  int EVENT_PKG = 9;

  /**
   * The feature id for the '<em><b>Owned Events</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_PKG__OWNED_EVENTS = 0;

  /**
   * The number of structural features of the '<em>Event Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Event Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PlacePkgImpl <em>Place Pkg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PlacePkgImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPlacePkg()
   * @generated
   */
  int PLACE_PKG = 10;

  /**
   * The feature id for the '<em><b>Owned Places</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE_PKG__OWNED_PLACES = 0;

  /**
   * The number of structural features of the '<em>Place Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE_PKG_FEATURE_COUNT = 1;

  /**
   * The number of operations of the '<em>Place Pkg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE_PKG_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.RepositoryImpl <em>Repository</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.RepositoryImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRepository()
   * @generated
   */
  int REPOSITORY = 11;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY__NAME = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Location</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY__LOCATION = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Repository</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Repository</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPOSITORY_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.SourceImpl <em>Source</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.SourceImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSource()
   * @generated
   */
  int SOURCE = 12;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__TITLE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Article</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__ARTICLE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Author</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__AUTHOR = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Origin</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__ORIGIN = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Storage</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE__STORAGE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Source</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 5;

  /**
   * The number of operations of the '<em>Source</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.SourceCitationImpl <em>Source Citation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.SourceCitationImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSourceCitation()
   * @generated
   */
  int SOURCE_CITATION = 13;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_CITATION__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Page</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_CITATION__PAGE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Source</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_CITATION__SOURCE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Source Citation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_CITATION_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Source Citation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_CITATION_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl <em>Document</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.DocumentImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocument()
   * @generated
   */
  int DOCUMENT = 14;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Abbreviation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__ABBREVIATION = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Author</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__AUTHOR = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Agency</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__AGENCY = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__DATE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__REFERENCE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__TITLE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Pages</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__PAGES = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Kind</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__KIND = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Source</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__SOURCE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__PLACE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 9;

  /**
   * The number of structural features of the '<em>Document</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 10;

  /**
   * The number of operations of the '<em>Document</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.EventImpl <em>Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.EventImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getEvent()
   * @generated
   */
  int EVENT = 16;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PersonEventImpl <em>Person Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PersonEventImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPersonEvent()
   * @generated
   */
  int PERSON_EVENT = 17;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyEventImpl <em>Family Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.FamilyEventImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamilyEvent()
   * @generated
   */
  int FAMILY_EVENT = 18;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl <em>Date Wrapper</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.DateWrapperImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDateWrapper()
   * @generated
   */
  int DATE_WRAPPER = 19;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.BirthImpl <em>Birth</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.BirthImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBirth()
   * @generated
   */
  int BIRTH = 20;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.BatismImpl <em>Batism</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.BatismImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBatism()
   * @generated
   */
  int BATISM = 21;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.MarriageImpl <em>Marriage</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.MarriageImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getMarriage()
   * @generated
   */
  int MARRIAGE = 34;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.BannsImpl <em>Banns</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.BannsImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBanns()
   * @generated
   */
  int BANNS = 35;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.DeathImpl <em>Death</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.DeathImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDeath()
   * @generated
   */
  int DEATH = 22;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.BurialImpl <em>Burial</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.BurialImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBurial()
   * @generated
   */
  int BURIAL = 23;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.OccupationImpl <em>Occupation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.OccupationImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getOccupation()
   * @generated
   */
  int OCCUPATION = 25;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.DivorceImpl <em>Divorce</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.DivorceImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDivorce()
   * @generated
   */
  int DIVORCE = 36;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.TitleImpl <em>Title</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.TitleImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getTitle()
   * @generated
   */
  int TITLE = 26;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PersonImpl <em>Person</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PersonImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPerson()
   * @generated
   */
  int PERSON = 38;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.NameImpl <em>Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.NameImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getName_()
   * @generated
   */
  int NAME = 39;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl <em>Place</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PlaceImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPlace()
   * @generated
   */
  int PLACE = 40;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl <em>Family</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.FamilyImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamily()
   * @generated
   */
  int FAMILY = 15;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Husband</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY__HUSBAND = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Wife</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY__WIFE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Children</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY__CHILDREN = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Events</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY__EVENTS = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Family</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>Family</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__ANNOTATIONS = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__DATE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__PLACE = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__CITATIONS = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__ID = EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__ANNOTATIONS = EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__DATE = EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__PLACE = EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__CITATIONS = EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT__PERSON = EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Person Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Person Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__ID = EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__ANNOTATIONS = EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__DATE = EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__PLACE = EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__CITATIONS = EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT__FAMILY = EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Family Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Family Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FAMILY_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__DATE = 0;

  /**
   * The feature id for the '<em><b>Qualifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__QUALIFIER = 1;

  /**
   * The feature id for the '<em><b>Day</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__DAY = 2;

  /**
   * The feature id for the '<em><b>Month</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__MONTH = 3;

  /**
   * The feature id for the '<em><b>Year</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__YEAR = 4;

  /**
   * The feature id for the '<em><b>Format</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER__FORMAT = 5;

  /**
   * The number of structural features of the '<em>Date Wrapper</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER_FEATURE_COUNT = 6;

  /**
   * The number of operations of the '<em>Date Wrapper</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATE_WRAPPER_OPERATION_COUNT = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH__PERSON = PERSON_EVENT__PERSON;

  /**
   * The number of structural features of the '<em>Birth</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Birth</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BIRTH_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Godfather</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__GODFATHER = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Godmother</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM__GODMOTHER = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Batism</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Batism</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BATISM_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH__PERSON = PERSON_EVENT__PERSON;

  /**
   * The number of structural features of the '<em>Death</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Death</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEATH_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL__PERSON = PERSON_EVENT__PERSON;

  /**
   * The number of structural features of the '<em>Burial</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Burial</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BURIAL_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.WillImpl <em>Will</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.WillImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getWill()
   * @generated
   */
  int WILL = 24;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL__PERSON = PERSON_EVENT__PERSON;

  /**
   * The number of structural features of the '<em>Will</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Will</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WILL_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION__DESCRIPTION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Occupation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Occupation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OCCUPATION_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE__TITLE = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Title</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Title</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TITLE_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.NationalityImpl <em>Nationality</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.NationalityImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getNationality()
   * @generated
   */
  int NATIONALITY = 27;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY__DESCRIPTION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Nationality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Nationality</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NATIONALITY_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.SocialSecurityNumberImpl <em>Social Security Number</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.SocialSecurityNumberImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSocialSecurityNumber()
   * @generated
   */
  int SOCIAL_SECURITY_NUMBER = 28;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>SSN</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER__SSN = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Social Security Number</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Social Security Number</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOCIAL_SECURITY_NUMBER_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.PropertyImpl <em>Property</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.PropertyImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getProperty()
   * @generated
   */
  int PROPERTY = 29;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY__DESCRIPTION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Property</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROPERTY_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.ResidenceImpl <em>Residence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.ResidenceImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getResidence()
   * @generated
   */
  int RESIDENCE = 30;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Location</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE__LOCATION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Residence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Residence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RESIDENCE_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.OrdinationImpl <em>Ordination</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.OrdinationImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getOrdination()
   * @generated
   */
  int ORDINATION = 31;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION__DESCRIPTION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Ordination</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Ordination</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORDINATION_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.RetirementImpl <em>Retirement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.RetirementImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRetirement()
   * @generated
   */
  int RETIREMENT = 32;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__ID = PERSON_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__ANNOTATIONS = PERSON_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__DATE = PERSON_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__PLACE = PERSON_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__CITATIONS = PERSON_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__PERSON = PERSON_EVENT__PERSON;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT__DESCRIPTION = PERSON_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Retirement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT_FEATURE_COUNT = PERSON_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Retirement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETIREMENT_OPERATION_COUNT = PERSON_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.CensusImpl <em>Census</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.CensusImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getCensus()
   * @generated
   */
  int CENSUS = 33;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__ID = FAMILY_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__ANNOTATIONS = FAMILY_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__DATE = FAMILY_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__PLACE = FAMILY_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__CITATIONS = FAMILY_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS__FAMILY = FAMILY_EVENT__FAMILY;

  /**
   * The number of structural features of the '<em>Census</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS_FEATURE_COUNT = FAMILY_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Census</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CENSUS_OPERATION_COUNT = FAMILY_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__ID = FAMILY_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__ANNOTATIONS = FAMILY_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__DATE = FAMILY_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__PLACE = FAMILY_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__CITATIONS = FAMILY_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__FAMILY = FAMILY_EVENT__FAMILY;

  /**
   * The feature id for the '<em><b>Witnesses</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE__WITNESSES = FAMILY_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Marriage</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE_FEATURE_COUNT = FAMILY_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Marriage</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MARRIAGE_OPERATION_COUNT = FAMILY_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__ID = FAMILY_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__ANNOTATIONS = FAMILY_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__DATE = FAMILY_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__PLACE = FAMILY_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__CITATIONS = FAMILY_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS__FAMILY = FAMILY_EVENT__FAMILY;

  /**
   * The number of structural features of the '<em>Banns</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS_FEATURE_COUNT = FAMILY_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Banns</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BANNS_OPERATION_COUNT = FAMILY_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__ID = FAMILY_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__ANNOTATIONS = FAMILY_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__DATE = FAMILY_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__PLACE = FAMILY_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__CITATIONS = FAMILY_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__FAMILY = FAMILY_EVENT__FAMILY;

  /**
   * The feature id for the '<em><b>Witnesses</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE__WITNESSES = FAMILY_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Divorce</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE_FEATURE_COUNT = FAMILY_EVENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Divorce</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIVORCE_OPERATION_COUNT = FAMILY_EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.impl.AnulmentImpl <em>Anulment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.impl.AnulmentImpl
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getAnulment()
   * @generated
   */
  int ANULMENT = 37;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__ID = FAMILY_EVENT__ID;

  /**
   * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__ANNOTATIONS = FAMILY_EVENT__ANNOTATIONS;

  /**
   * The feature id for the '<em><b>Date</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__DATE = FAMILY_EVENT__DATE;

  /**
   * The feature id for the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__PLACE = FAMILY_EVENT__PLACE;

  /**
   * The feature id for the '<em><b>Citations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__CITATIONS = FAMILY_EVENT__CITATIONS;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT__FAMILY = FAMILY_EVENT__FAMILY;

  /**
   * The number of structural features of the '<em>Anulment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT_FEATURE_COUNT = FAMILY_EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Anulment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ANULMENT_OPERATION_COUNT = FAMILY_EVENT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__ID = IDENTIFIABLE_OBJECT__ID;

  /**
   * The feature id for the '<em><b>Names</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__NAMES = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sex</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__SEX = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Birth</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__BIRTH = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Batism</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__BATISM = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Death</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__DEATH = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 4;

  /**
   * The feature id for the '<em><b>Will</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__WILL = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 5;

  /**
   * The feature id for the '<em><b>Burial</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__BURIAL = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 6;

  /**
   * The feature id for the '<em><b>Occupations</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__OCCUPATIONS = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 7;

  /**
   * The feature id for the '<em><b>Residences</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__RESIDENCES = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 8;

  /**
   * The feature id for the '<em><b>Titles</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__TITLES = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 9;

  /**
   * The feature id for the '<em><b>Father</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__FATHER = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 10;

  /**
   * The feature id for the '<em><b>Mother</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON__MOTHER = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 11;

  /**
   * The number of structural features of the '<em>Person</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_FEATURE_COUNT = IDENTIFIABLE_OBJECT_FEATURE_COUNT + 12;

  /**
   * The number of operations of the '<em>Person</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERSON_OPERATION_COUNT = IDENTIFIABLE_OBJECT_OPERATION_COUNT + 0;

  /**
   * The feature id for the '<em><b>Adoptionname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__ADOPTIONNAME = 0;

  /**
   * The feature id for the '<em><b>Birthname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__BIRTHNAME = 1;

  /**
   * The feature id for the '<em><b>Censusname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__CENSUSNAME = 2;

  /**
   * The feature id for the '<em><b>Currentname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__CURRENTNAME = 3;

  /**
   * The feature id for the '<em><b>Formalname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__FORMALNAME = 4;

  /**
   * The feature id for the '<em><b>Givenname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__GIVENNAME = 5;

  /**
   * The feature id for the '<em><b>Marriedname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__MARRIEDNAME = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__NAME = 7;

  /**
   * The feature id for the '<em><b>Nickname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__NICKNAME = 8;

  /**
   * The feature id for the '<em><b>Othername</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__OTHERNAME = 9;

  /**
   * The feature id for the '<em><b>Surname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME__SURNAME = 10;

  /**
   * The number of structural features of the '<em>Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME_FEATURE_COUNT = 11;

  /**
   * The number of operations of the '<em>Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NAME_OPERATION_COUNT = 0;

  /**
   * The feature id for the '<em><b>Pays</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE__PAYS = 0;

  /**
   * The feature id for the '<em><b>Region</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE__REGION = 1;

  /**
   * The feature id for the '<em><b>Departement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE__DEPARTEMENT = 2;

  /**
   * The feature id for the '<em><b>Commune</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE__COMMUNE = 3;

  /**
   * The number of structural features of the '<em>Place</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE_FEATURE_COUNT = 4;

  /**
   * The number of operations of the '<em>Place</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PLACE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.DocumentKind <em>Document Kind</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.DocumentKind
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocumentKind()
   * @generated
   */
  int DOCUMENT_KIND = 41;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.DateQualifier <em>Date Qualifier</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.DateQualifier
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDateQualifier()
   * @generated
   */
  int DATE_QUALIFIER = 42;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.core.SexKind <em>Sex Kind</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.core.SexKind
   * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSexKind()
   * @generated
   */
  int SEX_KIND = 43;


  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.IdentifiableObject <em>Identifiable Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Identifiable Object</em>'.
   * @see net.sourceforge.gedbook.model.core.IdentifiableObject
   * @generated
   */
  EClass getIdentifiableObject();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.IdentifiableObject#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see net.sourceforge.gedbook.model.core.IdentifiableObject#getId()
   * @see #getIdentifiableObject()
   * @generated
   */
  EAttribute getIdentifiableObject_Id();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.AnnotatableObject <em>Annotatable Object</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Annotatable Object</em>'.
   * @see net.sourceforge.gedbook.model.core.AnnotatableObject
   * @generated
   */
  EClass getAnnotatableObject();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.AnnotatableObject#getAnnotations <em>Annotations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Annotations</em>'.
   * @see net.sourceforge.gedbook.model.core.AnnotatableObject#getAnnotations()
   * @see #getAnnotatableObject()
   * @generated
   */
  EReference getAnnotatableObject_Annotations();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Note <em>Note</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Note</em>'.
   * @see net.sourceforge.gedbook.model.core.Note
   * @generated
   */
  EClass getNote();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Note#getContent <em>Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Content</em>'.
   * @see net.sourceforge.gedbook.model.core.Note#getContent()
   * @see #getNote()
   * @generated
   */
  EAttribute getNote_Content();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Project <em>Project</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Project</em>'.
   * @see net.sourceforge.gedbook.model.core.Project
   * @generated
   */
  EClass getProject();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Project#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getName()
   * @see #getProject()
   * @generated
   */
  EAttribute getProject_Name();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Project#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getDescription()
   * @see #getProject()
   * @generated
   */
  EAttribute getProject_Description();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedRepositoryPkg <em>Owned Repository Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Repository Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedRepositoryPkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedRepositoryPkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedSourcePkg <em>Owned Source Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Source Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedSourcePkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedSourcePkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedDocumentPkg <em>Owned Document Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Document Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedDocumentPkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedDocumentPkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedPersonPkg <em>Owned Person Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Person Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedPersonPkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedPersonPkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedFamilyPkg <em>Owned Family Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Family Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedFamilyPkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedFamilyPkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedEventPkg <em>Owned Event Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Event Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedEventPkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedEventPkg();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Project#getOwnedPlacePkg <em>Owned Place Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Owned Place Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.Project#getOwnedPlacePkg()
   * @see #getProject()
   * @generated
   */
  EReference getProject_OwnedPlacePkg();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.RepositoryPkg <em>Repository Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repository Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.RepositoryPkg
   * @generated
   */
  EClass getRepositoryPkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.RepositoryPkg#getOwnedRepositories <em>Owned Repositories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Repositories</em>'.
   * @see net.sourceforge.gedbook.model.core.RepositoryPkg#getOwnedRepositories()
   * @see #getRepositoryPkg()
   * @generated
   */
  EReference getRepositoryPkg_OwnedRepositories();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.SourcePkg <em>Source Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.SourcePkg
   * @generated
   */
  EClass getSourcePkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.SourcePkg#getOwnedSources <em>Owned Sources</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Sources</em>'.
   * @see net.sourceforge.gedbook.model.core.SourcePkg#getOwnedSources()
   * @see #getSourcePkg()
   * @generated
   */
  EReference getSourcePkg_OwnedSources();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.DocumentPkg <em>Document Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.DocumentPkg
   * @generated
   */
  EClass getDocumentPkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.DocumentPkg#getOwnedDocuments <em>Owned Documents</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Documents</em>'.
   * @see net.sourceforge.gedbook.model.core.DocumentPkg#getOwnedDocuments()
   * @see #getDocumentPkg()
   * @generated
   */
  EReference getDocumentPkg_OwnedDocuments();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.PersonPkg <em>Person Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Person Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.PersonPkg
   * @generated
   */
  EClass getPersonPkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.PersonPkg#getOwnedPersons <em>Owned Persons</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Persons</em>'.
   * @see net.sourceforge.gedbook.model.core.PersonPkg#getOwnedPersons()
   * @see #getPersonPkg()
   * @generated
   */
  EReference getPersonPkg_OwnedPersons();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.FamilyPkg <em>Family Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Family Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.FamilyPkg
   * @generated
   */
  EClass getFamilyPkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.FamilyPkg#getOwnedFamilies <em>Owned Families</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Families</em>'.
   * @see net.sourceforge.gedbook.model.core.FamilyPkg#getOwnedFamilies()
   * @see #getFamilyPkg()
   * @generated
   */
  EReference getFamilyPkg_OwnedFamilies();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.EventPkg <em>Event Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.EventPkg
   * @generated
   */
  EClass getEventPkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.EventPkg#getOwnedEvents <em>Owned Events</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Events</em>'.
   * @see net.sourceforge.gedbook.model.core.EventPkg#getOwnedEvents()
   * @see #getEventPkg()
   * @generated
   */
  EReference getEventPkg_OwnedEvents();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.PlacePkg <em>Place Pkg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Place Pkg</em>'.
   * @see net.sourceforge.gedbook.model.core.PlacePkg
   * @generated
   */
  EClass getPlacePkg();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.PlacePkg#getOwnedPlaces <em>Owned Places</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Owned Places</em>'.
   * @see net.sourceforge.gedbook.model.core.PlacePkg#getOwnedPlaces()
   * @see #getPlacePkg()
   * @generated
   */
  EReference getPlacePkg_OwnedPlaces();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Repository <em>Repository</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repository</em>'.
   * @see net.sourceforge.gedbook.model.core.Repository
   * @generated
   */
  EClass getRepository();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Repository#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.core.Repository#getName()
   * @see #getRepository()
   * @generated
   */
  EAttribute getRepository_Name();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Repository#getLocation <em>Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Location</em>'.
   * @see net.sourceforge.gedbook.model.core.Repository#getLocation()
   * @see #getRepository()
   * @generated
   */
  EAttribute getRepository_Location();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Source <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source</em>'.
   * @see net.sourceforge.gedbook.model.core.Source
   * @generated
   */
  EClass getSource();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Source#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see net.sourceforge.gedbook.model.core.Source#getTitle()
   * @see #getSource()
   * @generated
   */
  EAttribute getSource_Title();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Source#getArticle <em>Article</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Article</em>'.
   * @see net.sourceforge.gedbook.model.core.Source#getArticle()
   * @see #getSource()
   * @generated
   */
  EAttribute getSource_Article();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Source#getAuthor <em>Author</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Author</em>'.
   * @see net.sourceforge.gedbook.model.core.Source#getAuthor()
   * @see #getSource()
   * @generated
   */
  EAttribute getSource_Author();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Source#getOrigin <em>Origin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Origin</em>'.
   * @see net.sourceforge.gedbook.model.core.Source#getOrigin()
   * @see #getSource()
   * @generated
   */
  EReference getSource_Origin();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Source#getStorage <em>Storage</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Storage</em>'.
   * @see net.sourceforge.gedbook.model.core.Source#getStorage()
   * @see #getSource()
   * @generated
   */
  EReference getSource_Storage();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.SourceCitation <em>Source Citation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source Citation</em>'.
   * @see net.sourceforge.gedbook.model.core.SourceCitation
   * @generated
   */
  EClass getSourceCitation();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.SourceCitation#getPage <em>Page</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Page</em>'.
   * @see net.sourceforge.gedbook.model.core.SourceCitation#getPage()
   * @see #getSourceCitation()
   * @generated
   */
  EAttribute getSourceCitation_Page();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.SourceCitation#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Source</em>'.
   * @see net.sourceforge.gedbook.model.core.SourceCitation#getSource()
   * @see #getSourceCitation()
   * @generated
   */
  EReference getSourceCitation_Source();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Document <em>Document</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document</em>'.
   * @see net.sourceforge.gedbook.model.core.Document
   * @generated
   */
  EClass getDocument();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getAbbreviation <em>Abbreviation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Abbreviation</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getAbbreviation()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Abbreviation();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getAuthor <em>Author</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Author</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getAuthor()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Author();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getAgency <em>Agency</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Agency</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getAgency()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Agency();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getDate <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Date</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getDate()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Date();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getReference <em>Reference</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reference</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getReference()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Reference();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getTitle()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Title();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getPages <em>Pages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pages</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getPages()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Pages();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Document#getKind <em>Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Kind</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getKind()
   * @see #getDocument()
   * @generated
   */
  EAttribute getDocument_Kind();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Document#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Source</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getSource()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_Source();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Document#getPlace <em>Place</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Place</em>'.
   * @see net.sourceforge.gedbook.model.core.Document#getPlace()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_Place();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event</em>'.
   * @see net.sourceforge.gedbook.model.core.Event
   * @generated
   */
  EClass getEvent();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Event#getDate <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Date</em>'.
   * @see net.sourceforge.gedbook.model.core.Event#getDate()
   * @see #getEvent()
   * @generated
   */
  EReference getEvent_Date();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Event#getPlace <em>Place</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Place</em>'.
   * @see net.sourceforge.gedbook.model.core.Event#getPlace()
   * @see #getEvent()
   * @generated
   */
  EReference getEvent_Place();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.core.Event#getCitations <em>Citations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Citations</em>'.
   * @see net.sourceforge.gedbook.model.core.Event#getCitations()
   * @see #getEvent()
   * @generated
   */
  EReference getEvent_Citations();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.PersonEvent <em>Person Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Person Event</em>'.
   * @see net.sourceforge.gedbook.model.core.PersonEvent
   * @generated
   */
  EClass getPersonEvent();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.PersonEvent#getPerson <em>Person</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Person</em>'.
   * @see net.sourceforge.gedbook.model.core.PersonEvent#getPerson()
   * @see #getPersonEvent()
   * @generated
   */
  EReference getPersonEvent_Person();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.FamilyEvent <em>Family Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Family Event</em>'.
   * @see net.sourceforge.gedbook.model.core.FamilyEvent
   * @generated
   */
  EClass getFamilyEvent();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.FamilyEvent#getFamily <em>Family</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Family</em>'.
   * @see net.sourceforge.gedbook.model.core.FamilyEvent#getFamily()
   * @see #getFamilyEvent()
   * @generated
   */
  EReference getFamilyEvent_Family();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.DateWrapper <em>Date Wrapper</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Date Wrapper</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper
   * @generated
   */
  EClass getDateWrapper();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#getDate <em>Date</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Date</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#getDate()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Date();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#getQualifier <em>Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Qualifier</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#getQualifier()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Qualifier();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#isDay <em>Day</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Day</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#isDay()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Day();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#isMonth <em>Month</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Month</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#isMonth()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Month();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#isYear <em>Year</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Year</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#isYear()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Year();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.DateWrapper#getFormat <em>Format</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Format</em>'.
   * @see net.sourceforge.gedbook.model.core.DateWrapper#getFormat()
   * @see #getDateWrapper()
   * @generated
   */
  EAttribute getDateWrapper_Format();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Birth <em>Birth</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Birth</em>'.
   * @see net.sourceforge.gedbook.model.core.Birth
   * @generated
   */
  EClass getBirth();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Batism <em>Batism</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Batism</em>'.
   * @see net.sourceforge.gedbook.model.core.Batism
   * @generated
   */
  EClass getBatism();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Batism#getGodfather <em>Godfather</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Godfather</em>'.
   * @see net.sourceforge.gedbook.model.core.Batism#getGodfather()
   * @see #getBatism()
   * @generated
   */
  EReference getBatism_Godfather();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Batism#getGodmother <em>Godmother</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Godmother</em>'.
   * @see net.sourceforge.gedbook.model.core.Batism#getGodmother()
   * @see #getBatism()
   * @generated
   */
  EReference getBatism_Godmother();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Marriage <em>Marriage</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Marriage</em>'.
   * @see net.sourceforge.gedbook.model.core.Marriage
   * @generated
   */
  EClass getMarriage();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Marriage#getWitnesses <em>Witnesses</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Witnesses</em>'.
   * @see net.sourceforge.gedbook.model.core.Marriage#getWitnesses()
   * @see #getMarriage()
   * @generated
   */
  EReference getMarriage_Witnesses();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Banns <em>Banns</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Banns</em>'.
   * @see net.sourceforge.gedbook.model.core.Banns
   * @generated
   */
  EClass getBanns();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Death <em>Death</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Death</em>'.
   * @see net.sourceforge.gedbook.model.core.Death
   * @generated
   */
  EClass getDeath();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Burial <em>Burial</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Burial</em>'.
   * @see net.sourceforge.gedbook.model.core.Burial
   * @generated
   */
  EClass getBurial();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Will <em>Will</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Will</em>'.
   * @see net.sourceforge.gedbook.model.core.Will
   * @generated
   */
  EClass getWill();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Occupation <em>Occupation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Occupation</em>'.
   * @see net.sourceforge.gedbook.model.core.Occupation
   * @generated
   */
  EClass getOccupation();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Occupation#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Occupation#getDescription()
   * @see #getOccupation()
   * @generated
   */
  EAttribute getOccupation_Description();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Divorce <em>Divorce</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Divorce</em>'.
   * @see net.sourceforge.gedbook.model.core.Divorce
   * @generated
   */
  EClass getDivorce();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Divorce#getWitnesses <em>Witnesses</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Witnesses</em>'.
   * @see net.sourceforge.gedbook.model.core.Divorce#getWitnesses()
   * @see #getDivorce()
   * @generated
   */
  EReference getDivorce_Witnesses();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Anulment <em>Anulment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Anulment</em>'.
   * @see net.sourceforge.gedbook.model.core.Anulment
   * @generated
   */
  EClass getAnulment();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Title <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Title</em>'.
   * @see net.sourceforge.gedbook.model.core.Title
   * @generated
   */
  EClass getTitle();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Title#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see net.sourceforge.gedbook.model.core.Title#getTitle()
   * @see #getTitle()
   * @generated
   */
  EAttribute getTitle_Title();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Nationality <em>Nationality</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nationality</em>'.
   * @see net.sourceforge.gedbook.model.core.Nationality
   * @generated
   */
  EClass getNationality();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Nationality#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Nationality#getDescription()
   * @see #getNationality()
   * @generated
   */
  EAttribute getNationality_Description();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.SocialSecurityNumber <em>Social Security Number</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Social Security Number</em>'.
   * @see net.sourceforge.gedbook.model.core.SocialSecurityNumber
   * @generated
   */
  EClass getSocialSecurityNumber();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.SocialSecurityNumber#getSSN <em>SSN</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>SSN</em>'.
   * @see net.sourceforge.gedbook.model.core.SocialSecurityNumber#getSSN()
   * @see #getSocialSecurityNumber()
   * @generated
   */
  EAttribute getSocialSecurityNumber_SSN();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Property <em>Property</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Property</em>'.
   * @see net.sourceforge.gedbook.model.core.Property
   * @generated
   */
  EClass getProperty();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Property#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Property#getDescription()
   * @see #getProperty()
   * @generated
   */
  EAttribute getProperty_Description();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Residence <em>Residence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Residence</em>'.
   * @see net.sourceforge.gedbook.model.core.Residence
   * @generated
   */
  EClass getResidence();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Residence#getLocation <em>Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Location</em>'.
   * @see net.sourceforge.gedbook.model.core.Residence#getLocation()
   * @see #getResidence()
   * @generated
   */
  EAttribute getResidence_Location();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Ordination <em>Ordination</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ordination</em>'.
   * @see net.sourceforge.gedbook.model.core.Ordination
   * @generated
   */
  EClass getOrdination();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Ordination#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Ordination#getDescription()
   * @see #getOrdination()
   * @generated
   */
  EAttribute getOrdination_Description();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Retirement <em>Retirement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Retirement</em>'.
   * @see net.sourceforge.gedbook.model.core.Retirement
   * @generated
   */
  EClass getRetirement();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Retirement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.core.Retirement#getDescription()
   * @see #getRetirement()
   * @generated
   */
  EAttribute getRetirement_Description();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Census <em>Census</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Census</em>'.
   * @see net.sourceforge.gedbook.model.core.Census
   * @generated
   */
  EClass getCensus();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Person <em>Person</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Person</em>'.
   * @see net.sourceforge.gedbook.model.core.Person
   * @generated
   */
  EClass getPerson();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.core.Person#getNames <em>Names</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Names</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getNames()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Names();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Person#getSex <em>Sex</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Sex</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getSex()
   * @see #getPerson()
   * @generated
   */
  EAttribute getPerson_Sex();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getBirth <em>Birth</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Birth</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getBirth()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Birth();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getBatism <em>Batism</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Batism</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getBatism()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Batism();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getDeath <em>Death</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Death</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getDeath()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Death();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getWill <em>Will</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Will</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getWill()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Will();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getBurial <em>Burial</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Burial</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getBurial()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Burial();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Person#getOccupations <em>Occupations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Occupations</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getOccupations()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Occupations();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Person#getResidences <em>Residences</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Residences</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getResidences()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Residences();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Person#getTitles <em>Titles</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Titles</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getTitles()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Titles();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getFather <em>Father</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Father</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getFather()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Father();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Person#getMother <em>Mother</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Mother</em>'.
   * @see net.sourceforge.gedbook.model.core.Person#getMother()
   * @see #getPerson()
   * @generated
   */
  EReference getPerson_Mother();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Name <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.core.Name
   * @generated
   */
  EClass getName_();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getAdoptionname <em>Adoptionname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Adoptionname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getAdoptionname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Adoptionname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getBirthname <em>Birthname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Birthname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getBirthname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Birthname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getCensusname <em>Censusname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Censusname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getCensusname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Censusname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getCurrentname <em>Currentname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Currentname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getCurrentname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Currentname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getFormalname <em>Formalname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Formalname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getFormalname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Formalname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getGivenname <em>Givenname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Givenname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getGivenname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Givenname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getMarriedname <em>Marriedname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Marriedname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getMarriedname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Marriedname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getSurname <em>Surname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Surname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getSurname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Surname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getName()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Name();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getNickname <em>Nickname</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nickname</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getNickname()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Nickname();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Name#getOthername <em>Othername</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Othername</em>'.
   * @see net.sourceforge.gedbook.model.core.Name#getOthername()
   * @see #getName_()
   * @generated
   */
  EAttribute getName_Othername();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Place <em>Place</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Place</em>'.
   * @see net.sourceforge.gedbook.model.core.Place
   * @generated
   */
  EClass getPlace();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Place#getPays <em>Pays</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pays</em>'.
   * @see net.sourceforge.gedbook.model.core.Place#getPays()
   * @see #getPlace()
   * @generated
   */
  EAttribute getPlace_Pays();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Place#getRegion <em>Region</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Region</em>'.
   * @see net.sourceforge.gedbook.model.core.Place#getRegion()
   * @see #getPlace()
   * @generated
   */
  EAttribute getPlace_Region();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Place#getDepartement <em>Departement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Departement</em>'.
   * @see net.sourceforge.gedbook.model.core.Place#getDepartement()
   * @see #getPlace()
   * @generated
   */
  EAttribute getPlace_Departement();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.core.Place#getCommune <em>Commune</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Commune</em>'.
   * @see net.sourceforge.gedbook.model.core.Place#getCommune()
   * @see #getPlace()
   * @generated
   */
  EAttribute getPlace_Commune();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.core.Family <em>Family</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Family</em>'.
   * @see net.sourceforge.gedbook.model.core.Family
   * @generated
   */
  EClass getFamily();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Family#getHusband <em>Husband</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Husband</em>'.
   * @see net.sourceforge.gedbook.model.core.Family#getHusband()
   * @see #getFamily()
   * @generated
   */
  EReference getFamily_Husband();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.core.Family#getWife <em>Wife</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Wife</em>'.
   * @see net.sourceforge.gedbook.model.core.Family#getWife()
   * @see #getFamily()
   * @generated
   */
  EReference getFamily_Wife();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Family#getChildren <em>Children</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Children</em>'.
   * @see net.sourceforge.gedbook.model.core.Family#getChildren()
   * @see #getFamily()
   * @generated
   */
  EReference getFamily_Children();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.core.Family#getEvents <em>Events</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Events</em>'.
   * @see net.sourceforge.gedbook.model.core.Family#getEvents()
   * @see #getFamily()
   * @generated
   */
  EReference getFamily_Events();

  /**
   * Returns the meta object for enum '{@link net.sourceforge.gedbook.model.core.DocumentKind <em>Document Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Document Kind</em>'.
   * @see net.sourceforge.gedbook.model.core.DocumentKind
   * @generated
   */
  EEnum getDocumentKind();

  /**
   * Returns the meta object for enum '{@link net.sourceforge.gedbook.model.core.DateQualifier <em>Date Qualifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Date Qualifier</em>'.
   * @see net.sourceforge.gedbook.model.core.DateQualifier
   * @generated
   */
  EEnum getDateQualifier();

  /**
   * Returns the meta object for enum '{@link net.sourceforge.gedbook.model.core.SexKind <em>Sex Kind</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Sex Kind</em>'.
   * @see net.sourceforge.gedbook.model.core.SexKind
   * @generated
   */
  EEnum getSexKind();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  CoreFactory getCoreFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.IdentifiableObjectImpl <em>Identifiable Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.IdentifiableObjectImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getIdentifiableObject()
     * @generated
     */
    EClass IDENTIFIABLE_OBJECT = eINSTANCE.getIdentifiableObject();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IDENTIFIABLE_OBJECT__ID = eINSTANCE.getIdentifiableObject_Id();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.AnnotatableObjectImpl <em>Annotatable Object</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.AnnotatableObjectImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getAnnotatableObject()
     * @generated
     */
    EClass ANNOTATABLE_OBJECT = eINSTANCE.getAnnotatableObject();

    /**
     * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ANNOTATABLE_OBJECT__ANNOTATIONS = eINSTANCE.getAnnotatableObject_Annotations();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.NoteImpl <em>Note</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.NoteImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getNote()
     * @generated
     */
    EClass NOTE = eINSTANCE.getNote();

    /**
     * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NOTE__CONTENT = eINSTANCE.getNote_Content();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.ProjectImpl <em>Project</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.ProjectImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getProject()
     * @generated
     */
    EClass PROJECT = eINSTANCE.getProject();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROJECT__NAME = eINSTANCE.getProject_Name();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROJECT__DESCRIPTION = eINSTANCE.getProject_Description();

    /**
     * The meta object literal for the '<em><b>Owned Repository Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_REPOSITORY_PKG = eINSTANCE.getProject_OwnedRepositoryPkg();

    /**
     * The meta object literal for the '<em><b>Owned Source Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_SOURCE_PKG = eINSTANCE.getProject_OwnedSourcePkg();

    /**
     * The meta object literal for the '<em><b>Owned Document Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_DOCUMENT_PKG = eINSTANCE.getProject_OwnedDocumentPkg();

    /**
     * The meta object literal for the '<em><b>Owned Person Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_PERSON_PKG = eINSTANCE.getProject_OwnedPersonPkg();

    /**
     * The meta object literal for the '<em><b>Owned Family Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_FAMILY_PKG = eINSTANCE.getProject_OwnedFamilyPkg();

    /**
     * The meta object literal for the '<em><b>Owned Event Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_EVENT_PKG = eINSTANCE.getProject_OwnedEventPkg();

    /**
     * The meta object literal for the '<em><b>Owned Place Pkg</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROJECT__OWNED_PLACE_PKG = eINSTANCE.getProject_OwnedPlacePkg();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.RepositoryPkgImpl <em>Repository Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.RepositoryPkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRepositoryPkg()
     * @generated
     */
    EClass REPOSITORY_PKG = eINSTANCE.getRepositoryPkg();

    /**
     * The meta object literal for the '<em><b>Owned Repositories</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPOSITORY_PKG__OWNED_REPOSITORIES = eINSTANCE.getRepositoryPkg_OwnedRepositories();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.SourcePkgImpl <em>Source Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.SourcePkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSourcePkg()
     * @generated
     */
    EClass SOURCE_PKG = eINSTANCE.getSourcePkg();

    /**
     * The meta object literal for the '<em><b>Owned Sources</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOURCE_PKG__OWNED_SOURCES = eINSTANCE.getSourcePkg_OwnedSources();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.DocumentPkgImpl <em>Document Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.DocumentPkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocumentPkg()
     * @generated
     */
    EClass DOCUMENT_PKG = eINSTANCE.getDocumentPkg();

    /**
     * The meta object literal for the '<em><b>Owned Documents</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_PKG__OWNED_DOCUMENTS = eINSTANCE.getDocumentPkg_OwnedDocuments();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PersonPkgImpl <em>Person Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PersonPkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPersonPkg()
     * @generated
     */
    EClass PERSON_PKG = eINSTANCE.getPersonPkg();

    /**
     * The meta object literal for the '<em><b>Owned Persons</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON_PKG__OWNED_PERSONS = eINSTANCE.getPersonPkg_OwnedPersons();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyPkgImpl <em>Family Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.FamilyPkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamilyPkg()
     * @generated
     */
    EClass FAMILY_PKG = eINSTANCE.getFamilyPkg();

    /**
     * The meta object literal for the '<em><b>Owned Families</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY_PKG__OWNED_FAMILIES = eINSTANCE.getFamilyPkg_OwnedFamilies();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.EventPkgImpl <em>Event Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.EventPkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getEventPkg()
     * @generated
     */
    EClass EVENT_PKG = eINSTANCE.getEventPkg();

    /**
     * The meta object literal for the '<em><b>Owned Events</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT_PKG__OWNED_EVENTS = eINSTANCE.getEventPkg_OwnedEvents();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PlacePkgImpl <em>Place Pkg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PlacePkgImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPlacePkg()
     * @generated
     */
    EClass PLACE_PKG = eINSTANCE.getPlacePkg();

    /**
     * The meta object literal for the '<em><b>Owned Places</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PLACE_PKG__OWNED_PLACES = eINSTANCE.getPlacePkg_OwnedPlaces();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.RepositoryImpl <em>Repository</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.RepositoryImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRepository()
     * @generated
     */
    EClass REPOSITORY = eINSTANCE.getRepository();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPOSITORY__NAME = eINSTANCE.getRepository_Name();

    /**
     * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPOSITORY__LOCATION = eINSTANCE.getRepository_Location();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.SourceImpl <em>Source</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.SourceImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSource()
     * @generated
     */
    EClass SOURCE = eINSTANCE.getSource();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE__TITLE = eINSTANCE.getSource_Title();

    /**
     * The meta object literal for the '<em><b>Article</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE__ARTICLE = eINSTANCE.getSource_Article();

    /**
     * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE__AUTHOR = eINSTANCE.getSource_Author();

    /**
     * The meta object literal for the '<em><b>Origin</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOURCE__ORIGIN = eINSTANCE.getSource_Origin();

    /**
     * The meta object literal for the '<em><b>Storage</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOURCE__STORAGE = eINSTANCE.getSource_Storage();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.SourceCitationImpl <em>Source Citation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.SourceCitationImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSourceCitation()
     * @generated
     */
    EClass SOURCE_CITATION = eINSTANCE.getSourceCitation();

    /**
     * The meta object literal for the '<em><b>Page</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE_CITATION__PAGE = eINSTANCE.getSourceCitation_Page();

    /**
     * The meta object literal for the '<em><b>Source</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOURCE_CITATION__SOURCE = eINSTANCE.getSourceCitation_Source();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.DocumentImpl <em>Document</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.DocumentImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocument()
     * @generated
     */
    EClass DOCUMENT = eINSTANCE.getDocument();

    /**
     * The meta object literal for the '<em><b>Abbreviation</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__ABBREVIATION = eINSTANCE.getDocument_Abbreviation();

    /**
     * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__AUTHOR = eINSTANCE.getDocument_Author();

    /**
     * The meta object literal for the '<em><b>Agency</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__AGENCY = eINSTANCE.getDocument_Agency();

    /**
     * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__DATE = eINSTANCE.getDocument_Date();

    /**
     * The meta object literal for the '<em><b>Reference</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__REFERENCE = eINSTANCE.getDocument_Reference();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__TITLE = eINSTANCE.getDocument_Title();

    /**
     * The meta object literal for the '<em><b>Pages</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__PAGES = eINSTANCE.getDocument_Pages();

    /**
     * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT__KIND = eINSTANCE.getDocument_Kind();

    /**
     * The meta object literal for the '<em><b>Source</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__SOURCE = eINSTANCE.getDocument_Source();

    /**
     * The meta object literal for the '<em><b>Place</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__PLACE = eINSTANCE.getDocument_Place();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.EventImpl <em>Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.EventImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getEvent()
     * @generated
     */
    EClass EVENT = eINSTANCE.getEvent();

    /**
     * The meta object literal for the '<em><b>Date</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT__DATE = eINSTANCE.getEvent_Date();

    /**
     * The meta object literal for the '<em><b>Place</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT__PLACE = eINSTANCE.getEvent_Place();

    /**
     * The meta object literal for the '<em><b>Citations</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENT__CITATIONS = eINSTANCE.getEvent_Citations();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PersonEventImpl <em>Person Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PersonEventImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPersonEvent()
     * @generated
     */
    EClass PERSON_EVENT = eINSTANCE.getPersonEvent();

    /**
     * The meta object literal for the '<em><b>Person</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON_EVENT__PERSON = eINSTANCE.getPersonEvent_Person();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyEventImpl <em>Family Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.FamilyEventImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamilyEvent()
     * @generated
     */
    EClass FAMILY_EVENT = eINSTANCE.getFamilyEvent();

    /**
     * The meta object literal for the '<em><b>Family</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY_EVENT__FAMILY = eINSTANCE.getFamilyEvent_Family();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl <em>Date Wrapper</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.DateWrapperImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDateWrapper()
     * @generated
     */
    EClass DATE_WRAPPER = eINSTANCE.getDateWrapper();

    /**
     * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__DATE = eINSTANCE.getDateWrapper_Date();

    /**
     * The meta object literal for the '<em><b>Qualifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__QUALIFIER = eINSTANCE.getDateWrapper_Qualifier();

    /**
     * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__DAY = eINSTANCE.getDateWrapper_Day();

    /**
     * The meta object literal for the '<em><b>Month</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__MONTH = eINSTANCE.getDateWrapper_Month();

    /**
     * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__YEAR = eINSTANCE.getDateWrapper_Year();

    /**
     * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DATE_WRAPPER__FORMAT = eINSTANCE.getDateWrapper_Format();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.BirthImpl <em>Birth</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.BirthImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBirth()
     * @generated
     */
    EClass BIRTH = eINSTANCE.getBirth();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.BatismImpl <em>Batism</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.BatismImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBatism()
     * @generated
     */
    EClass BATISM = eINSTANCE.getBatism();

    /**
     * The meta object literal for the '<em><b>Godfather</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BATISM__GODFATHER = eINSTANCE.getBatism_Godfather();

    /**
     * The meta object literal for the '<em><b>Godmother</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference BATISM__GODMOTHER = eINSTANCE.getBatism_Godmother();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.MarriageImpl <em>Marriage</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.MarriageImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getMarriage()
     * @generated
     */
    EClass MARRIAGE = eINSTANCE.getMarriage();

    /**
     * The meta object literal for the '<em><b>Witnesses</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MARRIAGE__WITNESSES = eINSTANCE.getMarriage_Witnesses();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.BannsImpl <em>Banns</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.BannsImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBanns()
     * @generated
     */
    EClass BANNS = eINSTANCE.getBanns();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.DeathImpl <em>Death</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.DeathImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDeath()
     * @generated
     */
    EClass DEATH = eINSTANCE.getDeath();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.BurialImpl <em>Burial</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.BurialImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getBurial()
     * @generated
     */
    EClass BURIAL = eINSTANCE.getBurial();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.WillImpl <em>Will</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.WillImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getWill()
     * @generated
     */
    EClass WILL = eINSTANCE.getWill();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.OccupationImpl <em>Occupation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.OccupationImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getOccupation()
     * @generated
     */
    EClass OCCUPATION = eINSTANCE.getOccupation();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OCCUPATION__DESCRIPTION = eINSTANCE.getOccupation_Description();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.DivorceImpl <em>Divorce</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.DivorceImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDivorce()
     * @generated
     */
    EClass DIVORCE = eINSTANCE.getDivorce();

    /**
     * The meta object literal for the '<em><b>Witnesses</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DIVORCE__WITNESSES = eINSTANCE.getDivorce_Witnesses();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.AnulmentImpl <em>Anulment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.AnulmentImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getAnulment()
     * @generated
     */
    EClass ANULMENT = eINSTANCE.getAnulment();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.TitleImpl <em>Title</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.TitleImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getTitle()
     * @generated
     */
    EClass TITLE = eINSTANCE.getTitle();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TITLE__TITLE = eINSTANCE.getTitle_Title();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.NationalityImpl <em>Nationality</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.NationalityImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getNationality()
     * @generated
     */
    EClass NATIONALITY = eINSTANCE.getNationality();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NATIONALITY__DESCRIPTION = eINSTANCE.getNationality_Description();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.SocialSecurityNumberImpl <em>Social Security Number</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.SocialSecurityNumberImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSocialSecurityNumber()
     * @generated
     */
    EClass SOCIAL_SECURITY_NUMBER = eINSTANCE.getSocialSecurityNumber();

    /**
     * The meta object literal for the '<em><b>SSN</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOCIAL_SECURITY_NUMBER__SSN = eINSTANCE.getSocialSecurityNumber_SSN();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PropertyImpl <em>Property</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PropertyImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getProperty()
     * @generated
     */
    EClass PROPERTY = eINSTANCE.getProperty();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROPERTY__DESCRIPTION = eINSTANCE.getProperty_Description();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.ResidenceImpl <em>Residence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.ResidenceImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getResidence()
     * @generated
     */
    EClass RESIDENCE = eINSTANCE.getResidence();

    /**
     * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RESIDENCE__LOCATION = eINSTANCE.getResidence_Location();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.OrdinationImpl <em>Ordination</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.OrdinationImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getOrdination()
     * @generated
     */
    EClass ORDINATION = eINSTANCE.getOrdination();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORDINATION__DESCRIPTION = eINSTANCE.getOrdination_Description();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.RetirementImpl <em>Retirement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.RetirementImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getRetirement()
     * @generated
     */
    EClass RETIREMENT = eINSTANCE.getRetirement();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETIREMENT__DESCRIPTION = eINSTANCE.getRetirement_Description();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.CensusImpl <em>Census</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.CensusImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getCensus()
     * @generated
     */
    EClass CENSUS = eINSTANCE.getCensus();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PersonImpl <em>Person</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PersonImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPerson()
     * @generated
     */
    EClass PERSON = eINSTANCE.getPerson();

    /**
     * The meta object literal for the '<em><b>Names</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__NAMES = eINSTANCE.getPerson_Names();

    /**
     * The meta object literal for the '<em><b>Sex</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PERSON__SEX = eINSTANCE.getPerson_Sex();

    /**
     * The meta object literal for the '<em><b>Birth</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__BIRTH = eINSTANCE.getPerson_Birth();

    /**
     * The meta object literal for the '<em><b>Batism</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__BATISM = eINSTANCE.getPerson_Batism();

    /**
     * The meta object literal for the '<em><b>Death</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__DEATH = eINSTANCE.getPerson_Death();

    /**
     * The meta object literal for the '<em><b>Will</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__WILL = eINSTANCE.getPerson_Will();

    /**
     * The meta object literal for the '<em><b>Burial</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__BURIAL = eINSTANCE.getPerson_Burial();

    /**
     * The meta object literal for the '<em><b>Occupations</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__OCCUPATIONS = eINSTANCE.getPerson_Occupations();

    /**
     * The meta object literal for the '<em><b>Residences</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__RESIDENCES = eINSTANCE.getPerson_Residences();

    /**
     * The meta object literal for the '<em><b>Titles</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__TITLES = eINSTANCE.getPerson_Titles();

    /**
     * The meta object literal for the '<em><b>Father</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__FATHER = eINSTANCE.getPerson_Father();

    /**
     * The meta object literal for the '<em><b>Mother</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERSON__MOTHER = eINSTANCE.getPerson_Mother();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.NameImpl <em>Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.NameImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getName_()
     * @generated
     */
    EClass NAME = eINSTANCE.getName_();

    /**
     * The meta object literal for the '<em><b>Adoptionname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__ADOPTIONNAME = eINSTANCE.getName_Adoptionname();

    /**
     * The meta object literal for the '<em><b>Birthname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__BIRTHNAME = eINSTANCE.getName_Birthname();

    /**
     * The meta object literal for the '<em><b>Censusname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__CENSUSNAME = eINSTANCE.getName_Censusname();

    /**
     * The meta object literal for the '<em><b>Currentname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__CURRENTNAME = eINSTANCE.getName_Currentname();

    /**
     * The meta object literal for the '<em><b>Formalname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__FORMALNAME = eINSTANCE.getName_Formalname();

    /**
     * The meta object literal for the '<em><b>Givenname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__GIVENNAME = eINSTANCE.getName_Givenname();

    /**
     * The meta object literal for the '<em><b>Marriedname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__MARRIEDNAME = eINSTANCE.getName_Marriedname();

    /**
     * The meta object literal for the '<em><b>Surname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__SURNAME = eINSTANCE.getName_Surname();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__NAME = eINSTANCE.getName_Name();

    /**
     * The meta object literal for the '<em><b>Nickname</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__NICKNAME = eINSTANCE.getName_Nickname();

    /**
     * The meta object literal for the '<em><b>Othername</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute NAME__OTHERNAME = eINSTANCE.getName_Othername();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.PlaceImpl <em>Place</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.PlaceImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getPlace()
     * @generated
     */
    EClass PLACE = eINSTANCE.getPlace();

    /**
     * The meta object literal for the '<em><b>Pays</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLACE__PAYS = eINSTANCE.getPlace_Pays();

    /**
     * The meta object literal for the '<em><b>Region</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLACE__REGION = eINSTANCE.getPlace_Region();

    /**
     * The meta object literal for the '<em><b>Departement</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLACE__DEPARTEMENT = eINSTANCE.getPlace_Departement();

    /**
     * The meta object literal for the '<em><b>Commune</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PLACE__COMMUNE = eINSTANCE.getPlace_Commune();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl <em>Family</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.impl.FamilyImpl
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getFamily()
     * @generated
     */
    EClass FAMILY = eINSTANCE.getFamily();

    /**
     * The meta object literal for the '<em><b>Husband</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY__HUSBAND = eINSTANCE.getFamily_Husband();

    /**
     * The meta object literal for the '<em><b>Wife</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY__WIFE = eINSTANCE.getFamily_Wife();

    /**
     * The meta object literal for the '<em><b>Children</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY__CHILDREN = eINSTANCE.getFamily_Children();

    /**
     * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FAMILY__EVENTS = eINSTANCE.getFamily_Events();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.DocumentKind <em>Document Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.DocumentKind
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDocumentKind()
     * @generated
     */
    EEnum DOCUMENT_KIND = eINSTANCE.getDocumentKind();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.DateQualifier <em>Date Qualifier</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.DateQualifier
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getDateQualifier()
     * @generated
     */
    EEnum DATE_QUALIFIER = eINSTANCE.getDateQualifier();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.core.SexKind <em>Sex Kind</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.core.SexKind
     * @see net.sourceforge.gedbook.model.core.impl.CorePackageImpl#getSexKind()
     * @generated
     */
    EEnum SEX_KIND = eINSTANCE.getSexKind();

  }

} //CorePackage
