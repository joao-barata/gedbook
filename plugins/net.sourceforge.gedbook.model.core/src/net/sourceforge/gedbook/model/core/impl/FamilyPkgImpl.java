/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyPkg;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Family Pkg</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyPkgImpl#getOwnedFamilies <em>Owned Families</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FamilyPkgImpl extends MinimalEObjectImpl.Container implements FamilyPkg {
  /**
   * The cached value of the '{@link #getOwnedFamilies() <em>Owned Families</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOwnedFamilies()
   * @generated
   * @ordered
   */
  protected EList<Family> ownedFamilies;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FamilyPkgImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.FAMILY_PKG;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Family> getOwnedFamilies() {
    if (ownedFamilies == null) {
      ownedFamilies = new EObjectContainmentEList.Resolving<Family>(Family.class, this, CorePackage.FAMILY_PKG__OWNED_FAMILIES);
    }
    return ownedFamilies;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.FAMILY_PKG__OWNED_FAMILIES:
        return ((InternalEList<?>)getOwnedFamilies()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.FAMILY_PKG__OWNED_FAMILIES:
        return getOwnedFamilies();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.FAMILY_PKG__OWNED_FAMILIES:
        getOwnedFamilies().clear();
        getOwnedFamilies().addAll((Collection<? extends Family>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY_PKG__OWNED_FAMILIES:
        getOwnedFamilies().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY_PKG__OWNED_FAMILIES:
        return ownedFamilies != null && !ownedFamilies.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //FamilyPkgImpl
