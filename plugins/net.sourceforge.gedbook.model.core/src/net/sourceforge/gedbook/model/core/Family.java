/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Family</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Family#getHusband <em>Husband</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Family#getWife <em>Wife</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Family#getChildren <em>Children</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Family#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getFamily()
 * @model
 * @generated
 */
public interface Family extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Husband</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Husband</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Husband</em>' reference.
   * @see #setHusband(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getFamily_Husband()
   * @model
   * @generated
   */
  Person getHusband();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Family#getHusband <em>Husband</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Husband</em>' reference.
   * @see #getHusband()
   * @generated
   */
  void setHusband(Person value);

  /**
   * Returns the value of the '<em><b>Wife</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Wife</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Wife</em>' reference.
   * @see #setWife(Person)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getFamily_Wife()
   * @model
   * @generated
   */
  Person getWife();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Family#getWife <em>Wife</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Wife</em>' reference.
   * @see #getWife()
   * @generated
   */
  void setWife(Person value);

  /**
   * Returns the value of the '<em><b>Children</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Person}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Children</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Children</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getFamily_Children()
   * @model
   * @generated
   */
  EList<Person> getChildren();

  /**
   * Returns the value of the '<em><b>Events</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.FamilyEvent}.
   * It is bidirectional and its opposite is '{@link net.sourceforge.gedbook.model.core.FamilyEvent#getFamily <em>Family</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Events</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Events</em>' reference list.
   * @see net.sourceforge.gedbook.model.core.CorePackage#getFamily_Events()
   * @see net.sourceforge.gedbook.model.core.FamilyEvent#getFamily
   * @model opposite="family"
   * @generated
   */
  EList<FamilyEvent> getEvents();

} // Family
