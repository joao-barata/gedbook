/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getAbbreviation <em>Abbreviation</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getAuthor <em>Author</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getAgency <em>Agency</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getReference <em>Reference</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getTitle <em>Title</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getPages <em>Pages</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getKind <em>Kind</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getSource <em>Source</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.Document#getPlace <em>Place</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument()
 * @model
 * @generated
 */
public interface Document extends IdentifiableObject {
  /**
   * Returns the value of the '<em><b>Abbreviation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Abbreviation</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Abbreviation</em>' attribute.
   * @see #setAbbreviation(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Abbreviation()
   * @model
   * @generated
   */
  String getAbbreviation();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getAbbreviation <em>Abbreviation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Abbreviation</em>' attribute.
   * @see #getAbbreviation()
   * @generated
   */
  void setAbbreviation(String value);

  /**
   * Returns the value of the '<em><b>Author</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Author</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Author</em>' attribute.
   * @see #setAuthor(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Author()
   * @model
   * @generated
   */
  String getAuthor();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getAuthor <em>Author</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Author</em>' attribute.
   * @see #getAuthor()
   * @generated
   */
  void setAuthor(String value);

  /**
   * Returns the value of the '<em><b>Agency</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Agency</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Agency</em>' attribute.
   * @see #setAgency(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Agency()
   * @model
   * @generated
   */
  String getAgency();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getAgency <em>Agency</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Agency</em>' attribute.
   * @see #getAgency()
   * @generated
   */
  void setAgency(String value);

  /**
   * Returns the value of the '<em><b>Date</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Date</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Date</em>' attribute.
   * @see #setDate(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Date()
   * @model
   * @generated
   */
  String getDate();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getDate <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Date</em>' attribute.
   * @see #getDate()
   * @generated
   */
  void setDate(String value);

  /**
   * Returns the value of the '<em><b>Reference</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reference</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reference</em>' attribute.
   * @see #setReference(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Reference()
   * @model
   * @generated
   */
  String getReference();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getReference <em>Reference</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reference</em>' attribute.
   * @see #getReference()
   * @generated
   */
  void setReference(String value);

  /**
   * Returns the value of the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Title</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Title</em>' attribute.
   * @see #setTitle(String)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Title()
   * @model
   * @generated
   */
  String getTitle();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getTitle <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Title</em>' attribute.
   * @see #getTitle()
   * @generated
   */
  void setTitle(String value);

  /**
   * Returns the value of the '<em><b>Pages</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pages</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pages</em>' attribute.
   * @see #setPages(int)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Pages()
   * @model
   * @generated
   */
  int getPages();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getPages <em>Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pages</em>' attribute.
   * @see #getPages()
   * @generated
   */
  void setPages(int value);

  /**
   * Returns the value of the '<em><b>Kind</b></em>' attribute.
   * The literals are from the enumeration {@link net.sourceforge.gedbook.model.core.DocumentKind}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Kind</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Kind</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.DocumentKind
   * @see #setKind(DocumentKind)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Kind()
   * @model
   * @generated
   */
  DocumentKind getKind();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getKind <em>Kind</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Kind</em>' attribute.
   * @see net.sourceforge.gedbook.model.core.DocumentKind
   * @see #getKind()
   * @generated
   */
  void setKind(DocumentKind value);

  /**
   * Returns the value of the '<em><b>Source</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source</em>' reference.
   * @see #setSource(Source)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Source()
   * @model
   * @generated
   */
  Source getSource();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getSource <em>Source</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source</em>' reference.
   * @see #getSource()
   * @generated
   */
  void setSource(Source value);

  /**
   * Returns the value of the '<em><b>Place</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Place</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Place</em>' reference.
   * @see #setPlace(Place)
   * @see net.sourceforge.gedbook.model.core.CorePackage#getDocument_Place()
   * @model
   * @generated
   */
  Place getPlace();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.core.Document#getPlace <em>Place</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Place</em>' reference.
   * @see #getPlace()
   * @generated
   */
  void setPlace(Place value);

} // Document
