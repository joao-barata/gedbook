/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Collection;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Person;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Family</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl#getHusband <em>Husband</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl#getWife <em>Wife</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.FamilyImpl#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FamilyImpl extends IdentifiableObjectImpl implements Family {
  /**
   * The cached value of the '{@link #getHusband() <em>Husband</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHusband()
   * @generated
   * @ordered
   */
  protected Person husband;

  /**
   * The cached value of the '{@link #getWife() <em>Wife</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWife()
   * @generated
   * @ordered
   */
  protected Person wife;

  /**
   * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getChildren()
   * @generated
   * @ordered
   */
  protected EList<Person> children;

  /**
   * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEvents()
   * @generated
   * @ordered
   */
  protected EList<FamilyEvent> events;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FamilyImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.FAMILY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getHusband() {
    if (husband != null && husband.eIsProxy()) {
      InternalEObject oldHusband = (InternalEObject)husband;
      husband = (Person)eResolveProxy(oldHusband);
      if (husband != oldHusband) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.FAMILY__HUSBAND, oldHusband, husband));
      }
    }
    return husband;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetHusband() {
    return husband;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHusband(Person newHusband) {
    Person oldHusband = husband;
    husband = newHusband;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.FAMILY__HUSBAND, oldHusband, husband));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person getWife() {
    if (wife != null && wife.eIsProxy()) {
      InternalEObject oldWife = (InternalEObject)wife;
      wife = (Person)eResolveProxy(oldWife);
      if (wife != oldWife) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.FAMILY__WIFE, oldWife, wife));
      }
    }
    return wife;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Person basicGetWife() {
    return wife;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWife(Person newWife) {
    Person oldWife = wife;
    wife = newWife;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.FAMILY__WIFE, oldWife, wife));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Person> getChildren() {
    if (children == null) {
      children = new EObjectResolvingEList<Person>(Person.class, this, CorePackage.FAMILY__CHILDREN);
    }
    return children;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FamilyEvent> getEvents() {
    if (events == null) {
      events = new EObjectWithInverseResolvingEList<FamilyEvent>(FamilyEvent.class, this, CorePackage.FAMILY__EVENTS, CorePackage.FAMILY_EVENT__FAMILY);
    }
    return events;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.FAMILY__EVENTS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getEvents()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case CorePackage.FAMILY__EVENTS:
        return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.FAMILY__HUSBAND:
        if (resolve) return getHusband();
        return basicGetHusband();
      case CorePackage.FAMILY__WIFE:
        if (resolve) return getWife();
        return basicGetWife();
      case CorePackage.FAMILY__CHILDREN:
        return getChildren();
      case CorePackage.FAMILY__EVENTS:
        return getEvents();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.FAMILY__HUSBAND:
        setHusband((Person)newValue);
        return;
      case CorePackage.FAMILY__WIFE:
        setWife((Person)newValue);
        return;
      case CorePackage.FAMILY__CHILDREN:
        getChildren().clear();
        getChildren().addAll((Collection<? extends Person>)newValue);
        return;
      case CorePackage.FAMILY__EVENTS:
        getEvents().clear();
        getEvents().addAll((Collection<? extends FamilyEvent>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY__HUSBAND:
        setHusband((Person)null);
        return;
      case CorePackage.FAMILY__WIFE:
        setWife((Person)null);
        return;
      case CorePackage.FAMILY__CHILDREN:
        getChildren().clear();
        return;
      case CorePackage.FAMILY__EVENTS:
        getEvents().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.FAMILY__HUSBAND:
        return husband != null;
      case CorePackage.FAMILY__WIFE:
        return wife != null;
      case CorePackage.FAMILY__CHILDREN:
        return children != null && !children.isEmpty();
      case CorePackage.FAMILY__EVENTS:
        return events != null && !events.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //FamilyImpl
