/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.impl;

import java.util.Date;

import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.DateQualifier;
import net.sourceforge.gedbook.model.core.DateWrapper;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Date Wrapper</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#getDate <em>Date</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#isDay <em>Day</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#isMonth <em>Month</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#isYear <em>Year</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.core.impl.DateWrapperImpl#getFormat <em>Format</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DateWrapperImpl extends MinimalEObjectImpl.Container implements DateWrapper {
  /**
   * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDate()
   * @generated
   * @ordered
   */
  protected static final Date DATE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDate()
   * @generated
   * @ordered
   */
  protected Date date = DATE_EDEFAULT;

  /**
   * The default value of the '{@link #getQualifier() <em>Qualifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQualifier()
   * @generated
   * @ordered
   */
  protected static final DateQualifier QUALIFIER_EDEFAULT = DateQualifier.UNKOWN;

  /**
   * The cached value of the '{@link #getQualifier() <em>Qualifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQualifier()
   * @generated
   * @ordered
   */
  protected DateQualifier qualifier = QUALIFIER_EDEFAULT;

  /**
   * The default value of the '{@link #isDay() <em>Day</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDay()
   * @generated
   * @ordered
   */
  protected static final boolean DAY_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDay() <em>Day</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDay()
   * @generated
   * @ordered
   */
  protected boolean day = DAY_EDEFAULT;

  /**
   * The default value of the '{@link #isMonth() <em>Month</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMonth()
   * @generated
   * @ordered
   */
  protected static final boolean MONTH_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isMonth() <em>Month</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMonth()
   * @generated
   * @ordered
   */
  protected boolean month = MONTH_EDEFAULT;

  /**
   * The default value of the '{@link #isYear() <em>Year</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isYear()
   * @generated
   * @ordered
   */
  protected static final boolean YEAR_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isYear() <em>Year</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isYear()
   * @generated
   * @ordered
   */
  protected boolean year = YEAR_EDEFAULT;

  /**
   * The default value of the '{@link #getFormat() <em>Format</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormat()
   * @generated
   * @ordered
   */
  protected static final String FORMAT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFormat() <em>Format</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormat()
   * @generated
   * @ordered
   */
  protected String format = FORMAT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DateWrapperImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return CorePackage.Literals.DATE_WRAPPER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Date getDate() {
    return date;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDate(Date newDate) {
    Date oldDate = date;
    date = newDate;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__DATE, oldDate, date));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DateQualifier getQualifier() {
    return qualifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setQualifier(DateQualifier newQualifier) {
    DateQualifier oldQualifier = qualifier;
    qualifier = newQualifier == null ? QUALIFIER_EDEFAULT : newQualifier;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__QUALIFIER, oldQualifier, qualifier));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDay() {
    return day;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDay(boolean newDay) {
    boolean oldDay = day;
    day = newDay;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__DAY, oldDay, day));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isMonth() {
    return month;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMonth(boolean newMonth) {
    boolean oldMonth = month;
    month = newMonth;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__MONTH, oldMonth, month));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isYear() {
    return year;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYear(boolean newYear) {
    boolean oldYear = year;
    year = newYear;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__YEAR, oldYear, year));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFormat() {
    return format;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFormat(String newFormat) {
    String oldFormat = format;
    format = newFormat;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.DATE_WRAPPER__FORMAT, oldFormat, format));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case CorePackage.DATE_WRAPPER__DATE:
        return getDate();
      case CorePackage.DATE_WRAPPER__QUALIFIER:
        return getQualifier();
      case CorePackage.DATE_WRAPPER__DAY:
        return isDay();
      case CorePackage.DATE_WRAPPER__MONTH:
        return isMonth();
      case CorePackage.DATE_WRAPPER__YEAR:
        return isYear();
      case CorePackage.DATE_WRAPPER__FORMAT:
        return getFormat();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case CorePackage.DATE_WRAPPER__DATE:
        setDate((Date)newValue);
        return;
      case CorePackage.DATE_WRAPPER__QUALIFIER:
        setQualifier((DateQualifier)newValue);
        return;
      case CorePackage.DATE_WRAPPER__DAY:
        setDay((Boolean)newValue);
        return;
      case CorePackage.DATE_WRAPPER__MONTH:
        setMonth((Boolean)newValue);
        return;
      case CorePackage.DATE_WRAPPER__YEAR:
        setYear((Boolean)newValue);
        return;
      case CorePackage.DATE_WRAPPER__FORMAT:
        setFormat((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case CorePackage.DATE_WRAPPER__DATE:
        setDate(DATE_EDEFAULT);
        return;
      case CorePackage.DATE_WRAPPER__QUALIFIER:
        setQualifier(QUALIFIER_EDEFAULT);
        return;
      case CorePackage.DATE_WRAPPER__DAY:
        setDay(DAY_EDEFAULT);
        return;
      case CorePackage.DATE_WRAPPER__MONTH:
        setMonth(MONTH_EDEFAULT);
        return;
      case CorePackage.DATE_WRAPPER__YEAR:
        setYear(YEAR_EDEFAULT);
        return;
      case CorePackage.DATE_WRAPPER__FORMAT:
        setFormat(FORMAT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case CorePackage.DATE_WRAPPER__DATE:
        return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
      case CorePackage.DATE_WRAPPER__QUALIFIER:
        return qualifier != QUALIFIER_EDEFAULT;
      case CorePackage.DATE_WRAPPER__DAY:
        return day != DAY_EDEFAULT;
      case CorePackage.DATE_WRAPPER__MONTH:
        return month != MONTH_EDEFAULT;
      case CorePackage.DATE_WRAPPER__YEAR:
        return year != YEAR_EDEFAULT;
      case CorePackage.DATE_WRAPPER__FORMAT:
        return FORMAT_EDEFAULT == null ? format != null : !FORMAT_EDEFAULT.equals(format);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (date: "); //$NON-NLS-1$
    result.append(date);
    result.append(", qualifier: "); //$NON-NLS-1$
    result.append(qualifier);
    result.append(", day: "); //$NON-NLS-1$
    result.append(day);
    result.append(", month: "); //$NON-NLS-1$
    result.append(month);
    result.append(", year: "); //$NON-NLS-1$
    result.append(year);
    result.append(", format: "); //$NON-NLS-1$
    result.append(format);
    result.append(')');
    return result.toString();
  }

} //DateWrapperImpl
