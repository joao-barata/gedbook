/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import net.sourceforge.gedbook.model.core.Project;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extended Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.ExtendedData#getReferencedProject <em>Referenced Project</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.ExtendedData#getGroups <em>Groups</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.ExtendedData#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getExtendedData()
 * @model
 * @generated
 */
public interface ExtendedData extends EObject {
  /**
   * Returns the value of the '<em><b>Referenced Project</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Referenced Project</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Referenced Project</em>' reference.
   * @see #setReferencedProject(Project)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtendedData_ReferencedProject()
   * @model
   * @generated
   */
  Project getReferencedProject();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.ExtendedData#getReferencedProject <em>Referenced Project</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Referenced Project</em>' reference.
   * @see #getReferencedProject()
   * @generated
   */
  void setReferencedProject(Project value);

  /**
   * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.FileGroup}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Groups</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtendedData_Groups()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<FileGroup> getGroups();

  /**
   * Returns the value of the '<em><b>Files</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.SourceFile}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Files</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Files</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtendedData_Files()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<SourceFile> getFiles();

} // ExtendedData
