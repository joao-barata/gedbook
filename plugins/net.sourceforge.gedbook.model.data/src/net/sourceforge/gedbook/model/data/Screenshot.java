/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Screenshot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getXLeftBottom <em>XLeft Bottom</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getYLeftBottom <em>YLeft Bottom</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getXRightTop <em>XRight Top</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getYRightTop <em>YRight Top</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getWidth <em>Width</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getHeight <em>Height</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getPage <em>Page</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getLocation <em>Location</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getTranscription <em>Transcription</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getComment <em>Comment</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Screenshot#getSignature <em>Signature</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot()
 * @model
 * @generated
 */
public interface Screenshot extends EObject {
  /**
   * Returns the value of the '<em><b>XLeft Bottom</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>XLeft Bottom</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>XLeft Bottom</em>' attribute.
   * @see #setXLeftBottom(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_XLeftBottom()
   * @model default="-1"
   * @generated
   */
  int getXLeftBottom();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getXLeftBottom <em>XLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>XLeft Bottom</em>' attribute.
   * @see #getXLeftBottom()
   * @generated
   */
  void setXLeftBottom(int value);

  /**
   * Returns the value of the '<em><b>YLeft Bottom</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>YLeft Bottom</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>YLeft Bottom</em>' attribute.
   * @see #setYLeftBottom(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_YLeftBottom()
   * @model default="-1"
   * @generated
   */
  int getYLeftBottom();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getYLeftBottom <em>YLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>YLeft Bottom</em>' attribute.
   * @see #getYLeftBottom()
   * @generated
   */
  void setYLeftBottom(int value);

  /**
   * Returns the value of the '<em><b>XRight Top</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>XRight Top</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>XRight Top</em>' attribute.
   * @see #setXRightTop(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_XRightTop()
   * @model default="-1"
   * @generated
   */
  int getXRightTop();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getXRightTop <em>XRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>XRight Top</em>' attribute.
   * @see #getXRightTop()
   * @generated
   */
  void setXRightTop(int value);

  /**
   * Returns the value of the '<em><b>YRight Top</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>YRight Top</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>YRight Top</em>' attribute.
   * @see #setYRightTop(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_YRightTop()
   * @model default="-1"
   * @generated
   */
  int getYRightTop();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getYRightTop <em>YRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>YRight Top</em>' attribute.
   * @see #getYRightTop()
   * @generated
   */
  void setYRightTop(int value);

  /**
   * Returns the value of the '<em><b>Width</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Width</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Width</em>' attribute.
   * @see #setWidth(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Width()
   * @model default="-1"
   * @generated
   */
  int getWidth();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getWidth <em>Width</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Width</em>' attribute.
   * @see #getWidth()
   * @generated
   */
  void setWidth(int value);

  /**
   * Returns the value of the '<em><b>Height</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Height</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Height</em>' attribute.
   * @see #setHeight(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Height()
   * @model default="-1"
   * @generated
   */
  int getHeight();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getHeight <em>Height</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Height</em>' attribute.
   * @see #getHeight()
   * @generated
   */
  void setHeight(int value);

  /**
   * Returns the value of the '<em><b>Page</b></em>' attribute.
   * The default value is <code>"-1"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Page</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Page</em>' attribute.
   * @see #setPage(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Page()
   * @model default="-1"
   * @generated
   */
  int getPage();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getPage <em>Page</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Page</em>' attribute.
   * @see #getPage()
   * @generated
   */
  void setPage(int value);

  /**
   * Returns the value of the '<em><b>Location</b></em>' attribute.
   * The literals are from the enumeration {@link net.sourceforge.gedbook.model.data.ScreenshotLocation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Location</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Location</em>' attribute.
   * @see net.sourceforge.gedbook.model.data.ScreenshotLocation
   * @see #setLocation(ScreenshotLocation)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Location()
   * @model
   * @generated
   */
  ScreenshotLocation getLocation();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getLocation <em>Location</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Location</em>' attribute.
   * @see net.sourceforge.gedbook.model.data.ScreenshotLocation
   * @see #getLocation()
   * @generated
   */
  void setLocation(ScreenshotLocation value);

  /**
   * Returns the value of the '<em><b>Transcription</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transcription</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transcription</em>' attribute.
   * @see #setTranscription(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Transcription()
   * @model
   * @generated
   */
  String getTranscription();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getTranscription <em>Transcription</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transcription</em>' attribute.
   * @see #getTranscription()
   * @generated
   */
  void setTranscription(String value);

  /**
   * Returns the value of the '<em><b>Annotation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Annotation</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Annotation</em>' attribute.
   * @see #setAnnotation(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Annotation()
   * @model
   * @generated
   */
  String getAnnotation();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getAnnotation <em>Annotation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Annotation</em>' attribute.
   * @see #getAnnotation()
   * @generated
   */
  void setAnnotation(String value);

  /**
   * Returns the value of the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Comment</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comment</em>' attribute.
   * @see #setComment(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Comment()
   * @model
   * @generated
   */
  String getComment();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getComment <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comment</em>' attribute.
   * @see #getComment()
   * @generated
   */
  void setComment(String value);

  /**
   * Returns the value of the '<em><b>Signature</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Signature</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Signature</em>' attribute.
   * @see #setSignature(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getScreenshot_Signature()
   * @model
   * @generated
   */
  String getSignature();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Screenshot#getSignature <em>Signature</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Signature</em>' attribute.
   * @see #getSignature()
   * @generated
   */
  void setSignature(String value);

} // Screenshot
