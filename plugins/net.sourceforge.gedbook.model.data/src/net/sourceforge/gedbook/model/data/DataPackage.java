/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.data.DataFactory
 * @model kind="package"
 * @generated
 */
public interface DataPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "data";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://net.sourceforge/gedbook/data/0.5.0";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "net.sourceforge.gedbook.model.data";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DataPackage eINSTANCE = net.sourceforge.gedbook.model.data.impl.DataPackageImpl.init();

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl <em>Extended Data</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExtendedData()
   * @generated
   */
  int EXTENDED_DATA = 0;

  /**
   * The feature id for the '<em><b>Referenced Project</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_DATA__REFERENCED_PROJECT = 0;

  /**
   * The feature id for the '<em><b>Groups</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_DATA__GROUPS = 1;

  /**
   * The feature id for the '<em><b>Files</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_DATA__FILES = 2;

  /**
   * The number of structural features of the '<em>Extended Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_DATA_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>Extended Data</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTENDED_DATA_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl <em>File Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.FileGroupImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getFileGroup()
   * @generated
   */
  int FILE_GROUP = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP__NAME = 0;

  /**
   * The feature id for the '<em><b>Exclusions</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP__EXCLUSIONS = 1;

  /**
   * The feature id for the '<em><b>Photos</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP__PHOTOS = 2;

  /**
   * The feature id for the '<em><b>Groups</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP__GROUPS = 3;

  /**
   * The feature id for the '<em><b>Files</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP__FILES = 4;

  /**
   * The number of structural features of the '<em>File Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP_FEATURE_COUNT = 5;

  /**
   * The number of operations of the '<em>File Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FILE_GROUP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.SourceFileImpl <em>Source File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.SourceFileImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getSourceFile()
   * @generated
   */
  int SOURCE_FILE = 2;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FILE__PATH = 0;

  /**
   * The feature id for the '<em><b>Document</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FILE__DOCUMENT = 1;

  /**
   * The number of structural features of the '<em>Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FILE_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SOURCE_FILE_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.SingleSourceFileImpl <em>Single Source File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.SingleSourceFileImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getSingleSourceFile()
   * @generated
   */
  int SINGLE_SOURCE_FILE = 3;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_SOURCE_FILE__PATH = SOURCE_FILE__PATH;

  /**
   * The feature id for the '<em><b>Document</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_SOURCE_FILE__DOCUMENT = SOURCE_FILE__DOCUMENT;

  /**
   * The feature id for the '<em><b>Extract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_SOURCE_FILE__EXTRACT = SOURCE_FILE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Single Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_SOURCE_FILE_FEATURE_COUNT = SOURCE_FILE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Single Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_SOURCE_FILE_OPERATION_COUNT = SOURCE_FILE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl <em>Multiple Source File</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getMultipleSourceFile()
   * @generated
   */
  int MULTIPLE_SOURCE_FILE = 4;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE__PATH = SOURCE_FILE__PATH;

  /**
   * The feature id for the '<em><b>Document</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE__DOCUMENT = SOURCE_FILE__DOCUMENT;

  /**
   * The feature id for the '<em><b>Number Of Pages</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES = SOURCE_FILE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Extracts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE__EXTRACTS = SOURCE_FILE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Multiple Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE_FEATURE_COUNT = SOURCE_FILE_FEATURE_COUNT + 2;

  /**
   * The number of operations of the '<em>Multiple Source File</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_SOURCE_FILE_OPERATION_COUNT = SOURCE_FILE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.ExtractImpl <em>Extract</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.ExtractImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExtract()
   * @generated
   */
  int EXTRACT = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRACT__NAME = 0;

  /**
   * The feature id for the '<em><b>Citations</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRACT__CITATIONS = 1;

  /**
   * The feature id for the '<em><b>Screenshots</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRACT__SCREENSHOTS = 2;

  /**
   * The number of structural features of the '<em>Extract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRACT_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>Extract</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXTRACT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl <em>Screenshot</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.ScreenshotImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getScreenshot()
   * @generated
   */
  int SCREENSHOT = 6;

  /**
   * The feature id for the '<em><b>XLeft Bottom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__XLEFT_BOTTOM = 0;

  /**
   * The feature id for the '<em><b>YLeft Bottom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__YLEFT_BOTTOM = 1;

  /**
   * The feature id for the '<em><b>XRight Top</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__XRIGHT_TOP = 2;

  /**
   * The feature id for the '<em><b>YRight Top</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__YRIGHT_TOP = 3;

  /**
   * The feature id for the '<em><b>Width</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__WIDTH = 4;

  /**
   * The feature id for the '<em><b>Height</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__HEIGHT = 5;

  /**
   * The feature id for the '<em><b>Page</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__PAGE = 6;

  /**
   * The feature id for the '<em><b>Location</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__LOCATION = 7;

  /**
   * The feature id for the '<em><b>Transcription</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__TRANSCRIPTION = 8;

  /**
   * The feature id for the '<em><b>Annotation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__ANNOTATION = 9;

  /**
   * The feature id for the '<em><b>Comment</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__COMMENT = 10;

  /**
   * The feature id for the '<em><b>Signature</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT__SIGNATURE = 11;

  /**
   * The number of structural features of the '<em>Screenshot</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT_FEATURE_COUNT = 12;

  /**
   * The number of operations of the '<em>Screenshot</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCREENSHOT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl <em>Exclusion Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExclusionGroup()
   * @generated
   */
  int EXCLUSION_GROUP = 7;

  /**
   * The feature id for the '<em><b>Exclusion Groups</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_GROUP__EXCLUSION_GROUPS = 0;

  /**
   * The feature id for the '<em><b>Exclusions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_GROUP__EXCLUSIONS = 1;

  /**
   * The number of structural features of the '<em>Exclusion Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_GROUP_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Exclusion Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_GROUP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.ExclusionImpl <em>Exclusion</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.ExclusionImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExclusion()
   * @generated
   */
  int EXCLUSION = 8;

  /**
   * The feature id for the '<em><b>Reason</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION__REASON = 0;

  /**
   * The feature id for the '<em><b>Excluded Events</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION__EXCLUDED_EVENTS = 1;

  /**
   * The number of structural features of the '<em>Exclusion</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Exclusion</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXCLUSION_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl <em>Photo Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getPhotoGroup()
   * @generated
   */
  int PHOTO_GROUP = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_GROUP__NAME = 0;

  /**
   * The feature id for the '<em><b>Photos</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_GROUP__PHOTOS = 1;

  /**
   * The feature id for the '<em><b>Photo</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_GROUP__PHOTO = 2;

  /**
   * The number of structural features of the '<em>Photo Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_GROUP_FEATURE_COUNT = 3;

  /**
   * The number of operations of the '<em>Photo Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_GROUP_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.impl.PhotoImpl <em>Photo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.impl.PhotoImpl
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getPhoto()
   * @generated
   */
  int PHOTO = 10;

  /**
   * The feature id for the '<em><b>Width</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__WIDTH = 0;

  /**
   * The feature id for the '<em><b>Height</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__HEIGHT = 1;

  /**
   * The feature id for the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__PATH = 2;

  /**
   * The feature id for the '<em><b>Caption</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__CAPTION = 3;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__DESCRIPTION = 4;

  /**
   * The feature id for the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__PERSON = 5;

  /**
   * The feature id for the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO__FAMILY = 6;

  /**
   * The number of structural features of the '<em>Photo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_FEATURE_COUNT = 7;

  /**
   * The number of operations of the '<em>Photo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHOTO_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link net.sourceforge.gedbook.model.data.ScreenshotLocation <em>Screenshot Location</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see net.sourceforge.gedbook.model.data.ScreenshotLocation
   * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getScreenshotLocation()
   * @generated
   */
  int SCREENSHOT_LOCATION = 11;


  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.ExtendedData <em>Extended Data</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extended Data</em>'.
   * @see net.sourceforge.gedbook.model.data.ExtendedData
   * @generated
   */
  EClass getExtendedData();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.data.ExtendedData#getReferencedProject <em>Referenced Project</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Referenced Project</em>'.
   * @see net.sourceforge.gedbook.model.data.ExtendedData#getReferencedProject()
   * @see #getExtendedData()
   * @generated
   */
  EReference getExtendedData_ReferencedProject();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.ExtendedData#getGroups <em>Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Groups</em>'.
   * @see net.sourceforge.gedbook.model.data.ExtendedData#getGroups()
   * @see #getExtendedData()
   * @generated
   */
  EReference getExtendedData_Groups();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.ExtendedData#getFiles <em>Files</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Files</em>'.
   * @see net.sourceforge.gedbook.model.data.ExtendedData#getFiles()
   * @see #getExtendedData()
   * @generated
   */
  EReference getExtendedData_Files();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.FileGroup <em>File Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>File Group</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup
   * @generated
   */
  EClass getFileGroup();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.FileGroup#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup#getName()
   * @see #getFileGroup()
   * @generated
   */
  EAttribute getFileGroup_Name();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.data.FileGroup#getExclusions <em>Exclusions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Exclusions</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup#getExclusions()
   * @see #getFileGroup()
   * @generated
   */
  EReference getFileGroup_Exclusions();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.FileGroup#getPhotos <em>Photos</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Photos</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup#getPhotos()
   * @see #getFileGroup()
   * @generated
   */
  EReference getFileGroup_Photos();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.FileGroup#getGroups <em>Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Groups</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup#getGroups()
   * @see #getFileGroup()
   * @generated
   */
  EReference getFileGroup_Groups();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.FileGroup#getFiles <em>Files</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Files</em>'.
   * @see net.sourceforge.gedbook.model.data.FileGroup#getFiles()
   * @see #getFileGroup()
   * @generated
   */
  EReference getFileGroup_Files();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.SourceFile <em>Source File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Source File</em>'.
   * @see net.sourceforge.gedbook.model.data.SourceFile
   * @generated
   */
  EClass getSourceFile();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.SourceFile#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Path</em>'.
   * @see net.sourceforge.gedbook.model.data.SourceFile#getPath()
   * @see #getSourceFile()
   * @generated
   */
  EAttribute getSourceFile_Path();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.data.SourceFile#getDocument <em>Document</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Document</em>'.
   * @see net.sourceforge.gedbook.model.data.SourceFile#getDocument()
   * @see #getSourceFile()
   * @generated
   */
  EReference getSourceFile_Document();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.SingleSourceFile <em>Single Source File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single Source File</em>'.
   * @see net.sourceforge.gedbook.model.data.SingleSourceFile
   * @generated
   */
  EClass getSingleSourceFile();

  /**
   * Returns the meta object for the containment reference '{@link net.sourceforge.gedbook.model.data.SingleSourceFile#getExtract <em>Extract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Extract</em>'.
   * @see net.sourceforge.gedbook.model.data.SingleSourceFile#getExtract()
   * @see #getSingleSourceFile()
   * @generated
   */
  EReference getSingleSourceFile_Extract();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.MultipleSourceFile <em>Multiple Source File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multiple Source File</em>'.
   * @see net.sourceforge.gedbook.model.data.MultipleSourceFile
   * @generated
   */
  EClass getMultipleSourceFile();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.MultipleSourceFile#getNumberOfPages <em>Number Of Pages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Number Of Pages</em>'.
   * @see net.sourceforge.gedbook.model.data.MultipleSourceFile#getNumberOfPages()
   * @see #getMultipleSourceFile()
   * @generated
   */
  EAttribute getMultipleSourceFile_NumberOfPages();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.MultipleSourceFile#getExtracts <em>Extracts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Extracts</em>'.
   * @see net.sourceforge.gedbook.model.data.MultipleSourceFile#getExtracts()
   * @see #getMultipleSourceFile()
   * @generated
   */
  EReference getMultipleSourceFile_Extracts();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.Extract <em>Extract</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Extract</em>'.
   * @see net.sourceforge.gedbook.model.data.Extract
   * @generated
   */
  EClass getExtract();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Extract#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.data.Extract#getName()
   * @see #getExtract()
   * @generated
   */
  EAttribute getExtract_Name();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.data.Extract#getCitations <em>Citations</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Citations</em>'.
   * @see net.sourceforge.gedbook.model.data.Extract#getCitations()
   * @see #getExtract()
   * @generated
   */
  EReference getExtract_Citations();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.Extract#getScreenshots <em>Screenshots</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Screenshots</em>'.
   * @see net.sourceforge.gedbook.model.data.Extract#getScreenshots()
   * @see #getExtract()
   * @generated
   */
  EReference getExtract_Screenshots();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.Screenshot <em>Screenshot</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Screenshot</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot
   * @generated
   */
  EClass getScreenshot();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getXLeftBottom <em>XLeft Bottom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>XLeft Bottom</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getXLeftBottom()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_XLeftBottom();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getYLeftBottom <em>YLeft Bottom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>YLeft Bottom</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getYLeftBottom()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_YLeftBottom();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getXRightTop <em>XRight Top</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>XRight Top</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getXRightTop()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_XRightTop();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getYRightTop <em>YRight Top</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>YRight Top</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getYRightTop()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_YRightTop();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getWidth <em>Width</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Width</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getWidth()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Width();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getHeight <em>Height</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Height</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getHeight()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Height();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getPage <em>Page</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Page</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getPage()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Page();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getLocation <em>Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Location</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getLocation()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Location();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getTranscription <em>Transcription</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Transcription</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getTranscription()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Transcription();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getAnnotation <em>Annotation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Annotation</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getAnnotation()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Annotation();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getComment <em>Comment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Comment</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getComment()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Comment();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Screenshot#getSignature <em>Signature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Signature</em>'.
   * @see net.sourceforge.gedbook.model.data.Screenshot#getSignature()
   * @see #getScreenshot()
   * @generated
   */
  EAttribute getScreenshot_Signature();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.ExclusionGroup <em>Exclusion Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exclusion Group</em>'.
   * @see net.sourceforge.gedbook.model.data.ExclusionGroup
   * @generated
   */
  EClass getExclusionGroup();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusionGroups <em>Exclusion Groups</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exclusion Groups</em>'.
   * @see net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusionGroups()
   * @see #getExclusionGroup()
   * @generated
   */
  EReference getExclusionGroup_ExclusionGroups();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusions <em>Exclusions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Exclusions</em>'.
   * @see net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusions()
   * @see #getExclusionGroup()
   * @generated
   */
  EReference getExclusionGroup_Exclusions();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.Exclusion <em>Exclusion</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Exclusion</em>'.
   * @see net.sourceforge.gedbook.model.data.Exclusion
   * @generated
   */
  EClass getExclusion();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Exclusion#getReason <em>Reason</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Reason</em>'.
   * @see net.sourceforge.gedbook.model.data.Exclusion#getReason()
   * @see #getExclusion()
   * @generated
   */
  EAttribute getExclusion_Reason();

  /**
   * Returns the meta object for the reference list '{@link net.sourceforge.gedbook.model.data.Exclusion#getExcludedEvents <em>Excluded Events</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Excluded Events</em>'.
   * @see net.sourceforge.gedbook.model.data.Exclusion#getExcludedEvents()
   * @see #getExclusion()
   * @generated
   */
  EReference getExclusion_ExcludedEvents();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.PhotoGroup <em>Photo Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Photo Group</em>'.
   * @see net.sourceforge.gedbook.model.data.PhotoGroup
   * @generated
   */
  EClass getPhotoGroup();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.PhotoGroup#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see net.sourceforge.gedbook.model.data.PhotoGroup#getName()
   * @see #getPhotoGroup()
   * @generated
   */
  EAttribute getPhotoGroup_Name();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.PhotoGroup#getPhotos <em>Photos</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Photos</em>'.
   * @see net.sourceforge.gedbook.model.data.PhotoGroup#getPhotos()
   * @see #getPhotoGroup()
   * @generated
   */
  EReference getPhotoGroup_Photos();

  /**
   * Returns the meta object for the containment reference list '{@link net.sourceforge.gedbook.model.data.PhotoGroup#getPhoto <em>Photo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Photo</em>'.
   * @see net.sourceforge.gedbook.model.data.PhotoGroup#getPhoto()
   * @see #getPhotoGroup()
   * @generated
   */
  EReference getPhotoGroup_Photo();

  /**
   * Returns the meta object for class '{@link net.sourceforge.gedbook.model.data.Photo <em>Photo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Photo</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo
   * @generated
   */
  EClass getPhoto();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Photo#getWidth <em>Width</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Width</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getWidth()
   * @see #getPhoto()
   * @generated
   */
  EAttribute getPhoto_Width();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Photo#getHeight <em>Height</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Height</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getHeight()
   * @see #getPhoto()
   * @generated
   */
  EAttribute getPhoto_Height();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Photo#getPath <em>Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Path</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getPath()
   * @see #getPhoto()
   * @generated
   */
  EAttribute getPhoto_Path();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Photo#getCaption <em>Caption</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Caption</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getCaption()
   * @see #getPhoto()
   * @generated
   */
  EAttribute getPhoto_Caption();

  /**
   * Returns the meta object for the attribute '{@link net.sourceforge.gedbook.model.data.Photo#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getDescription()
   * @see #getPhoto()
   * @generated
   */
  EAttribute getPhoto_Description();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.data.Photo#getPerson <em>Person</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Person</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getPerson()
   * @see #getPhoto()
   * @generated
   */
  EReference getPhoto_Person();

  /**
   * Returns the meta object for the reference '{@link net.sourceforge.gedbook.model.data.Photo#getFamily <em>Family</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Family</em>'.
   * @see net.sourceforge.gedbook.model.data.Photo#getFamily()
   * @see #getPhoto()
   * @generated
   */
  EReference getPhoto_Family();

  /**
   * Returns the meta object for enum '{@link net.sourceforge.gedbook.model.data.ScreenshotLocation <em>Screenshot Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Screenshot Location</em>'.
   * @see net.sourceforge.gedbook.model.data.ScreenshotLocation
   * @generated
   */
  EEnum getScreenshotLocation();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  DataFactory getDataFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl <em>Extended Data</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExtendedData()
     * @generated
     */
    EClass EXTENDED_DATA = eINSTANCE.getExtendedData();

    /**
     * The meta object literal for the '<em><b>Referenced Project</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTENDED_DATA__REFERENCED_PROJECT = eINSTANCE.getExtendedData_ReferencedProject();

    /**
     * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTENDED_DATA__GROUPS = eINSTANCE.getExtendedData_Groups();

    /**
     * The meta object literal for the '<em><b>Files</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTENDED_DATA__FILES = eINSTANCE.getExtendedData_Files();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl <em>File Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.FileGroupImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getFileGroup()
     * @generated
     */
    EClass FILE_GROUP = eINSTANCE.getFileGroup();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FILE_GROUP__NAME = eINSTANCE.getFileGroup_Name();

    /**
     * The meta object literal for the '<em><b>Exclusions</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FILE_GROUP__EXCLUSIONS = eINSTANCE.getFileGroup_Exclusions();

    /**
     * The meta object literal for the '<em><b>Photos</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FILE_GROUP__PHOTOS = eINSTANCE.getFileGroup_Photos();

    /**
     * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FILE_GROUP__GROUPS = eINSTANCE.getFileGroup_Groups();

    /**
     * The meta object literal for the '<em><b>Files</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FILE_GROUP__FILES = eINSTANCE.getFileGroup_Files();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.SourceFileImpl <em>Source File</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.SourceFileImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getSourceFile()
     * @generated
     */
    EClass SOURCE_FILE = eINSTANCE.getSourceFile();

    /**
     * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SOURCE_FILE__PATH = eINSTANCE.getSourceFile_Path();

    /**
     * The meta object literal for the '<em><b>Document</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SOURCE_FILE__DOCUMENT = eINSTANCE.getSourceFile_Document();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.SingleSourceFileImpl <em>Single Source File</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.SingleSourceFileImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getSingleSourceFile()
     * @generated
     */
    EClass SINGLE_SOURCE_FILE = eINSTANCE.getSingleSourceFile();

    /**
     * The meta object literal for the '<em><b>Extract</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SINGLE_SOURCE_FILE__EXTRACT = eINSTANCE.getSingleSourceFile_Extract();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl <em>Multiple Source File</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getMultipleSourceFile()
     * @generated
     */
    EClass MULTIPLE_SOURCE_FILE = eINSTANCE.getMultipleSourceFile();

    /**
     * The meta object literal for the '<em><b>Number Of Pages</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES = eINSTANCE.getMultipleSourceFile_NumberOfPages();

    /**
     * The meta object literal for the '<em><b>Extracts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MULTIPLE_SOURCE_FILE__EXTRACTS = eINSTANCE.getMultipleSourceFile_Extracts();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.ExtractImpl <em>Extract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.ExtractImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExtract()
     * @generated
     */
    EClass EXTRACT = eINSTANCE.getExtract();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXTRACT__NAME = eINSTANCE.getExtract_Name();

    /**
     * The meta object literal for the '<em><b>Citations</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTRACT__CITATIONS = eINSTANCE.getExtract_Citations();

    /**
     * The meta object literal for the '<em><b>Screenshots</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXTRACT__SCREENSHOTS = eINSTANCE.getExtract_Screenshots();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl <em>Screenshot</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.ScreenshotImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getScreenshot()
     * @generated
     */
    EClass SCREENSHOT = eINSTANCE.getScreenshot();

    /**
     * The meta object literal for the '<em><b>XLeft Bottom</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__XLEFT_BOTTOM = eINSTANCE.getScreenshot_XLeftBottom();

    /**
     * The meta object literal for the '<em><b>YLeft Bottom</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__YLEFT_BOTTOM = eINSTANCE.getScreenshot_YLeftBottom();

    /**
     * The meta object literal for the '<em><b>XRight Top</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__XRIGHT_TOP = eINSTANCE.getScreenshot_XRightTop();

    /**
     * The meta object literal for the '<em><b>YRight Top</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__YRIGHT_TOP = eINSTANCE.getScreenshot_YRightTop();

    /**
     * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__WIDTH = eINSTANCE.getScreenshot_Width();

    /**
     * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__HEIGHT = eINSTANCE.getScreenshot_Height();

    /**
     * The meta object literal for the '<em><b>Page</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__PAGE = eINSTANCE.getScreenshot_Page();

    /**
     * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__LOCATION = eINSTANCE.getScreenshot_Location();

    /**
     * The meta object literal for the '<em><b>Transcription</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__TRANSCRIPTION = eINSTANCE.getScreenshot_Transcription();

    /**
     * The meta object literal for the '<em><b>Annotation</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__ANNOTATION = eINSTANCE.getScreenshot_Annotation();

    /**
     * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__COMMENT = eINSTANCE.getScreenshot_Comment();

    /**
     * The meta object literal for the '<em><b>Signature</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCREENSHOT__SIGNATURE = eINSTANCE.getScreenshot_Signature();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl <em>Exclusion Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExclusionGroup()
     * @generated
     */
    EClass EXCLUSION_GROUP = eINSTANCE.getExclusionGroup();

    /**
     * The meta object literal for the '<em><b>Exclusion Groups</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXCLUSION_GROUP__EXCLUSION_GROUPS = eINSTANCE.getExclusionGroup_ExclusionGroups();

    /**
     * The meta object literal for the '<em><b>Exclusions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXCLUSION_GROUP__EXCLUSIONS = eINSTANCE.getExclusionGroup_Exclusions();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.ExclusionImpl <em>Exclusion</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.ExclusionImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getExclusion()
     * @generated
     */
    EClass EXCLUSION = eINSTANCE.getExclusion();

    /**
     * The meta object literal for the '<em><b>Reason</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXCLUSION__REASON = eINSTANCE.getExclusion_Reason();

    /**
     * The meta object literal for the '<em><b>Excluded Events</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXCLUSION__EXCLUDED_EVENTS = eINSTANCE.getExclusion_ExcludedEvents();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl <em>Photo Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getPhotoGroup()
     * @generated
     */
    EClass PHOTO_GROUP = eINSTANCE.getPhotoGroup();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO_GROUP__NAME = eINSTANCE.getPhotoGroup_Name();

    /**
     * The meta object literal for the '<em><b>Photos</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO_GROUP__PHOTOS = eINSTANCE.getPhotoGroup_Photos();

    /**
     * The meta object literal for the '<em><b>Photo</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO_GROUP__PHOTO = eINSTANCE.getPhotoGroup_Photo();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.impl.PhotoImpl <em>Photo</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.impl.PhotoImpl
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getPhoto()
     * @generated
     */
    EClass PHOTO = eINSTANCE.getPhoto();

    /**
     * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO__WIDTH = eINSTANCE.getPhoto_Width();

    /**
     * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO__HEIGHT = eINSTANCE.getPhoto_Height();

    /**
     * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO__PATH = eINSTANCE.getPhoto_Path();

    /**
     * The meta object literal for the '<em><b>Caption</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO__CAPTION = eINSTANCE.getPhoto_Caption();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PHOTO__DESCRIPTION = eINSTANCE.getPhoto_Description();

    /**
     * The meta object literal for the '<em><b>Person</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO__PERSON = eINSTANCE.getPhoto_Person();

    /**
     * The meta object literal for the '<em><b>Family</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHOTO__FAMILY = eINSTANCE.getPhoto_Family();

    /**
     * The meta object literal for the '{@link net.sourceforge.gedbook.model.data.ScreenshotLocation <em>Screenshot Location</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see net.sourceforge.gedbook.model.data.ScreenshotLocation
     * @see net.sourceforge.gedbook.model.data.impl.DataPackageImpl#getScreenshotLocation()
     * @generated
     */
    EEnum SCREENSHOT_LOCATION = eINSTANCE.getScreenshotLocation();

  }

} //DataPackage
