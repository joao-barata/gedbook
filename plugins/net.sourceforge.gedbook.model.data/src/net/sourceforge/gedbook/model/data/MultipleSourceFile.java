/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiple Source File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.MultipleSourceFile#getNumberOfPages <em>Number Of Pages</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.MultipleSourceFile#getExtracts <em>Extracts</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getMultipleSourceFile()
 * @model
 * @generated
 */
public interface MultipleSourceFile extends SourceFile {
  /**
   * Returns the value of the '<em><b>Number Of Pages</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Number Of Pages</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Number Of Pages</em>' attribute.
   * @see #setNumberOfPages(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getMultipleSourceFile_NumberOfPages()
   * @model
   * @generated
   */
  int getNumberOfPages();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.MultipleSourceFile#getNumberOfPages <em>Number Of Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Number Of Pages</em>' attribute.
   * @see #getNumberOfPages()
   * @generated
   */
  void setNumberOfPages(int value);

  /**
   * Returns the value of the '<em><b>Extracts</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.Extract}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extracts</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extracts</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getMultipleSourceFile_Extracts()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Extract> getExtracts();

} // MultipleSourceFile
