/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Photo;
import net.sourceforge.gedbook.model.data.PhotoGroup;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Photo Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl#getPhotos <em>Photos</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.PhotoGroupImpl#getPhoto <em>Photo</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhotoGroupImpl extends MinimalEObjectImpl.Container implements PhotoGroup {
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getPhotos() <em>Photos</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPhotos()
   * @generated
   * @ordered
   */
  protected EList<PhotoGroup> photos;

  /**
   * The cached value of the '{@link #getPhoto() <em>Photo</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPhoto()
   * @generated
   * @ordered
   */
  protected EList<Photo> photo;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PhotoGroupImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.PHOTO_GROUP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.PHOTO_GROUP__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PhotoGroup> getPhotos() {
    if (photos == null) {
      photos = new EObjectContainmentEList.Resolving<PhotoGroup>(PhotoGroup.class, this, DataPackage.PHOTO_GROUP__PHOTOS);
    }
    return photos;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Photo> getPhoto() {
    if (photo == null) {
      photo = new EObjectContainmentEList.Resolving<Photo>(Photo.class, this, DataPackage.PHOTO_GROUP__PHOTO);
    }
    return photo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.PHOTO_GROUP__PHOTOS:
        return ((InternalEList<?>)getPhotos()).basicRemove(otherEnd, msgs);
      case DataPackage.PHOTO_GROUP__PHOTO:
        return ((InternalEList<?>)getPhoto()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.PHOTO_GROUP__NAME:
        return getName();
      case DataPackage.PHOTO_GROUP__PHOTOS:
        return getPhotos();
      case DataPackage.PHOTO_GROUP__PHOTO:
        return getPhoto();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.PHOTO_GROUP__NAME:
        setName((String)newValue);
        return;
      case DataPackage.PHOTO_GROUP__PHOTOS:
        getPhotos().clear();
        getPhotos().addAll((Collection<? extends PhotoGroup>)newValue);
        return;
      case DataPackage.PHOTO_GROUP__PHOTO:
        getPhoto().clear();
        getPhoto().addAll((Collection<? extends Photo>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.PHOTO_GROUP__NAME:
        setName(NAME_EDEFAULT);
        return;
      case DataPackage.PHOTO_GROUP__PHOTOS:
        getPhotos().clear();
        return;
      case DataPackage.PHOTO_GROUP__PHOTO:
        getPhoto().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.PHOTO_GROUP__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case DataPackage.PHOTO_GROUP__PHOTOS:
        return photos != null && !photos.isEmpty();
      case DataPackage.PHOTO_GROUP__PHOTO:
        return photo != null && !photo.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //PhotoGroupImpl
