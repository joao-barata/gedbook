/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import net.sourceforge.gedbook.model.core.Event;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exclusion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.Exclusion#getReason <em>Reason</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Exclusion#getExcludedEvents <em>Excluded Events</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusion()
 * @model
 * @generated
 */
public interface Exclusion extends EObject {
  /**
   * Returns the value of the '<em><b>Reason</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reason</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reason</em>' attribute.
   * @see #setReason(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusion_Reason()
   * @model
   * @generated
   */
  String getReason();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Exclusion#getReason <em>Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reason</em>' attribute.
   * @see #getReason()
   * @generated
   */
  void setReason(String value);

  /**
   * Returns the value of the '<em><b>Excluded Events</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.Event}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Excluded Events</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Excluded Events</em>' reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusion_ExcludedEvents()
   * @model
   * @generated
   */
  EList<Event> getExcludedEvents();

} // Exclusion
