/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.util;

import net.sourceforge.gedbook.model.data.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.data.DataPackage
 * @generated
 */
public class DataAdapterFactory extends AdapterFactoryImpl {
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static DataPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataAdapterFactory() {
    if (modelPackage == null) {
      modelPackage = DataPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object) {
    if (object == modelPackage) {
      return true;
    }
    if (object instanceof EObject) {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataSwitch<Adapter> modelSwitch =
    new DataSwitch<Adapter>() {
      @Override
      public Adapter caseExtendedData(ExtendedData object) {
        return createExtendedDataAdapter();
      }
      @Override
      public Adapter caseFileGroup(FileGroup object) {
        return createFileGroupAdapter();
      }
      @Override
      public Adapter caseSourceFile(SourceFile object) {
        return createSourceFileAdapter();
      }
      @Override
      public Adapter caseSingleSourceFile(SingleSourceFile object) {
        return createSingleSourceFileAdapter();
      }
      @Override
      public Adapter caseMultipleSourceFile(MultipleSourceFile object) {
        return createMultipleSourceFileAdapter();
      }
      @Override
      public Adapter caseExtract(Extract object) {
        return createExtractAdapter();
      }
      @Override
      public Adapter caseScreenshot(Screenshot object) {
        return createScreenshotAdapter();
      }
      @Override
      public Adapter caseExclusionGroup(ExclusionGroup object) {
        return createExclusionGroupAdapter();
      }
      @Override
      public Adapter caseExclusion(Exclusion object) {
        return createExclusionAdapter();
      }
      @Override
      public Adapter casePhotoGroup(PhotoGroup object) {
        return createPhotoGroupAdapter();
      }
      @Override
      public Adapter casePhoto(Photo object) {
        return createPhotoAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object) {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.ExtendedData <em>Extended Data</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.ExtendedData
   * @generated
   */
  public Adapter createExtendedDataAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.FileGroup <em>File Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.FileGroup
   * @generated
   */
  public Adapter createFileGroupAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.SourceFile <em>Source File</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.SourceFile
   * @generated
   */
  public Adapter createSourceFileAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.SingleSourceFile <em>Single Source File</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.SingleSourceFile
   * @generated
   */
  public Adapter createSingleSourceFileAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.MultipleSourceFile <em>Multiple Source File</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.MultipleSourceFile
   * @generated
   */
  public Adapter createMultipleSourceFileAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.Extract <em>Extract</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.Extract
   * @generated
   */
  public Adapter createExtractAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.Screenshot <em>Screenshot</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.Screenshot
   * @generated
   */
  public Adapter createScreenshotAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.ExclusionGroup <em>Exclusion Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.ExclusionGroup
   * @generated
   */
  public Adapter createExclusionGroupAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.Exclusion <em>Exclusion</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.Exclusion
   * @generated
   */
  public Adapter createExclusionAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.PhotoGroup <em>Photo Group</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.PhotoGroup
   * @generated
   */
  public Adapter createPhotoGroupAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link net.sourceforge.gedbook.model.data.Photo <em>Photo</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see net.sourceforge.gedbook.model.data.Photo
   * @generated
   */
  public Adapter createPhotoAdapter() {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter() {
    return null;
  }

} //DataAdapterFactory
