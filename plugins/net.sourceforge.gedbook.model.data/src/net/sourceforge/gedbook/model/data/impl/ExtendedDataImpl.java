/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.core.Project;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.data.FileGroup;
import net.sourceforge.gedbook.model.data.SourceFile;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extended Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl#getReferencedProject <em>Referenced Project</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl#getGroups <em>Groups</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtendedDataImpl#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtendedDataImpl extends MinimalEObjectImpl.Container implements ExtendedData {
  /**
   * The cached value of the '{@link #getReferencedProject() <em>Referenced Project</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReferencedProject()
   * @generated
   * @ordered
   */
  protected Project referencedProject;

  /**
   * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroups()
   * @generated
   * @ordered
   */
  protected EList<FileGroup> groups;

  /**
   * The cached value of the '{@link #getFiles() <em>Files</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFiles()
   * @generated
   * @ordered
   */
  protected EList<SourceFile> files;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExtendedDataImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.EXTENDED_DATA;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Project getReferencedProject() {
    if (referencedProject != null && referencedProject.eIsProxy()) {
      InternalEObject oldReferencedProject = (InternalEObject)referencedProject;
      referencedProject = (Project)eResolveProxy(oldReferencedProject);
      if (referencedProject != oldReferencedProject) {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, DataPackage.EXTENDED_DATA__REFERENCED_PROJECT, oldReferencedProject, referencedProject));
      }
    }
    return referencedProject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Project basicGetReferencedProject() {
    return referencedProject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReferencedProject(Project newReferencedProject) {
    Project oldReferencedProject = referencedProject;
    referencedProject = newReferencedProject;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.EXTENDED_DATA__REFERENCED_PROJECT, oldReferencedProject, referencedProject));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FileGroup> getGroups() {
    if (groups == null) {
      groups = new EObjectContainmentEList.Resolving<FileGroup>(FileGroup.class, this, DataPackage.EXTENDED_DATA__GROUPS);
    }
    return groups;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceFile> getFiles() {
    if (files == null) {
      files = new EObjectContainmentEList.Resolving<SourceFile>(SourceFile.class, this, DataPackage.EXTENDED_DATA__FILES);
    }
    return files;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.EXTENDED_DATA__GROUPS:
        return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
      case DataPackage.EXTENDED_DATA__FILES:
        return ((InternalEList<?>)getFiles()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.EXTENDED_DATA__REFERENCED_PROJECT:
        if (resolve) return getReferencedProject();
        return basicGetReferencedProject();
      case DataPackage.EXTENDED_DATA__GROUPS:
        return getGroups();
      case DataPackage.EXTENDED_DATA__FILES:
        return getFiles();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.EXTENDED_DATA__REFERENCED_PROJECT:
        setReferencedProject((Project)newValue);
        return;
      case DataPackage.EXTENDED_DATA__GROUPS:
        getGroups().clear();
        getGroups().addAll((Collection<? extends FileGroup>)newValue);
        return;
      case DataPackage.EXTENDED_DATA__FILES:
        getFiles().clear();
        getFiles().addAll((Collection<? extends SourceFile>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.EXTENDED_DATA__REFERENCED_PROJECT:
        setReferencedProject((Project)null);
        return;
      case DataPackage.EXTENDED_DATA__GROUPS:
        getGroups().clear();
        return;
      case DataPackage.EXTENDED_DATA__FILES:
        getFiles().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.EXTENDED_DATA__REFERENCED_PROJECT:
        return referencedProject != null;
      case DataPackage.EXTENDED_DATA__GROUPS:
        return groups != null && !groups.isEmpty();
      case DataPackage.EXTENDED_DATA__FILES:
        return files != null && !files.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ExtendedDataImpl
