/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.PhotoGroup#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.PhotoGroup#getPhotos <em>Photos</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.PhotoGroup#getPhoto <em>Photo</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getPhotoGroup()
 * @model
 * @generated
 */
public interface PhotoGroup extends EObject {
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhotoGroup_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.PhotoGroup#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Photos</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.PhotoGroup}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Photos</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Photos</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhotoGroup_Photos()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<PhotoGroup> getPhotos();

  /**
   * Returns the value of the '<em><b>Photo</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.Photo}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Photo</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Photo</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhotoGroup_Photo()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Photo> getPhoto();

} // PhotoGroup
