/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.core.Event;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Exclusion;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exclusion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExclusionImpl#getReason <em>Reason</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExclusionImpl#getExcludedEvents <em>Excluded Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExclusionImpl extends MinimalEObjectImpl.Container implements Exclusion {
  /**
   * The default value of the '{@link #getReason() <em>Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReason()
   * @generated
   * @ordered
   */
  protected static final String REASON_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getReason() <em>Reason</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReason()
   * @generated
   * @ordered
   */
  protected String reason = REASON_EDEFAULT;

  /**
   * The cached value of the '{@link #getExcludedEvents() <em>Excluded Events</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExcludedEvents()
   * @generated
   * @ordered
   */
  protected EList<Event> excludedEvents;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExclusionImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.EXCLUSION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getReason() {
    return reason;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReason(String newReason) {
    String oldReason = reason;
    reason = newReason;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.EXCLUSION__REASON, oldReason, reason));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Event> getExcludedEvents() {
    if (excludedEvents == null) {
      excludedEvents = new EObjectResolvingEList<Event>(Event.class, this, DataPackage.EXCLUSION__EXCLUDED_EVENTS);
    }
    return excludedEvents;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.EXCLUSION__REASON:
        return getReason();
      case DataPackage.EXCLUSION__EXCLUDED_EVENTS:
        return getExcludedEvents();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.EXCLUSION__REASON:
        setReason((String)newValue);
        return;
      case DataPackage.EXCLUSION__EXCLUDED_EVENTS:
        getExcludedEvents().clear();
        getExcludedEvents().addAll((Collection<? extends Event>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.EXCLUSION__REASON:
        setReason(REASON_EDEFAULT);
        return;
      case DataPackage.EXCLUSION__EXCLUDED_EVENTS:
        getExcludedEvents().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.EXCLUSION__REASON:
        return REASON_EDEFAULT == null ? reason != null : !REASON_EDEFAULT.equals(reason);
      case DataPackage.EXCLUSION__EXCLUDED_EVENTS:
        return excludedEvents != null && !excludedEvents.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (reason: ");
    result.append(reason);
    result.append(')');
    return result.toString();
  }

} //ExclusionImpl
