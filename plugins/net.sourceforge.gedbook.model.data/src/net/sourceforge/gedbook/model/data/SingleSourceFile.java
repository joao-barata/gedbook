/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Source File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.SingleSourceFile#getExtract <em>Extract</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getSingleSourceFile()
 * @model
 * @generated
 */
public interface SingleSourceFile extends SourceFile {
  /**
   * Returns the value of the '<em><b>Extract</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Extract</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Extract</em>' containment reference.
   * @see #setExtract(Extract)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getSingleSourceFile_Extract()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  Extract getExtract();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.SingleSourceFile#getExtract <em>Extract</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Extract</em>' containment reference.
   * @see #getExtract()
   * @generated
   */
  void setExtract(Extract value);

} // SingleSourceFile
