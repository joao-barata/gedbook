/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.SingleSourceFile;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Source File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.SingleSourceFileImpl#getExtract <em>Extract</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleSourceFileImpl extends SourceFileImpl implements SingleSourceFile {
  /**
   * The cached value of the '{@link #getExtract() <em>Extract</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtract()
   * @generated
   * @ordered
   */
  protected Extract extract;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleSourceFileImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.SINGLE_SOURCE_FILE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Extract getExtract() {
    if (extract != null && extract.eIsProxy()) {
      InternalEObject oldExtract = (InternalEObject)extract;
      extract = (Extract)eResolveProxy(oldExtract);
      if (extract != oldExtract) {
        InternalEObject newExtract = (InternalEObject)extract;
        NotificationChain msgs = oldExtract.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DataPackage.SINGLE_SOURCE_FILE__EXTRACT, null, null);
        if (newExtract.eInternalContainer() == null) {
          msgs = newExtract.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DataPackage.SINGLE_SOURCE_FILE__EXTRACT, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, DataPackage.SINGLE_SOURCE_FILE__EXTRACT, oldExtract, extract));
      }
    }
    return extract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Extract basicGetExtract() {
    return extract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExtract(Extract newExtract, NotificationChain msgs) {
    Extract oldExtract = extract;
    extract = newExtract;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DataPackage.SINGLE_SOURCE_FILE__EXTRACT, oldExtract, newExtract);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExtract(Extract newExtract) {
    if (newExtract != extract) {
      NotificationChain msgs = null;
      if (extract != null)
        msgs = ((InternalEObject)extract).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DataPackage.SINGLE_SOURCE_FILE__EXTRACT, null, msgs);
      if (newExtract != null)
        msgs = ((InternalEObject)newExtract).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DataPackage.SINGLE_SOURCE_FILE__EXTRACT, null, msgs);
      msgs = basicSetExtract(newExtract, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SINGLE_SOURCE_FILE__EXTRACT, newExtract, newExtract));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.SINGLE_SOURCE_FILE__EXTRACT:
        return basicSetExtract(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.SINGLE_SOURCE_FILE__EXTRACT:
        if (resolve) return getExtract();
        return basicGetExtract();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.SINGLE_SOURCE_FILE__EXTRACT:
        setExtract((Extract)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.SINGLE_SOURCE_FILE__EXTRACT:
        setExtract((Extract)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.SINGLE_SOURCE_FILE__EXTRACT:
        return extract != null;
    }
    return super.eIsSet(featureID);
  }

} //SingleSourceFileImpl
