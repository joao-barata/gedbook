/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exclusion Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusionGroups <em>Exclusion Groups</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.ExclusionGroup#getExclusions <em>Exclusions</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusionGroup()
 * @model
 * @generated
 */
public interface ExclusionGroup extends EObject {
  /**
   * Returns the value of the '<em><b>Exclusion Groups</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.ExclusionGroup}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exclusion Groups</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exclusion Groups</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusionGroup_ExclusionGroups()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<ExclusionGroup> getExclusionGroups();

  /**
   * Returns the value of the '<em><b>Exclusions</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.Exclusion}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Exclusions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exclusions</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExclusionGroup_Exclusions()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Exclusion> getExclusions();

} // ExclusionGroup
