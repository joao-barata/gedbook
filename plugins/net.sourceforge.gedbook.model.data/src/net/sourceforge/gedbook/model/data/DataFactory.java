/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.data.DataPackage
 * @generated
 */
public interface DataFactory extends EFactory {
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  DataFactory eINSTANCE = net.sourceforge.gedbook.model.data.impl.DataFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Extended Data</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extended Data</em>'.
   * @generated
   */
  ExtendedData createExtendedData();

  /**
   * Returns a new object of class '<em>File Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>File Group</em>'.
   * @generated
   */
  FileGroup createFileGroup();

  /**
   * Returns a new object of class '<em>Single Source File</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Single Source File</em>'.
   * @generated
   */
  SingleSourceFile createSingleSourceFile();

  /**
   * Returns a new object of class '<em>Multiple Source File</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Multiple Source File</em>'.
   * @generated
   */
  MultipleSourceFile createMultipleSourceFile();

  /**
   * Returns a new object of class '<em>Extract</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Extract</em>'.
   * @generated
   */
  Extract createExtract();

  /**
   * Returns a new object of class '<em>Screenshot</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Screenshot</em>'.
   * @generated
   */
  Screenshot createScreenshot();

  /**
   * Returns a new object of class '<em>Exclusion Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exclusion Group</em>'.
   * @generated
   */
  ExclusionGroup createExclusionGroup();

  /**
   * Returns a new object of class '<em>Exclusion</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Exclusion</em>'.
   * @generated
   */
  Exclusion createExclusion();

  /**
   * Returns a new object of class '<em>Photo Group</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Photo Group</em>'.
   * @generated
   */
  PhotoGroup createPhotoGroup();

  /**
   * Returns a new object of class '<em>Photo</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Photo</em>'.
   * @generated
   */
  Photo createPhoto();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  DataPackage getDataPackage();

} //DataFactory
