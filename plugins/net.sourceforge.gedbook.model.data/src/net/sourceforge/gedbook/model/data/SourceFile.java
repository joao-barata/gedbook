/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import net.sourceforge.gedbook.model.core.Document;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.SourceFile#getPath <em>Path</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.SourceFile#getDocument <em>Document</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getSourceFile()
 * @model abstract="true"
 * @generated
 */
public interface SourceFile extends EObject {
  /**
   * Returns the value of the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Path</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path</em>' attribute.
   * @see #setPath(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getSourceFile_Path()
   * @model
   * @generated
   */
  String getPath();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.SourceFile#getPath <em>Path</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path</em>' attribute.
   * @see #getPath()
   * @generated
   */
  void setPath(String value);

  /**
   * Returns the value of the '<em><b>Document</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Document</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Document</em>' reference.
   * @see #setDocument(Document)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getSourceFile_Document()
   * @model
   * @generated
   */
  Document getDocument();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.SourceFile#getDocument <em>Document</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Document</em>' reference.
   * @see #getDocument()
   * @generated
   */
  void setDocument(Document value);

} // SourceFile
