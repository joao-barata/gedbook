/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.ScreenshotLocation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Screenshot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getXLeftBottom <em>XLeft Bottom</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getYLeftBottom <em>YLeft Bottom</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getXRightTop <em>XRight Top</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getYRightTop <em>YRight Top</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getHeight <em>Height</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getPage <em>Page</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getTranscription <em>Transcription</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getAnnotation <em>Annotation</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ScreenshotImpl#getSignature <em>Signature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScreenshotImpl extends MinimalEObjectImpl.Container implements Screenshot {
  /**
   * The default value of the '{@link #getXLeftBottom() <em>XLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXLeftBottom()
   * @generated
   * @ordered
   */
  protected static final int XLEFT_BOTTOM_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getXLeftBottom() <em>XLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXLeftBottom()
   * @generated
   * @ordered
   */
  protected int xLeftBottom = XLEFT_BOTTOM_EDEFAULT;

  /**
   * The default value of the '{@link #getYLeftBottom() <em>YLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYLeftBottom()
   * @generated
   * @ordered
   */
  protected static final int YLEFT_BOTTOM_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getYLeftBottom() <em>YLeft Bottom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYLeftBottom()
   * @generated
   * @ordered
   */
  protected int yLeftBottom = YLEFT_BOTTOM_EDEFAULT;

  /**
   * The default value of the '{@link #getXRightTop() <em>XRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXRightTop()
   * @generated
   * @ordered
   */
  protected static final int XRIGHT_TOP_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getXRightTop() <em>XRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getXRightTop()
   * @generated
   * @ordered
   */
  protected int xRightTop = XRIGHT_TOP_EDEFAULT;

  /**
   * The default value of the '{@link #getYRightTop() <em>YRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYRightTop()
   * @generated
   * @ordered
   */
  protected static final int YRIGHT_TOP_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getYRightTop() <em>YRight Top</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getYRightTop()
   * @generated
   * @ordered
   */
  protected int yRightTop = YRIGHT_TOP_EDEFAULT;

  /**
   * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWidth()
   * @generated
   * @ordered
   */
  protected static final int WIDTH_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWidth()
   * @generated
   * @ordered
   */
  protected int width = WIDTH_EDEFAULT;

  /**
   * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHeight()
   * @generated
   * @ordered
   */
  protected static final int HEIGHT_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHeight()
   * @generated
   * @ordered
   */
  protected int height = HEIGHT_EDEFAULT;

  /**
   * The default value of the '{@link #getPage() <em>Page</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPage()
   * @generated
   * @ordered
   */
  protected static final int PAGE_EDEFAULT = -1;

  /**
   * The cached value of the '{@link #getPage() <em>Page</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPage()
   * @generated
   * @ordered
   */
  protected int page = PAGE_EDEFAULT;

  /**
   * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocation()
   * @generated
   * @ordered
   */
  protected static final ScreenshotLocation LOCATION_EDEFAULT = ScreenshotLocation.INLINE;

  /**
   * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLocation()
   * @generated
   * @ordered
   */
  protected ScreenshotLocation location = LOCATION_EDEFAULT;

  /**
   * The default value of the '{@link #getTranscription() <em>Transcription</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTranscription()
   * @generated
   * @ordered
   */
  protected static final String TRANSCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTranscription() <em>Transcription</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTranscription()
   * @generated
   * @ordered
   */
  protected String transcription = TRANSCRIPTION_EDEFAULT;

  /**
   * The default value of the '{@link #getAnnotation() <em>Annotation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnnotation()
   * @generated
   * @ordered
   */
  protected static final String ANNOTATION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAnnotation() <em>Annotation</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnnotation()
   * @generated
   * @ordered
   */
  protected String annotation = ANNOTATION_EDEFAULT;

  /**
   * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected static final String COMMENT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComment()
   * @generated
   * @ordered
   */
  protected String comment = COMMENT_EDEFAULT;

  /**
   * The default value of the '{@link #getSignature() <em>Signature</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignature()
   * @generated
   * @ordered
   */
  protected static final String SIGNATURE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getSignature() <em>Signature</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSignature()
   * @generated
   * @ordered
   */
  protected String signature = SIGNATURE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ScreenshotImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.SCREENSHOT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getXLeftBottom() {
    return xLeftBottom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setXLeftBottom(int newXLeftBottom) {
    int oldXLeftBottom = xLeftBottom;
    xLeftBottom = newXLeftBottom;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__XLEFT_BOTTOM, oldXLeftBottom, xLeftBottom));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getYLeftBottom() {
    return yLeftBottom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYLeftBottom(int newYLeftBottom) {
    int oldYLeftBottom = yLeftBottom;
    yLeftBottom = newYLeftBottom;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__YLEFT_BOTTOM, oldYLeftBottom, yLeftBottom));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getXRightTop() {
    return xRightTop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setXRightTop(int newXRightTop) {
    int oldXRightTop = xRightTop;
    xRightTop = newXRightTop;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__XRIGHT_TOP, oldXRightTop, xRightTop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getYRightTop() {
    return yRightTop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setYRightTop(int newYRightTop) {
    int oldYRightTop = yRightTop;
    yRightTop = newYRightTop;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__YRIGHT_TOP, oldYRightTop, yRightTop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getWidth() {
    return width;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWidth(int newWidth) {
    int oldWidth = width;
    width = newWidth;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__WIDTH, oldWidth, width));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getHeight() {
    return height;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHeight(int newHeight) {
    int oldHeight = height;
    height = newHeight;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__HEIGHT, oldHeight, height));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getPage() {
    return page;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPage(int newPage) {
    int oldPage = page;
    page = newPage;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__PAGE, oldPage, page));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ScreenshotLocation getLocation() {
    return location;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLocation(ScreenshotLocation newLocation) {
    ScreenshotLocation oldLocation = location;
    location = newLocation == null ? LOCATION_EDEFAULT : newLocation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__LOCATION, oldLocation, location));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTranscription() {
    return transcription;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTranscription(String newTranscription) {
    String oldTranscription = transcription;
    transcription = newTranscription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__TRANSCRIPTION, oldTranscription, transcription));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAnnotation() {
    return annotation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAnnotation(String newAnnotation) {
    String oldAnnotation = annotation;
    annotation = newAnnotation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__ANNOTATION, oldAnnotation, annotation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getComment() {
    return comment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComment(String newComment) {
    String oldComment = comment;
    comment = newComment;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__COMMENT, oldComment, comment));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getSignature() {
    return signature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSignature(String newSignature) {
    String oldSignature = signature;
    signature = newSignature;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SCREENSHOT__SIGNATURE, oldSignature, signature));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.SCREENSHOT__XLEFT_BOTTOM:
        return getXLeftBottom();
      case DataPackage.SCREENSHOT__YLEFT_BOTTOM:
        return getYLeftBottom();
      case DataPackage.SCREENSHOT__XRIGHT_TOP:
        return getXRightTop();
      case DataPackage.SCREENSHOT__YRIGHT_TOP:
        return getYRightTop();
      case DataPackage.SCREENSHOT__WIDTH:
        return getWidth();
      case DataPackage.SCREENSHOT__HEIGHT:
        return getHeight();
      case DataPackage.SCREENSHOT__PAGE:
        return getPage();
      case DataPackage.SCREENSHOT__LOCATION:
        return getLocation();
      case DataPackage.SCREENSHOT__TRANSCRIPTION:
        return getTranscription();
      case DataPackage.SCREENSHOT__ANNOTATION:
        return getAnnotation();
      case DataPackage.SCREENSHOT__COMMENT:
        return getComment();
      case DataPackage.SCREENSHOT__SIGNATURE:
        return getSignature();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.SCREENSHOT__XLEFT_BOTTOM:
        setXLeftBottom((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__YLEFT_BOTTOM:
        setYLeftBottom((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__XRIGHT_TOP:
        setXRightTop((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__YRIGHT_TOP:
        setYRightTop((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__WIDTH:
        setWidth((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__HEIGHT:
        setHeight((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__PAGE:
        setPage((Integer)newValue);
        return;
      case DataPackage.SCREENSHOT__LOCATION:
        setLocation((ScreenshotLocation)newValue);
        return;
      case DataPackage.SCREENSHOT__TRANSCRIPTION:
        setTranscription((String)newValue);
        return;
      case DataPackage.SCREENSHOT__ANNOTATION:
        setAnnotation((String)newValue);
        return;
      case DataPackage.SCREENSHOT__COMMENT:
        setComment((String)newValue);
        return;
      case DataPackage.SCREENSHOT__SIGNATURE:
        setSignature((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.SCREENSHOT__XLEFT_BOTTOM:
        setXLeftBottom(XLEFT_BOTTOM_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__YLEFT_BOTTOM:
        setYLeftBottom(YLEFT_BOTTOM_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__XRIGHT_TOP:
        setXRightTop(XRIGHT_TOP_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__YRIGHT_TOP:
        setYRightTop(YRIGHT_TOP_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__WIDTH:
        setWidth(WIDTH_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__HEIGHT:
        setHeight(HEIGHT_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__PAGE:
        setPage(PAGE_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__LOCATION:
        setLocation(LOCATION_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__TRANSCRIPTION:
        setTranscription(TRANSCRIPTION_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__ANNOTATION:
        setAnnotation(ANNOTATION_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__COMMENT:
        setComment(COMMENT_EDEFAULT);
        return;
      case DataPackage.SCREENSHOT__SIGNATURE:
        setSignature(SIGNATURE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.SCREENSHOT__XLEFT_BOTTOM:
        return xLeftBottom != XLEFT_BOTTOM_EDEFAULT;
      case DataPackage.SCREENSHOT__YLEFT_BOTTOM:
        return yLeftBottom != YLEFT_BOTTOM_EDEFAULT;
      case DataPackage.SCREENSHOT__XRIGHT_TOP:
        return xRightTop != XRIGHT_TOP_EDEFAULT;
      case DataPackage.SCREENSHOT__YRIGHT_TOP:
        return yRightTop != YRIGHT_TOP_EDEFAULT;
      case DataPackage.SCREENSHOT__WIDTH:
        return width != WIDTH_EDEFAULT;
      case DataPackage.SCREENSHOT__HEIGHT:
        return height != HEIGHT_EDEFAULT;
      case DataPackage.SCREENSHOT__PAGE:
        return page != PAGE_EDEFAULT;
      case DataPackage.SCREENSHOT__LOCATION:
        return location != LOCATION_EDEFAULT;
      case DataPackage.SCREENSHOT__TRANSCRIPTION:
        return TRANSCRIPTION_EDEFAULT == null ? transcription != null : !TRANSCRIPTION_EDEFAULT.equals(transcription);
      case DataPackage.SCREENSHOT__ANNOTATION:
        return ANNOTATION_EDEFAULT == null ? annotation != null : !ANNOTATION_EDEFAULT.equals(annotation);
      case DataPackage.SCREENSHOT__COMMENT:
        return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
      case DataPackage.SCREENSHOT__SIGNATURE:
        return SIGNATURE_EDEFAULT == null ? signature != null : !SIGNATURE_EDEFAULT.equals(signature);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (xLeftBottom: ");
    result.append(xLeftBottom);
    result.append(", yLeftBottom: ");
    result.append(yLeftBottom);
    result.append(", xRightTop: ");
    result.append(xRightTop);
    result.append(", yRightTop: ");
    result.append(yRightTop);
    result.append(", width: ");
    result.append(width);
    result.append(", height: ");
    result.append(height);
    result.append(", page: ");
    result.append(page);
    result.append(", location: ");
    result.append(location);
    result.append(", transcription: ");
    result.append(transcription);
    result.append(", annotation: ");
    result.append(annotation);
    result.append(", comment: ");
    result.append(comment);
    result.append(", signature: ");
    result.append(signature);
    result.append(')');
    return result.toString();
  }

} //ScreenshotImpl
