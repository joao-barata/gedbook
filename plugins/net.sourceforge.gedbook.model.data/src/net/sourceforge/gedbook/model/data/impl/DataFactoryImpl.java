/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import net.sourceforge.gedbook.model.data.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DataFactoryImpl extends EFactoryImpl implements DataFactory {
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static DataFactory init() {
    try {
      DataFactory theDataFactory = (DataFactory)EPackage.Registry.INSTANCE.getEFactory(DataPackage.eNS_URI);
      if (theDataFactory != null) {
        return theDataFactory;
      }
    }
    catch (Exception exception) {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new DataFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataFactoryImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass) {
    switch (eClass.getClassifierID()) {
      case DataPackage.EXTENDED_DATA: return createExtendedData();
      case DataPackage.FILE_GROUP: return createFileGroup();
      case DataPackage.SINGLE_SOURCE_FILE: return createSingleSourceFile();
      case DataPackage.MULTIPLE_SOURCE_FILE: return createMultipleSourceFile();
      case DataPackage.EXTRACT: return createExtract();
      case DataPackage.SCREENSHOT: return createScreenshot();
      case DataPackage.EXCLUSION_GROUP: return createExclusionGroup();
      case DataPackage.EXCLUSION: return createExclusion();
      case DataPackage.PHOTO_GROUP: return createPhotoGroup();
      case DataPackage.PHOTO: return createPhoto();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue) {
    switch (eDataType.getClassifierID()) {
      case DataPackage.SCREENSHOT_LOCATION:
        return createScreenshotLocationFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue) {
    switch (eDataType.getClassifierID()) {
      case DataPackage.SCREENSHOT_LOCATION:
        return convertScreenshotLocationToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExtendedData createExtendedData() {
    ExtendedDataImpl extendedData = new ExtendedDataImpl();
    return extendedData;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FileGroup createFileGroup() {
    FileGroupImpl fileGroup = new FileGroupImpl();
    return fileGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SingleSourceFile createSingleSourceFile() {
    SingleSourceFileImpl singleSourceFile = new SingleSourceFileImpl();
    return singleSourceFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MultipleSourceFile createMultipleSourceFile() {
    MultipleSourceFileImpl multipleSourceFile = new MultipleSourceFileImpl();
    return multipleSourceFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Extract createExtract() {
    ExtractImpl extract = new ExtractImpl();
    return extract;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Screenshot createScreenshot() {
    ScreenshotImpl screenshot = new ScreenshotImpl();
    return screenshot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExclusionGroup createExclusionGroup() {
    ExclusionGroupImpl exclusionGroup = new ExclusionGroupImpl();
    return exclusionGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Exclusion createExclusion() {
    ExclusionImpl exclusion = new ExclusionImpl();
    return exclusion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PhotoGroup createPhotoGroup() {
    PhotoGroupImpl photoGroup = new PhotoGroupImpl();
    return photoGroup;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Photo createPhoto() {
    PhotoImpl photo = new PhotoImpl();
    return photo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ScreenshotLocation createScreenshotLocationFromString(EDataType eDataType, String initialValue) {
    ScreenshotLocation result = ScreenshotLocation.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertScreenshotLocationToString(EDataType eDataType, Object instanceValue) {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataPackage getDataPackage() {
    return (DataPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static DataPackage getPackage() {
    return DataPackage.eINSTANCE;
  }

} //DataFactoryImpl
