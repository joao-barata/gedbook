/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.MultipleSourceFile;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiple Source File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl#getNumberOfPages <em>Number Of Pages</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.MultipleSourceFileImpl#getExtracts <em>Extracts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultipleSourceFileImpl extends SourceFileImpl implements MultipleSourceFile {
  /**
   * The default value of the '{@link #getNumberOfPages() <em>Number Of Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNumberOfPages()
   * @generated
   * @ordered
   */
  protected static final int NUMBER_OF_PAGES_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getNumberOfPages() <em>Number Of Pages</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNumberOfPages()
   * @generated
   * @ordered
   */
  protected int numberOfPages = NUMBER_OF_PAGES_EDEFAULT;

  /**
   * The cached value of the '{@link #getExtracts() <em>Extracts</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExtracts()
   * @generated
   * @ordered
   */
  protected EList<Extract> extracts;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MultipleSourceFileImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.MULTIPLE_SOURCE_FILE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getNumberOfPages() {
    return numberOfPages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNumberOfPages(int newNumberOfPages) {
    int oldNumberOfPages = numberOfPages;
    numberOfPages = newNumberOfPages;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES, oldNumberOfPages, numberOfPages));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Extract> getExtracts() {
    if (extracts == null) {
      extracts = new EObjectContainmentEList.Resolving<Extract>(Extract.class, this, DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS);
    }
    return extracts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS:
        return ((InternalEList<?>)getExtracts()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES:
        return getNumberOfPages();
      case DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS:
        return getExtracts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES:
        setNumberOfPages((Integer)newValue);
        return;
      case DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS:
        getExtracts().clear();
        getExtracts().addAll((Collection<? extends Extract>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES:
        setNumberOfPages(NUMBER_OF_PAGES_EDEFAULT);
        return;
      case DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS:
        getExtracts().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.MULTIPLE_SOURCE_FILE__NUMBER_OF_PAGES:
        return numberOfPages != NUMBER_OF_PAGES_EDEFAULT;
      case DataPackage.MULTIPLE_SOURCE_FILE__EXTRACTS:
        return extracts != null && !extracts.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (numberOfPages: ");
    result.append(numberOfPages);
    result.append(')');
    return result.toString();
  }

} //MultipleSourceFileImpl
