/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.core.SourceCitation;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Screenshot;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extract</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtractImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtractImpl#getCitations <em>Citations</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExtractImpl#getScreenshots <em>Screenshots</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtractImpl extends MinimalEObjectImpl.Container implements Extract {
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getCitations() <em>Citations</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCitations()
   * @generated
   * @ordered
   */
  protected EList<SourceCitation> citations;

  /**
   * The cached value of the '{@link #getScreenshots() <em>Screenshots</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScreenshots()
   * @generated
   * @ordered
   */
  protected EList<Screenshot> screenshots;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExtractImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.EXTRACT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.EXTRACT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceCitation> getCitations() {
    if (citations == null) {
      citations = new EObjectResolvingEList<SourceCitation>(SourceCitation.class, this, DataPackage.EXTRACT__CITATIONS);
    }
    return citations;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Screenshot> getScreenshots() {
    if (screenshots == null) {
      screenshots = new EObjectContainmentEList.Resolving<Screenshot>(Screenshot.class, this, DataPackage.EXTRACT__SCREENSHOTS);
    }
    return screenshots;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.EXTRACT__SCREENSHOTS:
        return ((InternalEList<?>)getScreenshots()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.EXTRACT__NAME:
        return getName();
      case DataPackage.EXTRACT__CITATIONS:
        return getCitations();
      case DataPackage.EXTRACT__SCREENSHOTS:
        return getScreenshots();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.EXTRACT__NAME:
        setName((String)newValue);
        return;
      case DataPackage.EXTRACT__CITATIONS:
        getCitations().clear();
        getCitations().addAll((Collection<? extends SourceCitation>)newValue);
        return;
      case DataPackage.EXTRACT__SCREENSHOTS:
        getScreenshots().clear();
        getScreenshots().addAll((Collection<? extends Screenshot>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.EXTRACT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case DataPackage.EXTRACT__CITATIONS:
        getCitations().clear();
        return;
      case DataPackage.EXTRACT__SCREENSHOTS:
        getScreenshots().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.EXTRACT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case DataPackage.EXTRACT__CITATIONS:
        return citations != null && !citations.isEmpty();
      case DataPackage.EXTRACT__SCREENSHOTS:
        return screenshots != null && !screenshots.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ExtractImpl
