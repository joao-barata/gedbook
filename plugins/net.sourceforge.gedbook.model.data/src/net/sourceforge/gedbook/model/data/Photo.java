/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getWidth <em>Width</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getHeight <em>Height</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getPath <em>Path</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getCaption <em>Caption</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getDescription <em>Description</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getPerson <em>Person</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Photo#getFamily <em>Family</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto()
 * @model
 * @generated
 */
public interface Photo extends EObject {
  /**
   * Returns the value of the '<em><b>Width</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Width</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Width</em>' attribute.
   * @see #setWidth(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Width()
   * @model
   * @generated
   */
  int getWidth();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getWidth <em>Width</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Width</em>' attribute.
   * @see #getWidth()
   * @generated
   */
  void setWidth(int value);

  /**
   * Returns the value of the '<em><b>Height</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Height</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Height</em>' attribute.
   * @see #setHeight(int)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Height()
   * @model
   * @generated
   */
  int getHeight();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getHeight <em>Height</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Height</em>' attribute.
   * @see #getHeight()
   * @generated
   */
  void setHeight(int value);

  /**
   * Returns the value of the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Path</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path</em>' attribute.
   * @see #setPath(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Path()
   * @model
   * @generated
   */
  String getPath();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getPath <em>Path</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path</em>' attribute.
   * @see #getPath()
   * @generated
   */
  void setPath(String value);

  /**
   * Returns the value of the '<em><b>Caption</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Caption</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Caption</em>' attribute.
   * @see #setCaption(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Caption()
   * @model
   * @generated
   */
  String getCaption();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getCaption <em>Caption</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Caption</em>' attribute.
   * @see #getCaption()
   * @generated
   */
  void setCaption(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Person</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Person</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Person</em>' reference.
   * @see #setPerson(Person)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Person()
   * @model
   * @generated
   */
  Person getPerson();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getPerson <em>Person</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Person</em>' reference.
   * @see #getPerson()
   * @generated
   */
  void setPerson(Person value);

  /**
   * Returns the value of the '<em><b>Family</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Family</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Family</em>' reference.
   * @see #setFamily(Family)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getPhoto_Family()
   * @model
   * @generated
   */
  Family getFamily();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Photo#getFamily <em>Family</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Family</em>' reference.
   * @see #getFamily()
   * @generated
   */
  void setFamily(Family value);

} // Photo
