/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data;

import net.sourceforge.gedbook.model.core.SourceCitation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.Extract#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Extract#getCitations <em>Citations</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.Extract#getScreenshots <em>Screenshots</em>}</li>
 * </ul>
 *
 * @see net.sourceforge.gedbook.model.data.DataPackage#getExtract()
 * @model
 * @generated
 */
public interface Extract extends EObject {
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtract_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link net.sourceforge.gedbook.model.data.Extract#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Citations</b></em>' reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.core.SourceCitation}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Citations</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Citations</em>' reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtract_Citations()
   * @model
   * @generated
   */
  EList<SourceCitation> getCitations();

  /**
   * Returns the value of the '<em><b>Screenshots</b></em>' containment reference list.
   * The list contents are of type {@link net.sourceforge.gedbook.model.data.Screenshot}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Screenshots</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Screenshots</em>' containment reference list.
   * @see net.sourceforge.gedbook.model.data.DataPackage#getExtract_Screenshots()
   * @model containment="true" resolveProxies="true"
   * @generated
   */
  EList<Screenshot> getScreenshots();

} // Extract
