/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Exclusion;
import net.sourceforge.gedbook.model.data.ExclusionGroup;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exclusion Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl#getExclusionGroups <em>Exclusion Groups</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.ExclusionGroupImpl#getExclusions <em>Exclusions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExclusionGroupImpl extends MinimalEObjectImpl.Container implements ExclusionGroup {
  /**
   * The cached value of the '{@link #getExclusionGroups() <em>Exclusion Groups</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExclusionGroups()
   * @generated
   * @ordered
   */
  protected EList<ExclusionGroup> exclusionGroups;

  /**
   * The cached value of the '{@link #getExclusions() <em>Exclusions</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExclusions()
   * @generated
   * @ordered
   */
  protected EList<Exclusion> exclusions;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExclusionGroupImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.EXCLUSION_GROUP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ExclusionGroup> getExclusionGroups() {
    if (exclusionGroups == null) {
      exclusionGroups = new EObjectContainmentEList.Resolving<ExclusionGroup>(ExclusionGroup.class, this, DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS);
    }
    return exclusionGroups;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Exclusion> getExclusions() {
    if (exclusions == null) {
      exclusions = new EObjectContainmentEList.Resolving<Exclusion>(Exclusion.class, this, DataPackage.EXCLUSION_GROUP__EXCLUSIONS);
    }
    return exclusions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS:
        return ((InternalEList<?>)getExclusionGroups()).basicRemove(otherEnd, msgs);
      case DataPackage.EXCLUSION_GROUP__EXCLUSIONS:
        return ((InternalEList<?>)getExclusions()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS:
        return getExclusionGroups();
      case DataPackage.EXCLUSION_GROUP__EXCLUSIONS:
        return getExclusions();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS:
        getExclusionGroups().clear();
        getExclusionGroups().addAll((Collection<? extends ExclusionGroup>)newValue);
        return;
      case DataPackage.EXCLUSION_GROUP__EXCLUSIONS:
        getExclusions().clear();
        getExclusions().addAll((Collection<? extends Exclusion>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS:
        getExclusionGroups().clear();
        return;
      case DataPackage.EXCLUSION_GROUP__EXCLUSIONS:
        getExclusions().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.EXCLUSION_GROUP__EXCLUSION_GROUPS:
        return exclusionGroups != null && !exclusionGroups.isEmpty();
      case DataPackage.EXCLUSION_GROUP__EXCLUSIONS:
        return exclusions != null && !exclusions.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ExclusionGroupImpl
