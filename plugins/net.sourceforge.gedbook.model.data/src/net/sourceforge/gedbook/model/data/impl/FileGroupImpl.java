/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.impl;

import java.util.Collection;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExclusionGroup;
import net.sourceforge.gedbook.model.data.FileGroup;
import net.sourceforge.gedbook.model.data.PhotoGroup;
import net.sourceforge.gedbook.model.data.SourceFile;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl#getExclusions <em>Exclusions</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl#getPhotos <em>Photos</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl#getGroups <em>Groups</em>}</li>
 *   <li>{@link net.sourceforge.gedbook.model.data.impl.FileGroupImpl#getFiles <em>Files</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FileGroupImpl extends MinimalEObjectImpl.Container implements FileGroup {
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getExclusions() <em>Exclusions</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getExclusions()
   * @generated
   * @ordered
   */
  protected ExclusionGroup exclusions;

  /**
   * The cached value of the '{@link #getPhotos() <em>Photos</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPhotos()
   * @generated
   * @ordered
   */
  protected EList<PhotoGroup> photos;

  /**
   * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroups()
   * @generated
   * @ordered
   */
  protected EList<FileGroup> groups;

  /**
   * The cached value of the '{@link #getFiles() <em>Files</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFiles()
   * @generated
   * @ordered
   */
  protected EList<SourceFile> files;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected FileGroupImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return DataPackage.Literals.FILE_GROUP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName() {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName) {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.FILE_GROUP__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExclusionGroup getExclusions() {
    if (exclusions != null && exclusions.eIsProxy()) {
      InternalEObject oldExclusions = (InternalEObject)exclusions;
      exclusions = (ExclusionGroup)eResolveProxy(oldExclusions);
      if (exclusions != oldExclusions) {
        InternalEObject newExclusions = (InternalEObject)exclusions;
        NotificationChain msgs = oldExclusions.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DataPackage.FILE_GROUP__EXCLUSIONS, null, null);
        if (newExclusions.eInternalContainer() == null) {
          msgs = newExclusions.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DataPackage.FILE_GROUP__EXCLUSIONS, null, msgs);
        }
        if (msgs != null) msgs.dispatch();
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, DataPackage.FILE_GROUP__EXCLUSIONS, oldExclusions, exclusions));
      }
    }
    return exclusions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExclusionGroup basicGetExclusions() {
    return exclusions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetExclusions(ExclusionGroup newExclusions, NotificationChain msgs) {
    ExclusionGroup oldExclusions = exclusions;
    exclusions = newExclusions;
    if (eNotificationRequired()) {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DataPackage.FILE_GROUP__EXCLUSIONS, oldExclusions, newExclusions);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setExclusions(ExclusionGroup newExclusions) {
    if (newExclusions != exclusions) {
      NotificationChain msgs = null;
      if (exclusions != null)
        msgs = ((InternalEObject)exclusions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DataPackage.FILE_GROUP__EXCLUSIONS, null, msgs);
      if (newExclusions != null)
        msgs = ((InternalEObject)newExclusions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DataPackage.FILE_GROUP__EXCLUSIONS, null, msgs);
      msgs = basicSetExclusions(newExclusions, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.FILE_GROUP__EXCLUSIONS, newExclusions, newExclusions));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<PhotoGroup> getPhotos() {
    if (photos == null) {
      photos = new EObjectContainmentEList.Resolving<PhotoGroup>(PhotoGroup.class, this, DataPackage.FILE_GROUP__PHOTOS);
    }
    return photos;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FileGroup> getGroups() {
    if (groups == null) {
      groups = new EObjectContainmentEList.Resolving<FileGroup>(FileGroup.class, this, DataPackage.FILE_GROUP__GROUPS);
    }
    return groups;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SourceFile> getFiles() {
    if (files == null) {
      files = new EObjectContainmentEList.Resolving<SourceFile>(SourceFile.class, this, DataPackage.FILE_GROUP__FILES);
    }
    return files;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case DataPackage.FILE_GROUP__EXCLUSIONS:
        return basicSetExclusions(null, msgs);
      case DataPackage.FILE_GROUP__PHOTOS:
        return ((InternalEList<?>)getPhotos()).basicRemove(otherEnd, msgs);
      case DataPackage.FILE_GROUP__GROUPS:
        return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
      case DataPackage.FILE_GROUP__FILES:
        return ((InternalEList<?>)getFiles()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case DataPackage.FILE_GROUP__NAME:
        return getName();
      case DataPackage.FILE_GROUP__EXCLUSIONS:
        if (resolve) return getExclusions();
        return basicGetExclusions();
      case DataPackage.FILE_GROUP__PHOTOS:
        return getPhotos();
      case DataPackage.FILE_GROUP__GROUPS:
        return getGroups();
      case DataPackage.FILE_GROUP__FILES:
        return getFiles();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case DataPackage.FILE_GROUP__NAME:
        setName((String)newValue);
        return;
      case DataPackage.FILE_GROUP__EXCLUSIONS:
        setExclusions((ExclusionGroup)newValue);
        return;
      case DataPackage.FILE_GROUP__PHOTOS:
        getPhotos().clear();
        getPhotos().addAll((Collection<? extends PhotoGroup>)newValue);
        return;
      case DataPackage.FILE_GROUP__GROUPS:
        getGroups().clear();
        getGroups().addAll((Collection<? extends FileGroup>)newValue);
        return;
      case DataPackage.FILE_GROUP__FILES:
        getFiles().clear();
        getFiles().addAll((Collection<? extends SourceFile>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case DataPackage.FILE_GROUP__NAME:
        setName(NAME_EDEFAULT);
        return;
      case DataPackage.FILE_GROUP__EXCLUSIONS:
        setExclusions((ExclusionGroup)null);
        return;
      case DataPackage.FILE_GROUP__PHOTOS:
        getPhotos().clear();
        return;
      case DataPackage.FILE_GROUP__GROUPS:
        getGroups().clear();
        return;
      case DataPackage.FILE_GROUP__FILES:
        getFiles().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case DataPackage.FILE_GROUP__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case DataPackage.FILE_GROUP__EXCLUSIONS:
        return exclusions != null;
      case DataPackage.FILE_GROUP__PHOTOS:
        return photos != null && !photos.isEmpty();
      case DataPackage.FILE_GROUP__GROUPS:
        return groups != null && !groups.isEmpty();
      case DataPackage.FILE_GROUP__FILES:
        return files != null && !files.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //FileGroupImpl
