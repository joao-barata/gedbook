/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.data.util;

import net.sourceforge.gedbook.model.data.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see net.sourceforge.gedbook.model.data.DataPackage
 * @generated
 */
public class DataSwitch<T> extends Switch<T> {
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static DataPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataSwitch() {
    if (modelPackage == null) {
      modelPackage = DataPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage) {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject) {
    switch (classifierID) {
      case DataPackage.EXTENDED_DATA: {
        ExtendedData extendedData = (ExtendedData)theEObject;
        T result = caseExtendedData(extendedData);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.FILE_GROUP: {
        FileGroup fileGroup = (FileGroup)theEObject;
        T result = caseFileGroup(fileGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.SOURCE_FILE: {
        SourceFile sourceFile = (SourceFile)theEObject;
        T result = caseSourceFile(sourceFile);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.SINGLE_SOURCE_FILE: {
        SingleSourceFile singleSourceFile = (SingleSourceFile)theEObject;
        T result = caseSingleSourceFile(singleSourceFile);
        if (result == null) result = caseSourceFile(singleSourceFile);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.MULTIPLE_SOURCE_FILE: {
        MultipleSourceFile multipleSourceFile = (MultipleSourceFile)theEObject;
        T result = caseMultipleSourceFile(multipleSourceFile);
        if (result == null) result = caseSourceFile(multipleSourceFile);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.EXTRACT: {
        Extract extract = (Extract)theEObject;
        T result = caseExtract(extract);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.SCREENSHOT: {
        Screenshot screenshot = (Screenshot)theEObject;
        T result = caseScreenshot(screenshot);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.EXCLUSION_GROUP: {
        ExclusionGroup exclusionGroup = (ExclusionGroup)theEObject;
        T result = caseExclusionGroup(exclusionGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.EXCLUSION: {
        Exclusion exclusion = (Exclusion)theEObject;
        T result = caseExclusion(exclusion);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.PHOTO_GROUP: {
        PhotoGroup photoGroup = (PhotoGroup)theEObject;
        T result = casePhotoGroup(photoGroup);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case DataPackage.PHOTO: {
        Photo photo = (Photo)theEObject;
        T result = casePhoto(photo);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extended Data</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extended Data</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtendedData(ExtendedData object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>File Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>File Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFileGroup(FileGroup object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Source File</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Source File</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSourceFile(SourceFile object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Single Source File</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Single Source File</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSingleSourceFile(SingleSourceFile object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Multiple Source File</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Multiple Source File</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMultipleSourceFile(MultipleSourceFile object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Extract</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Extract</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExtract(Extract object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Screenshot</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Screenshot</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseScreenshot(Screenshot object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exclusion Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exclusion Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExclusionGroup(ExclusionGroup object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Exclusion</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Exclusion</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExclusion(Exclusion object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Photo Group</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Photo Group</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePhotoGroup(PhotoGroup object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Photo</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Photo</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePhoto(Photo object) {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object) {
    return null;
  }

} //DataSwitch
