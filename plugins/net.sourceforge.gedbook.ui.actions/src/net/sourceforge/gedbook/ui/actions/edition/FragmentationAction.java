/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.actions.edition;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.ControlAction;

/**
 * The fragmentation action handler.
 * 
 * @author Joao Barata
 */
public class FragmentationAction extends ControlAction {

  /**
   * Constructor.
   * @param editingDomain the editing domain.
   */
  public FragmentationAction(EditingDomain editingDomain) {
    super(editingDomain);
  }

  /**
   * @see org.eclipse.emf.edit.ui.action.ControlAction#getResource()
   * @return the fragment resource.
   */
  @Override
  protected Resource getResource() {
    URI uri = eObject.eResource().getURI();
    String newURI = "";
    for (int i = 1; i < uri.segmentCount(); i++) {
      if (i == uri.segmentCount() - 1) {
        newURI += uri.segment(i).substring(0, uri.segment(i).indexOf("." + uri.fileExtension())); //$NON-NLS-1$
        newURI += "_" + eObject.eClass().getName() + "."; //$NON-NLS-1$ //$NON-NLS-2$
        newURI += uri.fileExtension();
      } else {
        newURI += uri.segment(i) + "/"; //$NON-NLS-1$
      }
    }
    URI fragmentURI = URI.createPlatformResourceURI(newURI, true);
    ResourceSet resourceSet = domain.getResourceSet();
    if (!resourceSet.getURIConverter().exists(fragmentURI, null)) {
      return resourceSet.createResource(fragmentURI);
    }
    return resourceSet.getResource(fragmentURI, true);
  }
}
