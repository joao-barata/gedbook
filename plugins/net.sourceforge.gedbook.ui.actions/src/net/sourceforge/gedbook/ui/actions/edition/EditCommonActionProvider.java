/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.actions.edition;

import java.util.EventObject;

import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;
import net.sourceforge.gedbook.ui.views.navigator.GedBookCommonNavigator;

import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.CopyAction;
import org.eclipse.emf.edit.ui.action.CutAction;
import org.eclipse.emf.edit.ui.action.DeleteAction;
import org.eclipse.emf.edit.ui.action.PasteAction;
import org.eclipse.emf.edit.ui.action.RedoAction;
import org.eclipse.emf.edit.ui.action.UndoAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchCommandConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.BaseSelectionListenerAction;
import org.eclipse.ui.actions.TextActionHandler;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonMenuConstants;
import org.eclipse.ui.navigator.ICommonViewerSite;
import org.eclipse.ui.navigator.ICommonViewerWorkbenchSite;

/**
 * The edit contribution actions provider.
 * 
 * @author Joao Barata
 */
public class EditCommonActionProvider extends CommonActionProvider implements CommandStackListener {

  private GedBookUndoAction _undoAction;
  private GedBookRedoAction _redoAction;
  private BaseSelectionListenerAction _saveAction;
  private BaseSelectionListenerAction _copyAction;
  private BaseSelectionListenerAction _cutAction;
  private BaseSelectionListenerAction _pasteAction;
  private BaseSelectionListenerAction _deleteAction;
  private RenameAction _renameAction;
  private FragmentationAction _fragmentationAction;

  protected class GedBookUndoAction extends UndoAction implements ISelectionChangedListener {
    /**
     * Constructor.
     * 
     * @param editingDomain
     */
    public GedBookUndoAction(EditingDomain editingDomain) {
      super(editingDomain);
    }

    /**
     * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    @Override
    public void selectionChanged(SelectionChangedEvent event) {
      update();
    }
  }

  protected class GedBookRedoAction extends RedoAction implements ISelectionChangedListener {
    /**
     * Constructor.
     * 
     * @param editingDomain
     */
    public GedBookRedoAction(EditingDomain editingDomain) {
      super(editingDomain);
    }

    /**
     * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    @Override
    public void selectionChanged(SelectionChangedEvent event) {
      update();
    }
  }

  /**
   * Constructs the edit contribution actions provider.
   */
  public EditCommonActionProvider() {
    // Do nothing.
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#dispose()
   */
  @Override
  public void dispose() {
    _undoAction = null;
    _redoAction = null;

    GedBookEditingDomainProvider.getEditingDomain().getCommandStack().removeCommandStackListener(this);

    ISelectionProvider selectionProvider = getActionSite().getViewSite().getSelectionProvider();
    if (null != _cutAction) {
      selectionProvider.removeSelectionChangedListener(_saveAction);
      _saveAction = null;
    }
    if (null != _cutAction) {
      selectionProvider.removeSelectionChangedListener(_cutAction);
      _cutAction = null;
    }
    if (null != _copyAction) {
      selectionProvider.removeSelectionChangedListener(_copyAction);
      _copyAction = null;
    }
    if (null != _pasteAction) {
      selectionProvider.removeSelectionChangedListener(_pasteAction);
      _pasteAction = null;
    }
    if (null != _deleteAction) {
      selectionProvider.removeSelectionChangedListener(_deleteAction);
      _pasteAction = null;
    }
    if (null != _renameAction) {
      selectionProvider.removeSelectionChangedListener(_renameAction);
      _renameAction = null;
    }
    if (null != _fragmentationAction) {
      selectionProvider.removeSelectionChangedListener(_fragmentationAction);
      _fragmentationAction = null;
    }
    super.dispose();
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#fillActionBars(org.eclipse.ui.IActionBars)
   */
  @Override
  public void fillActionBars(IActionBars actionBars) {
    actionBars.setGlobalActionHandler(ActionFactory.SAVE.getId(), _saveAction);
    actionBars.setGlobalActionHandler(ActionFactory.UNDO.getId(), _undoAction);
    actionBars.setGlobalActionHandler(ActionFactory.REDO.getId(), _redoAction);
    actionBars.setGlobalActionHandler(ActionFactory.CUT.getId(), _cutAction);
    actionBars.setGlobalActionHandler(ActionFactory.COPY.getId(), _copyAction);
    actionBars.setGlobalActionHandler(ActionFactory.PASTE.getId(), _pasteAction);
    actionBars.setGlobalActionHandler(ActionFactory.DELETE.getId(), _deleteAction);
    actionBars.setGlobalActionHandler(ActionFactory.RENAME.getId(), _renameAction);
    actionBars.setGlobalActionHandler("fragmentation", _fragmentationAction);

    TextActionHandler textActionHandler = new TextActionHandler(actionBars);
    textActionHandler.setCutAction(_cutAction);
    textActionHandler.setCopyAction(_copyAction);
    textActionHandler.setPasteAction(_pasteAction);
    textActionHandler.setDeleteAction(_deleteAction);
    _renameAction.setTextActionHandler(textActionHandler);
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#fillContextMenu(org.eclipse.jface.action.IMenuManager)
   */
  @Override
  public void fillContextMenu(IMenuManager menu) {
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _undoAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _redoAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, new Separator());
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _cutAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _copyAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _pasteAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, new Separator());
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _deleteAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, new Separator());
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _renameAction);
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, new Separator());
    menu.appendToGroup(ICommonMenuConstants.GROUP_EDIT, _fragmentationAction);
  }

  /**
   * @see org.eclipse.ui.navigator.CommonActionProvider#init(org.eclipse.ui.navigator.ICommonActionExtensionSite)
   */
  @Override
  public void init(ICommonActionExtensionSite site) {
    super.init(site);
    ICommonViewerSite commonViewSite = site.getViewSite();
    if (commonViewSite instanceof ICommonViewerWorkbenchSite) {
      ICommonViewerWorkbenchSite commonViewerWorkbenchSite = (ICommonViewerWorkbenchSite) commonViewSite;
      GedBookCommonNavigator activePart = (GedBookCommonNavigator) commonViewerWorkbenchSite.getPart();
      ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();
      ISelectionProvider selectionProvider = commonViewSite.getSelectionProvider();

      _saveAction = new SaveAction();
      _saveAction.setActionDefinitionId(IWorkbenchCommandConstants.FILE_SAVE);
      _saveAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT));
      _saveAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT_DISABLED));
      registerToSelectionChanges(_saveAction, selectionProvider);

      _undoAction = new GedBookUndoAction(GedBookEditingDomainProvider.getEditingDomain());
      _undoAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_UNDO);
      _undoAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO));
      _undoAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_UNDO_DISABLED));
      registerToSelectionChanges(_undoAction, selectionProvider);

      _redoAction = new GedBookRedoAction(GedBookEditingDomainProvider.getEditingDomain());
      _redoAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_REDO);
      _redoAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_REDO));
      _redoAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_REDO_DISABLED));
      registerToSelectionChanges(_redoAction, selectionProvider);

      _cutAction = new CutAction(GedBookEditingDomainProvider.getEditingDomain());
      _cutAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_CUT);
      _cutAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_CUT));
      _cutAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_CUT_DISABLED));
      registerToSelectionChanges(_cutAction, selectionProvider);

      _copyAction = new CopyAction(GedBookEditingDomainProvider.getEditingDomain());
      _copyAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_COPY);
      _copyAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
      _copyAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_COPY_DISABLED));
      registerToSelectionChanges(_copyAction, selectionProvider);

      _pasteAction = new PasteAction(GedBookEditingDomainProvider.getEditingDomain());
      _pasteAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_PASTE);
      _pasteAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE));
      _pasteAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE_DISABLED));
      registerToSelectionChanges(_pasteAction, selectionProvider);

      _deleteAction = new DeleteAction(GedBookEditingDomainProvider.getEditingDomain());
      _deleteAction.setActionDefinitionId(IWorkbenchCommandConstants.EDIT_DELETE);
      _deleteAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE));
      _deleteAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE_DISABLED));
      registerToSelectionChanges(_deleteAction, selectionProvider);

      _renameAction = new RenameAction(activePart);
      _renameAction.setActionDefinitionId(IWorkbenchCommandConstants.FILE_RENAME);
      registerToSelectionChanges(_renameAction, selectionProvider);

      _fragmentationAction = new FragmentationAction(GedBookEditingDomainProvider.getEditingDomain());
      _fragmentationAction.setActionDefinitionId("fragmentation");
      _fragmentationAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_NEW_WIZARD));
      _fragmentationAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_NEW_WIZARD_DISABLED));
      registerToSelectionChanges(_fragmentationAction, selectionProvider);

      GedBookEditingDomainProvider.getEditingDomain().getCommandStack().addCommandStackListener(this);
    }
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#updateActionBars()
   */
  @Override
  public void updateActionBars() {
    if (_undoAction != null) {
      _undoAction.update();
    }
    if (_redoAction != null) {
      _redoAction.update();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void commandStackChanged(EventObject event) {
    updateActionBars();
  }

  /**
   * Register given action to the selection changes emitted by given selection
   * provider.
   * 
   * @param action
   * @param selectionProvider
   */
  public void registerToSelectionChanges(ISelectionChangedListener action, ISelectionProvider selectionProvider) {
    // Listen selection changes.
    selectionProvider.addSelectionChangedListener(action);
    // Set the current selection otherwise at first run, selection is lost.
    ISelection selection = selectionProvider.getSelection();
    if (!selection.isEmpty()) {
      action.selectionChanged(new SelectionChangedEvent(selectionProvider, selection));
    }
  }
}
