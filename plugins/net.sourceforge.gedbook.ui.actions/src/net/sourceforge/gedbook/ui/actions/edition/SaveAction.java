/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.actions.edition;

import java.io.IOException;

import net.sourceforge.gedbook.model.helpers.ResourceUtils;
import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.actions.BaseSelectionListenerAction;

/**
 * The save action.
 * 
 * @author Joao Barata
 */
public class SaveAction extends BaseSelectionListenerAction {
  /**
   * Constructs the save action.
   */
  public SaveAction() {
    super("Save"); //$NON-NLS-1$
  }

  /**
   * @see org.eclipse.ui.actions.BaseSelectionListenerAction#updateSelection(org.eclipse.jface.viewers.IStructuredSelection)
   */
  @Override
  protected boolean updateSelection(IStructuredSelection selection) {
    TransactionalEditingDomain ted = (TransactionalEditingDomain) GedBookEditingDomainProvider.getEditingDomain();
    return ((BasicCommandStack) ted.getCommandStack()).isSaveNeeded();
  }

  /**
   * @see org.eclipse.jface.action.Action#run()
   */
  @Override
  public void run() {
    TransactionalEditingDomain ted = (TransactionalEditingDomain) GedBookEditingDomainProvider.getEditingDomain();
    for (Resource resource : ted.getResourceSet().getResources()) {
      try {
        ResourceUtils.saveResource(resource);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
