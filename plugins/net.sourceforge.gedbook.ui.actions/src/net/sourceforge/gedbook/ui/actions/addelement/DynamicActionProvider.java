/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.actions.addelement;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonMenuConstants;
import org.eclipse.ui.navigator.ICommonViewerSite;
import org.eclipse.ui.navigator.ICommonViewerWorkbenchSite;

/**
 * @author Joao Barata
 */
public class DynamicActionProvider extends CommonActionProvider {
  /**
   * Dynamic creation action.
   */
  private GedBookCreationAction _dynamicAction;

  /**
   * Constructor
   */
  public DynamicActionProvider() {
    // do nothing
  }

  /**
   * @see org.eclipse.ui.navigator.CommonActionProvider#init(org.eclipse.ui.navigator.ICommonActionExtensionSite)
   */
  @Override
  public void init(ICommonActionExtensionSite site_p) {
    super.init(site_p);

    ICommonViewerSite commonViewerSite = site_p.getViewSite();
    if (commonViewerSite instanceof ICommonViewerWorkbenchSite) {
      ICommonViewerWorkbenchSite workbenchSite = (ICommonViewerWorkbenchSite) commonViewerSite;
      _dynamicAction = new GedBookCreationAction(workbenchSite.getSelectionProvider());
    }
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#fillActionBars(org.eclipse.ui.IActionBars)
   */
  @Override
  public void fillActionBars(IActionBars actionBars_p) {
    // do nothing
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#fillContextMenu(org.eclipse.jface.action.IMenuManager)
   */
  @Override
  public void fillContextMenu(IMenuManager menu_p) {
    menu_p.insertAfter(ICommonMenuConstants.GROUP_NEW, createContributionItem());
  }

  /**
   * @return
   */
  protected IContributionItem createContributionItem() {
    IMenuManager subMenuManager = new MenuManager("Add child"); //$NON-NLS-1$
    if (_dynamicAction.isSelectionCompatible()) {
      for (IContributionItem item : _dynamicAction.getDynamicActions()) {
        subMenuManager.add(item);
      }
    }
    return subMenuManager;
  }
}
