/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.actions.addelement;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;

import net.sourceforge.gedbook.ui.views.GedBookAdapterFactory;
import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.ui.action.CreateChildAction;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Base class to implement creation action.
 * 
 * @author Joao Barata
 */
public class GedBookCreationAction extends Action {

  /**
   * Selection provider.
   */
  private ISelectionProvider _provider;

  /**
   * Managed element.
   */
  protected EObject _element;

  /**
   * Constructor.
   * @param selectionProvider
   */
  public GedBookCreationAction(ISelectionProvider selectionProvider) {
    _provider = selectionProvider;
    setId(getClass().getName());
  }

  /**
   * Get the dynamic actions.
   * @return
   */
  public Collection<IContributionItem> getDynamicActions() {
	  ISelection selection = new StructuredSelection(_element);
    EditingDomain editingDomain = GedBookEditingDomainProvider.getEditingDomain();
    Collection<CommandParameter> newChildDescriptors = (Collection<CommandParameter>) editingDomain.getNewChildDescriptors(_element, null);
    for (CommandParameter cmd : newChildDescriptors) {
      cmd.setOwner(selection);
    }
    return generateCreateChildActions(newChildDescriptors, editingDomain);
  }

  /**
   * Generate create child actions.
   * @param descriptors
   * @param selection
   * @param editingDomain
   * @param condition
   * @return
   */
  protected Collection<IContributionItem> generateCreateChildActions(Collection<CommandParameter> descriptors, EditingDomain editingDomain) {
    List<IContributionItem> contributionItems = new ArrayList<IContributionItem>();
    if (null != descriptors) {
      // Map to store descriptors by type.
      Map<EClass, Set<CommandParameter>> descriptorsSortedByType = new HashMap<EClass, Set<CommandParameter>>();
      // Map to store descriptors by feature.
      Map<EReference, Set<CommandParameter>> descriptorsSortedByFeature = new HashMap<EReference, Set<CommandParameter>>();
      // Set of features displayed as sub menu managers.
      Set<EReference> featuresDisplayedAsSubMenuManager = new HashSet<EReference>(0);
      for (CommandParameter descriptor : descriptors) {
        // Handle feature
        EReference feature = descriptor.getEReference();
        Set<CommandParameter> featureDescriptors = descriptorsSortedByFeature.get(feature);
        if (null == featureDescriptors) {
          featureDescriptors = new HashSet<CommandParameter>(1);
          descriptorsSortedByFeature.put(feature, featureDescriptors);
        }
        // Add the current descriptor for this feature.
        featureDescriptors.add(descriptor);

        // Handle eClass
        EClass eClass = descriptor.getEValue().eClass();
        Set<CommandParameter> typeDescriptors = descriptorsSortedByType.get(eClass);
        if (null == typeDescriptors) {
          typeDescriptors = new HashSet<CommandParameter>(1);
          descriptorsSortedByType.put(eClass, typeDescriptors);
        }
        // Add the current descriptor for this type.
        typeDescriptors.add(descriptor);

        // More than one descriptors for this type, mark underlying feature to
        // display it as sub menu managers.
        if ((typeDescriptors.size() > 1) && (featureDescriptors.size() > 1)) {
          featuresDisplayedAsSubMenuManager.add(feature);
        }
      }

      // Iterate again over all descriptors to fill in the resulting list
      // according to features that should be displayed as sub menu manager.
      for (CommandParameter descriptor : descriptors) {
        EReference reference = descriptor.getEReference();
        if (featuresDisplayedAsSubMenuManager.contains(reference)) {
          // fill in as a sub menu manager.
          Set<CommandParameter> featureDescriptors = descriptorsSortedByFeature.get(reference);
          // Create a menu manager for this feature.
          MenuManager featureMenuManager = new MenuManager(getFeatureLabel(reference, descriptor.getEValue()));
          // Add it in the resulting list.
          contributionItems.add(featureMenuManager);
          // Create the list of available action items.
          List<IContributionItem> actionItems = new ArrayList<IContributionItem>();
          for (CommandParameter featureDescriptor : featureDescriptors) {
            fillContributionItems(editingDomain, actionItems, featureDescriptor);
          }
          // Add them in feature menu manager.
          for (IContributionItem dynamicItem : actionItems) {
            featureMenuManager.add(dynamicItem);
          }
        } else {
          // Fill in resulting list with a standalone descriptor.
          fillContributionItems(editingDomain, contributionItems, descriptor);
        }
      }
    }
    return contributionItems;
  }

  /**
   * Fill contribution items.
   * @param selection
   * @param editingDomain
   * @param condition
   * @param items
   * @param descriptor
   */
  protected void fillContributionItems(EditingDomain editingDomain, Collection<IContributionItem> items,
      CommandParameter descriptor) {
    GedBookCreateChildAction action = new GedBookCreateChildAction(editingDomain, (ISelection) descriptor.getOwner(), descriptor);
    if (action.isExecutable() && action.isEnabled()) {
      // Add it if enable and executable.
      items.add(new GedBookActionContributionItem(action));
    }
  }

  /**
   * Returns whether this action is compatible with current selection.
   * 
   * @return
   */
  public boolean isSelectionCompatible() {
    boolean isEnabled = false;
    // Check selected element has a type that matches the expected one.
    Object element = getSelection(EObject.class);
    if (element instanceof EObject) {
      _element = (EObject) element;
      isEnabled = true;
    }
    return isEnabled;
  }

  /**
   * Get the current selected object
   * 
   * @param objectType_p
   *            the object type that the selection must be an instance of
   */
  protected Object getSelection(Class<?> objectType_p) {
    Object selectedObject = null;
    // Get selected objects if any.
    IStructuredSelection selectedObjects = (IStructuredSelection) _provider.getSelection();
    // If not empty, get the first selected object.
    if (!selectedObjects.isEmpty()) {
      Object firstElement = selectedObjects.getFirstElement();
      if (selectedObjects.size() == 1 && objectType_p.isInstance(firstElement)) {
        selectedObject = firstElement;
      }
    }
    return selectedObject;
  }

  /**
   * Suffix used by EMF Edit when generating feature labels.
   */
  private static final String FEATURE_GENERATED_KEY_SUFFIX = "_feature"; //$NON-NLS-1$

  /**
   * Suffix used by EMF Edit when generating metaclass labels.
   */
  private static final String METACLASS_GENERATED_KEY_SUFFIX = "_type"; //$NON-NLS-1$

  /**
   * Prefix used by EMF Edit when generating labels.
   */
  private static final String GENERATED_KEY_PREFIX = "_UI_"; //$NON-NLS-1$

  /**
   * Get feature UI label.
   * @param feature
   * @param object
   * @return
   */
  protected String getFeatureLabel(EStructuralFeature feature, EObject object) {
    String featureLabel = null;
    ItemProviderAdapter genericItemProvider = getItemProvider(object);
    String featureKey = feature.getEContainingClass().getName() + "_" + feature.getName();
    featureLabel = genericItemProvider.getString(GENERATED_KEY_PREFIX + featureKey + FEATURE_GENERATED_KEY_SUFFIX);
    genericItemProvider.dispose();
    return featureLabel;
  }

  /**
   * Get class UI label.
   * @param cls
   * @param object
   * @return
   */
  protected String getMetaclassLabel(EClass cls, EObject object) {
    String metaclassLabel = null;
    ItemProviderAdapter genericItemProvider = getItemProvider(object);
    try {
      metaclassLabel = genericItemProvider.getString(GENERATED_KEY_PREFIX + cls.getName() + METACLASS_GENERATED_KEY_SUFFIX);
    } catch (MissingResourceException e) {
      metaclassLabel = "<<MissingResourceException>> [" + cls.getName() + "]"; //$NON-NLS-1$ //$NON-NLS-2$
    }
    genericItemProvider.dispose();
    return metaclassLabel;
  }

  /**
   * Get a generic item provider.
   * @return
   */
  protected ItemProviderAdapter getItemProvider(EObject object) {
    return (ItemProviderAdapter) GedBookAdapterFactory.getAdapterFactory(false).adapt(object, IItemLabelProvider.class);
  }

  /**
   * Extends {@link CreateChildAction} class.
   */
  class GedBookCreateChildAction extends CreateChildAction {
    /**
     * @param editingDomain
     * @param selection
     * @param descriptor
     */
    public GedBookCreateChildAction(EditingDomain editingDomain, ISelection selection, Object descriptor) {
      super((IWorkbenchPart) null, selection, descriptor);
      this.editingDomain = editingDomain;
    }

    /**
     * @see org.eclipse.emf.edit.ui.action.StaticSelectionCommandAction#configureAction(org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public void configureAction(ISelection selection) {
      super.configureAction(selection);

      EObject object = ((CommandParameter) descriptor).getEValue();
      EClass eClass = object.eClass();
      setText(getMetaclassLabel(eClass, object));

      if (getImageDescriptor() == null) {
        ImageDescriptor imageDescriptor = getImageDescriptor(object);
        if (null != imageDescriptor) {
          setImageDescriptor(imageDescriptor);
        } else {
          if (selection != null) {
            Object element = ((StructuredSelection) selection).getFirstElement();
            EStructuralFeature feature = ((CommandParameter) descriptor).getEStructuralFeature();
            ItemProviderAdapter itemProvider = getItemProvider(object);
            Object childImage = itemProvider.getCreateChildImage(element, feature, object, null);
            setImageDescriptor(ImageDescriptor.createFromURL((URL) childImage));
            itemProvider.dispose();
          }
        }
      }
    }

    /**
     * Get the image descriptor for given object based on generated item provider.
     * @param object
     * @return<code>null</code> if one of parameters is <code>null</code> or if no image descriptor is found.
     */
    protected ImageDescriptor getImageDescriptor(EObject object) {
      Object image = null;
      AdapterFactoryEditingDomain editingDomain = (AdapterFactoryEditingDomain) AdapterFactoryEditingDomain.getEditingDomainFor(object);
      if (null != editingDomain) {
        IItemLabelProvider provider = (IItemLabelProvider) editingDomain.getAdapterFactory().adapt(object, IItemLabelProvider.class);
        if (null != provider) {
          image = provider.getImage(object);
        }
      }
      return (null != image) ? ExtendedImageRegistry.getInstance().getImageDescriptor(image) : null;
    }

    /**
     * @see org.eclipse.jface.action.Action#isEnabled()
     */
    @Override
    public boolean isEnabled() {
      return super.isEnabled();
    }

    /**
     * Is this action executable (i.e underlying command is executable) ?
     * @return
     */
    public boolean isExecutable() {
      boolean result = false;
      // Precondition.
      if (null == command) {
        return result;
      }
      if (command instanceof CreateChildCommand) {
        CreateChildCommand childCommand = (CreateChildCommand) command;
        result = childCommand.getCommand() != UnexecutableCommand.INSTANCE;
      }
      return result;
    }

    /**
     * This executes the command.
     */
    @Override
    public void run() {
      final Command basicCreationCmd = command;
      CompoundCommand cmd = new CompoundCommand();

      // basic creation command
      cmd.append(basicCreationCmd);
      cmd.setLabel(basicCreationCmd.getLabel());
      cmd.setDescription(basicCreationCmd.getDescription());

      editingDomain.getCommandStack().execute(cmd);
    }

    /**
     * This returns the owner object upon which the command will act.
     */
    protected EObject getOwner() {
      EObject owner = null;
      Command cmd = ((CreateChildCommand) command).getCommand();
      if (cmd instanceof AddCommand) {
        owner = ((AddCommand) cmd).getOwner();
      } else if (cmd instanceof SetCommand) {
        owner = ((SetCommand) cmd).getOwner();
      }
      return owner;
    }

    /**
     * This returns the feature of the owner object upon the command will act.
     */
    protected EStructuralFeature getFeature() {
      EStructuralFeature feature = null;
      Command cmd = ((CreateChildCommand) command).getCommand();
      if (cmd instanceof AddCommand) {
        feature = ((AddCommand) cmd).getFeature();
      } else if (cmd instanceof SetCommand) {
        feature = ((SetCommand) cmd).getFeature();
      }
      return feature;
    }
  }
}
