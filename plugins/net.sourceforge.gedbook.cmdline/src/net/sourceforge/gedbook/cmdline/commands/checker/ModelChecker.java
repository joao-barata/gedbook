/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.checker;

import net.sourceforge.gedbook.cmdline.commands.AbstractCommand;
import net.sourceforge.gedbook.generator.CheckReportGenerator;
import net.sourceforge.gedbook.model.GedBookXMIResourceFactoryImpl;
import net.sourceforge.gedbook.model.ProjectImporter;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.ExtendedData;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author Joao Barata
 */
public class ModelChecker extends AbstractCommand {

  private String husband;
  private String wife;
  private String inputfilename;
  private String outputfilename;

  /**
   * {@inheritDoc}
   */
  public void run() {
    ProjectImporter.getInstance().loadModel(inputfilename);
    EObject project = ProjectImporter.getInstance().getCurrentProject();
    EObject extendedData = ProjectImporter.getInstance().getCurrentExtendedData();
    if (project instanceof Project && extendedData instanceof ExtendedData) {
      CheckReportGenerator checkReportGenerator = new CheckReportGenerator((Project) project, outputfilename, (ExtendedData) extendedData, husband, wife);
      checkReportGenerator.run();
    }
  }

  public void init() {
    //System.out.println("Register factory ..."); //$NON-NLS-1$
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new GedBookXMIResourceFactoryImpl());
    //System.out.println("Register packages ..."); //$NON-NLS-1$
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new ModelChecker().executeCommand(args);
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption(OptionBuilder.withLongOpt("input-file").withDescription("use given file for input").hasArg().create("i"));
    options.addOption(OptionBuilder.withLongOpt("output-file").withDescription("use given file for output").hasArg().create("o"));
    options.addOption(OptionBuilder.withLongOpt("husband-id").withDescription("use given id for husband").hasArg().create());
    options.addOption(OptionBuilder.withLongOpt("wife-id").withDescription("use given id for wife").hasArg().create());
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    if (line.hasOption("husband-id")) {
      husband = line.getOptionValue("husband-id");
    }
    if (line.hasOption("wife-id")) {
      wife = line.getOptionValue("wife-id");
    }
    if (line.hasOption("o")) {
      outputfilename = line.getOptionValue("output-file");
    }
    if (line.hasOption("i")) {
      inputfilename = line.getOptionValue("input-file");
    }
  }
}
