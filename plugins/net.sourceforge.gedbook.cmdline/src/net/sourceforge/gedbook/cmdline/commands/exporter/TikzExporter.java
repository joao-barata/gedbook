/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

/**
 * @author Joao Barata
 */
public class TikzExporter extends AbstractExporter {

  /**
   * {@inheritDoc}
	 */
  protected void export(Project project) {

    if (null != project) {
      Person root = ProjectQueryHelper.getInstance().getExistingPerson(project, "I1"); //$NON-NLS-1$
      if (null != root) {
        System.out.println("\\begin{tikzpicture}"); //$NON-NLS-1$
        System.out.println("\\tikzset{grow'=up,level distance=48pt}"); //$NON-NLS-1$
        System.out.println("\\tikzset{MALE/.style={align=center,anchor=north,fill=blue!20,rounded corners}}"); //$NON-NLS-1$
        System.out.println("\\tikzset{FEMALE/.style={align=center,anchor=north,fill=red!20,rounded corners}}"); //$NON-NLS-1$

        System.out.println("\\Tree "); //$NON-NLS-1$
        generateReport(root, 0);
        System.out.println("\\end{tikzpicture}"); //$NON-NLS-1$
      }
    }
  }

  private void generateReport(Person person, int deep) {
    Birth birth = person.getBirth();
    Death death = person.getDeath();
    Date birthDate = (null != birth) ? birth.getDate().getDate() : null;
    Date deathDate = (null != death) ? death.getDate().getDate() : null;

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
    String birthDateStr = (null != birthDate) ? formatter.format(birthDate) : ""; //$NON-NLS-1$
    String deathDateStr = (null != deathDate) ? formatter.format(deathDate) : ""; //$NON-NLS-1$

    System.out.println("[.\\node[" + person.getSex().getName() + "]{" + GedBookModelHelper.getFullName(person) //$NON-NLS-1$ //$NON-NLS-2$
        + ("".equals(birthDateStr) ? "" : "\\\\birth: " + birthDateStr) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        + ("".equals(deathDateStr) ? "" : "\\\\death: " + deathDateStr) + "};"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

    Person father = person.getFather();
    if (null != father) {
      generateReport(father, deep + 1);
    }
    Person mother = person.getMother();
    if (null != mother) {
      generateReport(mother, deep + 1);
    }
    System.out.println("]"); //$NON-NLS-1$
  }

  private static String getIndent(int deep) {
    String indent = ""; //$NON-NLS-1$
    for (int i = 0; i < deep; i++) {
      indent += "\t"; //$NON-NLS-1$
    }
    return indent;
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new TikzExporter().executeCommand(args);
  }
}
