/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.AbstractGenerator;
import net.sourceforge.gedbook.generator.AbstractGraphvizDotFullGenerator;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public abstract class AbstractFullTreeExporter extends AbstractFamilyExporter {

  protected abstract AbstractGraphvizDotFullGenerator getGraphvizDotGenerator(Project project, String output, Configuration conf, Person husband, Person wife);

  /**
   * {@inheritDoc}
   */
  protected void export(Project project) {
    if (null != project) {
      AbstractGenerator generator = getGraphvizDotGenerator(project, output, configurationFile,
        ProjectQueryHelper.getInstance().getExistingPerson(project, id1),
        ProjectQueryHelper.getInstance().getExistingPerson(project, id2));
      if (null != generator) {
        generator.run();
      }
    }
  }
}
