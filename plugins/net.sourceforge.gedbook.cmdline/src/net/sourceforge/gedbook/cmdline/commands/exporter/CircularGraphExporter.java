/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.BookGraphsGenerator;
import net.sourceforge.gedbook.model.core.Project;

/**
 * @author Joao Barata
 */
public class CircularGraphExporter extends AbstractFamilyExporter {

  /**
   * {@inheritDoc}
   */
  @Override
  protected void export(Project project) {
    BookGraphsGenerator graphs = new BookGraphsGenerator(project, output, configurationFile, id1, id2);
    graphs.run();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new CircularGraphExporter().executeCommand(args);
  }
}
