/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands;

import net.sourceforge.gedbook.cmdline.ICommandLinehandler;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author Joao Barata
 */
public abstract class AbstractCommand implements ICommandLinehandler, Runnable {

  /**
   * {@inheritDoc}
   */
  public void manageArguments(String[] args) {
    // create the command line parser
    CommandLineParser parser = new BasicParser();
    try {
      Options options = getOptions();
      // parse the command line arguments
      CommandLine line = parser.parse(getOptions(), args);
      handleArguments(line, options);
    } catch (ParseException exp) {
      // something went wrong
      System.err.println("Parsing failed. Reason: " + exp.getMessage());
    }
  }

  /**
	 * 
	 */
  protected void init() {
    // by default, do nothing (expected to be overridden, if necessary)
  }

  /**
   * {@inheritDoc}
   */
  public Options getOptions() {
    Options options = new Options();
    options.addOption("h", "help", false, "list the available options");
    options.addOption("v", "version", false, "print the version information");
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    if (line.hasOption("h")) {
      // automatically generate the help statement
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp(getCommandName(), options);
    }
    if (line.hasOption("v")) {
      System.out.println("0.6.0");
    }
  }

  /**
   * {@inheritDoc}
   */
  protected String getCommandName() {
    return getClass().getName();
  }

  /**
	 * 
	 */
  protected void executeCommand(String[] args) {
    manageArguments(args);
    init();
    run();
  }
}
