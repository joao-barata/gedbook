/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.BookGraphsGenerator;
import net.sourceforge.gedbook.generator.LatexBibGenerator;
import net.sourceforge.gedbook.generator.LatexBookAscendingGenerator;
import net.sourceforge.gedbook.generator.LatexBookGenerator;
import net.sourceforge.gedbook.model.core.Project;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class AscendingLatexBookExporter extends AbstractLatexBookExporter {

  /**
   * {@inheritDoc}
   */
  protected LatexBibGenerator getLatexBibGenerator(Project project, String output, Configuration conf) {
    return new LatexBibGenerator(project, output, conf);
  }

  /**
   * {@inheritDoc}
   */
  protected LatexBookGenerator getLatexBookGenerator(Project project, String output, Configuration conf, String id1, String id2) {
    return new LatexBookAscendingGenerator(project, output, conf, id1, id2);
  }

  /**
   * {@inheritDoc}
   */
  protected BookGraphsGenerator getBookGraphsGenerator(Project project, String output, Configuration conf, String id1, String id2) {
    return new BookGraphsGenerator(project, output, conf, id1, id2);
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new AscendingLatexBookExporter().executeCommand(args);
  }
}
