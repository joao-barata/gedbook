/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.AbstractGenerator;
import net.sourceforge.gedbook.generator.BookGraphsGenerator;
import net.sourceforge.gedbook.generator.LatexBibGenerator;
import net.sourceforge.gedbook.generator.LatexBookGenerator;
import net.sourceforge.gedbook.model.core.Project;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public abstract class AbstractLatexBookExporter extends AbstractFamilyExporter {

  protected abstract LatexBibGenerator getLatexBibGenerator(Project project, String output, Configuration conf);

  protected abstract LatexBookGenerator getLatexBookGenerator(Project project, String output, Configuration conf, String id1, String id2);

  protected abstract BookGraphsGenerator getBookGraphsGenerator(Project project, String output, Configuration conf, String id1, String id2);

  /**
   * {@inheritDoc}
   */
  protected void export(Project project) {
    if (null != project) {
      AbstractGenerator generator = getLatexBibGenerator(project, output, configurationFile);
      if (null != generator) {
        generator.run();
      }
      generator = getLatexBookGenerator(project, output, configurationFile, id1, id2);
      if (null != generator) {
        generator.run();
      }
      generator = getBookGraphsGenerator(project, output, configurationFile, id1, id2);
      if (null != generator) {
        generator.run();
      }
    }
  }
}
