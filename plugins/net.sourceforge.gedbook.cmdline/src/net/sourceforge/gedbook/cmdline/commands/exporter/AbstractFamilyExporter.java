/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * @author Joao Barata
 */
public abstract class AbstractFamilyExporter extends AbstractExporter {

  protected String id1;
  protected String id2;

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption(OptionBuilder.withLongOpt("first-identifier").withDescription("").hasArg().create("id1"));
    options.addOption(OptionBuilder.withLongOpt("second-identifier").withDescription("").hasArg().create("id2"));
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    if (line.hasOption("id1")) {
      id1 = line.getOptionValue("first-identifier");
    }
    if (line.hasOption("id2")) {
      id2 = line.getOptionValue("second-identifier");
    }
  }
}
