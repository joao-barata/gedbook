/*******************************************************************************
 * Copyright (c) 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.AWTFullAscendingGenerator;
import net.sourceforge.gedbook.generator.AbstractGenerator;
import net.sourceforge.gedbook.model.ProjectQueryHelper;
import net.sourceforge.gedbook.model.core.Project;

/**
 * @author Joao Barata
 */
public class AscendingGraphicalTreeExporter extends AbstractFamilyExporter {

  /**
   * {@inheritDoc}
   */
  @Override
  protected void export(Project project) {
    AbstractGenerator generator = new AWTFullAscendingGenerator(
      project, output, configurationFile,
      ProjectQueryHelper.getInstance().getExistingPerson(project, id1),
      ProjectQueryHelper.getInstance().getExistingPerson(project, id2));
    generator.run();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new AscendingGraphicalTreeExporter().executeCommand(args);
  }
}
