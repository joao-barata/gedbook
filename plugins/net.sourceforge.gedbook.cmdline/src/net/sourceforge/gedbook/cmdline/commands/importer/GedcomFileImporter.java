/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.importer;

import java.io.File;
import java.io.IOException;

import net.sourceforge.gedbook.model.GedBookXMIResourceFactoryImpl;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.gedcom.GedcomImporter;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author Joao Barata
 */
public class GedcomFileImporter extends AbstractImporter {

  private String inputGedcomFile;

  /**
   * {@inheritDoc}
   */
  public void run() {
    if (inputGedcomFile != null) {
      File inputFile = new File(inputGedcomFile);
      File outputFile = new File(outputfilename);
      if (inputFile.exists()) {
        importGedcom(inputFile, outputFile);
      } else {
        System.err.println("GedcomFileImporter.run() _ input file does not exist"); //$NON-NLS-1$
      }
    }
  }

  /**
   * Import the File and start to process it.
   * 
   * @param inputFile
   *          a Gedcom file
   * @param outputFile
   *          the output file
   */
  public void importGedcom(File inputFile, File outputFile) {
    try {
      GedcomImporter importer = new GedcomImporter(inputFile);
      importer.run();
      EObject project = importer.createModel(new NullProgressMonitor());
      ResourceUtils.saveObject(project, outputFile);
    } catch (IOException exception_p) {
      System.err.println("GedcomFileImporter.main(..) _ "); //$NON-NLS-1$
    }
  }

  public void init() {
    //System.out.println("Register factory ..."); //$NON-NLS-1$
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new GedBookXMIResourceFactoryImpl());
    //System.out.println("Register packages ..."); //$NON-NLS-1$
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new GedcomFileImporter().executeCommand(args);
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption("t", "temp-data", false, "generate intermediate xml data (for debug purpose)");
    options.addOption(OptionBuilder.withLongOpt("input-gedcom-file").withDescription("Gedcom file to be imported").hasArg().create("g"));
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    if (line.hasOption("g")) {
      inputGedcomFile = line.getOptionValue("input-gedcom-file");
    }
 }
}
