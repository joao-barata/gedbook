/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.generator.LatexBibGenerator;
import net.sourceforge.gedbook.model.core.Project;

/**
 * @author Joao Barata
 */
public class BibtexExporter extends AbstractExporter {

  /**
   * {@inheritDoc}
   */
  @Override
  protected void export(Project project) {
    LatexBibGenerator bibtex = new LatexBibGenerator(project, output, configurationFile);
    bibtex.run();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new BibtexExporter().executeCommand(args);
  }
}
