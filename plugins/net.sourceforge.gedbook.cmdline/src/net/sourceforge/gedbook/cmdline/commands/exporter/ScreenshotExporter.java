/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import java.io.IOException;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.latex.generator.IScreenshotExtractor;
import net.sourceforge.gedbook.latex.generator.utils.ScreenshotExtractorHelper;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.SourceFile;
import net.sourceforge.gedbook.model.data.MultipleSourceFile;
import net.sourceforge.gedbook.model.data.ExtendedData;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.FileGroup;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.eclipse.emf.ecore.EObject;

/**
 * @author Joao Barata
 */
public class ScreenshotExporter extends AbstractExporter {

  protected boolean force;

  /**
   * {@inheritDoc}
	 */
  protected void export(Project project) {
    if (null != project) {
      List<EObject> referencers = ResourceUtils.getReferencers(project, DataPackage.Literals.EXTENDED_DATA__REFERENCED_PROJECT);
      for (EObject referencer: referencers) {
        for (FileGroup group : ((ExtendedData) referencer).getGroups()) {
          for (SourceFile file : group.getFiles()) {
            String path = System.getenv(IConfigurationConstants.GEDBOOK_ARCHIVE_REPOSITORY) + file.getPath();
            String fileExtension = path.substring(path.lastIndexOf('.') + 1).toLowerCase();
            IScreenshotExtractor screenshotExtractor = ScreenshotExtractorHelper.getInstance().getScreenshotExtractors().get(fileExtension);
            if (screenshotExtractor != null) {
              if (file instanceof MultipleSourceFile) {
                for (Extract extract : ((MultipleSourceFile) file).getExtracts()) {
                  for (Screenshot screenshot : extract.getScreenshots()) {
                    String filename = file.getPath().substring(file.getPath().lastIndexOf('/') + 1);
                    filename = filename.substring(0, filename.lastIndexOf('.'));
                    filename += "_" + screenshot.getPage() + "_" + extract.getScreenshots().indexOf(screenshot);
                    try {
                      screenshotExtractor.extractScreenshot(path, "png", screenshot, output + "/" + filename, configurationFile);
                    } catch (IOException e) {
                      System.err.print("Error extracting file " + path);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new ScreenshotExporter().executeCommand(args);
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption(OptionBuilder.withLongOpt("force").withDescription("").hasArg().create("f"));
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    force = line.hasOption("f");
  }
}
