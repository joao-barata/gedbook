/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.exporter;

import net.sourceforge.gedbook.cmdline.commands.AbstractCommand;
import net.sourceforge.gedbook.model.ProjectImporter;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.data.DataPackage;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.AbstractFileConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * @author Joao Barata
 */
public abstract class AbstractExporter extends AbstractCommand {

  protected Configuration configurationFile;
  protected String inputfilename;
  protected String output;

  /**
   * {@inheritDoc}
   */
  public void run() {
    ProjectImporter.getInstance().loadModel(inputfilename);
    EObject object = ProjectImporter.getInstance().getCurrentProject();
    if (object instanceof Project) {
      export((Project) object);
    }
  }

  protected abstract void export(Project project);

  /**
   * {@inheritDoc}
   */
  protected void init() {
    //System.out.println("Register factory ..."); //$NON-NLS-1$
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
    //System.out.println("Register packages ..."); //$NON-NLS-1$
    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption(OptionBuilder.withLongOpt("configuration-file").withDescription("use given configuration file for generation").hasArg().create("c"));
    options.addOption(OptionBuilder.withLongOpt("input-model").withDescription("use given model for input").hasArg().create("i"));
    options.addOption(OptionBuilder.withLongOpt("output").withDescription("use given file/folder for output").hasArg().create("o"));
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    if (line.hasOption("i")) {
      inputfilename = line.getOptionValue("input-model");
    }
    if (line.hasOption("o")) {
      output = line.getOptionValue("output");
    }
    if (line.hasOption("c")) {
      String filepath = line.getOptionValue("configuration-file");
      try {
        configurationFile = new PropertiesConfiguration();
        ((AbstractConfiguration) configurationFile).setDelimiterParsingDisabled(true);
        ((AbstractFileConfiguration) configurationFile).load(filepath);
      } catch (ConfigurationException ex) {
        ex.printStackTrace();
      }
    }
  }
}
