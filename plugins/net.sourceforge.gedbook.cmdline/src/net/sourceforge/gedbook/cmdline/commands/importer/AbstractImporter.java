/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline.commands.importer;

import net.sourceforge.gedbook.cmdline.commands.AbstractCommand;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * @author Joao Barata
 */
public abstract class AbstractImporter extends AbstractCommand {

  protected String outputfilename;

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("static-access")
  public Options getOptions() {
    Options options = super.getOptions();
    options.addOption(OptionBuilder.withLongOpt("output-file").withDescription("use given file for output").hasArg().create("o"));
    return options;
  }

  /**
   * {@inheritDoc}
   */
  public void handleArguments(CommandLine line, Options options) {
    super.handleArguments(line, options);

    if (line.hasOption("o")) {
      outputfilename = line.getOptionValue("output-file");
    }
  }
}
