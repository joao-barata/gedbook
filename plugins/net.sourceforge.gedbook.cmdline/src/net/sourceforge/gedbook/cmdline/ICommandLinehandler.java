/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.cmdline;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

/**
 * @author Joao Barata
 */
public interface ICommandLinehandler {

  /**
   * create the Options
   */
  public Options getOptions();

  /**
   * handle the Arguments
   */
  public void handleArguments(CommandLine line, Options options);
}
