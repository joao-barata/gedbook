/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator.utils;

import java.util.HashMap;
import java.util.Map;

import net.sourceforge.gedbook.latex.generator.IScreenshotExtractor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.equinox.nonosgi.registry.RegistryFactoryHelper;

/**
 * @author Joao Barata
 */
public class ScreenshotExtractorHelper {
  
  private static ScreenshotExtractorHelper instance;

  private Map<String, IScreenshotExtractor> _screenshotExtractors;

  public static final String SCREENSHOT_EXTRACTOR_EXTENSION_POINT_ID = "screenshotExtractor";
  
  public static final String SCREENSHOT_EXTRACTOR_EXTENSION_POINT_CLASS_ATTRIBUTE = "class";
  
  public static final String SCREENSHOT_EXTRACTOR_EXTENSION_POINT_DOCTYPE_ATTRIBUTE = "doctype";

  private ScreenshotExtractorHelper() {
    // do nothing
  }

  public static ScreenshotExtractorHelper getInstance() {
    if (instance == null) {
      instance = new ScreenshotExtractorHelper();
    }
    return instance;
  }

  public Map<String, IScreenshotExtractor> getScreenshotExtractors() {
    if (_screenshotExtractors == null) {
      _screenshotExtractors = new HashMap<String, IScreenshotExtractor>();
      IExtensionRegistry extensionRegistry = RegistryFactoryHelper.getRegistry();
      if (extensionRegistry != null) {
        IConfigurationElement[] configElements = extensionRegistry.getConfigurationElementsFor("net.sourceforge.gedbook.latex.generator", SCREENSHOT_EXTRACTOR_EXTENSION_POINT_ID);
        for (IConfigurationElement configurationElement : configElements) {
          try {
            Object contributor = configurationElement.createExecutableExtension(SCREENSHOT_EXTRACTOR_EXTENSION_POINT_CLASS_ATTRIBUTE);
            if (contributor instanceof IScreenshotExtractor) {
              String doctype = configurationElement.getAttribute(SCREENSHOT_EXTRACTOR_EXTENSION_POINT_DOCTYPE_ATTRIBUTE);
              _screenshotExtractors.put(doctype, (IScreenshotExtractor) contributor);
            }
          } catch (CoreException exception_p) {
            exception_p.printStackTrace();
          }
        }
      }
    }
    return _screenshotExtractors;
  }

}
