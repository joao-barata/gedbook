/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator;

import java.io.IOException;

import org.apache.commons.configuration.Configuration;

import net.sourceforge.gedbook.model.data.Screenshot;

/**
 * @author Joao Barata
 */
public interface IScreenshotExtractor {

  void extractScreenshot(String documentFilepath, String documentType, Screenshot screenshot, String outputScreenshotFilepath, Configuration configurationFile) throws IOException;

}
