/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import net.sourceforge.gedbook.latex.generator.GedBookSectionGenerator;
import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.latex.generator.ISectionProvider;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.helpers.DateConverter;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class GedBookGeneratorHelper {

  /**
   * 
   */
  public static String getLongDate(DateWrapper date, Configuration configurationFile, boolean italic, boolean bold, boolean firstUpper) {
    String language = configurationFile.getString(IConfigurationConstants.DOCUMENT_LANGUAGE);
    String handleRepublicanDates = configurationFile.getString(IConfigurationConstants.HANDLE_REPUBLICAN_DATES);

    ISectionProvider provider = GedBookSectionGenerator.getInstance().getSectionProviders().get(language);
    if (null == language || null == provider) {
      throw new RuntimeException("unhandled property " + IConfigurationConstants.DOCUMENT_LANGUAGE + "=" + language); //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    if (null == date) {
      return provider.getUndefinedDate(firstUpper);
    }
    
    String prefix = provider.getDatePrefix(date, firstUpper);
    String dateRepublicaine = Boolean.parseBoolean(handleRepublicanDates) ?
      DateConverter.getInstance().conversionDateGregorienRepublicain2(date.getDate()) : null;

    Locale loc = new Locale(language);
    SimpleDateFormat dayOfWeekFormatter = new SimpleDateFormat("EEEE", loc); //$NON-NLS-1$
    SimpleDateFormat dayOfMonthFormatter = new SimpleDateFormat("dd", loc); //$NON-NLS-1$
    SimpleDateFormat monthFormatter = new SimpleDateFormat("MMMM", loc); //$NON-NLS-1$
    SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy", loc); //$NON-NLS-1$

    String longDate = (null != date && null != date.getDate()) ? EncodingUtils.latexEncoding((date.isDay() ? dayOfWeekFormatter.format(date.getDate()) + " " : GedBookModelHelper.EMPTY) + //$NON-NLS-1$
        (date.isDay() ? dayOfMonthFormatter.format(date.getDate()) + " " : GedBookModelHelper.EMPTY) + //$NON-NLS-1$
        (date.isMonth() ? monthFormatter.format(date.getDate()) + " " : GedBookModelHelper.EMPTY) + //$NON-NLS-1$
        (date.isYear() ? yearFormatter.format(date.getDate()) : GedBookModelHelper.EMPTY) +
        ((null == dateRepublicaine) ? GedBookModelHelper.EMPTY : " (" + dateRepublicaine + ")")) //$NON-NLS-1$ //$NON-NLS-2$
        : "..."; //$NON-NLS-1$

    if (italic) {
      return prefix + "\\textit{" + longDate + "}"; //$NON-NLS-1$ //$NON-NLS-2$
    } else if (bold) {
      return prefix + "\\textbf{" + longDate + "}"; //$NON-NLS-1$ //$NON-NLS-2$
    }
    return prefix + longDate;
  }
}
