/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator.providers;

import java.util.Comparator;
import java.util.Queue;

import net.sourceforge.gedbook.latex.generator.ISectionProvider;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class EmptySectionProvider implements ISectionProvider {

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBirthSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBatismSentence(Person person, Configuration configurationFile, boolean minipage) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getChildOrderSentence(Person person, Person father, Person mother, Project project) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getOrdinalIndicator(int number, SexKind gender) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBrothersOrSistersSentence(Person person, Person father, Person mother, Project project, Comparator<SourceCitation> eventComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getProfessionSentence(Person person, Comparator<Event> eventComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getLocationSentence(Person person, Comparator<Event> eventComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getParentsDeathSentence(Person person, Person father, Person mother, Person partner, Configuration configurationFile, Comparator<Person> deathDateComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDeathSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBurialSentence(Person person, Configuration configurationFile, boolean minipage) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getWillSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getMarriageSentence(Person husband, Person wife, Configuration configurationFile, Queue<Event> eventQueue, Comparator<SourceCitation> eventComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBannsSentence(Person husband, Person wife, Configuration configurationFile, Queue<Event> eventQueue) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getNextMarriageSentence(Person person, Family family, Configuration configurationFile, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getPreviousMarriageSentence(Person person, Family family, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getChildrenSentence(Person father, Person mother, Project project, Comparator<SourceCitation> eventComparator) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getReferenceSentence(Family family) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getAndCoordinatingConjunction() {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDatePrefix(DateWrapper date, boolean firstUpper) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUndefinedDate(boolean firstUpper) {
    return GedBookModelHelper.EMPTY;
  }
}
