/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.equinox.nonosgi.registry.RegistryFactoryHelper;

import net.sourceforge.gedbook.latex.generator.utils.ScreenshotExtractorHelper;
import net.sourceforge.gedbook.model.core.Banns;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Document;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Place;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.core.Will;
import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Photo;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.SourceFile;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelComparators;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;
import net.sourceforge.gedbook.model.helpers.ResourceUtils;

/**
 * @author Joao Barata
 */
public class GedBookSectionGenerator {

  public static final String SECTION_PROVIDER_EXTENSION_POINT_ID = "sectionProvider";
  
  public static final String SECTION_PROVIDER_EXTENSION_POINT_CLASS_ATTRIBUTE = "class";
  
  public static final String SECTION_PROVIDER_EXTENSION_POINT_LANGUAGE_ATTRIBUTE = "language";
  
  private Queue<Event> _eventQueue;

  private Map<String, ISectionProvider> _sectionProviders;

  private static GedBookSectionGenerator _instance;
  
  private GedBookSectionGenerator() {
    _eventQueue = new LinkedList<Event>();
  }
  
  public static GedBookSectionGenerator getInstance() {
    if (_instance == null) {
      _instance = new GedBookSectionGenerator();
    }
    return _instance;
  }

  public Map<String, ISectionProvider> getSectionProviders() {
    if (_sectionProviders == null) {
      _sectionProviders = new HashMap<String, ISectionProvider>();
      IExtensionRegistry extensionRegistry = RegistryFactoryHelper.getRegistry();
      if (extensionRegistry != null) {
        IConfigurationElement[] configElements = extensionRegistry.getConfigurationElementsFor("net.sourceforge.gedbook.latex.generator", SECTION_PROVIDER_EXTENSION_POINT_ID);
        for (IConfigurationElement configurationElement : configElements) {
          try {
            Object contributor = configurationElement.createExecutableExtension(SECTION_PROVIDER_EXTENSION_POINT_CLASS_ATTRIBUTE);
            if (contributor instanceof ISectionProvider) {
              String language = configurationElement.getAttribute(SECTION_PROVIDER_EXTENSION_POINT_LANGUAGE_ATTRIBUTE);
              _sectionProviders.put(language, (ISectionProvider) contributor);
            }
          } catch (CoreException exception_p) {
            exception_p.printStackTrace();
          }
        }
      }
    }
    return _sectionProviders;
  }

  /**
   * 
   */
  public void generateSection(BufferedWriter writer, Project project, Family family, Configuration configurationFile, BufferedWriter appendix, Set<Extract> appendixBuffer, Set<Family> familyBuffer, String familyIdentifier, String output) throws IOException {
    String language = configurationFile.getString(IConfigurationConstants.DOCUMENT_LANGUAGE);
    ISectionProvider provider = getSectionProviders().get(language);
    if (provider != null) {
      Person husband = family.getHusband();
      Person wife = family.getWife();

      writer.append("\\section{" + configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SECTIONPREFIX) + " " + GedBookModelHelper.getPersonLastName(husband) + " / " + GedBookModelHelper.getPersonLastName(wife) + "}" + GedBookModelHelper.NEWLINE);
      if (!familyBuffer.contains(family)) {
        writer.append("\\label{" + family.getId() + "}" + GedBookModelHelper.NEWLINE);
      }
      writer.newLine();
      writer.append("\\begin{center}" + GedBookModelHelper.NEWLINE);
      writer.append("\\includegraphics[width=" + configurationFile.getProperty(IConfigurationConstants.FAMILY_GRAPH_MAX_WIDTH)
        + ",height=" + configurationFile.getProperty(IConfigurationConstants.FAMILY_GRAPH_MAX_HEIGHT)
        + ",keepaspectratio=true]{" + configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/graphs/" + familyIdentifier + "." + GedBookModelHelper.DOT_FORMAT + "}" + GedBookModelHelper.NEWLINE);
      writer.append("\\end{center}" + GedBookModelHelper.NEWLINE);
      writer.newLine();

      if (familyBuffer.contains(family)) {
        writer.append("\\vspace{1cm}" + GedBookModelHelper.NEWLINE);
        writer.newLine();
        writer.append(provider.getReferenceSentence(family) + GedBookModelHelper.NEWLINE);     
        writer.newLine();
      } else {
        // husband
        writer.append("\\subsection{" + configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SUBSECTION1TITLE) + "}" + GedBookModelHelper.NEWLINE);
        writer.append("\\par" + GedBookModelHelper.NEWLINE);
        if (null != husband) {
          writer.append(getPersonSentence(provider, husband, husband.getFather(), husband.getMother(), wife, project, configurationFile));
          writer.append(flushSources(GedBookModelHelper.getFullName(husband), null, appendix, appendixBuffer, output, configurationFile));
        }
        // wife
        writer.append("\\subsection{" + configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SUBSECTION2TITLE) + "}" + GedBookModelHelper.NEWLINE);
        writer.append("\\par" + GedBookModelHelper.NEWLINE);
        if (null != wife) {
          writer.append(getPersonSentence(provider, wife, wife.getFather(), wife.getMother(), husband, project, configurationFile));
          writer.append(flushSources(null, GedBookModelHelper.getFullName(wife), appendix, appendixBuffer, output, configurationFile));
        }
        // marriage
        writer.append("\\subsection{" + configurationFile.getProperty(IConfigurationConstants.DOCUMENT_SUBSECTION3TITLE) + "}" + GedBookModelHelper.NEWLINE);
        writer.append("\\par" + GedBookModelHelper.NEWLINE);
        writer.append("\\InputIfFileExists{" + configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/timelines/" + family.getId() + ".tex}{}{}" + GedBookModelHelper.NEWLINE);
        writer.append("\\par" + GedBookModelHelper.NEWLINE);
        writer.append(provider.getMarriageSentence(husband, wife, configurationFile, _eventQueue, GedBookModelComparators.eventComparator));
        writer.append(provider.getPreviousMarriageSentence(husband, family, GedBookModelComparators.eventComparator, GedBookModelComparators.marriageDateComparator));
        writer.append(provider.getPreviousMarriageSentence(wife, family, GedBookModelComparators.eventComparator, GedBookModelComparators.marriageDateComparator));
        writer.append(provider.getBannsSentence(husband, wife, configurationFile, _eventQueue));
        writer.append(provider.getChildrenSentence(husband, wife, project, GedBookModelComparators.eventComparator));
        writer.append(provider.getNextMarriageSentence(husband, family, configurationFile, GedBookModelComparators.eventComparator, GedBookModelComparators.marriageDateComparator));
        writer.append(provider.getNextMarriageSentence(wife, family, configurationFile, GedBookModelComparators.eventComparator, GedBookModelComparators.marriageDateComparator));
        writer.append(flushSources(GedBookModelHelper.getFullName(husband), GedBookModelHelper.getFullName(wife), appendix, appendixBuffer, output, configurationFile));

        List<Photo> photos = retrievePhotos(family);
        for (Photo photo : photos) {
          writer.append("\\par" + GedBookModelHelper.NEWLINE);
          writer.append("\\begin{figure}[h]" + GedBookModelHelper.NEWLINE);
          writer.append("\\centering" + GedBookModelHelper.NEWLINE);
          writer.append("\\captionsetup{width=" + photo.getWidth() + "mm}" + GedBookModelHelper.NEWLINE);
          writer.append("\\includegraphics[width=" + photo.getWidth() + "mm,height=" + photo.getHeight() + "mm]{" + System.getenv(IConfigurationConstants.GEDBOOK_IMAGE_REPOSITORY) + photo.getPath() + "}" + GedBookModelHelper.NEWLINE);
          writer.append("\\caption[" + photo.getCaption() + "]{" + photo.getCaption() + " (" + photo.getDescription() + ").}" + GedBookModelHelper.NEWLINE);
          writer.append("\\end{figure}" + GedBookModelHelper.NEWLINE);
        }

        familyBuffer.add(family);
      }

      // new page
      writer.append("\\clearpage" + GedBookModelHelper.NEWLINE);
    }
    else {
      System.err.println("Error: unhandled property " + IConfigurationConstants.DOCUMENT_LANGUAGE + "=" + language);
    }
  }

  /**
   * 
   */
  protected String getPersonSentence(ISectionProvider provider, Person person, Person father, Person mother, Person partner, Project project, Configuration configurationFile) {
    String sentence = GedBookModelHelper.EMPTY;

    String photo = retrievePhoto(person);
    if (null != photo) {
      sentence += "\\begin{minipage}[c]{0.30\\linewidth}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      sentence += "\\begin{center}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      sentence += "\\includegraphics[height=40mm]{" + photo + "}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$ //$NON-NLS-2$
      sentence += "\\end{center}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      sentence += "\\end{minipage} \\hfill" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      sentence += "\\begin{minipage}[c]{0.70\\linewidth}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
    }

    sentence += provider.getBirthSentence(person, configurationFile, photo != null, _eventQueue);
    sentence += provider.getBatismSentence(person, configurationFile, photo != null);
    sentence += provider.getChildOrderSentence(person, father, mother, project);
    sentence += provider.getDeathSentence(person, configurationFile, photo != null, _eventQueue);
    sentence += provider.getBurialSentence(person, configurationFile, photo != null);
    sentence += provider.getWillSentence(person, configurationFile, photo != null, _eventQueue);

    if (null != photo) {
      sentence += "\\end{minipage}" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      sentence += GedBookModelHelper.flushEventNoteFootnote();
      sentence += "\\hfill \\break" + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
    }

    sentence += provider.getBrothersOrSistersSentence(person, father, mother, project, GedBookModelComparators.eventComparator);
    sentence += provider.getProfessionSentence(person, GedBookModelComparators.eventDateComparator);
    sentence += provider.getLocationSentence(person, GedBookModelComparators.eventDateComparator);
    sentence += provider.getParentsDeathSentence(person, father, mother, partner, configurationFile, GedBookModelComparators.deathDateComparator);
    
    return sentence;
  }

  /**
   * @param person
   * @return
   */
  protected String retrievePhoto(Person person) {
    for (EObject ref : ResourceUtils.getReferencers(person, DataPackage.Literals.PHOTO__PERSON)) {
      return System.getenv(IConfigurationConstants.GEDBOOK_IMAGE_REPOSITORY) + ((Photo) ref).getPath();
    }
    return null;
  }

  /**
   * @param family
   * @return
   */
  protected List<Photo> retrievePhotos(Family family) {
    List<Photo> result = new ArrayList<>();
    for (EObject ref : ResourceUtils.getReferencers(family, DataPackage.Literals.PHOTO__FAMILY)) {
      result.add((Photo) ref);
    }
    return result;
  }

  /**
   * 
   */
  protected String flushSources(String husband, String wife, BufferedWriter appendix, Set<Extract> appendixBuffer, String output, Configuration configurationFile) throws IOException {
    String sentence = "";

    // we keep this list to avoid duplications when an extract is related to multiple events (e.g same extract for DEATH and WILL)
    Set<Extract> flushedExtracts = new HashSet<Extract>();

    while (!_eventQueue.isEmpty()) {
      Event event = _eventQueue.remove();
      if (event instanceof Birth) {
        sentence += appendSources(event, flushedExtracts, husband, wife, appendix, appendixBuffer, output, configurationFile);
      } else if (event instanceof Death) {
        sentence += appendSources(event, flushedExtracts, husband, wife, appendix, appendixBuffer, output, configurationFile);
      } else if (event instanceof Will) {
        sentence += appendSources(event, flushedExtracts, husband, wife, appendix, appendixBuffer, output, configurationFile);
      } else if (event instanceof Banns) {
        sentence += appendSources(event, flushedExtracts, husband, wife, appendix, appendixBuffer, output, configurationFile);
      } else if (event instanceof Marriage) {
        sentence += appendSources(event, flushedExtracts, husband, wife, appendix, appendixBuffer, output, configurationFile);
      }
    }

    return sentence;
  }

  /**
   * 
   */
  private String getReference(SourceCitation citation, int screenshotIndex, int page) {
    return citation.getId() + "-" + screenshotIndex + "-" + page;
  }

  /**
   * 
   */
  private String appendSources(Event event_p, Set<Extract> flushedExtracts, String husband, String wife, BufferedWriter appendix, Set<Extract> appendixBuffer, String output, Configuration configurationFile) throws IOException {
    String sentence = "";
    if (null != event_p) {
      boolean extractScreenshots = Boolean.parseBoolean(configurationFile.getString(IConfigurationConstants.EXTRACT_SCREENSHOTS));
      List<SourceCitation> citations = new ArrayList<SourceCitation>(event_p.getCitations());
      Collections.sort(citations, GedBookModelComparators.eventComparator);
      for (SourceCitation citation : citations) {
        Document doc = citation.getSource();
        String abbr = doc.getAbbreviation();

        sentence += "\\par" + GedBookModelHelper.NEWLINE;

        List<EObject> referencers = ResourceUtils.getReferencers(citation, DataPackage.Literals.EXTRACT__CITATIONS);
        if (referencers.isEmpty()) {
          Place place = doc.getPlace();
          System.out.println("missing extract for "
              + (place != null ? place.getCommune() : "<null>")
              + " - " + abbr + " (" + doc.getDate() + ") p." + citation.getPage()
              + " [" + (husband != null ? husband : "")
              + ((husband != null && wife != null) ? " & " : "")
              + (wife != null ? wife : "") + "]");
        } else {
          for (EObject obj : referencers) {
            Extract extract = (Extract) obj;
            if (!flushedExtracts.contains(extract)) {
              flushedExtracts.add(extract);
              String path = System.getenv(IConfigurationConstants.GEDBOOK_ARCHIVE_REPOSITORY) + ((SourceFile) extract.eContainer()).getPath();

              /**
               * extracts the screenshots into individual files.
               */
              if (extractScreenshots) {
                String fileExtension = path.substring(path.lastIndexOf('.') + 1).toLowerCase();
                IScreenshotExtractor screenshotExtractor = ScreenshotExtractorHelper.getInstance().getScreenshotExtractors().get(fileExtension);
                if (screenshotExtractor != null) {
                  String folder = output + "/" + configurationFile.getProperty(IConfigurationConstants.FAMILY_COUPLE) + IConfigurationConstants.GENEALOGY_SUFFIX + "/screenshots";
                  for (Screenshot screenshot : extract.getScreenshots()) {
                    int page = screenshot.getPage();
                    String filename = citation.getId() + "_" + extract.getScreenshots().indexOf(screenshot) + "_" + page;
                    screenshotExtractor.extractScreenshot(path, "png", screenshot, folder + "/" + filename, configurationFile);
                  }
                }
              }

              /**
               * inserts the screenshots into the appendix. 
               */
              if (!appendixBuffer.contains(extract)) {
                int index = 1;
                boolean clearpage = false;
                for (Screenshot screenshot : extract.getScreenshots()) {
                  int x1 = screenshot.getXLeftBottom();
                  int y1 = screenshot.getYLeftBottom();
                  int x2 = screenshot.getXRightTop();
                  int y2 = screenshot.getYRightTop();
                  int w = screenshot.getWidth();
                  int h = screenshot.getHeight();
                  int page = screenshot.getPage();
    
                  String source = "\\insertSource{" + path + "}"
                      + "{" + EncodingUtils.latexEncoding(doc.getPlace().getCommune()) + " - " + abbr + " (" + doc.getDate() + "). \\cite[p." + page + "]{" + doc.getId() + "}}"
                      + "{fig:" + getReference(citation, index, page) + "}"
                      + "{viewport=" + x1 + " " + y1 + " " + x2 + " " + y2 + ",clip,height=" + h + "mm,width=" + w + "mm,page=" + page + "}" + GedBookModelHelper.NEWLINE;
                  appendix.append(source);
                  clearpage = true;
                  index++;
                }
                if (clearpage) {
                  appendix.append("\\clearpage" + GedBookModelHelper.NEWLINE);
                  appendix.newLine();
                }
                appendixBuffer.add(extract);
              }

              /**
               * inserts the screenshot transcriptions into the document. 
               */
              List<Screenshot> screenshots = extract.getScreenshots();
              if (screenshots.size() == 2) {
                String transcription1 = screenshots.get(0).getTranscription();
                String transcription2 = screenshots.get(1).getTranscription();
                if (!GedBookModelHelper.EMPTY.equals(transcription1) && !GedBookModelHelper.EMPTY.equals(transcription2)) {
                  int page1 = screenshots.get(0).getPage();
                  int page2 = screenshots.get(1).getPage();
                  String annotation1 = screenshots.get(0).getAnnotation();
                  String annotation2 = screenshots.get(1).getAnnotation();
                  String signature1 = screenshots.get(0).getSignature();
                  String signature2 = screenshots.get(1).getSignature();
                  String comment1 = screenshots.get(0).getComment();
                  String comment2 = screenshots.get(1).getComment();
  
                  sentence += "\\insertBoxedTranscription" + GedBookModelHelper.NEWLINE
                      + "{fig:" + getReference(citation, 1, page1) + "}" + GedBookModelHelper.NEWLINE
                      + "{fig:" + getReference(citation, 2, page2) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + EncodingUtils.latexEncoding(doc.getPlace().getCommune()) + " - " + abbr + " (" + doc.getDate() + ").}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == annotation1) ? "" : annotation1) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + transcription1 + "}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == signature1) ? "" : signature1) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == comment1) ? "" : comment1) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == annotation2) ? "" : annotation2) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + transcription2 + "}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == signature2) ? "" : signature2) + "}" + GedBookModelHelper.NEWLINE
                      + "{" + ((null == comment2) ? "" : comment2) + "}" + GedBookModelHelper.NEWLINE
                      + "\\FloatBarrier" + GedBookModelHelper.NEWLINE;
                }
              } else {
                int index = 1;
                for (Screenshot screenshot : extract.getScreenshots()) {
                  int page = screenshot.getPage();
                  String transcription = screenshot.getTranscription();
                  String annotation = screenshot.getAnnotation();
                  String comment = screenshot.getComment();
                  String signature = screenshot.getSignature();
                  if (!GedBookModelHelper.EMPTY.equals(transcription)) {
                    sentence += "\\insertBoxedTranscription" + GedBookModelHelper.NEWLINE
                        + "{fig:" + getReference(citation, index, page) + "}" + GedBookModelHelper.NEWLINE
                        + "{}" + GedBookModelHelper.NEWLINE
                        + "{" + EncodingUtils.latexEncoding(doc.getPlace().getCommune()) + " - " + abbr + " (" + doc.getDate() + ").}" + GedBookModelHelper.NEWLINE
                        + "{" + ((null == annotation) ? "" : annotation) + "}" + GedBookModelHelper.NEWLINE
                        + "{" + transcription + "}" + GedBookModelHelper.NEWLINE
                        + "{" + ((null == signature) ? "" : signature) + "}" + GedBookModelHelper.NEWLINE
                        + "{" + ((null == comment) ? "" : comment) + "}" + GedBookModelHelper.NEWLINE
                        + "{}{}{}{}" + GedBookModelHelper.NEWLINE
                        + "\\FloatBarrier" + GedBookModelHelper.NEWLINE;
                  }
                  index++;
                }
              }
            }
          }
        }
      }
    }
    return sentence;
  }
}
