/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator;

import java.util.Comparator;
import java.util.Queue;

import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.SourceCitation;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public interface ISectionProvider {

  /**
   * @param person
   * @param configurationFile
   * @param minipage
   * @param eventQueue
   * @return
   */
  String getBirthSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue);

  /**
   * @param person
   * @param configurationFile
   * @param minipage
   * @return
   */
  String getBatismSentence(Person person, Configuration configurationFile, boolean minipage);

  /**
   * @param person
   * @param father
   * @param mother
   * @param project
   * @return
   */
  String getChildOrderSentence(Person person, Person father, Person mother, Project project);

  /**
   * @param person
   * @param father
   * @param mother
   * @param project
   * @param eventComparator
   * @return
   */
  String getBrothersOrSistersSentence(Person person, Person father, Person mother, Project project, Comparator<SourceCitation> eventComparator);

  /**
   * @param person
   * @param eventComparator
   * @return
   */
  String getProfessionSentence(Person person, Comparator<Event> eventComparator);

  /**
   * @param person
   * @param eventComparator
   * @return
   */
  String getLocationSentence(Person person, Comparator<Event> eventComparator);

  /**
   * @param person
   * @param father
   * @param mother
   * @param partner
   * @param configurationFile
   * @param deathDateComparator
   * @return
   */
  String getParentsDeathSentence(Person person, Person father, Person mother, Person partner, Configuration configurationFile, Comparator<Person> deathDateComparator);

  /**
   * @param person
   * @param configurationFile
   * @param minipage
   * @param eventQueue
   * @return
   */
  String getDeathSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue);

  /**
   * @param person
   * @param configurationFile
   * @param minipage
   * @return
   */
  String getBurialSentence(Person person, Configuration configurationFile, boolean minipage);

  /**
   * @param person
   * @param configurationFile
   * @param minipage
   * @param eventQueue
   * @return
   */
  String getWillSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue);

  /**
   * @param husband
   * @param wife
   * @param configurationFile
   * @param eventQueue
   * @param eventComparator
   * @return
   */
  String getMarriageSentence(Person husband, Person wife, Configuration configurationFile, Queue<Event> eventQueue, Comparator<SourceCitation> eventComparator);

  /**
   * @param person
   * @param family
   * @param configurationFile
   * @param eventComparator
   * @param familyComparator
   * @return
   */
  String getNextMarriageSentence(Person person, Family family, Configuration configurationFile, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator);

  /**
   * @param person
   * @param family
   * @param eventComparator
   * @param familyComparator
   * @return
   */
  String getPreviousMarriageSentence(Person person, Family family, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator);

  /**
   * @param husband
   * @param wife
   * @param configurationFile
   * @param eventQueue
   * @return
   */
  String getBannsSentence(Person husband, Person wife, Configuration configurationFile, Queue<Event> eventQueue);

  /**
   * @param father
   * @param mother
   * @param project
   * @param eventComparator
   * @return
   */
  String getChildrenSentence(Person father, Person mother, Project project, Comparator<SourceCitation> eventComparator);

  /**
   * @param family
   * @return
   */
  String getReferenceSentence(Family family);

  /**
   * @param number
   * @param gender
   * @return
   */
  String getOrdinalIndicator(int number, SexKind gender);

  /**
   * @return
   */
  String getAndCoordinatingConjunction();

  /**
   * @param date
   * @param firstUpper
   * @return
   */
  String getDatePrefix(DateWrapper date, boolean firstUpper);

  /**
   * @param firstUpper
   * @return
   */
  String getUndefinedDate(boolean firstUpper);
}
