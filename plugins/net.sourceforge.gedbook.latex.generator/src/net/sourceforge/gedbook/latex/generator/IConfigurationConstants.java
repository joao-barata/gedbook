/*******************************************************************************
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.latex.generator;

/**
 * This interface defines keywords used in configuration files.
 * 
 * @author Joao Barata
 */
public interface IConfigurationConstants {

  /**
   * 
   */
  static final String GEDBOOK_HOME = "GEDBOOK_HOME";
  static final String GEDBOOK_USER = "GEDBOOK_USER";
  static final String GEDBOOK_ARCHIVE_REPOSITORY = "GEDBOOK_ARCHIVE_REPOSITORY";
  static final String GEDBOOK_IMAGE_REPOSITORY = "GEDBOOK_IMAGE_REPOSITORY";

  /**
   * 
   */
  static final String EXTRACT_CONVERT_BINARY = "extract.convert.binary";
  static final String EXTRACT_SCREENSHOTS = "extract.screenshots";
  static final String EXTRACT_DENSITY = "extract.density";
  static final String EXTRACT_RATIO = "extract.ratio";

  /**
   * 
   */
  static final String FAMILY_GRAPH_MAX_WIDTH = "family.graph.max.width";
  static final String FAMILY_GRAPH_MAX_HEIGHT = "family.graph.max.height";

  /**
   * 
   */
  static final String HANDLE_REPUBLICAN_DATES = "handle.republican.dates";

  /**
   * 
   */
  static final String FAMILY_COUPLE = "family.couple";

  /**
   * 
   */
  static final String DOCUMENT_FIRSTTITLE = "document.firsttitle";

  /**
   * 
   */
  static final String DOCUMENT_BIGTITLE = "document.bigtitle";

  /**
   * 
   */
  static final String DOCUMENT_SUBTITLE = "document.subtitle";
  static final String DOCUMENT_APPENDIX_SUBTITLE = "document.appendix.subtitle";

  /**
   * 
   */
  static final String DOCUMENT_AUTHOR = "document.author";

  /**
   * 
   */
  static final String DOCUMENT_LOGO = "document.logo";

  /**
   * 
   */
  static final String DOCUMENT_EDITOR = "document.editor";

  /**
   * 
   */
  static final String DOCUMENT_COPYRIGHT = "document.copyright";

  /**
   * 
   */
  static final String DOCUMENT_LANGUAGE = "document.language";

  static final String DOCUMENT_APPENDIXNAME = "document.appendixname";
  static final String DOCUMENT_CHAPTERNAME = "document.chaptername";
  static final String DOCUMENT_CHAPTERSUFFIX = "document.chaptersuffix";
  static final String DOCUMENT_SECTIONPREFIX = "document.sectionprefix";
  static final String DOCUMENT_SUBSECTION1TITLE = "document.subsection1title";
  static final String DOCUMENT_SUBSECTION2TITLE = "document.subsection2title";
  static final String DOCUMENT_SUBSECTION3TITLE = "document.subsection3title";
  static final String DOCUMENT_CONTENTSNAME = "document.contentsname";
  static final String DOCUMENT_LISTFIGURENAME = "document.listfigurename";
  static final String DOCUMENT_FIGURENAME = "document.figurename";
  static final String DOCUMENT_GLOSSARYNAME = "document.glossaryname";
  static final String DOCUMENT_ACRONYMNAME = "document.acronymname";
  static final String DOCUMENT_BIBNAME = "document.bibname";
  static final String DOCUMENT_PLACESCHAPTERNAME = "document.placeschaptername";
  static final String DOCUMENT_PUBLICATIONSNAME = "document.publications.name";
  static final String DOCUMENT_PREAMBULENAME = "document.preambule.name";
  static final String DOCUMENT_PREFACENAME = "document.preface.name";
  static final String DOCUMENT_REGISTEREXTRACTNAME = "document.registerextract.name";

  static final String DOCUMENT_NAMEINDEXTITLE = "document.nameindextitle";
  static final String DOCUMENT_PLACEINDEXTITLE = "document.placeindextitle";
  static final String DOCUMENT_ADRESSINDEXTITLE = "document.adressindextitle";
  static final String DOCUMENT_JOBINDEXTITLE = "document.jobindextitle";

  static final String DOCUMENT_SCREENSHOTANNOTATIONLABEL = "document.screenshot.annotation.label";
  static final String DOCUMENT_SCREENSHOTNOTELABEL = "document.screenshot.note.label";
  static final String DOCUMENT_SCREENSHOTSIGNATURELABEL = "document.screenshot.signature.label";
  static final String DOCUMENT_SCREENSHOTTRANSCRIPTIONLABEL = "document.screenshot.transcription.label";

  static final String DOCUMENT_TRANSCRIPTIONORIGINREFERENCELABEL = "document.transcription.origin.reference.label";

  static final String DOCUMENT_TIMELINE_SCALE = "document.timeline.scale";

  /**
   * 
   */
  static final String GENEALOGY_SUFFIX = "-genealogy";
  static final String APPENDIX_SUFFIX = "-appendix";
}
