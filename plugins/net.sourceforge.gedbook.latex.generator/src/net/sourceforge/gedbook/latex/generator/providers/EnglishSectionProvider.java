/**
 * Copyright (c) 2014, 2016 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.latex.generator.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import net.sourceforge.gedbook.latex.generator.ISectionProvider;
import net.sourceforge.gedbook.latex.generator.utils.GedBookGeneratorHelper;
import net.sourceforge.gedbook.model.core.Banns;
import net.sourceforge.gedbook.model.core.Batism;
import net.sourceforge.gedbook.model.core.Birth;
import net.sourceforge.gedbook.model.core.Burial;
import net.sourceforge.gedbook.model.core.DateQualifier;
import net.sourceforge.gedbook.model.core.DateWrapper;
import net.sourceforge.gedbook.model.core.Death;
import net.sourceforge.gedbook.model.core.Event;
import net.sourceforge.gedbook.model.core.Family;
import net.sourceforge.gedbook.model.core.FamilyEvent;
import net.sourceforge.gedbook.model.core.Marriage;
import net.sourceforge.gedbook.model.core.Occupation;
import net.sourceforge.gedbook.model.core.Person;
import net.sourceforge.gedbook.model.core.Project;
import net.sourceforge.gedbook.model.core.SexKind;
import net.sourceforge.gedbook.model.core.SourceCitation;
import net.sourceforge.gedbook.model.helpers.EncodingUtils;
import net.sourceforge.gedbook.model.helpers.GedBookModelHelper;

import org.apache.commons.configuration.Configuration;

/**
 * @author Joao Barata
 */
public class EnglishSectionProvider implements ISectionProvider {

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBirthSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    final String BIRTH_TEXT_TEMPLATE = "\\textbf{%s} was born%s %s"; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person) {
      Birth birth = person.getBirth();

      sentence += String.format(BIRTH_TEXT_TEMPLATE,
          GedBookModelHelper.getPersonFullName(person),
          GedBookModelHelper.getEventNoteFootnote(birth, minipage),
          (null != birth ? GedBookGeneratorHelper.getLongDate(birth.getDate(), configurationFile, true, false, false) : "...")); //$NON-NLS-1$

      String place = GedBookModelHelper.getPlaceName(birth);
      if (!place.isEmpty()) {
        sentence += ", in " + place; //$NON-NLS-1$
      }
      sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;

      eventQueue.add(birth);
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBatismSentence(Person person_p, Configuration configurationFile, boolean minipage) {
    final String BATISM_TEXT_TEMPLATE = "%s was baptized%s %s"; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      Batism batism = person_p.getBatism();
      if (null != batism) {
        sentence += String.format(BATISM_TEXT_TEMPLATE,
          SexKind.MALE.equals(person_p.getSex()) ? "He" : "She", //$NON-NLS-1$ //$NON-NLS-2$
          GedBookModelHelper.getEventNoteFootnote(batism, minipage),
          (null != batism ? GedBookGeneratorHelper.getLongDate(batism.getDate(), configurationFile, true, false, false) : "...")); //$NON-NLS-1$

        String place = GedBookModelHelper.getPlaceName(batism);
        if (!place.isEmpty()) {
          sentence += ", in " + place; //$NON-NLS-1$
        }
        sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getChildOrderSentence(Person person_p, Person father_p, Person mother_p, Project project) {
    final String CHILD_ORDER_TEXT_TEMPLATE = "%s is the %s known child of %s \\textit{[%s%s]} and %s \\textit{[%s%s]}."; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      Birth fatherBirth = (null != father_p) ? father_p.getBirth() : null;
      Death fatherDeath = (null != father_p) ? father_p.getDeath() : null;
      Birth motherBirth = (null != mother_p) ? mother_p.getBirth() : null;
      Death motherDeath = (null != mother_p) ? mother_p.getDeath() : null;
      List<Person> children = GedBookModelHelper.getAllChildren(project, father_p, mother_p);
      int childIndex = children.indexOf(person_p) + 1;

      sentence += String.format(CHILD_ORDER_TEXT_TEMPLATE, SexKind.MALE.equals(person_p.getSex()) ? "He" : "She", //$NON-NLS-1$ //$NON-NLS-2$
          (children.size() <= 1) ? "only" : (childIndex + "$^{" + getOrdinalIndicator(childIndex, person_p.getSex()) + "}$"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          (null != father_p ? GedBookModelHelper.getPersonFullName(father_p) : "..."), //$NON-NLS-1$
          (null != fatherBirth ? GedBookModelHelper.getShortDate(fatherBirth.getDate()) : "..."), //$NON-NLS-1$
          (null != fatherDeath ? " - " + GedBookModelHelper.getShortDate(fatherDeath.getDate()) : GedBookModelHelper.EMPTY), //$NON-NLS-1$
          (null != mother_p ? GedBookModelHelper.getPersonFullName(mother_p) : "..."), //$NON-NLS-1$
          (null != motherBirth ? GedBookModelHelper.getShortDate(motherBirth.getDate()) : "..."), //$NON-NLS-1$
          (null != motherDeath ? " - " + GedBookModelHelper.getShortDate(motherDeath.getDate()) : GedBookModelHelper.EMPTY)) + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getOrdinalIndicator(int number, SexKind gender) {
    if (number == 1) {
      return "st"; //$NON-NLS-1$
    } else if (number == 2) {
      return "nd"; //$NON-NLS-1$
    } else if (number == 3) {
      return "rd"; //$NON-NLS-1$
    }
    return "th"; //$NON-NLS-1$
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBrothersOrSistersSentence(Person person_p, Person father_p, Person mother_p, Project project, Comparator<SourceCitation> eventComparator) {
    // final String BROTHERSANDSISTERS_TEXT_TEMPLATE = "%s %sa %s fr\\`{e}re%s et %s soeur%s";

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      List<Person> children = GedBookModelHelper.getAllChildren(project, father_p, mother_p);
      children.remove(person_p);
      int childrenSize = children.size();
      int brothers = GedBookModelHelper.countMales(children);
      int sisters = GedBookModelHelper.countFemales(children);

      sentence += (SexKind.MALE.equals(person_p.getSex()) ? "He" : "She") + " has "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      if (brothers == 0 && sisters == 0) {
        sentence += "no brother nor sister known"; //$NON-NLS-1$
      } else if (brothers > 0 && sisters == 0) {
        sentence += brothers + " brother" + ((brothers > 1) ? "s" : GedBookModelHelper.EMPTY) //$NON-NLS-1$ //$NON-NLS-2$
          + " known"; //$NON-NLS-1$
      } else if (brothers == 0 && sisters > 0) {
        sentence += sisters + " sister" + ((sisters > 1) ? "s" : GedBookModelHelper.EMPTY) //$NON-NLS-1$ //$NON-NLS-2$
          + " known"; //$NON-NLS-1$
      } else if (brothers > 0 && sisters > 0) {
        sentence += brothers + " brother" + ((brothers > 1) ? "s" : GedBookModelHelper.EMPTY) //$NON-NLS-1$ //$NON-NLS-2$
          + " and " + sisters + " sister" + ((sisters > 1) ? "s" : GedBookModelHelper.EMPTY) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          + " known"; //$NON-NLS-1$
      }
      sentence += (childrenSize > 0) ? " : " : GedBookModelHelper.EMPTY; //$NON-NLS-1$
      for (int i = 0; i < childrenSize; i++) {
        Person child = children.get(i);
        Birth childBirth = child.getBirth();
        Death childDeath = child.getDeath();
        sentence += GedBookModelHelper.getPersonFullName(child);
        if (childBirth != null || childDeath != null) {
          sentence += " \\textit{[" //$NON-NLS-1$
            + (null == childBirth ? "..." : GedBookModelHelper.getShortDate(childBirth.getDate()) + GedBookModelHelper.getEventSourceFootnote(childBirth, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
            + (null == childDeath ? GedBookModelHelper.EMPTY : " - " + GedBookModelHelper.getShortDate(childDeath.getDate()) + GedBookModelHelper.getEventSourceFootnote(childDeath, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
            + "]}"; //$NON-NLS-1$
        }
        if (i == childrenSize - 2) {
          sentence += " and "; //$NON-NLS-1$
        } else if (i < childrenSize - 2) {
          sentence += ", "; //$NON-NLS-1$
        }
      }
      sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getProfessionSentence(Person person_p, Comparator<Event> eventComparator) {
    final String PROFESSION_TEXT_TEMPLATE = "%s exercised the profession%s %s."; //$NON-NLS-1$
    String jobs = GedBookModelHelper.EMPTY;
    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      List<Occupation> occupations = person_p.getOccupations();
      Map<String, Collection<Occupation>> map = GedBookModelHelper.compressOccupations(occupations);
      Set<String> keys = map.keySet();
      int keyCounter = 0;
      for (String key : keys) {
        Collection<Occupation> occupation = map.get(key);
        if (null != occupation) {
          if (keyCounter != 0) {
            if (keyCounter == keys.size() - 1) {
              jobs += " and "; //$NON-NLS-1$
            } else {
              jobs += ", "; //$NON-NLS-1$
            }
          }
          jobs += "of <<" + EncodingUtils.latexEncoding(key) + GedBookModelHelper.getMultipleEventFootnote(occupation, getAndCoordinatingConjunction(), eventComparator) + "\\index[jobs]{" + EncodingUtils.latexEncoding(key) + "}>>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
      }
      if (!jobs.isEmpty()) {
        sentence += String.format(PROFESSION_TEXT_TEMPLATE,
          SexKind.MALE.equals(person_p.getSex()) ? "He" : "She",
          map.keySet().size() > 1 ? "s" : GedBookModelHelper.EMPTY,
          jobs
        ) + GedBookModelHelper.NEWLINE; //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getLocationSentence(Person person, Comparator<Event> eventComparator) {
    String sentence = GedBookModelHelper.EMPTY;

    // TODO to be implemented
    
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getParentsDeathSentence(Person person_p, Person father_p, Person mother_p, Person partner_p, Configuration configurationFile, Comparator<Person> deathDateComparator) {
    final String PARENT_DEATH_TEMPLATE = "%s, %s \\textit{[%s]}, died %s, in %s, when %s was %s years."; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      Birth personBirth = person_p.getBirth();

      for (Person parent : GedBookModelHelper.getParentsDeathList(person_p, father_p, mother_p, partner_p, deathDateComparator)) {
        Birth parentBirth = parent.getBirth();
        Death parentDeath = parent.getDeath();
        sentence += String.format(PARENT_DEATH_TEMPLATE, SexKind.MALE.equals(parent.getSex()) ? (parent.equals(partner_p) ? "Her husband" : "Her father") : (parent.equals(partner_p) ? "His wife" : "His mother"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
          GedBookModelHelper.getPersonFullName(parent), (null != parentBirth ? GedBookModelHelper.getShortDate(parentBirth.getDate()) : "..."), //$NON-NLS-1$
          (null != parentDeath ? GedBookGeneratorHelper.getLongDate(parentDeath.getDate(), configurationFile, true, false, false) : "..."), //$NON-NLS-1$
          GedBookModelHelper.getPlaceName(parentDeath), SexKind.MALE.equals(person_p.getSex()) ? "he" : "she", //$NON-NLS-1$ //$NON-NLS-2$
          (null != personBirth && null != parentDeath ? Integer.valueOf(GedBookModelHelper.computeAge(personBirth.getDate(), parentDeath.getDate())) : "...")) + GedBookModelHelper.NEWLINE; //$NON-NLS-1$
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDeathSentence(Person person_p, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    final String DEATH_TEXT_TEMPLATE1 = "%s died%s %s"; //$NON-NLS-1$
    final String DEATH_TEXT_TEMPLATE2 = ", at the age of %s years"; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person_p) {
      Death death = person_p.getDeath();
      if (null != death) {
        sentence += String.format(DEATH_TEXT_TEMPLATE1,
          SexKind.MALE.equals(person_p.getSex()) ? "He" : "She", //$NON-NLS-1$ //$NON-NLS-2$
          GedBookModelHelper.getEventNoteFootnote(death, minipage),
          (null != death ? GedBookGeneratorHelper.getLongDate(death.getDate(), configurationFile, true, false, false) : "...")); //$NON-NLS-1$

        String place = GedBookModelHelper.getShortPlace(death);
        if (!place.isEmpty()) {
          sentence += ", in " + GedBookModelHelper.getPlaceName(death); //$NON-NLS-1$
        }

        Birth birth = person_p.getBirth();
        if (null != birth) {
          sentence += String.format(DEATH_TEXT_TEMPLATE2, (null != birth ? Integer.valueOf(GedBookModelHelper.computeAge(birth.getDate(), death.getDate())) : "...")); //$NON-NLS-1$
        }
        sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;

        eventQueue.add(death);
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBurialSentence(Person person, Configuration configurationFile, boolean minipage) {
    final String BURIAL_TEXT_TEMPLATE = "%s was buried%s %s, in %s."; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != person) {
      Burial burial = person.getBurial();
      if (null != burial) {
        sentence += String.format(BURIAL_TEXT_TEMPLATE,
          SexKind.MALE.equals(person.getSex()) ? "He" : "She", //$NON-NLS-1$ //$NON-NLS-2$
          GedBookModelHelper.getEventNoteFootnote(burial, minipage),
          GedBookGeneratorHelper.getLongDate(burial.getDate(), configurationFile, true, false, false),
          GedBookModelHelper.getPlaceName(burial)) + GedBookModelHelper.NEWLINE;
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getWillSentence(Person person, Configuration configurationFile, boolean minipage, Queue<Event> eventQueue) {
    return GedBookModelHelper.EMPTY;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getMarriageSentence(Person husband, Person wife, Configuration configurationFile, Queue<Event> eventQueue, Comparator<SourceCitation> eventComparator) {
    // final String MARRIAGE_TEXT_TEMPLATE =
    //    "%s, %s \\textit{[%s]}, \\^{a}g\\'{e} de %s ans, \\'{e}pousa %s \\textit{[%s]}, \\^{a}g\\'{e}e de %s ans, \\`{a} %s."; //$NON-NLS-1$

    String sentence = GedBookModelHelper.EMPTY;
    if (null != husband && null != wife) {
      Family family = GedBookModelHelper.getFamily(husband, wife);
      for (FamilyEvent evt : family.getEvents()) {
        if (evt instanceof Marriage) {
          Birth husbandBirth = husband.getBirth();
          DateWrapper husbandBirthDate = (null != husbandBirth) ? husbandBirth.getDate() : null;
          Date hd = (null != husbandBirthDate) ? husbandBirthDate.getDate() : null;
          Death husbandDeath = husband.getDeath();
          DateWrapper husbandDeathDate = (null != husbandDeath) ? husbandDeath.getDate() : null;

          Birth wifeBirth = wife.getBirth();
          DateWrapper wifeBirthDate = (null != wifeBirth) ? wifeBirth.getDate() : null;
          Date wd = (null != wifeBirthDate) ? wifeBirthDate.getDate() : null;
          DateWrapper marriageDate = evt.getDate();
          Date md = (null != marriageDate) ? marriageDate.getDate() : null;
          Death wifeDeath = wife.getDeath();
          DateWrapper wifeDeathDate = (null != wifeDeath) ? wifeDeath.getDate() : null;

          if (null != md) {
            sentence += GedBookGeneratorHelper.getLongDate(marriageDate, configurationFile, false, true, true) + ", "; //$NON-NLS-1$
          }
          sentence += GedBookModelHelper.getPersonFullName(husband);
          sentence += " \\textit{[" //$NON-NLS-1$
              + (null == husbandBirth ? "..." : GedBookModelHelper.getShortDate(husbandBirthDate) + GedBookModelHelper.getEventSourceFootnote(husbandBirth, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
              + (null == husbandDeath ? GedBookModelHelper.EMPTY : " - " + GedBookModelHelper.getShortDate(husbandDeathDate) + GedBookModelHelper.getEventSourceFootnote(husbandDeath, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
              + "]}"; //$NON-NLS-1$
          sentence += (hd != null && md != null) ? ", aged " + Integer.valueOf(GedBookModelHelper.computeAge(husbandBirth.getDate(), marriageDate)) + " years," : GedBookModelHelper.EMPTY; //$NON-NLS-1$ //$NON-NLS-2$
          sentence += " married " + GedBookModelHelper.getPersonFullName(wife); //$NON-NLS-1$
          sentence += " \\textit{[" //$NON-NLS-1$
              + (null == wifeBirth ? "..." : GedBookModelHelper.getShortDate(wifeBirthDate) + GedBookModelHelper.getEventSourceFootnote(wifeBirth, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
              + (null == wifeDeath ? GedBookModelHelper.EMPTY : " - " + GedBookModelHelper.getShortDate(wifeDeathDate) + GedBookModelHelper.getEventSourceFootnote(wifeDeath, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
              + "]}"; //$NON-NLS-1$
          sentence += (wd != null && md != null) ? ", aged " + Integer.valueOf(GedBookModelHelper.computeAge(wifeBirth.getDate(), marriageDate)) + " years" : GedBookModelHelper.EMPTY; //$NON-NLS-1$ //$NON-NLS-2$
  
          String place = GedBookModelHelper.getShortPlace(evt);
          if (!place.isEmpty()) {
            sentence += ", in " + GedBookModelHelper.getPlaceName(evt); //$NON-NLS-1$
          }
          sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;
  
          eventQueue.add(evt);
        }
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getBannsSentence(Person husband_p, Person wife_p, Configuration configurationFile, Queue<Event> eventQueue) {
    String sentence = GedBookModelHelper.EMPTY;
    if (null != husband_p && null != wife_p) {
      Family family = GedBookModelHelper.getFamily(husband_p, wife_p);
      List<Banns> banns = new ArrayList<Banns>();
      for (FamilyEvent evt : family.getEvents()) {
        if (evt instanceof Banns) {
          banns.add((Banns) evt);
        }
      }
      int nbBanns = banns.size();
      if (nbBanns != 0) {
        sentence += "The banns of marriage took place "; //$NON-NLS-1$
        for (int i = 0; i < nbBanns; i++) {
          Banns bann = banns.get(i);
          sentence += GedBookGeneratorHelper.getLongDate(bann.getDate(), configurationFile, true, false, false);

          String place = GedBookModelHelper.getShortPlace(bann);
          if (!place.isEmpty()) {
            sentence += ", in " + GedBookModelHelper.getPlaceName(bann); //$NON-NLS-1$
          }

          if (i == nbBanns - 2) {
            sentence += " and "; //$NON-NLS-1$
          } else if (i < nbBanns - 2) {
            sentence += ", "; //$NON-NLS-1$
          }

          eventQueue.add(bann);
        }
        sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;
      }
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getNextMarriageSentence(Person person, Family family, Configuration configurationFile, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator) {
    String sentence = GedBookModelHelper.EMPTY;

    // TODO to be implemented
    
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getPreviousMarriageSentence(Person person, Family family, Comparator<SourceCitation> eventComparator, Comparator<Family> familyComparator) {
    String sentence = GedBookModelHelper.EMPTY;

    // TODO to be implemented
    
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getChildrenSentence(Person father_p, Person mother_p, Project project, Comparator<SourceCitation> eventComparator) {
    String sentence = GedBookModelHelper.EMPTY;
    if (null != father_p && null != mother_p) {
      List<Person> children = GedBookModelHelper.getAllChildren(project, father_p, mother_p);
      int nbChildren = children.size();

      if (nbChildren == 0) {
        sentence += "They had no known children"; //$NON-NLS-1$
      } else {
        sentence += "They had " + nbChildren + " known children : "; //$NON-NLS-1$ //$NON-NLS-2$

        for (int i = 0; i < nbChildren; i++) {
          Person child = children.get(i);
          Birth childBirth = child.getBirth();
          Death childDeath = child.getDeath();
          sentence += GedBookModelHelper.getPersonFullName(child) + " \\textit{[" //$NON-NLS-1$
            + (null == childBirth ? "..." : GedBookModelHelper.getShortDate(childBirth.getDate()) + GedBookModelHelper.getEventSourceFootnote(childBirth, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
            + (null == childDeath ? GedBookModelHelper.EMPTY : " - " + GedBookModelHelper.getShortDate(childDeath.getDate()) + GedBookModelHelper.getEventSourceFootnote(childDeath, getAndCoordinatingConjunction(), eventComparator)) //$NON-NLS-1$
            + "]}"; //$NON-NLS-1$
          if (i == nbChildren - 2) {
            sentence += " and "; //$NON-NLS-1$
          } else if (i < nbChildren - 2) {
            sentence += ", "; //$NON-NLS-1$
          }
        }
      }
      sentence += GedBookModelHelper.DOT + GedBookModelHelper.NEWLINE;
    }
    return sentence;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getReferenceSentence(Family family) {
    return "See \\S \\ref{" + family.getId() + "} \\nameref{" + family.getId() + "}, page \\pageref{" + family.getId() + "}.";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getAndCoordinatingConjunction() {
    return "and";
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDatePrefix(DateWrapper date, boolean firstUpper) {
    String prefix = GedBookModelHelper.EMPTY;

    if (DateQualifier.BEFORE.equals(date.getQualifier())) {
      prefix += "before "; //$NON-NLS-1$
    } else if (DateQualifier.AFTER.equals(date.getQualifier())) {
      prefix += "after "; //$NON-NLS-1$
    }

    if (!date.isDay() && !date.isMonth()) {
      prefix += "the year "; //$NON-NLS-1$
    } else if (!date.isDay()) {
      prefix += "the month of "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    } else {
      prefix += "the "; //$NON-NLS-1$
    }

    return EncodingUtils.latexEncoding(firstUpper ? Character.toString(prefix.charAt(0)).toUpperCase() + prefix.substring(1) : prefix);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUndefinedDate(boolean firstUpper) {
    return (firstUpper ? "O" : "o") + "n ..."; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  }
}
