/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.editors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import de.vonloesch.pdf4eclipse.Messages;
import de.vonloesch.pdf4eclipse.PDFPageViewer;
import de.vonloesch.pdf4eclipse.model.IPDFFile;
import de.vonloesch.pdf4eclipse.model.IPDFPage;
import de.vonloesch.pdf4eclipse.model.PDFFactory;

/**
 * This class is a simplified version of the pdf4eclipse's PDFEditor class
 *
 * @see de.vonloesch.pdf4eclipse.editors.PDFEditor
 * @author Joao Barata
 */
public abstract class PDFPageEditor extends EditorPart {

	private static final float MOUSE_ZOOMFACTOR = 0.2f;

	protected PDFPageViewer pv;
	private IPDFFile f;
	private ScrolledComposite sc;
	private Cursor cursorHand;
	private Cursor cursorArrow;

	public PDFPageEditor() {
		super();
	}

	/**
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();

		if (sc != null) {
		  sc.dispose();
		}
		if (pv != null) {
		  pv.dispose();
		}
		if (cursorArrow != null) {
		  cursorArrow.dispose();
		}
		if (cursorHand != null) {
		  cursorHand.dispose();
		}

		if (f != null) {
		  f.close();
		}
		f = null;
		pv = null;
	}

	public abstract int getPdfPage();
	public abstract File getPdfFile() throws PartInitException;
  public abstract PDFPageViewer getPdfPageViewer(Composite parent);

  /**
   * @see org.eclipse.ui.part.EditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
   */
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(input.getName());
		readPdfFile();
	}

	/**
	 * @throws PartInitException
	 */
	public void readPdfFile() throws PartInitException{
		f = null;
		try {
			f = PDFFactory.openPDFFile(getPdfFile(), PDFFactory.STRATEGY_SUN_JPEDAL);
		} catch (FileNotFoundException fnfe) {
			throw new PartInitException(Messages.PDFEditor_ErrorMsg3, fnfe);
		} catch (IOException ioe) {
			throw new PartInitException(Messages.PDFEditor_ErrorMsg4, ioe);
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
	}


	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(final Composite parent) {
		cursorHand = new Cursor(Display.getDefault(), SWT.CURSOR_HAND);
		cursorArrow = new Cursor(Display.getDefault(), SWT.CURSOR_ARROW);

		parent.setLayout(new FillLayout());
		sc = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		pv = getPdfPageViewer(sc);
		sc.setContent(pv);

		// Speed up scrolling when using a wheel mouse
		ScrollBar vBar = sc.getVerticalBar();
		vBar.setIncrement(10);

		//Add zooming by using mouse wheel and ctrl key (contributed by MeisterYeti)
		pv.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseScrolled(MouseEvent e) {
				if((e.stateMask & SWT.CTRL) > 0) {
					Point o = getOrigin();
					Point oldSize = pv.getSize();
					pv.setZoomFactor(Math.max(pv.getZoomFactor() + MOUSE_ZOOMFACTOR*(float)e.count/10, 0));
					int mx = Math.round((float)pv.getSize().x * ((float)e.x / oldSize.x)) - (e.x-o.x);
					int my = Math.round((float)pv.getSize().y * ((float)e.y / oldSize.y)) - (e.y-o.y);
					setOrigin(new Point(mx,my));
					return;
				}
			}
		});

		//Add panning of page using middle mouse button (contributed by MeisterYeti)
		pv.addMouseListener(new MouseListener() {
			Point start;
			MouseMoveListener mml = new MouseMoveListener() {
				@Override
				public void mouseMove(MouseEvent e) {
					if((e.stateMask & SWT.BUTTON2) == 0) {
						pv.removeMouseMoveListener(this);
						pv.setCursor(cursorArrow);
						return;
					}
					Point o = sc.getOrigin();
					sc.setOrigin(o.x-(e.x-start.x), o.y-(e.y-start.y));
				}
			};

			@Override
			public void mouseUp(MouseEvent e) {
				if (e.button != 2) {
					return;
				}
				pv.removeMouseMoveListener(mml);
				pv.setCursor(cursorArrow);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (e.button != 2) {
					return;
				}
				start = new Point(e.x, e.y);
				pv.addMouseMoveListener(mml);
				pv.setCursor(cursorHand);
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			  //
			}
		});		

		if (f != null) {
			showPage(getPdfPage());
		}
	}
	
	private void showPage(int pageNr) {
		if (pageNr < 1) {
		  pageNr = 1;
		}
		if (pageNr > f.getNumPages()) {
		  pageNr = f.getNumPages();
		}
		IPDFPage page = f.getPage(pageNr);
		pv.showPage(page);
	}

	@Override
	public void setFocus() {
		sc.setFocus();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class required) {
		return super.getAdapter(required);
	}

	private Point getOrigin() {
		if (!sc.isDisposed()) {
		  return sc.getOrigin();
		}
		return null;
	}

	private void setOrigin(Point p) {
		sc.setRedraw(false);
		if (p != null) {
		  sc.setOrigin(p);
		}
		sc.setRedraw(true);
	}

}
