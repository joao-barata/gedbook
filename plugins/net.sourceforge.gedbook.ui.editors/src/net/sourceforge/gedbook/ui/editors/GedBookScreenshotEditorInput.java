/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.editors;

import java.lang.ref.WeakReference;

import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Screenshot;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.views.properties.IPropertySheetPage;

/**
 * @author Joao Barata
 */
public class GedBookScreenshotEditorInput implements IEditorInput {

  private WeakReference<Screenshot> _input;

  /**
   * Constructor.
   *
   * @param extract
   */
  public GedBookScreenshotEditorInput(Screenshot screenshot) {
    _input = new WeakReference<Screenshot>(screenshot);
  }

  /**
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof GedBookScreenshotEditorInput) {
      return _input.get().equals(((GedBookScreenshotEditorInput) obj).getScreenshot());
    }
    return false;
  }

  /**
   * @return
   */
  public Screenshot getScreenshot() {
    if (null != _input) {
      return _input.get();
    }
    return null;
  }

  /**
   * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
   */
  @SuppressWarnings("rawtypes")
  public Object getAdapter(Class adapter) {
    if (IPropertySheetPage.class.equals(adapter)) {
      return null != _input ? _input.get() : null;
    }
    return null;
  }

  /**
   * @see org.eclipse.ui.IEditorInput#exists()
   */
  public boolean exists() {
    return false;
  }

  /**
   * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
   */
  public ImageDescriptor getImageDescriptor() {
    return null;
  }

  /**
   * @see org.eclipse.ui.IEditorInput#getName()
   */
  public String getName() {
    if (null != _input) {
      return ((Extract) _input.get().eContainer()).getName();
    }
    return null;
  }

  /**
   * @see org.eclipse.ui.IEditorInput#getToolTipText()
   */
  public String getToolTipText() {
    if (null != _input) {
      return ((Extract) _input.get().eContainer()).getName();
    }
    return null;
  }

  /**
   * @see org.eclipse.ui.IEditorInput#getPersistable()
   */
  public IPersistableElement getPersistable() {
    return null;
  }
}
