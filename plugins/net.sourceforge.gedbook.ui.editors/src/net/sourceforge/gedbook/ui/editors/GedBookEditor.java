/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.editors;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.sourceforge.gedbook.latex.generator.IConfigurationConstants;
import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.SourceFile;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.NotificationFilter;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.vonloesch.pdf4eclipse.PDFPageViewer;

/**
 * @author Joao Barata
 */
public class GedBookEditor extends PDFPageEditor implements ResourceSetListener, ITabbedPropertySheetPageContributor, ISelectionProvider {

	public static final String ID = "net.sourceforge.gedbook.ui.editor"; //$NON-NLS-1$

  /**
   * Constructor.
   */
	public GedBookEditor() {
		super();
	}

	/**
	 * @see net.sourceforge.gedbook.ui.editors.PDFPageEditor#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
	  super.init(site, input);

	  setSite(site);
    getSite().setSelectionProvider(this);
    
    setInputWithNotify(input);
    setPartName(input.getName());

    ((TransactionalEditingDomain) TransactionUtil.getEditingDomain(((GedBookScreenshotEditorInput) input).getScreenshot())).addResourceSetListener(this);

    readPdfFile();
  }

  @Override
  public void dispose() {
    ((TransactionalEditingDomain) TransactionUtil.getEditingDomain(((GedBookScreenshotEditorInput) getEditorInput()).getScreenshot())).removeResourceSetListener(this);
    super.dispose();
  }

  /**
   * @see de.vonloesch.pdf4eclipse.editors.PDFEditor#getPdfPage()
   */
  @Override
  public int getPdfPage() {
    IEditorInput input = getEditorInput();
    if (input instanceof GedBookScreenshotEditorInput) {
      Screenshot screenshot = ((GedBookScreenshotEditorInput) input).getScreenshot();
      return screenshot.getPage();
    }
    return -1;
	}

	/**
	 * @see de.vonloesch.pdf4eclipse.editors.PDFEditor#getPdfFile()
	 */
  @Override
	public File getPdfFile() {
		IEditorInput input = getEditorInput();
		if (input instanceof GedBookScreenshotEditorInput) {
	    Screenshot screenshot = ((GedBookScreenshotEditorInput) input).getScreenshot();
	    Extract extract = (Extract) screenshot.eContainer();
	    SourceFile src = (SourceFile) extract.eContainer();
	    String path = System.getenv(IConfigurationConstants.GEDBOOK_ARCHIVE_REPOSITORY) + src.getPath();

	    return new File(path);
		}
		return null;
	}

  /**
   * 
   */
  public Rectangle getPageSelection(float zoomFactor, float height) {
    IEditorInput input = getEditorInput();
    if (input instanceof GedBookScreenshotEditorInput) {
      Screenshot screenshot = ((GedBookScreenshotEditorInput) input).getScreenshot();
      return new Rectangle(
        (int) (screenshot.getXLeftBottom() * zoomFactor),
        (int) ((height - screenshot.getYRightTop()) * zoomFactor),
        (int) ((screenshot.getXRightTop() - screenshot.getXLeftBottom()) * zoomFactor),
        (int) ((screenshot.getYRightTop() - screenshot.getYLeftBottom()) * zoomFactor)); 
    }
    return null;
  }

  @Override
  public PDFPageViewer getPdfPageViewer(final Composite parent) {
    return new PDFPageViewer(parent, null) {
      @Override
      public void paintControl(PaintEvent event) {
        super.paintControl(event);
        
        event.gc.setForeground(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
        event.gc.drawRectangle(getPageSelection(getZoomFactor(), getPage().getHeight()));
      }
    };
  }

  /**
   * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
   */
  @Override
  public void setFocus() {
    //super.setFocus();
  }

  /**
   * @see de.vonloesch.pdf4eclipse.editors.PDFEditor#getAdapter(java.lang.Class)
   */
	@SuppressWarnings("rawtypes")
  @Override
	public Object getAdapter(Class adapter) {
    if (adapter == IPropertySheetPage.class) {
      return new TabbedPropertySheetPage(this);
    } else if (adapter == IPropertySource.class) {
      return ((GedBookScreenshotEditorInput) getEditorInput()).getScreenshot();
    } 
		return super.getAdapter(adapter);
	}

  /**
   * {@inheritDoc}
   */
  @Override
  public String getContributorId() {
    return GedBookUIEditorsPlugin.PLUGIN_ID;
  }

  List<ISelectionChangedListener> selectionChangedListeners = new ArrayList<ISelectionChangedListener>();

  /**
   * {@inheritDoc}
   */
  @Override
  public void addSelectionChangedListener(ISelectionChangedListener listener) {
    selectionChangedListeners.add(listener);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ISelection getSelection() {
    return new StructuredSelection(((GedBookScreenshotEditorInput) getEditorInput()).getScreenshot());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeSelectionChangedListener(ISelectionChangedListener listener) {
    selectionChangedListeners.remove(listener);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setSelection(ISelection selection) {
    for (ISelectionChangedListener listener : selectionChangedListeners) {
      listener.selectionChanged(new SelectionChangedEvent(this, selection));
    }
  }

  @Override
  public NotificationFilter getFilter() {
    return null;
  }

  @Override
  public Command transactionAboutToCommit(ResourceSetChangeEvent event) throws RollbackException {
    return null;
  }

  @Override
  public void resourceSetChanged(ResourceSetChangeEvent event) {
    if (pv != null) {
      pv.redraw();
    }
  }

  @Override
  public boolean isAggregatePrecommitListener() {
    return false;
  }

  @Override
  public boolean isPrecommitOnly() {
    return false;
  }

  @Override
  public boolean isPostcommitOnly() {
    return false;
  }
}
