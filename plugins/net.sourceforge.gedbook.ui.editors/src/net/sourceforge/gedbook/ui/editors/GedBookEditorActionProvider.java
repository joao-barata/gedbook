/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.editors;

import net.sourceforge.gedbook.model.data.Extract;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.model.data.SourceFile;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionContext;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonMenuConstants;

/**
 * @author Joao Barata
 */
public class GedBookEditorActionProvider extends CommonActionProvider {

  /**
   *
   */
  public GedBookEditorActionProvider() {
    //
  }

  /**
   * @see org.eclipse.ui.actions.ActionGroup#fillContextMenu(org.eclipse.jface.action.IMenuManager)
   */
  @Override
  public void fillContextMenu(IMenuManager menu) {
    menu.insertAfter(ICommonMenuConstants.GROUP_OPEN, createContributionItem());
  }

  /**
   *
   */
  protected IContributionItem createContributionItem() {
    Action action = new Action("Open Screenshot Editor") {
      @Override
      public void run() {
        ActionContext ctx = getContext();
        if (null != ctx) {
          ISelection selection = ctx.getSelection();
          if (selection instanceof IStructuredSelection) {
            Object obj = ((IStructuredSelection) selection).getFirstElement();
            if (obj instanceof Screenshot) {
              String editorId = null;
              IEditorInput editorInput = null;

              Screenshot screenshot = (Screenshot) obj;
              Extract extract = (Extract) screenshot.eContainer();
              SourceFile src = (SourceFile) extract.eContainer();
              String path = src.getPath();
              if (path.endsWith(".pdf")) {
                editorId = GedBookEditor.ID;
                editorInput = new GedBookScreenshotEditorInput(screenshot);
              } else if (path.endsWith(".gif") || path.endsWith(".jpg") || path.endsWith(".png")) {
                
              }

              if (editorInput != null && editorId != null) {
                IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
                IWorkbenchPage page = window.getActivePage();
                try {
                  IEditorPart part = page.openEditor(editorInput, editorId, true);
                } catch (PartInitException e) {
                  e.printStackTrace();
                }
              }
            }
          }
        }
      }
    };

    return new ActionContributionItem(action);
  }
}
