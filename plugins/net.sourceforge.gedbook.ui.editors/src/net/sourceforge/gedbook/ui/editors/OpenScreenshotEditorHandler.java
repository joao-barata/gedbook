/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.editors;

import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.ui.views.navigator.GedBookCommonNavigator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * @author Joao Barata
 */
public class OpenScreenshotEditorHandler extends AbstractHandler {

  /**
   * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
   */
  @Override
  public Object execute(ExecutionEvent event) throws ExecutionException {
    IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
    IWorkbenchPage page = window.getActivePage();
    GedBookCommonNavigator view = (GedBookCommonNavigator) page.findView(GedBookCommonNavigator.ID);
    ISelection selection = view.getSite().getSelectionProvider().getSelection();
    if (selection instanceof IStructuredSelection) {
      Object obj = ((IStructuredSelection) selection).getFirstElement();
      if (obj instanceof Screenshot) {
        GedBookScreenshotEditorInput input = new GedBookScreenshotEditorInput((Screenshot) obj);
        try {
          page.openEditor(input, GedBookEditor.ID);
        } catch (PartInitException e) {
          throw new RuntimeException(e);
        }
      }
    }
    return null;
  }
}