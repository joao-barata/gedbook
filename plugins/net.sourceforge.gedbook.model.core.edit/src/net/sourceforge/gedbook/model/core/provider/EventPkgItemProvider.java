/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.model.core.provider;


import java.util.Collection;
import java.util.List;

import net.sourceforge.gedbook.model.core.CoreFactory;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.core.EventPkg;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.sourceforge.gedbook.model.core.EventPkg} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EventPkgItemProvider 
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource {
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventPkgItemProvider(AdapterFactory adapterFactory) {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
    if (itemPropertyDescriptors == null) {
      super.getPropertyDescriptors(object);

    }
    return itemPropertyDescriptors;
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
    if (childrenFeatures == null) {
      super.getChildrenFeatures(object);
      childrenFeatures.add(CorePackage.Literals.EVENT_PKG__OWNED_EVENTS);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child) {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns EventPkg.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object) {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/EventPkg")); //$NON-NLS-1$
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object) {
    return getString("_UI_EventPkg_type"); //$NON-NLS-1$
  }
  

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification) {
    updateChildren(notification);

    switch (notification.getFeatureID(EventPkg.class)) {
      case CorePackage.EVENT_PKG__OWNED_EVENTS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createEvent()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createPersonEvent()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createFamilyEvent()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createBirth()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createBatism()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createDeath()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createBurial()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createWill()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createOccupation()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createTitle()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createNationality()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createSocialSecurityNumber()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createProperty()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createResidence()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createOrdination()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createRetirement()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createCensus()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createMarriage()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createBanns()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createDivorce()));

    newChildDescriptors.add
      (createChildParameter
        (CorePackage.Literals.EVENT_PKG__OWNED_EVENTS,
         CoreFactory.eINSTANCE.createAnulment()));
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator() {
    return ((IChildCreationExtender)adapterFactory).getResourceLocator();
  }

}
