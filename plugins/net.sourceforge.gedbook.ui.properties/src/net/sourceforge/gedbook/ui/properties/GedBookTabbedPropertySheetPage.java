/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.properties;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * @author Joao Barata
 */
public class GedBookTabbedPropertySheetPage extends TabbedPropertySheetPage {

  public static final String GEDBOOK_EDITOR_CONTRIBUTOR_ID = "net.sourceforge.gedbook.ui.properties.editor"; //$NON-NLS-1$

  /**
   * @param editor_p
   */
  public GedBookTabbedPropertySheetPage(ITabbedPropertySheetPageContributor editor_p) {
    super(editor_p);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void init(IPageSite pageSite) {
    super.init(pageSite);
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage#dispose()
   */
  @Override
  public void dispose() {
    super.dispose();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void labelProviderChanged(LabelProviderChangedEvent event_p) {
    if (!getControl().isDisposed()) {
      super.labelProviderChanged(event_p);
    }
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage#refresh()
   */
  @Override
  public void refresh() {
    if (getCurrentTab() != null) {
      super.refresh();
    }
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage#getTitleText(ISelection)
   */
  public String getTitleText(ISelection selection) {
    return super.getTitleText(selection);
  }
}
