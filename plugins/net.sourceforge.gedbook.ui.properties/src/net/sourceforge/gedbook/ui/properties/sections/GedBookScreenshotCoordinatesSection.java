/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.properties.sections;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * @author Joao Barata
 */
public class GedBookScreenshotCoordinatesSection extends AbstractPropertySection implements IFilter {

  private static final int XVALUE = 0;
  private static final int YVALUE = 1;
  
  private Spinner[] leftBottomSpinnerFields = new Spinner[2];
  private Spinner[] rightTopSpinnerFields = new Spinner[2];

  private Screenshot element;

  private void createCoordinateWidget(Composite form, String labelText, Spinner[] spinnerFields, final EStructuralFeature[] features) {

    Group grp = getWidgetFactory().createGroup(form, labelText);
    grp.setBackground(form.getDisplay().getSystemColor(SWT.COLOR_WHITE));
    grp.setLayout(new GridLayout(4, false));
    grp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

    getWidgetFactory().createLabel(grp, "X:", SWT.NONE);
    spinnerFields[XVALUE] = new Spinner(grp, SWT.BORDER);
    spinnerFields[XVALUE].setMinimum(0);
    spinnerFields[XVALUE].setMaximum(3000);
    spinnerFields[XVALUE].setIncrement(5);
    spinnerFields[XVALUE].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
    spinnerFields[XVALUE].addModifyListener(new ModifyListener() {
      @Override
      public void modifyText(ModifyEvent e) {
        Integer newValue = ((Spinner) e.getSource()).getSelection();
        Integer oldValue = (Integer) element.eGet(features[XVALUE]);
        if (!newValue.equals(oldValue)) {
          EditingDomain domain = GedBookEditingDomainProvider.getEditingDomain();
          domain.getCommandStack().execute(SetCommand.create(domain, element, features[XVALUE], newValue));
          //element.eSet(features[XVALUE], newValue);
        }
      }
    });

    getWidgetFactory().createLabel(grp, "Y:", SWT.NONE);
    spinnerFields[YVALUE] = new Spinner(grp, SWT.BORDER);
    spinnerFields[YVALUE].setMinimum(0);
    spinnerFields[YVALUE].setMaximum(3000);
    spinnerFields[YVALUE].setIncrement(5);
    spinnerFields[YVALUE].setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
    spinnerFields[YVALUE].addModifyListener(new ModifyListener() {
      @Override
      public void modifyText(ModifyEvent e) {
        Integer newValue = ((Spinner) e.getSource()).getSelection();
        Integer oldValue = (Integer) element.eGet(features[YVALUE]);
        if (!newValue.equals(oldValue)) {
          EditingDomain domain = GedBookEditingDomainProvider.getEditingDomain();
          domain.getCommandStack().execute(SetCommand.create(domain, element, features[YVALUE], newValue));
          //element.eSet(features[YVALUE], newValue);
        }
      }
    });
  }

  /**
   * @param spinnerField
   * @param feature
   */
  void loadData(Spinner spinnerField, EStructuralFeature feature) {
    Integer spinnerValue = (Integer) element.eGet(feature);
    if (spinnerValue != null) {
      spinnerField.setSelection(spinnerValue);
    }
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#createControls(org.eclipse.swt.widgets.Composite, org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
   */
  @Override
  public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
    super.createControls(parent, aTabbedPropertySheetPage);

    Composite form = getWidgetFactory().createFlatFormComposite(parent);
    form.setLayout(new GridLayout(2, false));

    createCoordinateWidget(form, "Left Bottom", leftBottomSpinnerFields,
      new EStructuralFeature[] { DataPackage.Literals.SCREENSHOT__XLEFT_BOTTOM, DataPackage.Literals.SCREENSHOT__YLEFT_BOTTOM });
    createCoordinateWidget(form, "Right Top", rightTopSpinnerFields,
      new EStructuralFeature[] { DataPackage.Literals.SCREENSHOT__XRIGHT_TOP, DataPackage.Literals.SCREENSHOT__YRIGHT_TOP });
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#setInput(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
   */
  @Override
  public void setInput(IWorkbenchPart part, ISelection selection) {
    super.setInput(part, selection);

    element = (Screenshot) ((IStructuredSelection) selection).getFirstElement();

    loadData(leftBottomSpinnerFields[XVALUE], DataPackage.Literals.SCREENSHOT__XLEFT_BOTTOM);
    loadData(leftBottomSpinnerFields[YVALUE], DataPackage.Literals.SCREENSHOT__YLEFT_BOTTOM);
    loadData(rightTopSpinnerFields[XVALUE], DataPackage.Literals.SCREENSHOT__XRIGHT_TOP);
    loadData(rightTopSpinnerFields[YVALUE], DataPackage.Literals.SCREENSHOT__YRIGHT_TOP);
  }

  /**
   * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
   */
  public boolean select(Object toTest) {
    return toTest instanceof Screenshot;
  }
}
