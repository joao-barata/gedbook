/**
 * Copyright (c) 2014 Joao Barata
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *   Joao Barata - initial API and implementation
 */
package net.sourceforge.gedbook.ui.properties.sections;

import net.sourceforge.gedbook.model.data.DataPackage;
import net.sourceforge.gedbook.model.data.Screenshot;
import net.sourceforge.gedbook.ui.views.GedBookEditingDomainProvider;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

/**
 * @author Joao Barata
 */
public class GedBookScreenshotTranscriptionSection extends AbstractPropertySection implements IFilter {

  private Text annotationTextField;
  private Text commentTextField;
  private Text transcriptionTextField;
  private Text signatureTextField;

  private Screenshot element;

  private Text createTextWidget(Composite form, String labelText, int nblines) {
    GridData data;

    CLabel labelField = getWidgetFactory().createCLabel(form, labelText); //$NON-NLS-1$
    data = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
    labelField.setLayoutData(data);

    Text textField = getWidgetFactory().createText(form, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.H_SCROLL | SWT.READ_ONLY); //$NON-NLS-1$
    data = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
    data.heightHint = nblines * textField.getLineHeight();
    data.widthHint = 10; // <-- minimum width, will grow based on other fields
    textField.setLayoutData(data);

    return textField;
  }

  /**
   * @param textField
   * @param feature
   */
  void loadData(Text textField, EStructuralFeature feature) {
    String textValue = (String) element.eGet(feature);
    if (textValue != null) {
      textField.setText(textValue);
    }
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#createControls(org.eclipse.swt.widgets.Composite, org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage)
   */
  @Override
  public void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
    super.createControls(parent, aTabbedPropertySheetPage);

    Composite form = getWidgetFactory().createFlatFormComposite(parent);
    form.setLayout(new GridLayout(2, false));

    annotationTextField = createTextWidget(form, "Annotation:", 2);
    transcriptionTextField = createTextWidget(form, "Transcription:", 5);
    signatureTextField = createTextWidget(form, "Signature:", 2);
    commentTextField = createTextWidget(form, "Comment:", 2);
  }

  /**
   * @see org.eclipse.ui.views.properties.tabbed.AbstractPropertySection#setInput(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
   */
  @Override
  public void setInput(IWorkbenchPart part, ISelection selection) {
    super.setInput(part, selection);

    element = (Screenshot) ((IStructuredSelection) selection).getFirstElement();

    loadData(annotationTextField, DataPackage.Literals.SCREENSHOT__ANNOTATION);
    loadData(transcriptionTextField, DataPackage.Literals.SCREENSHOT__TRANSCRIPTION);
    loadData(signatureTextField, DataPackage.Literals.SCREENSHOT__SIGNATURE);
    loadData(commentTextField, DataPackage.Literals.SCREENSHOT__COMMENT);
  }

  /**
   * @see org.eclipse.jface.viewers.IFilter#select(java.lang.Object)
   */
  public boolean select(Object toTest) {
    return toTest instanceof Screenshot;
  }
}
