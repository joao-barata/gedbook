/*******************************************************************************
 * Copyright (c) 2018 THALES GLOBAL SERVICES.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  
 * Contributors:
 *    Thales - initial API and implementation
 *******************************************************************************/
package net.sourceforge.gedbook.latex.generator.junit.tests;

import java.net.URL;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.junit.Test;

import net.sourceforge.gedbook.latex.generator.junit.GedBookLatexGeneratorJUnitPlugin;
import net.sourceforge.gedbook.model.GedBookXMIResourceFactoryImpl;
import net.sourceforge.gedbook.model.core.CorePackage;
import net.sourceforge.gedbook.model.data.DataPackage;

/**
 * @author Joao Barata
 */
public class GenerateTimelineTestCase {

  /**
   * @see org.polarsys.capella.test.framework.api.BasicTestArtefact#getRequiredTestModels()
   */
  @Test
  public void generate() {
    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new GedBookXMIResourceFactoryImpl());

    CorePackage.eINSTANCE.eClass();
    DataPackage.eINSTANCE.eClass();

    ResourceSet resourceSet = new ResourceSetImpl();
    ECrossReferenceAdapter adapter = new ECrossReferenceAdapter();
    resourceSet.eAdapters().add(adapter);

    URL myurl = GedBookLatexGeneratorJUnitPlugin.getContext().getBundle().getEntry("/model/test.gedmodel");
    //get the workspace
    IWorkspace workspace= ResourcesPlugin.getWorkspace();
    //create the path to the file
    IPath location= new Path(myurl.getPath());
    //try to get the IFile (returns null if it could not be found in the workspace)
    IFile file = workspace.getRoot().getFileForLocation(location);
    
    //try {
      URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
      //URI uri = URI.createFileURI(this.getClass().getClassLoader().getResource("/model/test.gedmodel").toURI().toString());
      Resource resource = resourceSet.getResource(uri, true);
      EObject root = resource.getContents().get(0);
      
      
      System.out.println(root.toString());
      
    //} catch (URISyntaxException e) {
    //  e.printStackTrace();
    //}
  }

}
