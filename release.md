# GedBook Release Note 

## Version 0.6.0 (current version / work in progress)

### New functionalities

* Project creation wizard allowing to import a gedcom file<br/>
* Specific explorer to browse models<br/>
* Set of property views to modify models<br/>
* Set of actions to edit models (add new, delete, cut, copy, paste, undo, redo, ...)<br/>
* Editor to visually define screenshot coordinates

### Commit log

_612e12b 2014-09-29 simplify add child menu creation_<br/>
_b26ced0 2014-09-28 fix checkstyle warnings_<br/>
_368d46f 2014-09-28 update version to 0.6.0_<br/>
_c09771e 2014-09-28 add an editor for screenshots_<br/>
_419cff3 2014-09-26 add new edition menus_<br/>
_3973f20 2014-09-25 add new icons_<br/>
_2b7ea55 2014-09-24 modify file and edit menus_<br/>
_a21463a 2014-09-24 remove dependency to PDFRenderer_<br/>
_8f157ea 2014-09-24 update version to 0.6.0_<br/>
_9d8b498 2014-09-15 add some UI plugins containing basic components_

## Version 0.5.0 (released)

### New functionalities

* Integrated command line environment<br/>
* Command to import a gedcom file (console mode)<br/>
* Command to generate latex files (console mode)<br/>
* Full examples (with models and generated files)

### Commit log

_66f42bf 2014-09-13 fix census event issue_<br/>
_bbb8366 2014-09-12 change model structure (add packages)_<br/>
_e7fd140 2014-09-12 add custom item provider plugins_<br/>
_289da17 2014-09-12 fix import issue when multiple banns (with same date)_<br/>
_fe58c35 2014-08-31 fixed: [#11] ticket_<br/>
_bc82be1 2014-08-19 fixed: [#10] ticket_<br/>
_e7c70f3 2014-08-19 fix build issues_<br/>
_e74eab5 2014-08-19 fixed: [#9] ticket_<br/>
_1aae5ef 2014-08-18 fix generation issue in "fulltree" latex files_<br/>
_30d2e8e 2014-08-18 fix build issue_<br/>
_396de24 2014-08-18 fixed: [#8] ticket_<br/>
_57576df 2014-08-17 fix generation issue in timeline graphs_<br/>
_62708e8 2014-08-17 fix generation issue in latex files_<br/>
_79b3ecf 2014-08-17 fixed: [#7] ticket_<br/>
_c59928a 2014-08-16 fixed: [#6] ticket_<br/>
_1966bac 2014-08-16 fix build issues_<br/>
_24be2f8 2014-08-16 fix build issues_<br/>
_6e1ba47 2014-08-16 fixed: [#2] ticket_<br/>
_78f2d1f 2014-08-10 fixed: [#5] ticket_<br/>
_bced797 2014-08-09 fixed: [#3] and [#4] tickets_<br/>
_2606646 2014-08-08 packaged execution environment_<br/>
_1d116b8 2014-08-06 fix build issues_<br/>
_23fdc38 2014-08-05 fix build issues_<br/>
_7d2584d 2014-08-05 add dependencies and build artifacts_<br/>
_2dfb27d 2014-08-04 initial commit_<br/>
_b1e08d4 2014-08-02 initial commit_<br/>
_1800b40 2014-07-31 initial commit_<br/>
_bd03d2c 2014-07-29 initial commit_
